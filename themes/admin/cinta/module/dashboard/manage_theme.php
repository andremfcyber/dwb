<style type="text/css">
    .theme-active{
        position: absolute;
        width: 100%;
        height: 228px;
        top: 0;
        padding-top: 75px;
        padding-right: 45px;
        padding-left: 45px;
    }
    .theme-active h1{
        color: #aaa;
    }
    .hbs{
      border-bottom: dashed 1px #ccc;
      padding-bottom: 15px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Themes</h3>

            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">

                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#website">Website</a></li>
                  <li><a data-toggle="tab" href="#admin">Admin</a></li>
                  <li><a data-toggle="tab" href="#store">Store</a></li>
                </ul>

                <div class="tab-content">
                  <div id="website" class="tab-pane fade in active">
                    <h3 class="hbs">Installed Front Themes</h3>

                     <?php foreach ($website_themes as $at) { ?>

                            <div class="col-lg-3 col-md-4 col-xs-6">
                                <a href="<?php echo $at['detail']; ?>" class="d-block mb-4 h-100">
                                   <img class="img-fluid img-thumbnail" style="height: 228px;" src="<?php echo $at['thumb_url']; ?>" alt="">
                                   <?php if($at['active']){ ?>
                                    <div class="overlay theme-active"><h1>Activated</h1></div>
                                   <?php } ?>
                                </a>
                                <h3><?php echo $at['name']; ?></h3>
                            </div>

                      <?php } ?>
                    
                  </div>
                  <div id="admin" class="tab-pane fade">
                    <h3 class="hbs">Installed Admin Themes</h3>
                    <div class="row text-center text-lg-left">
                        <?php foreach ($admin_themes as $at) { ?>

                            <div class="col-lg-3 col-md-4 col-xs-6">
                                <a href="<?php echo $at['detail']; ?>" class="d-block mb-4 h-100">
                                   <img class="img-fluid img-thumbnail" style="height: 228px;" src="<?php echo $at['thumb_url']; ?>" alt="">
                                   <?php if($at['active']){ ?>
                                    <div class="overlay theme-active"><h1>Activated</h1></div>
                                   <?php } ?>
                                </a>
                                <h3><?php echo $at['name']; ?></h3>
                            </div>

                        <?php } ?>
                    </div>
                  </div>
                  <div id="store" class="tab-pane fade">
                    <h3 class="hbs">Browse Theme</h3>
                    <div class="alert alert-info"> <h1 style="text-align: center;">Coming Soon</h1> </div>
                  </div>
                </div>

            </div>
        </div>
    </div>
   
</div>