<style>
    .main-header .logo { 
        height: 58px; 
    }
    .skin-green-custom .sidebar-menu > li:hover > a, .skin-green-custom .sidebar-menu > li.active > a, .skin-green-custom .sidebar-menu > li.menu-open > a {
        color: #ffffff;
        background: #e72c34;
        border-top-left-radius: 7px;
        border-bottom-left-radius: 5px;
    }
    .skin-green-custom .sidebar-menu > li > .treeview-menu {
        margin: -1px 0px 0 0px;
        background: #e02b32;
        padding-bottom: 12px !important;
        /* border-bottom-left-radius: 3px; */
    }
    .skin-green-custom .sidebar-menu .treeview-menu > li > a {
        color: #ffffff;
    }
    .content-header > .breadcrumb > li > a {
        display: none!important;
    }
</style>
<!-- Header Navbar: style can be found in header.less -->
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>B</b>NC</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lgs"> 
        <!-- <img width="100%" src="<?php //echo base_url('upload/logo/').$_profil['logo']  ?>" alt="" style="width: 166px; padding-top: 8px;">  -->
        <img src="<?php  echo $theme_url; ?>/assets/img/bank-eka-logo-2-mini.png" width="100%" style="width: 166px; padding-top: 8px;"> 
    </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <?php 
        if(isset($header_toggle_sidebar_visibility)){
            if($header_toggle_sidebar_visibility){
    ?>
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <img src="<?php echo $theme_url; ?>/assets/img/ic-menu.png">
            <span class="sr-only"></span>
        </a>
    <?php
            }
        }else{
    ?>
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <img src="<?php echo $theme_url; ?>/assets/img/ic-menu.png">
            <span class="sr-only"></span>
        </a>
    <?php
        }
    ?>
    
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav pull-right">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                    <?php if($this->session->userdata('picture')!=""){?>
                        <div class="user-profile-dropdown"><img src="<?php echo $this->session->userdata('picture'); ?>" class="scale" data-scale="best-fill" data-align="center"></div>
                    <?php }else{ ?>
                        <div class="user-profile-dropdown"><img src="<?php echo base_url(); ?>assets/dist/img/profile_default.jpg" class="scale" data-scale="best-fill" data-align="center"></div>
                    <?php } ?>

                    <span class="hidden-xs">
                    <?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name'); ?>

                    </span>
                </a>
                <ul class="dropdown-menu">
                    
                    <!-- User image -->
                    <li class="user-header">
                        <?php if($this->session->userdata('picture')!=""){?>
                            <div class="user-profile-big"><img src="<?php echo $this->session->userdata('picture'); ?>" class="scale" data-scale="best-fill" data-align="center"></div>
                        <?php }else{ ?>
                            <div class="user-profile-big"><img src="<?php echo base_url(); ?>assets/dist/img/profile_default.jpg" class="scale" data-scale="best-fill" data-align="center"></div>
                        <?php } ?>

                        <p style="font-size: 13px;">
                            <?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name')?> <br> <?php echo $this->session->userdata('email');?>
                        </p>
                    </li>
                    <!-- Menu Body -->

                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <div class="ip"><span class="label label-info" style="font-size: 11px;"> IP : <?php echo $_SERVER["REMOTE_ADDR"]; ?></span></div>
                        </div>
                        <div class="pull-right">
                            <a href="javascript:edit_profile();" class="btn btn-success btn-flat"><i class="fa fa-user" aria-hidden="true"></i></a>
                            <a href="javascript:change_password();" class="btn btn-warning btn-flat"><i class="fa fa-key" aria-hidden="true"></i></a>
                            <a href="<?php echo base_url('auth/session/logout'); ?>" alt="Logout" class="btn btn-danger btn-flat"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                        </div>
                    </li>
                </ul>
            </li>
        
            <!-- Notifications: style can be found in dropdown.less -->
            <!-- <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img class="nav-icon" src="<?php echo $theme_url; ?>/assets/img/ic-notif.png">
                <span class="label label-danger">10</span>
                </a>
                <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                    inner menu: contains the actual data
                    <ul class="menu">
                    <li>
                        <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                        page and may cause design problems
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                    </li>
                    </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
                </ul>
            </li> -->

            <!-- Search -->
            <!-- <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img class="nav-icon" src="<?php echo $theme_url; ?>/assets/img/ic-search.png">
                <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                </form>
                </ul>
            </li> -->

            <!-- Control Sidebar Toggle Button -->
            <?php 
                $user_group = $this->mcore->getUserGroupName($this->session->userdata('id')) ;
                if($user_group == "Admin"){
            ?>
                <li>
                    <a href="#">
                    <img class="nav-icon" src="<?php echo $theme_url; ?>/assets/img/ic-setting.png">
                    </a>
                </li>
            <?php
                }
            ?>
        </ul>
    </div>
    </nav>
</header>


