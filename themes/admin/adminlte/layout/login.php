<?php
$app['base_url'] =  $theme_url;
$app['assetdir'] = "/assets";
$app['title'] = $this->settings->site_name.'|'.$title;
$app['css'] = array(
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/css/bootstrap.min.css',
    "font_awesome" => $app['base_url'].$app['assetdir'].'/bower_components/components-font-awesome/css/font-awesome.min.css',
    "icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/square/blue.css',
    "adminlte" => $app['base_url'].$app['assetdir'].'/css/AdminLTE.css',
);
$app['footer_js'] = array(
    // 'blockUI' => base_url().'assets/dist/js/jquery.blockUI.js',
    "jquery" => $app['base_url'].$app['assetdir'].'/plugins/jQuery/jquery-2.2.3.min.js',
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/js/bootstrap.min.js',
    "icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/icheck.min.js',
);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $app['title']; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php
    /* generate css */
    $css_key = array_keys($app['css']);
    for($i=0;$i < count($app['css']);$i++){
        echo '<link rel="stylesheet" href="'.$app['css'][$css_key[$i]].'">';
    }
    ?>
    <!-- <title>BPRNUSAMBA CEPIRING | MASUK</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?= base_url('images/icons/favicon.ico') ?>"/>
    <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/vendor/bootstrap/css/bootstrap.min.css') ?>">
    <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') ?>">
    <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/fonts/iconic/css/material-design-iconic-font.min.css') ?>">
    <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/vendor/animate/animate.css') ?>">
    <!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/vendor/css-hamburgers/hamburgers.min.css') ?>">
    <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/vendor/animsition/css/animsition.min.css') ?>">
    <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/vendor/select2/select2.min.css') ?>">
    <!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/vendor/daterangepicker/daterangepicker.css') ?>">
    <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/css/util.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/login/css/main.css') ?>">
    <!--===============================================================================================-->
    <link href="https://fonts.googleapis.com/css?family=Livvic&display=swap" rel="stylesheet">
    <style>
        body{
         font-family: 'Livvic', sans-serif !important;
        }
        .login-page{
            background: url(<?= $theme_url.'/assets/img/asdasdff.jpg' ?>) no-repeat center center fixed !important; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .wrap-login100 {
            padding: 100px 55px 10px 55px !important;
        }
        .btn-login{
            background: #0f75bc;
        }
        .focus-input100::before {
            background: #0f75bc;
        }
        .alert-text{
            font-size: 13px;
        }
    </style>
</head>
<body class="hold-transition login-page">

   <?php echo $this->load->view("../../themes/admin/{$this->theme}/module/{$this->theme_module}/{$module_view}"); ?>

<?php

/* generate js */
$footer_js_key = array_keys($app['footer_js']);
for($i=0;$i < count($app['footer_js']);$i++){
    echo '<script src="'.$app['footer_js'][$footer_js_key[$i]].'"></script>';
}
?>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/login/vendor/jquery/jquery-3.2.1.min.js') ?>"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/login/vendor/animsition/js/animsition.min.js') ?>"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/login/vendor/bootstrap/js/popper.js') ?>"></script>
    <script src="<?= base_url('assets/login/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/login/vendor/select2/select2.min.js') ?>"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/login/vendor/daterangepicker/moment.min.js') ?>"></script>
    <script src="<?= base_url('assets/login/vendor/daterangepicker/daterangepicker.js') ?>"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/login/vendor/countdowntime/countdowntime.js') ?>"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('assets/login/js/main.js') ?>"></script>
    
    <script src="<?= base_url('assets/dist/js/jquery.blockUI.js') ?>"></script>

<script>
    var base_url = "<?php echo base_url(); ?>";
    $(function () {
        $("#login").submit(function(){

            loading();
            blockUI();
            var data = $(this).serialize();
            var target = '<?php echo base_url(); ?>auth/session/login';

            disable_form();

            $.post(target,data,function(res){
                 if(res.granted){
                     granted();
                    //  $.unblockUI();
                 }else{
                     if(res.message){
                         alert(res.message)
                     }
                     denied();
                     enable_form();
                     $.unblockUI();
                 }
            },'json');

            return false;
        });
    });

    /*--------------------------------------
        BlockUI 
    ---------------------------------------*/
    function blockUI(message){
        $.blockUI({
            message: '<span class="text-semibold"><img src="'+base_url+'/assets/dist/img/loading_argan.gif'+'" style="height: 30px;">'+ (message ? message : ' Sedang Memproses') +'</span>',
            baseZ: 10000,
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.3,
                cursor: 'wait'
            },
            css: {
                'z-index': 10020,
                padding: '10px 5px',
                margin: '0px',
                width: '20%',
                top: '40%',
                left: '40%',
                'text-align': 'center',
                color: 'rgb(92, 132, 50)',
                border: '0px',
                'background-color': 'rgb(255, 255, 255)',
                cursor: 'wait',
                'border-radius': '12px',
                border: '2px rgb(92, 132, 50) solid',
                'font-size': '16px',
                'min-width': "95px",
                // border: 0,
                // padding: '10px 15px',
                // color: '#fff',
                // '-webkit-border-radius': 2,
                // '-moz-border-radius': 2,
                // backgroundColor: '#333',
                // 'z-index' : 10020,
                // width: '25%',
                // left: '38%',
            } 
        })
    }

    function blockElement(element, message, border){
        border = border ? border : '0px';
        $(element).block({
            message: '<span class="text-semibold"><img src="'+base_url+'/assets/dist/img/loading_argan.gif'+'" style="height: 30px;">'+(message ? message : ' Sedang Memproses') +'</span>',
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.3,
                cursor: 'wait',
                borderRadius: border,
            },
            css: {
                'z-index': 10020,
                padding: '10px 5px',
                margin: '0px',
                width: '20%',
                top: '40%',
                left: '40%',
                'text-align': 'center',
                color: 'rgb(92, 132, 50)',
                border: '0px',
                'background-color': 'rgb(255, 255, 255)',
                cursor: 'wait',
                'border-radius': '12px',
                border: '2px rgb(92, 132, 50) solid',
                'font-size': '16px',
                'min-width': "95px",
            } 
        });

    }

    function disable_form(){
        $("#email").attr('disabled','disabled');
        $("#password").attr('disabled','disabled');
        $("#remember").attr('disabled','disabled');
        $("#submit_btn").attr('disabled','disabled');
    }
    function enable_form(){
        $("#email").removeAttr('disabled');
        $("#password").removeAttr('disabled');
        $("#remember").removeAttr('disabled');
        $("#submit_btn").removeAttr('disabled');
    }
    function loading(){
        $("#loading").fadeIn();
        $("#denied").hide();
        $("#granted").hide();
    }
    function denied(){
        $("#loading").hide();
        $("#denied").fadeIn();
        $("#granted").hide();
    }
    function granted(){
        $("#loading").hide();
        $("#denied").hide();
        $("#granted").fadeIn();

        setTimeout('window.location.reload()',500);
    }
</script>
</body>
</html>