<div class="limiter">
  <div class="container-login100">
    <div class="wrap-login100">

      <!-- Login -->
      <span class="login100-form-title p-b-24">
        <a class="logo" href="<?php echo base_url('') ?>"> 
           <img style="width: 250px;" src="<?php echo base_url('upload/logo/').$_profil['logo']  ?>" alt=""> 
    		  
           <!-- <img src="<?php  //echo $theme_url; ?>/assets/img/bank-eka-logo-2-mini.png" width="100%" style="width: 250px;">  -->
          </a>
      </span>
        <hr>
      <div style="margin-bottom: 30px !important;">
        <!-- Alert -->
        <div id="loading" class="alert alert-info alert-dismissible" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> Proses</h4>
            <span class="alert-text">Pengecekan... Mohon Bersabar !</span>
        </div>
        <div id="denied" class="alert alert-danger alert-dismissible" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Akses Dicekal!</h4>
            <span class="alert-text">Nama Pengguna Atau Kata Sandi Salah. Silahkan Cek Kembali!</span>
        </div>
        <div id="granted" class="alert alert-success alert-dismissible" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Akses Diterima!</h4>
            <span class="alert-text">Sedang dialihkan ke Dashboard... Mohon Bersabar!</span>
        </div>
      </div>

      <form class="login100-form validate-form" id="login">
        <div class="wrap-input100 validate-input" data-validate = "Masukan Username">
          <input class="input100" type="text" name="username">
          <span class="focus-input100" data-placeholder="Nama Penggunan"></span>
        </div>

        <div class="wrap-input100 validate-input" data-validate="Masukan Password">
          <span class="btn-show-pass">
            <i class="zmdi zmdi-eye"></i>
          </span>
          <input class="input100" type="password" name="password">
          <span class="focus-input100" data-placeholder="Kata Sandi"></span>
        </div>

        <div class="container-login100-form-btn">
          <div class="wrap-login100-form-btn">
            <div class="login100-form-bgbtn"></div>
            <button class="login100-form-btn btn-login" id="inputa">
              MASUK
            </button>
          </div>
        </div>

        <div class="text-center p-t-115">
          
        </div>
      </form>
    </div>
  </div>
</div>