<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Formulir Data Nasabah</h3>

            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">

                <form id="form">
                  <input type="hidden" name="id_product" value="<?php echo $id_product;?>">
                  <input type="hidden" name="id_nasabah" value="<?php echo $id_nasabah;?>">
                <?php 
                  foreach($form_header as $fh){
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo $fh['name_header']?></h3>

                            </div>
                            

                            <div class="box-body">
                              
                                
                              <?php

                                $form_attr = $this->db->get_where("form_fileds",array("form_header_id" => $fh['id']))->result_array();

                                foreach ($form_attr as $fa) {
                                   
                                  $input_type = $fa['input_type'];

                                 
                                  switch ($input_type) {

                                    case 'text':

                                      $data_formulir_nasabah = $this->db->get_where("data_formulir_nasabah",
                                          array(
                                              "id_nasabah" => $id_nasabah,
                                              "id_product" => $id_product,
                                              "id_form_field" => $fa['id']
                                          )
                                      )->result_array();

                                      
                                      generateTextInput($fa['label'],$fa['id'],$data_formulir_nasabah);

                                      break;

                                    case 'checkbox':

                                      $form_attr_source = $this->db->get_where("form_attribut",array("form_filed_id" => $fa['id']))->result_array();

                                      // echo "<pre>";
                                      // print_r($form_attr_source);
                                      // echo "</pre>";
                                      generateCheckbox($fa['label'],$fa['id'],$form_attr_source,$id_nasabah,$id_product);
                                    
                                    default:
                                      # code...
                                      break;

                                  }


                                }
                                
                              ?>
                             
                            </div>

                        </div>
                    </div>
                   
                </div>
                <?php
                  }
                ?>

                 </form>
            </div>
        </div>
    </div>
   
</div>
<script type="text/javascript">
  
  $(document).ready(function(){

    $("#form").submit(function(){

      var data = $(this).serialize();
      var target = base_url + 'product/save_data_formulir';

      $.post(target, data, function(res){

        if(res.status == "1"){

          toastr.success(res.msg, "Response Server");

        }else{

          toastr.error(res.msg, "Response Server");

        }

      },'json');
      
      return false;

    });

  });

</script>