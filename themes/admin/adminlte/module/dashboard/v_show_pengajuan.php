<div class="box box-whatsapp">
    <div class="box-body">
      <section class="add-section spad pt-0">
        <div class="container">
          <div class="add-warp">
            
            <div class="row add-text-warp">
              <div class="col-lg-12">
                <h2  class="green" style="text-align:left; font-size:20px">PRODUK</h2>
              </div>
              <br>
              
              <div class="col-lg-12">
                <p style="font-size:12px" class="gray">
                  <H5>ID NASABAH : <?php echo $show['id_nasabah']; ?></H5>
                  <H5>NAMA : <?php echo $show['name']; ?></H5>
                  <H5>TYPE : <?php echo $show['nama_type_product']; ?></H5>
                </p>
              </div>
              
            </div>
            <hr>
            <div class="row add-text-warp">
              <div class="col-lg-12">
                <h2  class="green" style="text-align:left; font-size:20px">DATA NASABAH</h2>
              </div>
              <br>
              
              <div class="col-lg-12">
                <p style="font-size:12px" class="gray">
                  <H5>NIK : <?php echo $show['nik']; ?></H5>
                  <H5>NAMA : <?php echo $show['nama']; ?></H5>
                </p>
              </div>
              
            </div>
            <hr>
            <div class="row add-text-warp">
              <div class="col-lg-12">
                <h2  class="green" style="text-align:left; font-size:20px">STATUS PENGAJUAN</h2>
              </div>
              <br>
              
              <div class="col-lg-12">
                <p style="font-size:12px" class="gray">
                  <H5>STATUS : <?php echo $show['status']; ?></H5>
                </p>
              </div>
              
            </div>

            <div class="row add-text-warp">
              <div class="col-lg-12"><br>
                 <a class="btn btn-sm btn-success" href="<?php echo base_url().'detail_nasabah2/guzzle_get/'.$show['id_nasabah'].'/'.$show['nik'];?>">Cek Data Dukcapil</a>
              </div>
            </div>
      </section> 
     
    </div>
</div>