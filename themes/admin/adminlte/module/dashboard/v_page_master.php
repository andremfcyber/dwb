<style>
    .btn-block {
        text-align: left;
        padding: 17px;
        font-size: 16px;
        display: block;
        width: 100%;
    }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
            <a href="<?php echo base_url(); ?>frontend/Halaman/add" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Page</a>
        </div>
      </div>
      <div class="box-body"> 
        <div class="row">
            <div class="col-md-6">
                <table id="examples" class="table table-striped" style="width:100%">
                <thead>
                    <tr>  
                        <th class="p-2"><i class="fa fa-cog"> </i> PROFILE</th> 
                    </tr>
                </thead>
                <tbody> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=selamat-datang" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Selamat Datang
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=sejarah" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Sejarah
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=visi-misi" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Visi-Misi
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=identitas-perusahaan" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Identitas Perusahaan
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=struktur-organisasi" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i>  
                                Struktur Organisasi
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=budaya-perusahaan" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Budaya Perusahaan
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=kodeetik-perusahaan" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Kode Etik Perusahaan
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=penghargaan" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Penghargaan / Awards
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=mengapa-memilihkami" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Mengapa Memilih kami
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=whistle-blowingsystem" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Whistle Blowing System
                            </a>
                        </th> 
                    </tr>  
                </tbody>
                <tfoot> 
                </tfoot>
            </table> 
            </div>
            <div class="col-md-6">
                <table id="examples" class="table table-striped" style="width:100%">
                <thead>
                    <tr>  
                        <th class="p-2"><i class="fa fa-cog"> </i> MANAGEMENT</th> 
                    </tr>
                </thead>
                <tbody> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=dewan-komisaris" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Dewan Komisaris
                            </a>
                        </th> 
                    </tr> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=direksi" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Direksi
                            </a>
                        </th> 
                    </tr> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=komite-perusahaan" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Komite-Komite Perusahaan
                            </a>
                        </th> 
                    </tr> 
                </tbody>
                <tfoot> 
                </tfoot>
            </table> 
            </div>
            <div class="col-md-6">
                <table id="examples" class="table table-striped" style="width:100%">
                <thead>
                    <tr>  
                        <th class="p-2"><i class="fa fa-cog"> </i> PRODUK LAYANAN</th> 
                    </tr>
                </thead>
                <tbody>  
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>product/kredit" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Kredit
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>product/Deposito" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Deposito
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>product/tabungan" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Tabungan
                            </a>
                        </th> 
                    </tr>  
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=jaringan-atm" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Jaringan ATM
                            </a>
                        </th> 
                    </tr> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=sms-banking" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                SMS Banking
                            </a>
                        </th> 
                    </tr> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=edc" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                EDC
                            </a>
                        </th> 
                    </tr> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=mobile-banking" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Mobile Banking
                            </a>
                        </th> 
                    </tr> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=syarat-ketentuan" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Syarat & Ketentuan
                            </a>
                        </th> 
                    </tr> 
                </tbody>
                <tfoot> 
                </tfoot>
            </table> 
            </div>
            <div class="col-md-6">
                <table id="examples" class="table table-striped" style="width:100%">
                <thead>
                    <tr>  
                        <th class="p-2"><i class="fa fa-cog"> </i> MANAGEMENT</th> 
                    </tr>
                </thead>
                <tbody>  
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=kantor-pusat" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Kantor Pusat
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=kantor-cabang" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Kantor Cabang
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/halaman/?page=lokasi-atm" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Lokasi ATM
                            </a>
                        </th> 
                    </tr> 
                </tbody>
                <tfoot> 
                </tfoot>
            </table> 
            </div>
            <div class="col-md-6">
                <table id="examples" class="table table-striped" style="width:100%">
                <thead>
                    <tr>  
                        <th class="p-2"><i class="fa fa-cog"> </i> MANAGEMENT</th> 
                    </tr>
                </thead>
                <tbody> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/agenda" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Agenda
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/pressrealese" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Berita / Announcement
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/artikel" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Artikel
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/cerpel" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Testimoni
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/laporan" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Laporan
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/career" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Karir
                            </a>
                        </th> 
                    </tr> 
                </tbody>
                <tfoot> 
                </tfoot>
            </table> 
            </div>
            <div class="col-md-6">
                <table id="examples" class="table table-striped" style="width:100%">
                <thead>
                    <tr>  
                        <th class="p-2"><i class="fa fa-cog"> </i> BERITA</th> 
                    </tr>
                </thead>
                <tbody> 
                    <tr>  
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/galeri" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Foto
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/filedownload" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Download
                            </a>
                        </th> 
                    </tr>
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/fax" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                FaQ
                            </a>
                        </th> 
                    </tr> 
                </tbody>
                <tfoot> 
                </tfoot>
            </table>  
            </div>
            <div class="col-md-6">
                <table id="examples" class="table table-striped" style="width:100%">
                <thead>
                    <tr>  
                        <th class="p-2"><i class="fa fa-cog"> </i> KONTAK</th> 
                    </tr>
                </thead>
                <tbody>  
                    <tr>
                        <th>
                            <a href="<?php echo base_url(); ?>frontend/kontak" class="btn btn-success p-5 btn-block">
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i> 
                                Pengaduan/Kontak
                            </a>
                        </th> 
                    </tr> 
                </tbody>
                <tfoot> 
                </tfoot>
            </table> 
            </div>
        </div>
      </div>
      <div class="box-footer"> 
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>