<div class="box box-whatsapp">
    <div class="box-body">
      <section class="add-section spad pt-0">
        <div class="container">
          <div class="add-warp">
            
            <div class="row add-text-warp">
              <div class="col-lg-12">
              </div>
              <div class="box-body">
                <table class="table table-bordered table-striped" style="width: 1050px;">
                
                <tr>
                  <th>Nama Nasabah</th>
                  <th>Nama Di Dukcapil</th>
                </tr>
                <tr>
                  <td><?php echo $wa['nama'] ?></td>
                  <td><?php echo $wa['nama_lgkp'] ?></td>
                </tr>
                <tr>
                  <th>NIK Nasabah</th>
                  <th>NIK Di Dukcapil</th>
                </tr>
                <tr>
                  <td><?php echo $wa['nik'] ?></td>
                  <td><?php echo $wa['nik'] ?></td>
                </tr>
                <tr>
                  <th>No Telepon</th>
                  <th>No Telepon</th>
                </tr>
                <tr>
                  <td><?php echo $wa['no_tlp'] ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th>Email</th>
                  <th>Email</th>
                </tr>
                <tr>
                  <td><?php echo $wa['email'] ?></td>
                  <td></td>
                </tr>
                <tr>
                  <th>No Kartu Keluarga</th>
                  <th>No Kartu Keluarga</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['no_kk'] ?></td>
                </tr>
                <tr>
                  <th>Nama Ibu Kandung</th>
                  <th>Nama Ibu Kandung</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['nama_lgkp_ibu'] ?></td>
                </tr>
                <tr>
                  <th>Tanggal Lahir</th>
                  <th>Tanggal Lahir</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['tgl_lhr'] ?></td>
                </tr>
                <tr>
                  <th>Tempat Lahir</th>
                  <th>Tempat Lahir</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['tmpt_lhr'] ?></td>
                </tr>
                <tr>
                  <th>Alamat</th>
                  <th>Alamat</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['alamat'] ?></td>
                </tr>
                <tr>
                  <th>No RT</th>
                  <th>No RT</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['no_rt'] ?></td>
                </tr>
                <tr>
                  <th>No RW</th>
                  <th>No RW</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['no_rw'] ?></td>
                </tr>
                <tr>
                  <th>Kelurahan</th>
                  <th>Kelurahan</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['kel_name'] ?></td>
                </tr>
                <tr>
                  <th>Kecamatan</th>
                  <th>Kecamatan</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['kec_name'] ?></td>
                </tr>
                <tr>
                  <th>Kabupaten</th>
                  <th>Kabupaten</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['kab_name'] ?></td>
                </tr>
                <tr>
                  <th>Provinsi</th>
                  <th>Provinsi</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['prop_name'] ?></td>
                </tr>
                <tr>
                  <th>Jenis Kelamin</th>
                  <th>Jenis Kelamin</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['jenis_klmin'] ?></td>
                </tr>
                <tr>
                  <th>Agama</th>
                  <th>Agama</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['agama'] ?></td>
                </tr>
                <tr>
                  <th>Jenis Pekerjaan</th>
                  <th>Jenis Pekerjaan</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['jenis_pkrjn'] ?></td>
                </tr>
                <tr>
                  <th>Status Kawin</th>
                  <th>Status Kawin</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['status_kawin'] ?></td>
                </tr>
                <tr>
                  <th>Kode Pos</th>
                  <th>Kode Pos</th>
                </tr>
                <tr>
                  <td></td>
                  <td><?php echo $wa['kode_pos'] ?></td>
                </tr>
              </table>
              <br>
              <button onclick="goBack()" class="btn btn-default">Back</button> 
              </div>
            </div>
      </section> 
     
    </div>
</div>

<script>
    function goBack() {
        window.history.back();
    }
</script>