<div class="box box-whatsapp">
    <div class="box-body">

      <section class="recipes-section spad pt-0">
      <div class="container">
        <div class="row">
          <div class="col-lg-3">
          </div>
          <div class="col-lg-2">
            <div class="text-center">
              <a data-toggle="modal" data-target="#myModal">
              <img class="img-responsive" src="<?=base_url('assets/images/'.$wa['photo_selfie'])?>" style="width:200px; height:200;">
              </a>
              <p class="green">Photo Selfie</p>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="text-center">
              <a data-toggle="modal" data-target="#myModal2">
              <img class="img-responsive" src="<?=base_url('assets/images/'.$wa['photo_ktp'])?>" style="width:200px; height:200;">
              </a>
              <p class="green">Photo KTP</p>
            </div>
          </div>
          <div class="col-lg-3">
          </div>
        </div>
      </div>
    </section>
     
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Photo Selfie</h4>
        </div>
        <div class="modal-body">
          <center>  
            <img class="img-responsive" src="<?=base_url('assets/images/'.$wa['photo_selfie'])?>" style="width:200px; height:200;">
          </center>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Photo KTP</h4>
        </div>
        <div class="modal-body">
          <center>  
            <img class="img-responsive" src="<?=base_url('assets/images/'.$wa['photo_ktp'])?>" style="width:200px; height:200;">
          </center>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
        </div>
      </div>
    </div>
  </div>