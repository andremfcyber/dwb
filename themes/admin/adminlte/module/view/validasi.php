<style type="text/css">
    ._box {
        /* height: 700px; */
        position: relative;
        border: 1px #eee solid;
        padding: 23px 11px;
    }
    ._box_title {
        font-size: 17px;
        position: absolute;
        background: white;
        top: -12px;
        left: 21px;
        padding: 0px 10px;
    }

</style>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box box-default">   
            <div class="box-header with-border" style="margin-bottom: 15px;">
                <div style="text-align: center;font-size: 28px !important"> 
                    <span><img src="<?= base_url('/assets/dist/img/validate.png') ?>" style="height: 60px;"> Validasi Nasabah</span>
                </div>
                <div class="box-tools pull-right">
                    
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="_box">
                            <span class="_box_title">Data Pengajuan Nasabah</span>
                            <div>
                                <div class="title"></div>
                            </div>
                            <!-- <?= map_x($detail_nasabah) ?> -->
                            <table class="table table-bordered">
                                <tr>
                                    <td>NIK</td>
                                    <td colspan="2"><?= $detail_nasabah['nik']?></td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td colspan="2"><?= $detail_nasabah['nama']?></td>
                                </tr>
                                <tr>
                                    <td>No.HP</td>
                                    <td colspan="2"><?= $detail_nasabah['no_tlp']?></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td colspan="2"><?= $detail_nasabah['email']?></td>
                                </tr>
                                <tr>
                                    <td>ALamat Sekarang</td>
                                    <td colspan="2"><?= $detail_nasabah['alamat_lengkap_sekarang']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Provinsi</td>
                                    <td><?= $detail_nasabah['provinsi_sekarang']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kab/Kota</td>
                                    <td><?= $detail_nasabah['kabkot_sekarang']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kecamatan</td>
                                    <td><?= $detail_nasabah['kecamatan_sekarang']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kelurahan</td>
                                    <td><?= $detail_nasabah['kelurahan_sekarang']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kodepos</td>
                                    <td><?= $detail_nasabah['kode_pos_sekarang']?></td>
                                </tr>
                                <tr>
                                    <td>Alamat Sesuai KTP</td>
                                    <td colspan="2"><?= $detail_nasabah['is_same_ktp'] == 't' ? 'Ya' : 'Tidak' ?></td>
                                </tr>
                                <?php $is_same_ktp = $detail_nasabah['is_same_ktp'] == 't' ? 1 : 0 ?>
                                <tr>
                                    <td>ALamat KTP</td>
                                    <td colspan="2"><?= $is_same_ktp ? $detail_nasabah['alamat_lengkap_sekarang'] : $detail_nasabah['alamat_lengkap_ktp']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Provinsi</td>
                                    <td><?=  $is_same_ktp ? $detail_nasabah['provinsi_sekarang'] : $detail_nasabah['provinsi_ktp']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kab/Kota</td>
                                    <td><?=  $is_same_ktp ? $detail_nasabah['kabkot_sekarang'] : $detail_nasabah['kabkot_ktp']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kecamatan</td>
                                    <td><?=  $is_same_ktp ? $detail_nasabah['kecamatan_sekarang'] : $detail_nasabah['kecamatan_ktp']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kelurahan</td>
                                    <td><?=  $is_same_ktp ? $detail_nasabah['kelurahan_sekarang'] : $detail_nasabah['kelurahan_ktp']?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Kodepos</td>
                                    <td><?= $is_same_ktp ? $detail_nasabah['kode_pos_sekarang'] : $detail_nasabah['kode_pos_ktp']?></td>
                                </tr>
                                <tr>
                                    <td>Foto</td>
                                    <td colspan="2">
                                        <img src="<?= base_url('upload/photo/'.$detail_nasabah['photo_selfie'])?>" class="img-thumbnail"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Foto KTP</td>
                                    <td colspan="2">
                                        <img src="<?= base_url('upload/photo/'.$detail_nasabah['photo_ktp'])?>" class="img-thumbnail"/>
                                    </td>
                                </tr>
                            </table>
                            
                            <?php if($detail_nasabah['status'] == '1') { ?>
                            <div class="form-group">
                                <button class="btn btn-success" onclick="onValidate()"><i class="fa fa-check-square-o"></i> Validasi</button>
                                <button class="btn btn-danger" onclick="form_reject()"><i class="fa fa-window-close-o"></i> Tolak</button>
                                <button class="btn btn-info pull-right" onclick="check_dukcapil()"><i class="fa fa-info"></i> Cek Dukcapil</button>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="_box">
                            <span class="_box_title">Data Dukcapil</span>
                            <div id="dukcapil_map">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="form-group">
                    <button class="btn btn-default" onclick="window.history.back();"> Kembali</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    let url_map_dukcapil = "<?= $dukcapil_url_map ?>"
    let url_reject = "<?= $url_reject ?>"
    let url_validate = "<?= $url_validate ?>"
    var stat_p = "<?= $detail_nasabah['status'] ?>"

    function check_dukcapil(){
        blockUI('Pengecekan Berlangsung ...');
        $.get(url_map_dukcapil).done((res) => {
            $('#dukcapil_map').html(res)  
            $.unblockUI();
        }).fail((xhr) => {
            $('#dukcapil_map').html('<h3>Layanan Tidak Tersedia</h3>')
            $.unblockUI();
        })
        // $('#dukcapil_map').load(url_map_dukcapil,function(){
        //     $.unblockUI();
        // });
    }
    
    function form_reject(id){
        blockUI();
        let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
        let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Alasan Penolakan</h3></span>`;

        $('#dynamicModal .modal-header').html(title);
        $('#dynamicModal .modal-body').load(url_reject,function(){
            $.unblockUI();
            $('#dynamicModal').modal({show:true});
        });
    }

    function onValidate(){
        var nasabah = <?= json_encode($detail_nasabah);?>;
        var data = {
            nasabah : JSON.stringify(nasabah),
        }

		blockUI();
		setTimeout(function() {
			
            if(typeof dukcapil !== 'undefined'){
                data.dukcapil = JSON.stringify(dukcapil)
            }
            console.log(data)
			$.post(url_validate, data, function(response){
				if (response.status === "1"){
                    toastr.success(response.msg, 'Berhasil')
                    setTimeout(() => {
                        window.location.reload()
                    }, 3000);
					$.unblockUI();
				}else{
                    toastr.error(response.msg, 'Duarrr !!')
					$.unblockUI();
				}
			}, 'json');	
		}, 1000);
		
	}
    
    $(() => {
        if(stat_p != 1 && stat_p != 6){
            check_dukcapil()
        }  
    })
</script>
