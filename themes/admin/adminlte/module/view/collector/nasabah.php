<!-- <link href="<?= base_url('/assets/dist/css/datatbles.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/buttons.dataTables.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/toastr.min.css') ?>" rel="stylesheet"> -->
<style media="print">
    .table { 
        width: 100%;
        padding: 10px;
        border-collapse: collapse;
    }
    
    .border-transparent{
        border-top: 1px solid transparent !important; 
    }
    .table tbody tr td{
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid transparent !important;
    }
    table tbody tr th{
        width : 30%;
    }
  
</style>

<div class="col-md-12 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Nasabah
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-3">
                <img src="<?= base_url('upload/photo/').$detail['photo_selfie'] ?>">
            </div>
            <div class="col-md-9">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">Nama Nasabah</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['nama'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">NIK</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['nik'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">No.Telepon</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['no_tlp'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">No.Telepon2</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['no_tlp2'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">No.Telepon</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['no_tlp_saudara'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">Jenis Kelmarin</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['jenis_kelamin'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">Email</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['email'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">Alamat Sekarang</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['alamat_lengkap'] ?></td>
                            </tr>
                            <tr>
                                <th style="border-top: 1px solid transparent; width: 30%">Alamat KTP</th>
                                <td style="border-top: 1px solid transparent;">: <?= $detail['alamat_lengkap_ktp'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 table-responsive">
                <h4>Pinjaman Nasabah</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Tenor</th>
                            <th>Tagihan(Bulan)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($pinjaman as $data){ ?>
                            <tr>
                                <td><?= $data['name'] ?></td>
                                <td><?= $data['tenor'] ?></td>
                                <td><?= $data['tagihan_bulanan'] ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>