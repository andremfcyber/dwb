
<style media="print">
    .table { 
        width: 100%;
        padding: 10px;
        border-collapse: collapse;
    }
    
    .border-transparent{
        border-top: 1px solid transparent !important; 
    }
    .table tbody tr td{
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid transparent !important;
    }
  
</style>

<div class="col-md-12 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Hasil Penagihan
            </div>
        </div>
        <div class="box-body">
            <div class="col-sm-6 ">
                Nasabah
                <address>
                    <strong><?= isset($detail->nama) ? $detail->nama : '' ?></strong><br>
                    <?= isset($detail->no_tlp) ? $detail->no_tlp : ''?><br>
                    <?= isset($detail->email) ? $detail->email : ''?><br>
                    <?= isset($detail->alamat_lengkap) ? $detail->alamat_lengkap : ''?>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
            Collector
                <address>
                    <strong><?= isset($detail->nama_collector) ? $detail->nama_collector : '' ?></strong><br>
                        <?= isset($detail->no_telp_collector) ? $detail->no_telp_collector : '' ?><br>
                        <?= isset($detail->email_collector) ? $detail->email_collector : '' ?><br>
                       
                </address>
            </div>
            <div class="col-md-12">
                <h2 class="page-header">
                    <i class="fa fa-file"></i> Detail
                    <small class="pull-right"><?= isset($detail->created_at) ? $detail->created_at :''?></small>
                </h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Nama Produk:</th>
                                        <td style="border-top: 1px solid transparent;"><?= isset($detail->name) ? $detail->name : ''?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Angsuraan Ke-</th>
                                        <td style="border-top: 1px solid transparent;"><?= isset($detail->angsuran_ke) ? $detail->angsuran_ke : '' ?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Tagihan</th>
                                        <td style="border-top: 1px solid transparent;">Rp. <?= isset($detail->bayar_bulan) ? $detail->angsuran_ke : '' ?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Alamat</th>
                                        <td style="border-top: 1px solid transparent;"><?php if(isset($detail->alamat_sesuai)){ if($detail->alamat_sesuai == true){ echo "Alamat Sesuai";}else{ echo "Alamat Tidak Sesuai";}} ?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Bertemu Dengan</th>
                                        <td style="border-top: 1px solid transparent;"><?= isset($detail->bertemu_dengan) ? $detail->bertemu_dengan : ''?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Karakter Nasabah</th>
                                        <td style="border-top: 1px solid transparent;"><?= isset($detail->karakter_nasabah) ? $detail->karakter_nasabah : ''?></td>
                                    </tr>
                                     <tr>
                                        <th style="border-top: 1px solid transparent;">Lokasi Bertemu</th>
                                        <td style="border-top: 1px solid transparent;"><?= isset($detail->lokasi_bertemu) ? $detail->lokasi_bertemu : ''?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Resumne Singkat Nasabah</th>
                                        <td style="border-top: 1px solid transparent;"><?= isset($detail->resume_singkat_nasabah) ? $detail->resume_singkat_nasabah : ''?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Tanggal penagihan:</th>
                                        <td style="border-top: 1px solid transparent;"><?= isset($detail->tgl_penagihan) ? $detail->tgl_penagihan : ''?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Jumlah bayar:</th>
                                        <td style="border-top: 1px solid transparent;">Rp.<?= isset($detail->nominal_bayar) ? $detail->nominal_bayar : ''?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Tgl bayar:</th>
                                        <td style="border-top: 1px solid transparent;"><?= isset($detail->tgl_pembayaran_terbaru) ? $detail->tgl_pembayaran_terbaru : ''?></td>
                                    </tr>
                                     <tr>
                                        <th style="border-top: 1px solid transparent;">ALasan Telat Bayar:</th>
                                        <td style="border-top: 1px solid transparent;"><?= isset($detail->alasan_telat_bayar) ? $detail->alasan_telat_bayar : ''?></td>
                                    </tr>
                                    <tr>
                                        <th style="border-top: 1px solid transparent;">Bukti bayar:</th>
                                        <td style="border-top: 1px solid transparent;"><?php if(isset($detail->bukti_bayar)){ ?><img src="<?= base_url('upload/bukti_bayar/').$detail->bukti_bayar ?>" width="300" height="300"> <?php } ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="col-md-12">
                    <?php if(isset($detail->status_angsuran)){ ?>
                    <?php if($detail->status_angsuran == 0 && $detail->nominal_bayar == $detail->bayar_bulan){?>
                      <button type="button" class="btn btn-success btn-sm" onclick="approve_detail_tagihan(<?= $detail->id_angsuran ?>)"> Approve</button>
                    <?php }else if ($detail->status_angsuran == 1){?>
                        <div class="col-md-2 alert alert-success">
                            Sudah di Approve
                        </div>
                    <?php }}?>
                    
                    </div>
                    <br>
                </div>
                
            </div>
        </div>
    </div>
</div>

<script>

    function approve_detail_tagihan(id){
        var target = "<?= base_url('collector/inventorycollect/approve_detail_tagihan'); ?>";
        $.get(`${target}/${id}`).done((res) => {
            toastr.success(res.message);
            window.location.reload();
        }).fail((xhr) => {
            toastr.error(xhr.responseJSON.message);
        })
    }


</script>