<!-- <link href="<?= base_url('/assets/dist/css/datatbles.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/buttons.dataTables.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/toastr.min.css') ?>" rel="stylesheet"> -->
<style media="print">
    .table { 
        width: 100%;
        padding: 10px;
        border-collapse: collapse;
    }
    
    .border-transparent{
        border-top: 1px solid transparent !important; 
    }
    .table tbody tr td{
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid transparent !important;
    }
    table tbody tr th{
        width : 30%;
    }
  
</style>

<div class="col-md-12 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Settlement
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <table width="100%">
                    <tr>
                        <td style="border-top: 1px solid transparent;">Total Tagihan : Rp. <b><?= number_format($settlement['tot_bayar']) ?></b></td>
                        <td style="border-top: 1px solid transparent;">
                            <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#setor-settle">Setor Ke Teller</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box-footer">
            <br>
            <div class="col-xs-12 table-responsive">
                <table id="dttable" class="table table-bordered table-striped dataTable no-footer" role="grid" aria-describedby="dttable_info">
                    <thead>
                        <tr>
                            <th>No. Slip</th>
                            <th>No. Rekening</th>
                            <th>Nama Nasabah</th>
                            <th>Nominal Bayar</th>
                            <th>Tanggal Bayar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($list_nasabah as $data){ ?>
                            <tr>
                                <td><?= $data['no_slip'] ?></td>
                                <td><?= $data['no_rek'] ?></td>
                                <td><?= $data['nama'] ?></td>
                                <td><?= 'Rp. '.number_format($data['total_bayar']) ?></td>
                                <td><?= $data['tgl_kunjungan'] ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="setor-settle" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                Setor Settlement
                <button type="button" class="close close-dynamic" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                 <div class="row">
                    <div class="col-md-12">
                        <form action="<?= base_url('collector/settlement/reaset_settlement/').$settlement['ao'] ?>" id="form-confirm">
                            <div class="form-body">
                            <p>Apakah Anda yakin ingin menyetorkanya?</p>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ok</button>
                                <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-"></i> Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#dttable').DataTable();
    });

    $('#form-confirm').submit((e) => {
        console.log(e.target.action)
        // blockUI("Selalu berdo'a semuanya lancar");
        e.preventDefault()
            $.post(e.target.action, function(res){
                if(res.status == "1"){
                    toastr.success(res.msg, 'Response Server');
                    setTimeout(() => {
                        window.location.reload(); 
                    }, 1500);
                }else{
                    toastr.error(res.msg, 'Response Server');
                }

            },'json');

            return false;
    })
</script>