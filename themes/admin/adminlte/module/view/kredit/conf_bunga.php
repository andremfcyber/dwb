<!-- <link href="<?= base_url('/assets/dist/css/datatbles.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/buttons.dataTables.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('/assets/dist/css/toastr.min.css') ?>" rel="stylesheet"> -->
<style type="text/css">

</style>

<div class="col-md-5 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Aturan Bunga
            </div>
            <div class="box-tools pull-right">
                
            </div>
        </div>
        <div class="box-body">
            <form class="form" id="aturan">
                <div class="form-group">
                    <label>Bunga per Tahun</label>
                    <div class="input-group">
                        <input class="form-control" value="<?= isset($detail['bunga']) ? $detail['bunga'] : '0' ?>" name="bunga">
                        <span class="input-group-addon">%</span>
                    </div>
                </div>
                <div class="form-group">
                    <label>Jenis Bunga</label>
                    <select class="form-control select2" name="jenis_bunga">
                        <option value="flat" <?php if(isset($detail['jenis_bunga']) && $detail['jenis_bunga'] == 'flat') echo "selected"  ?> >Flat</option>
                        <option value="anuitas" <?php if(isset($detail['jenis_bunga']) && $detail['jenis_bunga'] == 'anuitas') echo "selected"  ?> >Anuitas</option>
                        <option value="efektif" <?php if(isset($detail['jenis_bunga']) && $detail['jenis_bunga'] == 'efektif') echo "selected"  ?> >Efektif</option>
                    </select>
                </div>
            </form>
            <button class="btn btn-sm btn-success" onclick="update_aturan()"><i class="fa fa-save"></i> Simpan</button>
            <a class="btn btn-sm btn-default" href="javascript:window.history.go(-1);">Kembali</a>
        </div>
    </div>
</div>

<div class="col-md-5 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Musiman
            </div>
        </div>
        <div class="box-body">
            <form class="form" id="aturan_musiman">
                <div class="form-group">
                    <label>Musiman</label>
                    <select class="form-control select2" name="musiman">
                        <option value="0" <?php if(isset($_musiman[0]['musiman']) && $_musiman[0]['musiman'] == '0') echo "selected"  ?> >Tidak</option>
                        <option value="1" <?php if(isset($_musiman[0]['musiman']) && $_musiman[0]['musiman'] == '1') echo "selected"  ?> >Ya</option>
                    </select>
                </div>
            </form>
            <button class="btn btn-sm btn-success" onclick="update_aturan_musiman()"><i class="fa fa-save"></i> Simpan</button>
        </div>
    </div>
</div>

<div class="col-md-12 col-xs-12">
    <div class="box box-default">
        <div class="box-header with-border" style="margin-bottom: 10px;">
            <div class="box-title">
                Aturan Bulan
            </div>
            <div class="box-tools pull-right">
               
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-6">
                <b>List Bulan </b>
                <br>
                <br>    
                <table class="table table-bordered table-striped">    
                    <thead>
                        <tr>
                            <td>Bulan</td>
                            <td style="width: 20%"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($_bulan) && count($_bulan) > 0) { ?>
                            <?php foreach($_bulan as $data) { ?>
                                <tr class="_info" id="tr_<?= $data['id'] ?>">
                                    <td><?= $data['bulan'] ?></td>
                                    <td class="text-center">
                                        <button class="btn btn-primary btn-sm" onclick="edit_inline(<?= $data['id'] ?>)"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger btn-sm" onclick="delete_data(<?= $data['id'] ?>)"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr class="_update" id="uid_tr_<?= $data['id'] ?>" style="display: none">
                                    <td>
                                        <input class="form-control" name="bulan" value="<?= $data['bulan'] ?>">
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-success btn-sm" onclick="update_data(<?= $data['id'] ?>)"><i class="fa fa-save"></i> Simpan</button>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="3" class="text-center">Belum Ada Bulan</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
               
                <b>Tambah Baru : </b>
                <br>
                <form id="tambah_bulan_kredit">
                    <input type="hidden" value="flat" name="jenis_bunga">
                    <div class="form-group">
                        <label>Bulan <span style="font-size: 10px;" class="text-info"></span></label>
                        <input class="form-control" name="bulan">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-success" onclick="tambah_bulan_kredit()"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>

<!-- <script src="<?= base_url('/assets/dist/js/datatables.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/bs3-datatables.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/datatables.buttons.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/jszip.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/pdfmake.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/vfs_fonts.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/buttons.html5.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/btn-print.min.js') ?>"></script>
<script src="<?= base_url('/assets/dist/js/toastr.min.js') ?>"></script> -->
<script src="<?= base_url('/assets/dist/js/jquery.number.min.js') ?>"></script>
<script>
    let id_kredit = '<?= isset($detail['id_kredit_produk']) ? $detail['id_kredit_produk'] : ''  ?>'
    var url = {
        update_rule : "<?= base_url('product/kredit/save_conf_bunga/'); ?>",
        update_musiman : "<?= base_url('product/kredit/save_conf_musiman/'); ?>",
        add_bulan : "<?= base_url('product/kredit/add_conf_bulan/'); ?>",
        edit_bulan : "<?= base_url('product/kredit/edit_conf_bulan/'); ?>",
        delete_bulan : "<?= base_url('product/kredit/delete_conf_bulan/'); ?>",
    }
    

    $('[name="bunga"]').number( true, 2 );

    function check_is_valid(){
        let validate = $('#aturan').serializeArray();
        // console.log(validate)
        if(validate.length <= 0){
            return false;
        }
        let _err = 0;
        for(let i = 0; i < validate.length; i++){
            if(!validate[i].value){
                _err++
            }
        }

        if(_err > 0){
            return false;
        }
        return true;
    }
    
    function bunga_valid(bunga){
        bunga = parseFloat(bunga);
        // console.log(bunga)
        if(bunga > 100){
            return false;
        }else{
            return true;
        }
    }

    function update_aturan(){
        let validate = check_is_valid('#aturan');
		if(validate){
            let formData = $('#aturan').serializeArray();
            //check bunga
            let bunga = $('#aturan [name="bunga"]').val();
            if(!bunga_valid(bunga)){
                toastr.error('Bunga Melebihi 100');
                return;
            }
            // console.log(formData);
            $.post(`${url.update_rule}/${id_kredit}`, formData).done((res) => {
                toastr.success(res.msg, 'Berhasil !');
            }).fail((xhr) => {
                toastr.error(xhr.responseJSON.msg);
            })
        }else{
            $.unblockUI();
			toastr.error('Silahkan Lengkapi Form');
        }
    }

// Musiman
    function update_aturan_musiman(){
        // let validate = check_is_valid();
		// if(validate){
            let formData = $('#aturan_musiman').serializeArray();
            //check bunga
            let bunga = $('#aturan_musiman [name="musiman"]').val();
            // console.log(formData);
            $.post(`${url.update_musiman}/${id_kredit}`, formData).done((res) => {
                toastr.success(res.msg, 'Berhasil !');
            }).fail((xhr) => {
                toastr.error(xhr.responseJSON.msg);
            })
        // }else{
        //     $.unblockUI();
		// 	toastr.error('Silahkan Lengkapi Form');
        // }
    }

// Add Bulan Kredit
    function tambah_bulan_kredit(){
        let validate = check_is_valid('#tambah_bulan_kredit');
		if(validate){
            let formData = $('#tambah_bulan_kredit').serializeArray();
            let bulan = $('#tambah_bulan_kredit [name="bulan"]').val();
            if(!bulan_numeric(bulan)){
                toastr.error('Bulan Harus berupa Angka dan tidak boleh bilangan negatif');
                return;
            }
            // console.log(formData);
            $.post(`${url.add_bulan}/${id_kredit}`, formData).done((res) => {
                toastr.success(res.msg, 'Berhasil !');
                setTimeout(() => {                
                    window.location.href = `${location.protocol}//${location.host}${location.pathname}`;
                }, 2000);
            }).fail((xhr) => {
                toastr.error(xhr.responseJSON.msg);
            })
        }else{
            $.unblockUI();
			toastr.error('Silahkan Lengkapi Form');
        }
    }

    function edit_inline(uid){
        $('._info').show();
        $('._update').hide();
        $('#uid_tr_'+uid).show();
        $('#tr_'+uid).hide();
    }

    function delete_data(id){
        $.get(`${url.delete_bulan}/${id}`).done((res) => {
            toastr.success(res.msg);
            window.location.href = `${location.protocol}//${location.host}${location.pathname}`;
        }).fail((xhr) => {
            toastr.error(xhr.responseJSON.msg);
        })
    }

    function bulan_numeric(bulan){
        if(!isNaN(bulan) && bulan >= 0){
            return true;
        }else{
            return false;
        }
    }

    function check_is_valid(form){
        let validate = $(form).serializeArray();
        // console.log(validate)
        if(validate.length <= 0){
            return false;
        }
        let _err = 0;
        for(let i = 0; i < validate.length; i++){
            if(!validate[i].value){
                _err++
            }
        }

        if(_err > 0){
            return false;
        }
        return true;
    }

    function check_is_valid_update(uid){
        let bulan = $('#uid_tr_'+uid+' [name="bulan"]').val();
        if(!bulan){
            return false;
        }else{
            return true;
        }
    }

    function update_data(uid){
        let validate = check_is_valid_update(uid);
		if(validate){
            //check bunga
            let bulan = $('#uid_tr_'+uid+' [name="bulan"]').val();
            let formData =  {
                bulan: bulan,
                id_produk: id_kredit,
            };
            // console.log(bulan, bulan_numeric(bulan));
            if(!bulan_numeric(bulan)){
                toastr.error('Bulan Harus berupa Angka dan tidak boleh bilangan negatif');
                return;
            }
            // console.log(formData);
            $.post(`${url.edit_bulan}/${uid}`, formData).done((res) => {
                toastr.success(res.msg, 'Berhasil !');
                setTimeout(() => {                
                    window.location.href = `${location.protocol}//${location.host}${location.pathname}`;
                }, 2000);
            }).fail((xhr) => {
                toastr.error(xhr.responseJSON.msg);
            })
        }else{
            $.unblockUI();
			toastr.error('Silahkan Lengkapi Form');
        }
    }

</script>