<link href="https://fonts.googleapis.com/css?family=Miriam+Libre:400,700" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/7737514/7707592/css/fonts.css">
<style type="text/css">

body{
	background-color: beige;
}
h1{
	text-align: center;
    color: #525252;
    font-family: "Whitney A","Whitney B",sans-serif;
    text-shadow: 2px 2px #ccc;
}
h2{
	font-size: 21px;
    letter-spacing: -.02em;
    font-family: "Miriam Libre",sans-serif;
    -webkit-font-smoothing: antialiased;
    text-align: center;
}
pre {
    z-index: 2;
    height: 321px;
    top: 46px;
    width: 554px;
    margin-left: -278px;
    padding-bottom: 0;
    overflow: hidden
}
.macbook {
    position: relative;
}

.macbook svg {
    width: 100%;
    height: 425px;
    position: relative;
    top: -340px;
}

pre.line-numbers,pre.line-numbers>code {
    position: relative
}

.docs article code[class*=language-],.docs article pre[class*=language-] {
    font-size: 12px;
    line-height: 2;
    vertical-align: middle
}

code[class*=language-],pre[class*=language-] {
    color: #000;
    text-shadow: 0 1px #fff;
    text-align: left;
    line-height: 1.7;
    font-size: 12px;
    -moz-tab-size: 4;
    -o-tab-size: 4;
    tab-size: 4;
    -webkit-hyphens: none;
    -ms-hyphens: none;
    hyphens: none
}

code[class*=language-] ::-moz-selection,code[class*=language-]::-moz-selection,pre[class*=language-] ::-moz-selection,pre[class*=language-]::-moz-selection {
    text-shadow: none;
    background: #b3d4fc
}

code[class*=language-] ::selection,code[class*=language-]::selection,pre[class*=language-] ::selection,pre[class*=language-]::selection {
    text-shadow: none;
    background: #b3d4fc
}

@media print {
    code[class*=language-],pre[class*=language-] {
        text-shadow: none
    }
}

pre[class*=language-] {
    margin: 10px 0 20px;
    overflow: auto
}

:not(pre)>code[class*=language-],pre[class*=language-] {
    background: rgba(238,238,238,.35);
    border-radius: 3px;
    padding: 10px;
    box-shadow: 0 1px 1px rgba(0,0,0,.125)
}

:not(pre)>code[class*=language-] {
    background: #f0f2f1;
    color: #f4645f;
    padding: 1px 5px;
    border-radius: 3px
}

.token.cdata,.token.comment,.token.doctype,.token.prolog,.token.punctuation {
    color: #999
}

.namespace {
    opacity: .7
}

.token.attr-name,.token.boolean,.token.constant,.token.deleted,.token.number,.token.property,.token.scope,.token.symbol,.token.tag {
    color: #DA564A
}

.token.builtin,.token.char,.token.inserted,.token.selector,.token.string {
    color: #2E7D32
}

.language-css .token.string,.style .token.string,.token.entity,.token.operator,.token.url {
    color: #555
}

.token.atrule,.token.attr-value,.token.keyword {
    color: #07a
}

.token.function {
    color: #555
}

.token.important,.token.regex,.token.variable {
    color: #4EA1DF
}

pre.line-numbers {
    padding-left: 3.8em;
    padding-top: 0;
    margin-top: -1px;
    border-radius: 0;
    counter-reset: linenumber
}

.line-numbers .line-numbers-rows {
    position: absolute;
    pointer-events: none;
    top: -4px;
    padding-top: 0;
    font-size: 100%;
    left: -3.8em;
    width: 3em;
    letter-spacing: -1px;
    background: #f0f2f1;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none
}

.slide-menu h2,.wrap {
    position: relative
}

.callout.rule,footer.main p {
    letter-spacing: 2px;
    text-transform: uppercase
}

.line-numbers-rows>span {
    pointer-events: none;
    display: block;
    counter-increment: linenumber
}

.line-numbers-rows>span:before {
    content: counter(linenumber);
    color: #999;
    display: block;
    padding-right: .8em;
    text-align: right
}

.callouts:after,.packages:after,.panel.features .blocks .block:after,.sidebar h2:after,nav.main:after {
    content: ""
}

.docs .slide-main-nav .nav-docs,.overlay,.slide-menu:not(.scotch-panel-left) {
    display: none
}

.dark-code code[class*=language-],.dark-code pre[class*=language-] {
    color: #f8f8f2;
    text-shadow: 0 1px rgba(0,0,0,.3);
    text-align: left;
    line-height: 1.5;
    -moz-tab-size: 4;
    -o-tab-size: 4;
    tab-size: 4;
    -webkit-hyphens: none;
    -ms-hyphens: none;
    hyphens: none
}

.dark-code pre[class*=language-] {
    padding: 1em;
    margin: .5em 0;
    overflow: auto;
    border-radius: .3em
}

.dark-code :not(pre)>code[class*=language-],.dark-code pre[class*=language-] {
    background: #272822
}

.dark-code :not(pre)>code[class*=language-] {
    padding: .1em;
    border-radius: .3em
}

.dark-code .token.cdata,.dark-code .token.comment,.dark-code .token.doctype,.dark-code .token.prolog {
    color: #708090
}

.dark-code .token.punctuation {
    color: #f8f8f2
}

.dark-code .namespace {
    opacity: .7
}

.dark-code .token.constant,.dark-code .token.deleted,.dark-code .token.property,.dark-code .token.symbol,.dark-code .token.tag {
    color: #f92672
}

.dark-code .token.boolean,.dark-code .token.number {
    color: #ae81ff
}

.dark-code .token.attr-name,.dark-code .token.builtin,.dark-code .token.char,.dark-code .token.inserted,.dark-code .token.selector,.dark-code .token.string {
    color: #a6e22e
}

.dark-code .language-css .token.string,.dark-code .style .token.string,.dark-code .token.entity,.dark-code .token.operator,.dark-code .token.url,.dark-code .token.variable {
    color: #f8f8f2
}

.dark-code .token.atrule,.dark-code .token.attr-value {
    color: #e6db74
}

.dark-code .token.keyword {
    color: #66d9ef
}

.dark-code .token.important,.dark-code .token.regex {
    color: #fd971f
}
</style>
<h1>You are awesome!</h1>
<h2>Welcome to Nusamba Cepiring</h2>

<center>
<a class="waves-effect waves-light btn" href="<?php echo base_url(); ?>admin">Login to admin</a>
<br>
<br>
<div class="macbook">
<pre class="line-numbers language-php"><code class=" language-php"><span class="token delimiter">&lt;?php</span>


<span class="token keyword">class</span> <span class="token class-name">Dashboard</span> <span class="token keyword">extends</span> <span class="token class-name">Admin_Controller</span>
<span class="token punctuation">{</span>

	<span class="token comment" spellcheck="true">/**
	 * Dreaming of something more?
	 *
	 * @with  <span class="searchword">Cinta</span>
	 */</span>
	<span class="token keyword">public</span> <span class="token keyword">function</span> <span class="token function">index<span class="token punctuation">(</span></span><span class="token punctuation">)</span>
	<span class="token punctuation">{</span>
	<span class="token comment" spellcheck="true">	// Have a fresh start...
</span>	<span class="token punctuation">}</span>

<span class="token punctuation">}</span><span class="line-numbers-rows"><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span></code></pre>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" width="694.1px" height="384px" viewBox="0 0 694.1 384" enable-background="new 0 0 694.1 384" xml:space="preserve">
<defs>
</defs>
<g id="macbook" transform="translate(1.000000, 1.000000)" sketch:type="MSLayerGroup">
	<path id="body-top" sketch:type="MSShapeGroup" fill="#FEFEFE" stroke="#8492A5" stroke-width="2" d="M594,0H98
		C84.5,0,73,11.1,73,24.8V351h546V24.8C619,11.1,607.5,0,594,0z"></path>
	<circle id="webcam" sketch:type="MSShapeGroup" fill="none" stroke="#8492A5" stroke-width="2" cx="347" cy="19" r="4">
	</circle>
	<g id="body-bottom-group" transform="translate(0.000000, 351.000000)" sketch:type="MSShapeGroup">
		<path id="body-bottom" fill="#FDFDFD" stroke="#8492A5" stroke-width="2" d="M640.8,31H51.3C20.6,31,0,20.5,0,16V2.4C0,1.1,1.3,0,3,0
			h686.1c1.7,0,3,1.1,3,2.4v14.1C692.1,20.1,676.1,31,640.8,31z"></path>
		<path id="bottom-seam" fill="none" stroke="#8492A5" stroke-linecap="square" d="M0.5,14.5h689.7"></path>
	</g>
	<rect id="screen" x="95" y="39" sketch:type="MSShapeGroup" fill="#FFFFFF" stroke="#8492A5" width="501.1" height="292">
	</rect>
	<path id="touchpad" sketch:type="MSShapeGroup" fill="#FFFFFF" stroke="#8492A5" d="M421,352v3.1c0,2.2-4.3,2.9-7.6,2.9H278.8
		c-3.5,0-7.8-0.7-7.8-2.9V352"></path>
</g>
</svg>
</div>
</center>