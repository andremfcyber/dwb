<style>
    body .alignleft, .entry-content-wrapper a:hover .alignleft {
        float: left;
        margin: 4px 30px 0px 0;
        display: block;
        position: relative;
    }
   .entry-content ul {
       margin-left:21px;
       margin-bottom:21px;
   }

   ol {
    margin-left: 39px;
   }
   
   .redb {
       color:#d61228;
       font-weight:bold;
   }
</style>
<section class="text-center" style="background-image: linear-gradient(224deg, #c61125 30%, #d61228 28%)">
    <div class="container">
        <div class="p-5">
            <h2 class="text-white">Kode Etik Perusahaan</h2>
        </div>
    </div>
</section>
<section class="p-1 mt-4 mb-4"  >
    <div class="container">
        <?php 
            foreach ($v_halaman as $v_half):
                echo $v_half['isi'].'<hr>';
            endforeach;
        ?> 
    <!-- <main class="template-page content av-content-small units" role="main" itemprop="mainContentOfPage"> 
            <h4 class="av-special-heading-tag" itemprop="headline"><i class="fa fa-info"></i> <b class="redb">Kode Etik</b> dan <b class="redb">Pedoman Perilaku</b></h4>
        <hr>
                    <section class="slide-entry flex_column post-entry slide-entry-overview slide-loop-1 slide-parity-odd av_fullwidth first" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                            <h4 class="slide-entry-title entry-title" itemprop="headline">DI LINGKUNGAN KERJA</h4>
                            <div class="slide-entry-excerpt entry-content" itemprop="text">
                                <ol>
                                    <li style="text-align: justify;">
                                        Bekerja secara profesional dengan mengedepankan 7 (tujuh) Budi Utama Bank Eka, mentaati, mematuhi, dan melaksanakan sistem dan prosedur tata kerja di Bank Eka serta peraturan perundang-undangan yang berlaku;
                                    </li>
                                    <li style="text-align: justify;">
                                        Menjaga nama baik perusahaan dengan mengutamakan prinsip Good Corporate Governance;<br />
                                        Bersikap, berperilaku, berbuat, berucap sesuai dengan visi, misi budaya kerja perusahaan;
                                    </li>
                                    <li style="text-align: justify;">Melaksanakan seluruh tugas yang diberikan perusahaan dengan sebaik-baiknya dan dengan penuh rasa tanggung jawab;</li>
                                    <li style="text-align: justify;">
                                        Melaksanakan seluruh tugas/perintah dan pekerjaanyang diberikan atasan dengan sebaik-baiknya dan dengan penuh rasa tanggung jawab kecuali yang bertentangan dengan ketentuan perusahaan atau peraturan
                                        perundang-undangan yang berlaku;
                                    </li>
                                    <li style="text-align: justify;">Melakukan pencatatan denganbenar mengenai transaksi dan data yang berkaitan dengan kegiatan usaha Bank Eka;</li>
                                    <li style="text-align: justify;">Melakukan perubahan atau penghapusan data berdasarkan otorisasi pejabat yang berwenang sesuai yang ditetapkan Bank Eka;</li>
                                    <li style="text-align: justify;">Menciptakan dan memelihara suasana kerja dan lingkungan kerja yang mendorong produktivitas dan keharmonisan antar sesama karyawan;</li>
                                    <li style="text-align: justify;">Memberikan contoh dan menjadi teladan yang baik di lingkungan kerja;</li>
                                    <li style="text-align: justify;">Menjaga kerapihan, kebersihan diri, keselamatan kerja dan tempat kerja masing-masing serta lingkunganya;</li>
                                    <li style="text-align: justify;">Menghormati pimpinan dan sesama karyawan;</li>
                                    <li style="text-align: justify;">Berusaha meningkatkan ilmu pengetahuan dan keterampilan;</li>
                                    <li style="text-align: justify;">
                                        Bersedia berbagi ilmu pengetahuan, keterampilan, pengalaman kepada bawahan atau rekan kerja dengan mengutamakan kerjasama tim dan menghindarkan diri persaingan yang tidak sehat;
                                    </li>
                                    <li style="text-align: justify;">Menempatkan kepentingan Bank Eka di atas kepentingan pribadi;</li>
                                    <li style="text-align: justify;">
                                        Mencegah, menghindari, dan tidak melakukan penekanan atau intimidasi sesama rekan kerja, atasan atau bawahannya untuk kepentingan tertentu, baik kepentingan pribadi atau pihak lain;
                                    </li>
                                    <li style="text-align: justify;">Memahami dan mematuhi serta melaksanakan ketentuan Perjanjian Kerja Bersama antara Bank Eka dengan Serikat Karyawan Bank Eka;</li>
                                    <li style="text-align: justify;">Dilarang melakukan penghinaan dalam bentuk tindakan atau menggunakan kata-kata tidak terpuji terhadap rekan kerja, atasan atau bawahannya;</li>
                                    <li style="text-align: justify;">Dilarang melakukan tindakan, perbuatan yang bertentangan dengan norma-norma agama dan nilai-nilai sosial;</li>
                                    <li style="text-align: justify;">Dilarang melakukan perbuatan tercela yangdapat mengurangi kepercayaan masyarakat terhadap citra Bank Eka dan perbankan pada umumnya.</li>
                                </ol>
                            </div>
                        </section>
                        <br>
                    <section class="slide-entry flex_column post-entry slide-entry-overview slide-loop-3 slide-parity-odd av_fullwidth first" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                            <h4 class="slide-entry-title entry-title" itemprop="headline">TERKAIT DENGAN KEPENTINGAN PRIBADI</h4>
                            <div class="slide-entry-excerpt entry-content" itemprop="text">
                                <ol>
                                    <li style="text-align: justify;">
                                        Menghindari segala bentuk kegiatan yang mengarah kepada benturan kepentingan, dan wajib segera melapor kepada pejabat yang berwenang apabila terdapat kegiatan dimaksud tidak dapat dihindarkan;
                                    </li>
                                    <li style="text-align: justify;">Mencegah perbuatan yang dapat menimbulkan terjadinya benturan kepentingan sehingga dapat merugikan perusahaan;</li>
                                    <li style="text-align: justify;">Menghindarkan diri dari keterlibatan pengambilan keputusan dalam hal terdapat benturan kepentingan;</li>
                                    <li style="text-align: justify;">
                                        Dilarang melakukan pungutan tidak sah, mengambil, memanfaatkan barang, uang, tabungan, deposito, setoran angsuran kredit dan&nbsp;atau setoran lainnya dari nasabah atau calon nasabah untuk kepentingan
                                        pribadi&nbsp;atau pihak lain;
                                    </li>
                                    <li style="text-align: justify;">Dilarang menerima hadiah atau imbalan secara langsung maupun tidak langsung terkait dengan tugas dan tanggung jawabnya untuk kepentingan pribadi maupun keluarganya;</li>
                                    <li style="text-align: justify;">Dilarang menggunakan fasilitas Bank Eka&nbsp;dan jam kerja untuk kepentingan pribadi tanpa seijin pejabat yang berwenang;</li>
                                    <li style="text-align: justify;">Dilarang menggunakan atau menerima fasilitas milik nasabah dan atau mitra serta dilarang memperoleh atau meminta pinjaman dari nasabah atau mitra untuk kepentingan pribadi.</li>
                                </ol>
                            </div>
                        </section>
                        <br>
                    <section class="slide-entry flex_column post-entry slide-entry-overview slide-loop-4 slide-parity-odd av_fullwidth first" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                            <h4 class="slide-entry-title entry-title" itemprop="headline">DI LUAR KEGIATAN BANK EKA</h4>
                            <div class="slide-entry-excerpt entry-content" itemprop="text">
                                <ol>
                                    <li style="text-align: justify;">
                                        Dilarang melakukan kegiatan-kegiatan usaha di luar Bank Eka yang dimungkinkan dapat mengakibatkan terbengkalainya pekerjaan dan menyebabkan tidak bisa mencurahkan tenaga dan pikiran sepenuhnya bagi Bank Eka
                                        atau berisiko timbulnya benturan kepentingan;
                                    </li>
                                    <li style="text-align: justify;">
                                        Dilarang menjadi pengurus atau pegawai perusahaan lain ataupun menjalankan perusahaan sendiri kecuali diperbolehkan menurut peraturan perusahaan dan perundang-undangan yang berlaku;
                                    </li>
                                    <li style="text-align: justify;">Dilarang menjadi anggota, fungsionaris atau pengurus partai politik maupun organisasi kemasyarakatan yang bermuatan politik yang berisiko mengganggu kinerja karyawan;</li>
                                    <li style="text-align: justify;">
                                        Dapat melaksanakan aktivitas diluar Bank&nbsp;Eka dan atau menjadi anggota perkumpulan/organisasi dengan izin tertulis pimpinan perusahaan sepanjang memenuhi persyaratan&nbsp;pokok&nbsp;sebagai berikut:<br />
                                        a. Bukan perkumpulan/organisasi terlarang;<br />
                                        b. Tidak menggunakan jabatan, fasilitas, simbol, atribut dan identitas yang berkaitan dengan Bank<br />
                                        <span style="color: #ffffff;">b.</span> Eka;<br />
                                        c. Tidak mengganggu konsentrasi dan kegiatan kerja.
                                    </li>
                                    <li style="text-align: justify;">Perkumpulan/Organisasi tersebut tidak berbenturan dengan Bank Eka;</li>
                                    <li style="text-align: justify;">
                                        Dapat melaksanakan aktivitas diluar Bank Eka dan atau menjadi pengurus paguyuban/komite sekolah di lingkungan tempat tinggal karyawan dengan izin tertulis pimpinan perusahaan.
                                    </li>
                                </ol>
                                <p style="text-align: justify;"></p>
                            </div>
                        </section>
                        <br>
                    <section class="slide-entry flex_column post-entry slide-entry-overview slide-loop-5 slide-parity-odd av_fullwidth first" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                            <h4 class="slide-entry-title entry-title" itemprop="headline">TERKAIT DENGAN INFORMASI BANK EKA</h4>
                            <div class="slide-entry-excerpt entry-content" itemprop="text">
                                <ol>
                                    <li style="text-align: justify;">Melaksanakan ketaatan dan kepatuhan dalam menjaga kerahasiaan bank sesuai dengan undang-undang yang berlaku;</li>
                                    <li style="text-align: justify;">
                                        Melaporkan kepada atasan atau pimpinan yang berwenang apabila mendapat tekanan dari pihak manapun untuk melakukan penyimpangan pencatatan atau hal-hal lain yang bertentangan dengan kebijakan internal Bank Eka
                                        dan atau Peraturan dan Perundang-undangan yang berlaku;
                                    </li>
                                    <li style="text-align: justify;">
                                        Dilarang menyimpan arsip nasabah dan data keuangan lainnya di tempat-tempat yang memungkinkan orang lain yang tidak berhak dapat melihatnya, mengetahui, mengambil, menyalin serta menggandakanya;
                                    </li>
                                    <li style="text-align: justify;">
                                        Tidak memberikan, meyerahkan, membuka, membocorkan informasi, taktik, strategi, dan/atau data bank dengan cara, maksud, alasan, dan bentuk apapun, kecuali atas persetujuan pimpinan dan/atau kewenangan yang
                                        diberikan sesuai dengan jabatannya.
                                    </li>
                                </ol>
                            </div>
                        </section>
                        <br>
                    <section class="slide-entry flex_column post-entry slide-entry-overview slide-loop-6 slide-parity-odd post-entry-last av_fullwidth first" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                            <h4 class="slide-entry-title entry-title" itemprop="headline">TERKAIT DENGAN PENCEGAHAN TINDAKAN FRAUD</h4>
                            <div class="slide-entry-excerpt entry-content" itemprop="text">
                                <ol>
                                    <li style="text-align: justify;">Bekerja dengan jujur, dan tanggung jawab untuk kepentingan Bank Eka;</li>
                                    <li style="text-align: justify;">Menolak setiap pemberian uang atau barang dari pihak ketiga dalam rangka tugas;</li>
                                    <li style="text-align: justify;">Memahami, mematuhi dan melaksanakan&nbsp;semua Kebijakan dan Prosedur serta peraturan internal lainnya;</li>
                                    <li style="text-align: justify;">Mematuhi Peraturan Otoritas Jasa Keuangan, Peraturan Bank Indonesia, dan perundang-undangan yang berlaku;</li>
                                    <li style="text-align: justify;">
                                        Berkewajiban melaporkan kepada atasan atau yang berwenang apabila mengetahui terjadi tindakan yang bertentangan dengan kebijakan&nbsp;internal, sistem dan prosedur&nbsp;perusahaan dan atau peraturan
                                        perundang-undangan yang berlaku, yang dapat merugikan Bank Eka;
                                    </li>
                                    <li style="text-align: justify;">Dilarang menyalahgunakan wewenangnya untuk kepentingan pribadi, golongan,maupun pihak lain&nbsp;yang dapat merugikan Bank Eka;</li>
                                </ol>
                            </div>
                        </section> 
        </main> -->

    </div>
</section>