<style>
    body .alignleft, .entry-content-wrapper a:hover .alignleft {
        float: left;
        margin: 4px 30px 0px 0;
        display: block;
        position: relative;
    }
   .entry-content ul {
       margin-left:21px;
       margin-bottom:21px;
   }
   .boldl {
       font-weight:bold;
       color:#d61228;
   }
</style>
<section class="text-center" style="background-image: linear-gradient(224deg, #c61125 30%, #d61228 28%)">
    <div class="container">
        <div class="p-5">
            <h2 class="text-white">Jaringan Kantor</h2>
        </div>
    </div>
</section>
<section class="p-1 mt-4 mb-4"  >
    <div class="container"> 
        <div class="row">
            <div class="col-md-4" style="border:1px solid #DDD;">
            <hr>
            <h2 class="titlepage boldl">Kantor Pusat</h2> 
            <hr>
                <article class="post-entry post-entry-type-page post-entry-151" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                    <div class="entry-content-wrapper clearfix">
                        <div class="entry-content" itemprop="text">
                            <p>
                                <img
                                    class="alignnone wp-image-861 size-large"
                                    src="http://www.bank-eka.co.id/wp-content/uploads/2016/02/Revisi-Website-3-1030x388.jpg"
                                    alt=""
                                    width="100%"  
                                />
                            </p>
                            <p><strong>Jl. Jend. Ahmad Yani No.70</strong></p>
                            <p>Telp (0725) 41246</p>
                            <p>Fax (0725) 42957</p>
                            <p>Kota Metro</p>
                        </div>
                    </div>
                </article> 
            </div>
            <div class="col-md-4" style="border:1px solid #DDD;">

                <hr>
                <h2 class="titlepage boldl">Kantor Cabang</h2> 
                <hr>

                
                <article class="post-entry post-entry-type-page post-entry-151" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                    <div class="entry-content-wrapper clearfix">
                        <div class="entry-content" itemprop="text">
                         
                            <img class="avia_image " src="https://www.bank-eka.co.id/wp-content/uploads/2016/02/Bandar-Lampung.png" alt="" title="Bandar Lampung" itemprop="contentURL">
                            
                            <p style="text-align: center;"><strong>KANTOR CABANG BANDAR LAMPUNG<br>
                            </strong>Jl. Ahmad Yani No 20<br>
                            Telp. (0721) 242500<br>
                            Faks. (0721)242400<br>
                            Kota Bandar Lampung</p>
                        </div>
                    </div>
                </article> 
               
            </div>
            <div class="col-md-4" style="border:1px solid #DDD;">
            <hr>
                <h2 class="titlepage boldl">LOKASI ATM </h2> 
                <hr>

                
                <article class="post-entry post-entry-type-page post-entry-151" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                    <div class="entry-content-wrapper clearfix">
                        <div class="entry-content" itemprop="text">
                         
                            <img class="avia_image " src="https://www.bank-eka.co.id/wp-content/uploads/2016/02/Bandar-Lampung.png" alt="" title="Bandar Lampung" itemprop="contentURL">
                            
                            <p style="text-align: center;"><strong>KANTOR PUSAT METRO<br>
                            Jl. Ahmad Yani No. 70 <br>
                            Kota Metro, Lampung
                        </div>
                    </div>
                </article> 
               
            </div>
        </div>

    </div>
</section>