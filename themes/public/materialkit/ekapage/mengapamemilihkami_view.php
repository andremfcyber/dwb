<style>
    .redbold {
        font-size:33px;
        color: #DE1213; 
        font-weight:bold;
    }
    .redb { 
        color: #DE1213; 
        font-weight:bold;
    }
    .red {
        color:#DE1213;
    }
    p {
        margin: 0px;
        font-size: 15px;
        color: #605c5c;
        line-height: 1.1;
    }
</style>
<!-- <section class="text-center" style="background-image: linear-gradient(224deg, #c61125 30%, #d61228 28%)">
    <div class="container">
        <div class="p-5">
            <h2 class="text-white">Selamat Datang</h2>
        </div>
    </div>
</section> -->
<section class="p-1"  >
    <div class="container">
        <nav class="mt-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Mengapa Memilih Kami</li>
            </ol>
        </nav>

        <b>Mengapa Memilih Kami ?</b>
        <h4> 
            <b class="redb">7 Budi Utama Bank EKA</b> <br>
        </h4> 
    </div>
    <?php 
        $banner = get_baner_by_kode('mengapa-memilihkami');
        if($banner!=""){ 
    ?>
        <img src="<?php echo base_url('upload/photo/'.$banner['foto_baner']); ?> " style="width : 100%; height : auto;" class="rounded" alt="Responsive image"> -->
    <?php  } ?>
<!-- <img class="aligncenter wp-image-1874 size-full mt-2" src="<?php //echo base_url(); ?>assets/images/lines-with-photo.png"   width="100%"> -->
    <div class="container"> 
        <div class="row m-2 d-flex">  
            <div class="col-md-12 pb-3 mt-3">
                <div class="text-center">
                    <h4> 
                        <b class="redb"> Mengapa #BersamaBankEka ?</b> <br>
                    </h4> 
                </div>

            </div>
        </div>

    </div>
</section>
<section class="add-section _spad_pad pt-0">
    <div class="container">
        <div class="add-warp">
            <div class="row add-text-warp">
               
                <br />
                <div class="col-lg-12">
                    <div class="row">
                        <?php 
                             foreach ($v_halaman as $v_half): 
                                // if($v_half['type'] == 'fitur'):
                        ?>
                            <div class="col-lg-4 col-md-6 col-sm-126 col-xs-12">
                                <div class="med-br">
                                    <div class="med-br-img">
                                        <img src="<?php if(!empty($v_half['foto'])){ echo base_url('upload/photo/').$v_half['foto'];  }else{  echo base_url()."/upload/noimg.jpg"; } ?>" alt="Icon" />
                                    </div>
                                    <div class="med-br-body">
                                        <span class="med-br-title"><?php echo $v_half['judul']; ?></span>
                                        <div class="med-br-content">
                                        <?php echo $v_half['isi']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>     
                        <?php
                                // endif;
                             endforeach;
                        ?>
                        <!-- <div class="col-lg-4 col-md-6 col-sm-126 col-xs-12">
                            <div class="med-br">
                                <div class="med-br-img">
                                    <img src="<?php echo base_url(); ?>assets/images/w-safe.png" alt="Icon" />
                                </div>
                                <div class="med-br-body">
                                    <span class="med-br-title">Aman</span>
                                    <div class="med-br-content">
                                    BANK EKA terdaftar 
                                    di LPS dan diawasi OJK
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-126 col-xs-12">
                            <div class="med-br">
                                <div class="med-br-img">
                                    <img src="<?php echo base_url(); ?>assets/images/w-hadiah.png" alt="Icon" />
                                </div>
                                <div class="med-br-body">
                                    <span class="med-br-title">Hadiah Menanti</span>
                                    <div class="med-br-content">
                                        Produk kami memiliki
                                        berbagai macam hadiah
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-126 col-xs-12">
                            <div class="med-br">
                                <div class="med-br-img">
                                    <img src="<?php echo base_url(); ?>assets/images/w-mobile.png" alt="Icon" />
                                </div>
                                <div class="med-br-body">
                                    <span class="med-br-title">Mobile Banking</span>
                                    <div class="med-br-content">
                                    Kemudahan dalam 
                                    genggaman Bank Eka
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-126 col-xs-12">
                            <div class="med-br">
                                <div class="med-br-img">
                                    <img src="<?php echo base_url(); ?>assets/images/w-fast.png" alt="Icon" />
                                </div>
                                <div class="med-br-body">
                                    <span class="med-br-title">Cepat</span>
                                    <div class="med-br-content">
                                    Persyaratan kredit mudah.
                                    Proses 3 hari kerja.
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-126 col-xs-12">
                            <div class="med-br">
                                <div class="med-br-img">
                                    <img src="<?php echo base_url(); ?>assets/images/w-ter.png" alt="Icon" />
                                </div>
                                <div class="med-br-body">
                                    <span class="med-br-title">Terbesar & Terpercaya</span>
                                    <div class="med-br-content">
                                        Memiliki Jaringan yang
                                        cukup luas dan terpercaya
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-126 col-xs-12">
                            <div class="med-br">
                                <div class="med-br-img">
                                    <img src="<?php echo base_url(); ?>assets/images/w-safe.png" alt="Icon" />
                                </div>
                                <div class="med-br-body">
                                    <span class="med-br-title">Aman</span>
                                    <div class="med-br-content">
                                        BANK EKA terdaftar 
                                        di LPS dan diawasi OJK
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br/>

<section class="add-section _spad_pad">
	<div class="container">
		<div class="add-warp">
			<div class="row add-text-warp">
			 
				<div class="col-lg-12">
				 
					<div class="mitra owl-carousel">
						<?php foreach($_pengawas as $data) { ?>
							<div class="item mx-auto">
								<div class="frame gmbr-wi1">
									<img src="<?php echo base_url(); ?>upload/noimg.jpg" data-src="<?php echo base_url('upload/logo/'.$data['logo'])?>" alt="pengawas bpr" class="lazy img-thumbnailwa rounded mx-auto d-block" >	
								</div>
							</div>
						<?php } ?>
					</div>		


 
				</div>
			</div>
		</div>
	</div>
</section> 