 
<style>
    .redbold {
        font-size:33px;
        color: #DE1213; 
        font-weight:bold;
    }
    .redb { 
        color: #DE1213; 
        font-weight:bold;
    }
</style> 
<section class="p-1"  >
    <div class="container">
        <nav class="mt-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Selamat Datang</li>
            </ol>
        </nav>
        <?php 
            foreach ($v_halaman as $v_half): 
         ?> 
         <!-- <div class="text-center p-3">
            <h2><?php  //echo $v_half['judul']; ?></h2>
        </div> -->
        <div class="row m-2 d-flex" >
            <div class="col-md-8 align-selft pb-3">
                <?php  echo $v_half['isi']; ?>
            </div> 
            <div class="col-md-4">
                <div class="img">
                    <img class="lazy" data-src="<?php if(!empty($v_half['foto'])){ echo base_url('upload/photo/').$v_half['foto'];  }else{  echo base_url()."/upload/noimg.jpg"; } ?>" alt="img" />
                </div> 
            </div>
        </div>
        <?php
            endforeach;
        ?> 
        <!-- <div class="row m-2 d-flex" >
            <div class="col-md-8 align-selft pb-3">
            
            <h2 class="redbold">Selamat Datang</h2>
            <h2>
                Kami terus <b class="redb">Bekerja,</b> <br>
                <b class="redb">bekerja</b> dan <b class="redb">selalu bekerja</b> untuk <br>
                mewujudkan 
                cita-cita dan visi kami.
            </h2>

          </div>
            <div class="col-md-8">
            <b>Pembaca website yang budiman, </b> 
            <p>Sudah hampir setengah abad Bank Eka memberikan pelayanan perbankan kepada masyarakat. Belum banyak BPR yang mampu bertahan melakukan aktivitasnya dalam kurun waktu yang cukup lama, namun BPR Eka Bumi Artha telah membuktikan mampu menjadikan dirinya sebagai salah satu BPR yang eksis dan terdepan di Indonesia. </p> 

            <p>Modal, SDM, Teknologi, dan Pasar adalah 4 pilar yang menjadi fokus kami untuk memperkuat kepercayaan masyarakat kepada Bank Eka.</p>

            <p>Kepercayaan masyarakat kepada Bank Eka, menjadi “mahkota perusahaan” yang harus senantiasa dijaga dengan sebaik-baiknya, karena dengan kepercayaan itulah Bank Eka dapat tumbuh dan berkembang menjadi sebuah bank yang besar dan sehat.</p> 
           <p>Modal yang kuat, SDM yang berintegritas dan mumpuni, pasar yang luas dan solid serta didukung teknologi yang handal akan menjadi sebuah kekuatan luar biasa untuk menggarap pasar sasaran secara lebih optimal dan menguntungkan. Ini telah menjadi visi, misi, dan komitmen dari seluruh jajaran pemegang saham, pengurus, dan para karyawan di Bank Eka.</p> 

           <p>Kami terus bekerja, bekerja dan selalu bekerja untuk mewujudkan cita-cita dan visi kami “Menjadi bank terbaik dan membanggakan dalam pembiayaan mikro, kecil, dan menengah”.</p> 

           <p>Berbekal pengalaman selama hampir setengah abad, telah menjadikan Bank Eka tumbuh semakin dewasa, sehingga dapat terus memahami arti “kepercayaan”, yang tidak berhenti sebatas mahkota perusahaan, tetapi harus menjadi brand equity kami yang tak terhingga nilainya.</p> 

           <p>Terima kasih kami haturkan kepada seluruh nasabah dan mitra kerja atas loyalitas dan dukungannya  kepada Bank Eka, semoga kerjasama yang terjalin dapat terus menjadi sinergi positif yang semakin luar biasa manfaatnya bagi kemajuan kita bersama. Aamiiin.</p><br>

            Eko Budiyono<br>

            Direktur Utama
            <br>
            <br>

            </div>
            <div class="col-md-4">

            </div>
        </div> -->
    </div>
</section>