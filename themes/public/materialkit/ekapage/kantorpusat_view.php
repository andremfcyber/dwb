<style>
    body .alignleft, .entry-content-wrapper a:hover .alignleft {
        float: left;
        margin: 4px 30px 0px 0;
        display: block;
        position: relative;
    }
   .entry-content ul {
       margin-left:21px;
       margin-bottom:21px;
   }
   .brdash {
    background: #f9f9f9; padding: 15px; border: 2px dashed #ffc027;
   }
</style>
<section class="text-center" style="background-image: linear-gradient(224deg, #c61125 30%, #d61228 28%)">
    <div class="container">
        <div class="p-5">
            <h2 class="text-white">Kantor Pusat</h2>
        </div>
    </div>
</section>
<section class="p-1 mt-4 mb-4"  >
    <div class="container">
    <?php 
            foreach ($v_halaman as $v_half): 
         ?> 
        
        <div class="row m-2 d-flex brdash " >
            <div class="col-md-6">
                <div class="img">
                    <img class="lazy" data-src="<?php if(!empty($v_half['foto'])){ echo base_url('upload/photo/').$v_half['foto'];  }else{  echo base_url()."/upload/noimg.jpg"; } ?>" alt="img" width="100%" />
                </div> 
            </div>
            <div class="col-md-6 align-selft pb-3">
                <?php  echo $v_half['isi']; ?>
            </div> 
        </div>
        <?php
            endforeach;
        ?> 
    </div>
</section>