<style>
	.spada_ {
		padding-top: 30px;
		padding-bottom: 35px;
	}
	

	@media (min-width: 320px) and (max-width: 480px) {
	
	.box-grid{
		flex-direction: column;
		text-align: center;
	}
	
	}
	.box-grid{
		border: 1px #ddd solid;
		display: flex;
		border-radius: 10px;
		margin-bottom: 20px;
		box-shadow: 0px 5px 3px #ddd !important;
	}
	.box-img{
		padding: 23px;
	}
	.box-img img{
		width: 100px;
	}
	.box-content{
		padding: 12px 5px 20px 5px;
	}
	.box-content .title{
		color: #366a34;
		font-weight: 500;
		font-size: 19px;
	}
	.box-content .content{
		font-size: 13px;
	}
	.box-content .action{

	}
	.box-content .action a{
		font-size: 13px;
		background: #ee832e;
		color: #fff;
		padding: 5px;
		text-transform: uppercase;
		border-radius: 4px;
	}
	.box-content .action a:hover{
		cursor: pointer;
		color: #fff;
	}
	._box{
		border-top-left-radius: 10px;
		border-bottom-right-radius: 10px;
		border-left: 3px #ddd solid;
		border-right: 3px #ddd solid;
		border-bottom: 1px #ddd solid;
		border-top: 1px #ddd solid;
		/* box-shadow: 0px 3px #eeee; */
	}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
</style>
	<section class="add-section spada_">
		<div class="container">
				<!-- <div class="col-lg-12 " style="text-align: center; margin-bottom: 20px; padding : 0px">
					<img src="<?php //echo base_url('upload/photo/').get_baner_by_kode('1_agenda')  ?>" style="width : 100%; height : auto" class="rounded" alt="Responsive image">
				</div> -->
			<div class="add-warp">	
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<ol class="breadcrumba _box">
							<li class="breadcrumb-item"><a class="gray" href="<?= base_url('public/home') ?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Agenda</li>
						</ol>
						<div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
						
							<a href="<?= base_url('public/home/press_realese') ?>">
								<button type="button" class="btn btn-primary g">
									Press Realese
								</button>
							</a>
					
							<a href="<?= base_url('public/home/cerita_pelanggan') ?>">
								<button type="button" class="btn btn-primary g">
									Cerita Pelanggan
								</button>
							</a>
						
							<a href="<?= base_url('public/home/laporan') ?>">
								<button type="button" class="btn btn-primary g">
									Laporan
								</button>
							</a>
						
							<!-- <a href="<?= base_url('public/home/blog') ?>">
								<button type="button" class="btn btn-primary g">
									Blog
								</button>
							</a> -->
					
							<a href="<?= base_url('public/home/karir') ?>">
								<button type="button" class="btn btn-primary g">
									Karir
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<section class="add-section spad pt-0">
		<div class="container">
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-4">
						<div class="yt">
							Agenda
						</div>
						<div class="ytb">
						</div>
					</div>
				</div>
				<br>
				<div class="col-lg-12">
					<?php if($_agenda) { ?>
					    <?php foreach($_agenda as $re) { ?>
					        <div class="col-lg-12 col-sm-12">
						        <div class="box-grid">
							        <div class="box-img">
						    	        <img src="<?php echo base_url('upload/photo/').$re['foto'] ?>" alt="Icon">
							        </div>
							        <div class="box-content">
								       <span class="title"><?= $re['nama']; ?></span>
								            <p class="content">
								            <?= substr($re['keterangan'],0, 100).'...' ?>
								           </p>
							        </div>
						        </div>
					        </div>
					    <?php } ?>
				    <?php }else{ ?>
					    <div class="col-md-12">
						    <div class="text-center"> 
							   Belum Ada Agenda 
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
	