<style>
	.spada_ {
		padding-top: 30px;
		padding-bottom: 35px;
	}
	._box{
		border-top-left-radius: 10px;
		border-bottom-right-radius: 10px;
		border-left: 3px #ddd solid;
		border-right: 3px #ddd solid;
		border-bottom: 1px #ddd solid;
		border-top: 1px #ddd solid;
		/* box-shadow: 0px 3px #eeee; */
	}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
</style>
		
	<section class="add-section spada_">
		<div class="container">
			<div class="col-md-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php echo base_url('upload/photo/').get_baner_by_kode('1_corevalue')  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
			</div>
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<ol class="breadcrumba _box">
							<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Core Value</li>
						</ol>
						<div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
							<a href="<?php echo base_url('public/home/profil') ?>">
								<button type="button" class="btn btn-primary g">
									Tentang Kami
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/visi_misi') ?>">
								<button type="button" class="btn btn-primary g">
									Visi Misi
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/struktur') ?>">
								<button type="button" class="btn btn-primary g">
									Struktur Organisasi
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/awards') ?>">
								<button type="button" class="btn btn-primary g">
									Awards
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/kenapa_memilih_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Kenapa Memilih Kami
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/faq') ?>">
								<button type="button" class="btn btn-primary g">
									FAQ
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/hubungi_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Hubungi Kami
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="add-section spad pt-0">
		<div class="container">
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-4">
						<div class="yt">
							Core Value
						</div>
						<div class="ytb">
						</div>
					</div>
					<div class="col-lg-12">
						<br>
						<p style="text-align:left; font-size:12px"  class="green">
							Membangun citra BPR SYARIAH Mitra Harmoni Kota Semarang dengan
							mengedepankan nilai-nilai individu yang islami, profesional, Ikhlas ber-Muamalah
						dan ber-Akhlaqul Kharimah.</i></p>
					</div>
					<br>
					<div class="col-lg-12">
						<table class="table table-striped">
							<thead class="g">
								<tr>
									<th ><?php echo count($_corevalue); ?> Core Value  <?php echo $_profil['nama']; ?></th>
								</tr>
							</thead>
							<tbody>
							<?php if($_corevalue){
								foreach($_corevalue as $_data){

								
							?>
								<tr>
									<td><?php echo $_data['nama']; ?></td>
								</tr>
								
							<?php 
								}
							}
							?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
																																																																																																																																																																																																																																																																																																																																																																																																																																