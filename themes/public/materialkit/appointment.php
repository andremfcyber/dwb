<style>
    ._box{
        border: 2px #ddd solid;
        padding: 28px 10px;
        margin: 17px 0px;
        border-radius: 8px;
        position: relative;
    }
    ._box_title{
        position: absolute;
        top: -30px;
        left: 18px;
        text-align: center;
        font-size: 25px;
        background: white;
        /* height: 36px; */
        padding: 11px;
    }
    #map-canvas{
        margin: 10px auto;
        width: calc(100% - 5px);
        height: 400px;
        border-radius: 10px;
        border: 2px #ddd solid;
    }
    #kode_negara{
        position: absolute;
        top: 39px;
        left: 18px;
    }
</style>
<!-- Add section end -->
<section class="add-section spad">
    <div class="container">
        <form id="form_pengajuan" enctype='multipart/form-data' action="<?php echo base_url().'public/home/save_appointment/'.$nasabah_product['id'] ?>">
            <div class="container">
                <div class="row" style="justify-content: center;">
                    <div class="col-md-12">
                        <div class="_box">
                            <span class="_box_title green">Formulir Pertemuan</span>
                            <div class="row" style="margin: 10px;">
                                <div class="form-group col-md-6" style="padding: 0px 4px;">
                                    <label for="tanggal_appoiment">
                                        Tanggal Pertemuan
                                    </label>
                                    <input type="text" name="tanggal_appoiment" class="form-control" id="tanggal_appoiment" placeholder="dd/mm/yyyy ">
                                </div>

                                <div class="form-group col-md-6" style="padding: 0px 4px;">
                                    <label for="waktu_appoiment">
                                        Waktu Pertemuan
                                    </label>
                                    <input type="text" name="waktu_appoiment" class="form-control" id="waktu_appoiment" placeholder="hh:mm">
                                </div>
                                <div class="form-group col-md-12" style="padding: 0px 4px;">
                                    <label for="lokasi">
                                        Lokasi
                                    </label>
                                    <input type="text" class="form-control textbox" name="lokasi" id="searchmap">
                                    <!-- <textarea name="lokasi" class="form-control" id="lokasi" placeholder="Lokasi" style="height: 120px"></textarea> -->
                                    <div id="map-canvas"></div>
                                    <input type="hidden" id="lat" name="lat">
                                    <input type="hidden" id="lng" name="lng">
                                </div>
                                <div class="form-group col-md-12" style="padding: 0px 4px;"> 
                                    <label class="green">Nama Pihak Yang Data di Hubungi</label>
                                    <input class="form-control" name="pihak_lain"> 
                                </div>
                                <div class="form-group col-md-6" style="padding: 0px 4px;"> 
                                    <label class="green">No.Hp</label>
                                    <span id="kode_negara">+62</span>
                                    <input class="form-control no_hp" name="no_telp_pihak_lain" style="padding-left: 48px;"> 
                                </div>
                                <?php 
                                    $hubungan = [
                                        "Orang Tua",
                                        "Sodara Kandung",
                                        "Sepupu",
                                        "Teman/Sahabat",
                                        "Suami/Istri",
                                    ];
                                ?>
                                <div class="form-group col-md-6" style="padding: 0px 4px;"> 
                                    <label class="green">Hubungan</label>
                                    <select name="hubungan" class="form-control">
                                        <?php
                                            foreach($hubungan as $_name){
                                                echo '<option value="'.$_name.'">'.$_name.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-12" style="padding: 0px 4px;"> 
                                    <label class="green">Alamat</label>
                                    <textarea class="form-control" name="alamat_pihak_lain" id=""></textarea>
                                </div>
                                <div class="col-md-12" style="text-align: center">
                                    <div style="font-size: 14px; padding: 10px 8px;">
                                        <span>Pastikan semua data terisi dengan benar agar memudahkan kami dalam memproses data.</span>
                                    </div>
                                    <div class="form-group" style="text-align: text-center">
                                        <button type="submit" class="btn btn-primary elips-btn">Kirim</button>
                                    </div>
                                </div> 
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<script src="<?= base_url('assets/dist/js/jquery.mask.js') ?>"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDO7ux0CVng-weBxmxNS7GpThvJvFqtLAQ&amp;libraries=places"></script>
<script type="text/javascript">
    $(function() {
        set_map(0,0)
        $('#tanggal_appoiment').mask('00/00/0000') 
        $('#waktu_appoiment').mask('00:00') 
        $('#no_hp').mask('000000000000') 

        $('form').submit((e) => {
            e.preventDefault()
            blockUI();
            let validate = check_is_valid();
            if(validate){
                var data = $('form').serializeArray();
                var target = $('form').attr('action');
                // console.log(data);
                setTimeout(function() {
                    $.post(target, data, function(res) {
                        if(res.status == 1){
                            $.unblockUI();
                            toastr.success(res.message);
                            setTimeout(() => {
                                window.location.href = res.link_tracking
                            }, 2000);
                        }else{
                            $.unblockUI();
                            toastr.error(res.message);
                        }
                    });
                }, 1000);
            }else{
                $.unblockUI();
                toastr.error('Silahkan Lengkapi Form');
            }
        })
    });
    
    function set_map(lat,lng){
        lng = parseFloat(lng);
        lat = parseFloat(lat);
        var map = new google.maps.Map(document.getElementById('map-canvas'),{
            center:{
                lat: lat ? lat : 0,
                lng: lng ? lng : 0
            },
            zoom: 16
        });

        var marker = new google.maps.Marker({
            position: {
                lat: lat ? lat : 0,
                lng: lng ? lng : 0
            },
            map: map,
            draggable: true
        });
        var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
        
        google.maps.event.addListener(searchBox, 'places_changed', function(){

            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for(i=0; place=places[i]; i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }

            map.fitBounds(bounds);
            map.setZoom(16);

        });

        google.maps.event.addListener(marker,'position_changed', function(){

            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);

        });
    }

    function check_is_valid(){
        let validate = $('form').serializeArray();
        let _err = 0;
        for(let i = 0; i < validate.length; i++){
            if(!validate[i].value){
                _err++
            }
        }

        if(_err > 0){
            return false;
        }
        return true;
    }

    function onSubmit(){
        blockUI();
        let validate = check_is_valid();
        if(validate){
            var data = $('form').serializeArray();
            var target = $('form').attr('action');

            // console.log(data);
            setTimeout(function() {
                $.post(target, data, function(res) {
                    if(res.status == 1){
                        $("#dynamicModal .modal-body").html(res.message);
                        $("#dynamicModal .modal-header").html("");
                        $("#dynamicModal").modal({
                            show: true
                        });
                        $.unblockUI();
                        setTimeout(() => {
                            window.location.href = res.link_tracking
                        }, 5000);
                    }else{
                        $.unblockUI();
                        toastr.error(res.message);
                    }
                });
            }, 500);

        }else{
            $.unblockUI();
            toastr.error('Silahkan Lengkapi Form');
        }
    }
</script>