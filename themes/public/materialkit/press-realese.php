<style>
p {
    font-size: 12.1px;
    color: #838383;
    line-height: 2.1;
}

/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
  
  
}


/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  
   .mdm_baru{
      flex-direction: column;
      text-align: center;
   }
  
}
.mdm_baru {
    display: -ms-flexbox;
    display: flex;
    /* -ms-flex-align: start;
    align-items: flex-start; */
}
.box-img{
   padding: 10px;
}
.box-img img{
   width: 200px;
}
.box-content{
   padding: 12px 5px 20px 5px;
}
.box-content .title{
   color: #21438b;
   font-weight: 500;
   font-size: 19px;
}
.box-content .content{
   font-size: 13px;
}
.box-content .action{

}
.box-content .action a{
   font-size: 13px;
   background: transfarent;
   color: #fff;
   padding: 5px;
   text-transform: uppercase;
   border-radius: 4px;
}
.box-content .action a:hover{
   cursor: pointer;
   color: #fff;
}
.spad {
    padding-top: 30px;
    padding-bottom: 40px !important;
}

p {
    font-size: 11px;
    color: #838383;
    line-height: 2.1;
}
._box{
	border-top-left-radius: 10px;
	border-bottom-right-radius: 10px;
    border-left: 3px #ddd solid;
    border-right: 3px #ddd solid;
    border-bottom: 1px #ddd solid;
    border-top: 1px #ddd solid;
    /* box-shadow: 0px 3px #eeee; */
}
.breadcrumb-item a{
	color: #f6861f !important;
}
.card {
	position: relative;
	margin: 8px 0px;
    /* border-radius: 3px !important; */
    box-shadow: 3px 5px #ddd !important;
	border-radius: 15px;
	min-height: 503px;
}
.card-bodys.card-body-cs {
	height: 290px;
    border-radius: 15px;
}
img.card-img-top.card-img-cs {
    border-top-left-radius: 15px !important;
    border-top-right-radius: 15px !important;
    object-fit: cover;
    height: 280px;
}
h5.card-title.card-title-cs {
    text-transform: uppercase;
    color: #21438b;
    margin-top: 11px;
    font-size: 20px;
}
.card-date-cs {
    text-align: right;
    margin: 10px -6px;
}
.card-content-cs{
	font-size: 14px;
	margin-bottom: 45px;
}
a.btn-slk {
	font-size: 14px;
    background: #f6861f;
    color: white;
    position: absolute;
    padding: 11px;
    right: 0px;
    bottom: 0px;
    border-bottom-right-radius: 15px;
    border-top-left-radius: 15px;
}
.right-bar {
    border: 2px #ddd dashed;
    border-radius: 5px;
    padding: 11px 15px;
    margin: 32px 0px;
}
.right-bar-title span{
	color: #21438b;
	font-size: 31px;
}
.right-bar-content {
	margin: 10px 0px;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
}
a.kategori_btn {
	padding: 10px;
    background: #f6861f;
    color: #fff;
    text-transform: uppercase;
    border-bottom-right-radius: 10px;
    border-top-left-radius: 10px;
    font-size: 14px;
    /* margin-bottom: 10px; */
    margin: 3px;
}
.recent {
    display: flex;
    flex-direction: row;
    margin: 11px 0px;
}
img.recent-img {
    height: 100px;
    object-fit: cover;
    width: 100px;
    border-radius: 11px;
    border: 2px #ddd solid;
}
.recent-cover-img {
    margin-right: 11px;
}
.recent-content {
    display: flex;
    flex-direction: column;
    justify-content: center;
}
a.recent-content-title {
    color: #21438b !important;
}
.alert-cs{
	color: #00781b;
	background-color: #fbfffc;
	border-color: #c3e6cb;
	border: 2px solid;
}
.container-btn {
	margin-top: 31px;
	display: flex;
	flex-direction: row;
	justify-content: space-around;
}
a.recent-content-title {
	color: #21438b !important;
}
a.btn-artikel {
	background: white;
	color: #f6861f;
	padding: 3px 9px;
	border-radius: 70px;
	border: 2px solid #f6861f;
}
a.btn-artikel.disabled {
	background: white;
	color: #ddd;
	padding: 3px 9px;
	border-radius: 70px;
	border: 2px solid #ddd;
}
a.btn-artikel.disabled:hover {
	background: white;
	color: #ddd;
	cursor: not-allowed;
}
a.btn-artikel:hover {
	background: #f6861f;
	color: #fff;
}
.flex_center{
	display: flex;
	flex-direction: row;
	justify-content: center;
}
</style>
	<section class="add-section spad">
		<div class="container">
			<!-- <div class="col-md-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php //echo base_url('upload/photo/').get_baner_by_kode('1_press_realese')  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
			</div> -->
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
							
							<a href="<?= base_url('public/home/agenda') ?>">
								<button type="button" class="btn btn-primary g">
									Agenda
								</button>
							</a>
						
							<a href="<?= base_url('public/home/cerita_pelanggan') ?>">
								<button type="button" class="btn btn-primary g">
									Cerita Pelanggan
								</button>
							</a>
						
							<a href="<?= base_url('public/home/laporan') ?>">
								<button type="button" class="btn btn-primary g">
									Laporan
								</button>
							</a>
						
							<!-- <a href="<?= base_url('public/home/blog') ?>">
								<button type="button" class="btn btn-primary g">
									Blog
								</button>
							</a> -->
					
							<a href="<?= base_url('public/home/karir') ?>">
								<button type="button" class="btn btn-primary g">
									Karir
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-12" style="margin-top : 20px;">
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumba _box" style="margin-bottom: 0px !important;">
									<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home	') ?>">Beranda</a></li>
									<li class="breadcrumb-item active" aria-current="page">Press Realese</li>
								</ol>
							</div>
						</div>
					</div>
					<div class="col-md-8" style="margin-top: 25px;">
						<div class="row">			
							<?php if(count($results['results']) > 0) { ?>
								<?php foreach($results['results'] as $_data) { ?>
									<div class="col-sm-6" style="margin-bottom : 10px">
										<div class="card">
											<img class="card-img-top card-img-cs lazy"  src="<?php echo base_url(); ?>upload/noimg.jpg" data-src="<?php echo base_url('upload/photo/').$_data['foto']  ?>" alt="Kredit Murah">
											<div class="card-bodys card-body-cs">
												<div class="container">
													<div class="card-date-cs">
														<span><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $_data['created_at']; ?></span>
														<span><i class="fa fa-user" aria-hidden="true"></i> <?php echo $_data['created_by']; ?></span>
													</div>
													<h5 class="card-title card-title-cs"><?php echo $_data['judul']; ?></h5>
													<p class="card-content-cs">
														<?= spoiler($_data['isi']) ?>
													</p>
													<a class="btn-slk" href="<?php echo base_url().'public/home/press_realese_detail/'.$_data['id']; ?>">Baca Selengkapnya  >></a>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
								<?php 
								
									$next = $page + 1;
									$next_status = $next > $results['pagination']['more'] ? 'disabled' : '';	
									$prev =  $page - 1;
									$prev_status = $prev < 1 ? 'disabled' : '';							
									
								?>
						
							<?php }else{ ?>
								<div class="col-sm-12">
									<div class="text-center">
										Belum Ada Press Realese
									</div>
								</div>
							<?php } ?>
						</div>
						<div class="row">
							<div class = "col-md-12">
								<?php if($results['results']) { ?>
								<div class="container-btn">
									<a class="btn-artikel <?php echo isset($prev_status) ? $prev_status : '' ?>" href="<?php echo base_url().'public/home/artikel?page='.$prev; ?>">
										<i class="fa fa-arrow-left"></i>
									</a>
									<a class="btn-artikel <?php echo isset($next_status) ? $next_status : '' ?>" href="<?php echo base_url().'public/home/artikel?page='.$next; ?>">
										<i class="fa fa-arrow-right"></i>
									</a>
								</div>
								<?php } ?>
							</div>
						</div>
						<br>
					</div>
					<div class="col-md-4">
						<div class="right-bar">
							<div class="right-bar-title">
								<span>Kategori</span>
							</div>
							<div class="right-bar-content">
								<?php if(count($_kategori) > 0) { ?>
									<?php foreach($_kategori as $_data) { ?>
										<a class="kategori_btn" href="<?= base_url('public/home/press_realese?kategori=').$_data['id']; ?>"><?php echo $_data['nama'] ?></a>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
						
						<div class="right-bar">
							<div class="right-bar-title">
								<span>Press Realese Lainnya</span>
							</div>
							<div class="right-bar-content">
								<?php if(count($_lainnya) > 0) { ?>
									<?php foreach($_lainnya as $_data) { ?>
										<div class="recent">
											<div class="recent-cover-img">
												<img class="recent-img lazy"  src="<?php echo base_url(); ?>upload/noimg.jpg" data-src="<?php echo base_url('upload/photo/').$_data['foto']  ?>" alt="Kredit Bebas Bunga">
											</div>
											<div class="recent-content">
												<a class="recent-content-title" href="<?php echo base_url().'public/home/press_realese/'.$_data['id']; ?>"><?= $_data['judul'] ?></a>
												<span class="recent-content-spoiler"><?= spoiler($_data['isi'], 68) ?></span>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	