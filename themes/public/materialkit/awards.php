<style>
	@media (min-width: 320px) and (max-width: 480px) {
			#theGrid .box {
			cursor: pointer;
			margin-bottom: 10px;
			padding: 0 5px;
			min-height: 200px;
		}

		.col-4 {
			-ms-flex: 0 0 33.333333%;
			flex: 100%;
			max-width: 100%;
		}
		
	}
	.spada_ {
		padding-top: 30px;
		padding-bottom: 35px;
	}
	._box{
		border-top-left-radius: 10px;
		border-bottom-right-radius: 10px;
		border-left: 3px #ddd solid;
		border-right: 3px #ddd solid;
		border-bottom: 1px #ddd solid;
		border-top: 1px #ddd solid;
		/* box-shadow: 0px 3px #eeee; */
	}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
	.container-btn {
		margin-top: 31px;
		display: flex;
		flex-direction: row;
		justify-content: space-around;
	}
	a.recent-content-title {
		color: #21438b !important;
	}
	a.btn-artikel {
		background: white;
		color: #f6861f;
		padding: 3px 9px;
		border-radius: 70px;
		border: 2px solid #f6861f;
	}
	a.btn-artikel.disabled {
		background: white;
		color: #ddd;
		padding: 3px 9px;
		border-radius: 70px;
		border: 2px solid #ddd;
	}
	a.btn-artikel.disabled:hover {
		background: white;
		color: #ddd;
		cursor: not-allowed;
	}
	a.btn-artikel:hover {
		background: #f6861f;
		color: #fff;
	}
	.flex_center{
		display: flex;
		flex-direction: row;
		justify-content: center;
	}
</style>
	<section class="add-section spada_">
		<div class="container">
			<div class="col-md-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php echo base_url('upload/photo/').get_baner_by_kode('1_awards')  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
			</div>
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<ol class="breadcrumba _box">
							<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Awards</li>
						</ol>
						<div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
							<a href="<?php echo base_url('public/home/profil') ?>">
								<button type="button" class="btn btn-primary g">
									Tentang Kami
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/visi_misi') ?>">
								<button type="button" class="btn btn-primary g">
									Visi Misi
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/struktur') ?>">
								<button type="button" class="btn btn-primary g">
									Struktur Organisasi
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/budaya_perusahaan') ?>">
								<button type="button" class="btn btn-primary g">
									Budaya Perusahaan
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/kenapa_memilih_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Kenapa Memilih Kami
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/faq') ?>">
								<button type="button" class="btn btn-primary g">
									FAQ
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/hubungi_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Hubungi Kami
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="add-section spad pt-0">
		<div class="container">
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<h2 class="green" style="text-align:left; font-size:30px">Awards</h2>
					</div>
					<!-- <ol class="breadcrumba">
						<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Awards</li>
					</ol> -->
					<br><br>
					<!-- <div class="col-lg-12">
						<p style="text-align:left; font-size:15px" class="gray">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porttitor justo vel eros gravida ultricies. Cras bibendum velit vel quam blandit, pulvinar lobortis velit sagittis. Morbi tristique dolor eu diam egestas, id pulvinar tortor faucibus. Pellentesque lacinia leo tincidunt, cursus felis quis, dictum justo. Quisque sit amet semper nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin et purus eros. Sed maximus lobortis ex nec maximus. Nunc vel mi dignissim, vehicula orci quis, faucibus lectus. Sed ornare purus semper sagittis interdum.
						</p>
					</div> -->
					<br>
					<div class="col-lg-12">
						<div id="theGrid">
							<div class="container">
								<div class="row" id="recipegrid">
									<?php if($results['results'] > 0){ ?>
										<?php foreach($results['results'] as $_data){ ?>
											<div class="col-4 box" >
												<div class="imgHolder">
													<img src="<?php echo base_url('upload/photo/').$_data['foto'] ?>" alt="awards" class="img-responsive cover">
													<div class="in-box w-100">
														<div class="super-box h-100 w-100 text-center d-flex justify-content-center align-items-center">
															<div class="box-text">
																<p class="font-weight-bold"></p>
															</div>
														</div>
													</div>
												</div>
												<div class="description-box">
													<p class="article_name font-weight-bold text-center"> <?php echo $_data['keterangan'] ?></p>
												</div>
											</div>				
										<?php }?>
									<?php 
							
										$next = $page + 1;
										$next_status = $next > $results['pagination']['more'] ? 'disabled' : '';	
										$prev =  $page - 1;
										$prev_status = $prev < 1 ? 'disabled' : '';							
										
									?>
										<!-- <div class = "col-md-12">
											<div class="text-center">
												<nav aria-label="Page navigation example">
													<ul class="pagination">
														<li class="page-item <?php echo $prev_status ?>"><a class="page-link" href="<?php echo base_url().'public/home/awards?page='.$prev; ?>">Previous</a></li>
														<li class="page-item <?php echo $next_status ?>"><a class="page-link" href="<?php echo base_url().'public/home/awards?page='.$next; ?>">Next</a></li>
													</ul>
												</nav> 
											</div>
										</div> -->
									<?php }else{ ?>
										<div class="col-sm-12">
												<div class="text-center">
													Belum Ada Awards
												</div>
											</div>
									<?php } ?>
								</div>
								<div class="row">
									<div class = "col-md-12">
										<?php if($results['results']) { ?>
										<div class="container-btn">
											<a class="btn-artikel <?php echo isset($prev_status) ? $prev_status : '' ?>" href="<?php echo base_url().'public/home/awards?page='.$prev; ?>">
												<i class="fa fa-arrow-left"></i>
											</a>
											<a class="btn-artikel <?php echo isset($next_status) ? $next_status : '' ?>" href="<?php echo base_url().'public/home/awards?page='.$next; ?>">
												<i class="fa fa-arrow-right"></i>
											</a>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
																																																																																																																																																																																																																																																																																																																																																																																																																																										 -->