<style>
	.form-nominal{
		width: 100%;
		border: 1px solid #ccc;
		border-radius: 5px;
		padding: 10px 10px 10px 35px;
	}
	#span-rp{
		position: absolute;
		top: 43px;
		left: 14px;
	}
	.sp-blog-item {
		box-shadow: none !important;
		border: 1px #ddd solid;
	}
	.btn-calc{
		background: #21438b;
		color: white;
		font-weight: bolder;
		border: none;
	}
	._label_pilih{
		background: #f6861f;
		color: white !important;
		padding: 16px;
		text-align: center;
		margin: 52px 0px !important;
		font-size: 21px !important;
	}
	.bg-foot{
		background: #e6e6e6;
	}
	.bg-cepiring{
		background: #21438b;
	}

	.simulasi_kredit{
		font-size: 25px;
		padding: 30px 0px;
		color: #21438b;
	}
	/* 
	##Device = Desktops
	##Screen = 1281px to higher resolution desktops
	*/

	@media (min-width: 1281px) {
	
	
	
	}

	/* 
	##Device = Laptops, Desktops
	##Screen = B/w 1025px to 1280px
	*/

	@media (min-width: 1025px) and (max-width: 1280px) {
	
	
	
	}


	/* 
	##Device = Most of the Smartphones Mobiles (Portrait)
	##Screen = B/w 320px to 479px
	*/

	@media (min-width: 320px) and (max-width: 480px) {
	
	.box-grid{
		flex-direction: column;
		text-align: center;
	}
	
	}
	.box-grid{
		border: 1px #ddd solid;
		display: flex;
		border-radius: 10px;
		margin-bottom: 20px;
	}
	.box-img{
		padding: 23px;
	}
	.box-img img{
		width: 100px;
	}
	.box-content{
		padding: 12px 5px 20px 5px;
	}
	.box-content .title{
		color: #21438b;
		font-weight: 500;
		font-size: 19px;
	}
	.box-content .content{
		font-size: 13px;
	}
	.box-content .action{

	}
	.box-content .action a{
		font-size: 13px;
		background: #f6861f;
		color: #fff;
		padding: 5px;
		text-transform: uppercase;
		border-radius: 4px;
	}
	.box-content .action a:hover{
		cursor: pointer;
		color: #fff;
	}
	.spad {
		padding-top: 30px;
		padding-bottom: 40px !important;
	}

	.btn-calc {
		background: #e91e63;
		color: white;
		font-weight: bolder;
		border: none;
	}
	.bg-cepiring {
		background: #e91e63;
	}
	.simulasi_kredit {
		margin: 0px!important;
		font-size: 25px;
		padding: 15px;
		color: #e91e63;
	}
	.redbl {
	color:red;
	font-weight:bold;
	}
	.redcard {
	background-image:url('<?php echo base_url(); ?>assets/images/redcard.webp');
	background-size: cover;
		background-repeat: round;
	}
	.box-content .title {
		/* color: #ffad00; */
		font-weight: bold;
		font-size: 20px;
	}
	.box-content .content {
		font-size: 15px;
		/* color: #FFF; */
		line-height: 20px;
		padding-right: 42px;
	}
</style>
<?php 
	$banner = get_baner_by_kode('tabungan');
	if($banner!=""){ 
?>
	<img src="<?php echo base_url('upload/photo/'.$banner['foto_baner']); ?> " style="width : 100%; height : auto;" class="rounded" alt="Responsive image"> -->
<?php  } ?>
	<section class="add-section spad pt-0">
		<div class="container"> 
			<div class="row justify-content-center  mt-3">
				<div class="col-md-9" style="text-align: center; margin-bottom: 20px;">
					<h3 class="simulasi_kredit" class="redbl">Pilih Tabungan terbaik kami </h3>
					<p>Kami menawarkan tabungan terbaik. Pilih dan temukan sesuai keinginan anda <br>
					lalu daftarkan secara online sekarang</p>
				</div>
			</div>
			<div class="row add-text-warp">
				<?php if($show) { ?>
					<?php foreach($show as $re) { ?>
					<div class="col-lg-6 col-sm-12">
						<div class="box-grid">
							<div class="box-img">
								<?php
									if(!isset($re['thumbnail']) || $re['thumbnail'] == ''){
										$src = $theme_url.'/img_frontend/tangan.png';
									}else{
										$src = base_url('upload/thumbnail/'.$re['thumbnail']);
									}
								?>
								<img class="lazy" src="<?php echo base_url(); ?>upload/noimg.jpg" data-src="<?= $src ?>" alt="Kredit Gratis" style="height: 90px; width: 90px; object-fit: cover;">
							</div>
							<div class="box-content">
								<span class="title"><?= $re['name']; ?></span>
								<p class="content">
								<?= spoiler($re['deskripsi'],96).'...' ?>
								</p>
								<div class="action">
								<a href="<?= base_url().'public/home/show/'.$re['id'].'/'.$re['product_type_id']; ?>">Ajukan</a>
								<a href="<?php echo base_url().'public/home/detail_/'.$re['id'].'/'.$re['product_type_id'].'/tabungan'; ?>">Detail</a>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</section>
		<!-- Gallery section end -->
		
		
		<!-- Footer section  -->
		