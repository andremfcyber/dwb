</section> 
<footer class="footer-section set-bg" style="padding-bottom : 4px">
	<div class="row p-3 mb-3">
		<div class="col-lg-6 col-xs-12">
        <img style="padding: 6px;height: 98px;" src="<?php echo base_url('upload/logo/').$_profil['logo']  ?>" alt=""> 
                <br>
			<span style="color:white; font-size:15px"  class="mrgn-b-5"><?php if($_kontak){ echo $_kontak['alamat'];} ?></span><br>
			<span style="color:white; font-size:15px"  class="mrgn-b-5"><i class="fa fa-phone"></i>  Telepon : <a href="tel:<?php if($_kontak){ echo $_kontak['notelpon'];} ?>" style="color : #fff"><u><?php if($_kontak){ echo $_kontak['notelpon'];} ?></u></a> </span><br>
      <span style="color:white; font-size:15px"  class="mrgn-b-5"><i class="fa fa-whatsapp"></i> WhatsApp :  <a href="https://wa.me/<?php if($_kontak){ echo $_kontak['no_wa'];} ?>" style="color : #fff" target="blank"><u><?php if($_kontak){ echo $_kontak['no_wa'];} ?></u></a></span><br>
			<!-- <span style="color:white; font-size:15px"  class="mrgn-b-5"><i class="fa fa-fax"></i> Fax :  <?php //if($_kontak){ echo $_kontak['fax'];} ?></span><br>
			<span style="color:white; font-size:15px"  class="mrgn-b-5"><i class="fa fa-envelope"></i> Email :  <?php //if($_kontak){ echo $_kontak['email'];} ?></span><br> -->
        <br>
        <br>
        <br>
    </div>  
		<div class="col-lg-2 col-6 col-sm-6 col-xs-6">
      <div class="foot-link">
            <h3>Berita</h3>
            <ul>  
              <li><a class="item" href="<?php echo base_url('/agenda') ?>">Agenda</a> </li> 
              <li><a class="item" href="<?php echo base_url('/press_realese') ?>"> Berita</a> </li> 
              <li><a class="item" href="<?php echo base_url('/cerita_pelanggan') ?>">Testimoni</a> </li> 
              <li><a class="item" href="<?php echo base_url('/laporan') ?>">Laporan</a> </li>  
              <li><a class="item" href="<?php echo base_url('/karir') ?>">Karir</a> </li> 
          </ul>
      </div>
    </div>
		<div class="col-lg-2 col-6 col-sm-6  col-xs-6">
      <div class="foot-link">
        <h3>Produk</h3>
        <ul> 
          <li><a class="item" href="<?php echo base_url('/kredit') ?>">Kredit</a> </li>
          <li><a class="item" href="<?php echo base_url('/deposito') ?>">Deposito</a> </li>
          <li><a class="item" href="<?php echo base_url('/tabungan') ?>">Tabungan</a> </li>
        </ul>
      </div>
    </div>
		<div class="col-lg-2 col-6 col-sm-6  col-xs-6">
      <div class="foot-link">
        <h3>Media</h3>
          <ul>  
            <li> <a class="item" href="<?php echo base_url('/galeri') ?>">Foto</a></li>
        		<li><a class="item" href="<?php echo base_url('/download') ?>"> Download</a></li>	
            <li> <a class="item" href="<?php echo base_url('/faq') ?>">FAQ</a> </li> 
        		 <li> <a class="item" href="<?php echo base_url('/artikel') ?>">Artikel</a> </li>    
          </ul>
        </div>
    </div> 
	</div>
 
</footer>
<div class="footer-end">
    <div class="text-center">
      <p style="color : #fff"> Developed Manage by Tranyar | Operation by BNC IT Center</p>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_dinamis" tabindex="-1" role="dialog" aria-labelledby="modal-dong" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_dinamis_title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal_dinamis_body">
      </div>
      <div class="modal-footer" id="modal_footer">
      </div>
    </div>
  </div>
</div>

<!-- js -->

<script src="<?php echo $theme_url;?>/js_frontend/owl.carousel.min.js"></script>
<script src="<?php echo $theme_url;?>/js_frontend/main.js"></script>
<script src="<?php echo $theme_url;?>/js_frontend/bootstrap.min.js"></script>
<script src="<?= base_url('assets/dist/js/jquery.mask.js') ?>"></script>
<script src="<?= base_url('assets/dist/js/sweetalert2.all.js') ?>"></script>
<script src="<?= base_url('assets/plugins/toastr/toastr.min.js')?>"></script>
<script src="<?= base_url('assets/dist/js/autoNumeric.min.js') ?>"></script>
<script src="<?php echo $theme_url;?>/js_frontend/select2.min.js"></script>  
<script type="text/javascript" src="<?php echo $theme_url;?>/js_frontend/jquery.lazy.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_url;?>/js_frontend/jquery.lazy.plugins.min.js"></script>
  
<script>

/** SENPAI UI JS **/

$(function(){
  setTimeout(function(){
       $("body").fadeIn('slide');
    },500); //  

  $('.lazy').Lazy({ 
      effect: 'fadeIn'
  });  

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('<?php echo base_url(); ?>assets/sw.js')
      .then((reg) => console.log('Berhasil', reg))
      .catch((err) => console.log('Gagal', err));
  }

});

/** END SENPAI UI JS **/

$('.rupiah').number( true, 2 ,',','.');
$('.no_hp').mask('0000000000000');
$('.nik').mask('0000000000000000');
$( ".select2" ).select2({
	theme: "bootstrap"
});

function subs(){
  Swal.fire(
      'Terimakasih',
      'Telah mendukung BPR Nusamba Cepiring',
      'success'
  )
}

function alert_success(title, message){
  Swal.fire(
      title,
      message,
      'success'
  )
}
function alert_error(title, message){
  Swal.fire(
      title,
      message,
      'error'
  )
} 

function blockUI(message){
    $.blockUI({
        message: '<span class="text-semibold"><img src="'+base_url+'/assets/dist/img/loading_argan.gif'+'" style="height: 30px;">'+ (message ? message : ' Sedang Memproses') +'</span>',
        baseZ: 10000,
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.3,
            cursor: 'wait'
        },
        css: {
            'z-index': 10020,
            padding: '10px 5px',
            margin: '0px',
            width: '20%',
            top: '40%',
            left: '40%',
            'text-align': 'center',
            color: 'rgb(92, 132, 50)',
            border: '0px',
            'background-color': 'rgb(255, 255, 255)',
            cursor: 'wait',
            'border-radius': '12px',
            border: '2px rgb(92, 132, 50) solid',
            'font-size': '16px',
            'min-width': "95px",
        } 
    })
}

function blockElement(element, message, border){
    border = border ? border : '0px';
    $(element).block({
        message: '<span class="text-semibold"><img src="'+base_url+'/assets/dist/img/loading_argan.gif'+'" style="height: 30px;">'+(message ? message : ' Sedang Memproses') +'</span>',
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.3,
            cursor: 'wait',
            borderRadius: border,
        },
        css: {
            'z-index': 10020,
            padding: '10px 5px',
            margin: '0px',
            width: '20%',
            top: '40%',
            left: '40%',
            'text-align': 'center',
            color: 'rgb(92, 132, 50)',
            border: '0px',
            'background-color': 'rgb(255, 255, 255)',
            cursor: 'wait',
            'border-radius': '12px',
            border: '2px rgb(92, 132, 50) solid',
            'font-size': '16px',
            'min-width': "95px",
        } 
    });

}
 
var _TRIGGER_CHANGE = {
    PROVINSI  : true,
    KABUPATEN : true,
    KECAMATAN : true,
    KELURAHAN : true,
};
var uri_special = {
    get_all_provinces : '<?= base_url("public_service/provinsi") ?>',
    get_cities_by_province_id : '<?= base_url("public_service/kota?provinsi_id=:id") ?>',
    get_districts_by_city_id  : '<?= base_url("public_service/kecamatan?kota_id=:id") ?>',
    get_villages_by_district_id : '<?= base_url("public_service/kelurahan?kecamatan_id=:id") ?>',
};

function set_provinsi(name){
    $('[name="provinsi_'+name+'"]').empty();
    $('[name="provinsi_'+name+'"]').append('<option selected="true" value="" disabled>Pilih Provinsi</option>');
    $('[name="kota_'+name+'"]').empty();
    $('[name="kota_'+name+'"]').append('<option selected="true" value="" disabled>Pilih Kabupaten</option>');
    $('[name="kecamatan_'+name+'"]').empty();
    $('[name="kecamatan_'+name+'"]').append('<option selected="true" value="" disabled>Pilih Kecamatan</option>');
    $('[name="kelurahan_'+name+'"]').empty();
    $('[name="kelurahan_'+name+'"]').append('<option selected="true" value="" disabled>Pilih Kelurahan</option>');
	$.get(uri_special.get_all_provinces,function(data){
        $.each(data,function(index, val) {
        $('[name="provinsi_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
        });
    });
}

function change_provinsi(name){
    // alert('test');
    let id = $('[name="provinsi_'+name+'"]').val();

    $('[name="kota_'+name+'"]').empty();
    $('[name="kota_'+name+'"]').append('<option disabled selected="true" value="" disabled>Pilih Kabupaten</option>');

    $('[name="kecamatan_'+name+'"]').empty();
    $('[name="kecamatan_'+name+'"]').append('<option disabled selected="true" value="" disabled>Pilih Kecamatan</option>');

    $('[name="kelurahan_'+name+'"]').empty();
    $('[name="kelurahan_'+name+'"]').append('<option disabled selected="true" value="" disabled>Pilih Kelurahan</option>');

	$('[name="kodepos_'+name+'"]').val(null);

    if(_TRIGGER_CHANGE.PROVINSI){
      $.get(uri_special.get_cities_by_province_id.replace(':id',id),function(data){
        $.each(data,function(index, val) {
          // console.log('kabupaten',val)
          $('[name="kota_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
        });
      });     
    }
}

function change_kabupaten(name){

    let id = $('[name="kota_'+name+'"]').val();

    $('[name="kecamatan_'+name+'"]').empty();
    $('[name="kecamatan_'+name+'"]').append('<option value="-1" disabled selected="true" value="" disabled>Pilih Kecamatan</option>');

    $('[name="kelurahan_'+name+'"]').empty();
    $('[name="kelurahan_'+name+'"]').append('<option value="-1" disabled selected="true" value="" disabled>Pilih Kelurahan</option>');

    if(_TRIGGER_CHANGE.KABUPATEN){
      $.get(uri_special.get_districts_by_city_id.replace(':id',id),function(data){
        $.each(data,function(index, val) {
          // console.log('kecamatan',val)
          $('[name="kecamatan_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
        });
      });
    }
}

function change_kecamatan(name){

    let id = $('[name="kecamatan_'+name+'"]').val();

    $('[name="kelurahan_'+name+'"]').empty();
    $('[name="kelurahan_'+name+'"]').append('<option value="-1" disabled selected="true" value="" disabled>Pilih Kelurahan</option>');

    if(_TRIGGER_CHANGE.KECAMATAN){
      $.get(uri_special.get_villages_by_district_id.replace(':id',id),function(data){
        $.each(data,function(index, val) {
          // console.log('kelurahan',val)
          $('[name="kelurahan_'+name+'"]').append('<option value="'+val.id+'"  data-kodepos="'+val.kodepos+'">'+val.name+'</select>');
        });
      });
    }
}

function change_kelurahan(name){
	let kodepos = $('[name="kelurahan_'+name+'"] option:selected').data('kodepos');
	// alert(kodepos)
	$('[name="kodepos_'+name+'"]').val(kodepos);
}

function set_wilayah(provinsi_id,kabupaten_id,kecamatan_id,kelurahan_id,name){
    _TRIGGER_CHANGE = {
      PROVINSI  : false,
      KABUPATEN : false,
      KECAMATAN : false,
      KELURAHAN : false,
    };

    $('[name="provinsi_'+name+'"]').val(provinsi_id).trigger('change');
    $.get(uri_special.get_cities_by_province_id.replace(':id',provinsi_id),function(data){
      $.each(data,function(index, val) {
        // console.log('kabupaten',val)
        $('[name="kota_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
      });
      $('[name="kota_'+name+'"]').val(kabupaten_id).trigger('change');
      $.get(uri_special.get_districts_by_city_id.replace(':id',kabupaten_id),function(data){
        $.each(data,function(index, val) {
          // console.log('kecamatan',val)
          $('[name="kecamatan_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
        });
        $('[name="kecamatan_'+name+'"]').val(kecamatan_id).trigger('change');
        $.get(uri_special.get_villages_by_district_id.replace(':id',kecamatan_id),function(data){
          $.each(data,function(index, val) {
            // console.log('kelurahan',val)
            $('[name="kelurahan_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
          });
          $('[name="kelurahan_'+name+'"]').val(kelurahan_id).trigger('change');
          _TRIGGER_CHANGE = {
            PROVINSI  : true,
            KABUPATEN : true,
            KECAMATAN : true,
            KELURAHAN : true,
          };
        });
      });
    });
}

function set_modal_dinamis(title, body, footer){
	$('#modal_dinamis_title').html(title);
	$('#modal_dinamis_body').html(body);
	$('#modal_dinamis_footer').html(footer);
}

function open_modal_dinamis(){
	$('#modal_dinamis').modal('show');
}

function close_modal_dinamis(){
	$('#modal_dinamis').modal('hide');
}

function render_error(arr_err){
	let html = '';
	arr_err.forEach((value) => {
		html += '<li class="list-cs">'+value+'</li>';
	});
	return html;
}

const myOptions = {
  digitGroupSeparator        : '.',
  decimalCharacter           : ',',
  maximumValue	       : '99999999999999',
  minimumValue	       : '0',
  currencySymbol             : 'Rp. ',
  decimalPlacesShownOnBlur: 2,
  formatOnPageLoad: false,
};

$('.aRupiah').autoNumeric('init', myOptions);
const swal = Swal.mixin({
  customClass: {
      confirmButton: 'btn btn-primary',
      cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})
 
	// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	// (function(){
	// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	// s1.async=true;
	// s1.src='https://embed.tawk.to/5d2620e87a48df6da243eaa0/default';
	// s1.charset='UTF-8';
	// s1.setAttribute('crossorigin','*');
	// s0.parentNode.insertBefore(s1,s0);
	// })();
 
		function myFunction() {
			var x = document.getElementById("myTopnav");
			if (x.className === "topnav") {
				x.className += " responsive";
			} else {
				x.className = "topnav";
			}
		}

	</script>
</body>

</html>