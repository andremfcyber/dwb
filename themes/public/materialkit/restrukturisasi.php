<style>
	.form-nominal{
		width: 100%;
		border: 1px solid #ccc;
		border-radius: 5px;
		padding: 10px 10px 10px 35px;
	}
	#span-rp{
		position: absolute;
		top: 43px;
		left: 14px;
	}
	.sp-blog-item {
		box-shadow: none !important;
		border: 1px #ddd solid;
	}
	.btn-calc{
		background: #366a34;
		color: white;
		font-weight: bolder;
		border: none;
	}
	._label_pilih{
		background: #ee832e;
		color: white !important;
		padding: 16px;
		text-align: center;
		margin: 52px 0px !important;
		font-size: 21px !important;
	}
	.bg-foot{
		background: #e6e6e6;
	}
	.bg-cepiring{
		background: #366a34;
	}

	.simulasi_kredit{
		font-size: 25px;
		padding: 30px 0px;
		color: #366a34;
	}
/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
	.flexi{
		display: flex;
	}
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
	.flexi{
		display: flex;
	}
  
}


/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  
   .box-grid{
      flex-direction: column;
      text-align: center;
   }
   .flexi{
	display: block;
	}

}
	.box-grid{
		border: 1px #ddd solid;
		display: flex;
		border-radius: 10px;
		margin-bottom: 20px;
	}
	.box-img{
		padding: 23px;
	}
	.box-img img{
		width: 100px;
	}
	.box-content{
		padding: 12px 5px 20px 5px;
	}
	.box-content .title{
		color: #366a34;
		font-weight: 500;
		font-size: 19px;
	}
	.box-content .content{
		font-size: 13px;
	}
	.box-content .action{

	}
	.box-content .action a{
		font-size: 13px;
		background: #ee832e;
		color: #fff;
		padding: 5px;
		text-transform: uppercase;
		border-radius: 4px;
	}
	.box-content .action a:hover{
		cursor: pointer;
		color: #fff;
	}
	.spad {
		padding-top: 30px;
		padding-bottom: 40px !important;
	}

	.label-file {
		cursor: pointer;
		font-size: 14px;
		background: white;
		border: 2px #ddd solid;
		padding: 8px 15px;
		margin-bottom: 0px;
		border-radius: 4px;
		width: 50%;
		/* background: #28a745; */
		color: black;
		/* box-shadow: 1px 2px 3px 0px rgb(94, 94, 94); */
	}
	.file-custom {
		opacity: 0;
		position: relative;
		z-index: 0;
		cursor: pointer;
		top: -34px;
		width: 100%;
	}
	._box{
		border: 2px #ddd solid;
		padding: 28px 10px;
		margin: 17px 0px;
		border-radius: 8px;
		position: relative;
	}
	._box_title{
		position: absolute;
		top: -30px;
		left: 18px;
		text-align: center;
		font-size: 25px;
		background: white;
		/* height: 36px; */
		padding: 11px;
	}
	#kode_negara{
		position: absolute;
		top: 39px;
		left: 28px;
	}
	._aox{
			border-top-left-radius: 10px;
			border-bottom-right-radius: 10px;
			border-left: 3px #ddd solid;
			border-right: 3px #ddd solid;
			border-bottom: 1px #ddd solid;
			border-top: 1px #ddd solid;
			/* box-shadow: 0px 3px #eeee; */
		}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
	.container-btn {
		margin-top: 31px;
		display: flex;
		flex-direction: row;
		justify-content: space-around;
	}
	a.recent-content-title {
		color: #21438b !important;
	}
	a.btn-artikel {
		background: white;
		color: #f6861f;
		padding: 3px 9px;
		border-radius: 70px;
		border: 2px solid #f6861f;
	}
	a.btn-artikel.disabled {
		background: white;
		color: #ddd;
		padding: 3px 9px;
		border-radius: 70px;
		border: 2px solid #ddd;
	}
	a.btn-artikel.disabled:hover {
		background: white;
		color: #ddd;
		cursor: not-allowed;
	}
	a.btn-artikel:hover {
		background: #f6861f;
		color: #fff;
	}
	.flex_center{
		display: flex;
		flex-direction: row;
		justify-content: center;
	}
</style>
	<section class="add-section spad">
		<div class="container">
			<div class="add-warp">
                <div class="col-lg-12">
                    <br>
                    <div class="_box">
                        <form action="<?php echo site_url('public/home/add_restrukturisasi') ?>" method="POST" enctype="multipart/form-data">
                            <span class="_box_title green">Restrukturisasi</span>
                            <div class="row" style="margin-right: 0px; margin-left: 0px">
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">Nama Lengkap</label>
                                        <input class="form-control" name="nama" required> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">No.Rekening 1</label>
                                        <input class="form-control" name="norek1" required>  
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">No.Rekening 2</label>
                                        <input class="form-control" name="norek2" required>  
                                    </div>
                                </div> -->
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">No.Hp</label>
                                        <span id="kode_negara">+62</span>
                                        <input class="form-control no_hp" name="no_hp" style="padding-left: 48px;" required>  
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">Alamat</label>
                                        <textarea class="form-control" name="alamat" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" style="padding: 0px 4px;"> 
                                        <label class="green">Alasan</label>
                                        <textarea class="form-control" name="alasan" rows="3"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary g">Kirim</button>
                                </div>
                            </div>
                        </form>
                    </div> 
                    
                </div>
			</div>
		</div>
	</section>
																																																																																																																																																																																																																																																																																																																																																																																																																				