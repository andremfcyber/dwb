/* =================================
	------------------------------------
	Food Blog - Web Template
	Version: 1.0
	------------------------------------ 
====================================*/


'use strict';


$(window).on('load', function() {
	/*------------------
		Preloder
	--------------------*/
	$(".loader").fadeOut(); 
	$("#preloder").delay(400).fadeOut("slow");
	
	
	/*------------------
		Gallery item
	--------------------*/
	$('.gs-item').each(function() {
		var item_w = $(this).width();
		$(this).height(item_w);
	});
	
});




(function($) {
	
	/*------------------
		Navigation
	--------------------*/
	$('.nav-switch').on('click', function(event) {
		$('.main-menu').slideToggle(400);
		event.preventDefault();
	});
	
	
	/*------------------
		Background Set
	--------------------*/
	$('.set-bg').each(function() {
		var bg = $(this).data('setbg');
		$(this).css('background-image', 'url(' + bg + ')');
	});
	
	$('.nonloop-block-11').owlCarousel({
		center: false,
		items: 1,
		loop: true,
		autoplay: true,
		stagePadding: 20,
		margin:50,
		nav: false,
		dots: true,
		smartSpeed: 1000,
		navText: ['<span class="ion-chevron-left">', '<span class="ion-chevron-right">'],
		responsive:{
		  600:{
			stagePadding: 20,
			items:1
		  },
		  800:{
			stagePadding: 20,
			items:2
		  },
		  1000:{
			items:2
		  }
		}
	});

	$('.mitra').owlCarousel({
		center: false,
		items: 1,
		loop: true,
		center: true,
		autoplay: true,
		stagePadding: 40,
		autoWidth: true,
		margin:50,
		nav: false,
		dots: true,
		smartSpeed: 1000,
		// navText: ['<span class="ion-chevron-left">', '<span class="ion-chevron-right">'],
		responsive:{
		  600:{
			stagePadding: 20,
			items:1
		  },
		  800:{
			stagePadding: 20,
			items:2
		  },
		  1000:{
			items:2
		  }
		}
	});

	var ow = $('.ow');
	var owg = $('.owg');
	var owc = $('.owc');
	
	owg.owlCarousel({
		loop:true,
		margin: 10,
		autoWidth:true,
		nav: false,
		autoplay:true,
		autoplayTimeout: 3000,
		autoplayHoverPause:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:4
			},
			1000:{
				items:3
			}
		}
	})
	
	$('.customNextBtnf').click(function() {
		owg.trigger('next.owl.carousel');
	});
	$('.customPreviousBtnf').click(function() {
		owg.trigger('prev.owl.carousel');
	});
	
	owc.owlCarousel({
		loop:false,
		margin:10,
		nav:false,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:4
			},
			1000:{
				items:4
			}
		}
	})
	
	$('.customNextBtnc').click(function() {
		owc.trigger('next.owl.carousel');
	});
	$('.customPreviousBtnc').click(function() {
		owc.trigger('prev.owl.carousel');
	});
	
	
	ow.owlCarousel({
		loop:false,
		margin:10,
		nav:false,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:4
			},
			1000:{
				items:3
			}
		}
	})
	
	$('.customNextBtn').click(function() {
		ow.trigger('next.owl.carousel');
	});
	$('.customPreviousBtn').click(function() {
		ow.trigger('prev.owl.carousel');
	});
	/*------------------
		Hero Slider
	--------------------*/
	$('.hero-slider').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        items: 1,
        dots: false,
        mouseDrag: false,
        autoplay: true,
        animateOut: 'fadeOut',
    	animateIn: 'fadeIn',
    	navText: [' ', '<i class="fa fa-angle-right"></i>'],
	});
	
	
	/*------------------
		Add Carousel
	--------------------*/
    $('.add-slider').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        items: 1,
        dots: false,
        autoplay: true,
        animateOut: 'fadeOut',
    	animateIn: 'fadeIn',
	});
	
	
	
	/*------------------
		Gallery Carousel
	--------------------*/
    $('.gallery-slider').owlCarousel({
		loop:false,
		autoplay:true,
		nav:true,
		dots: true,
		responsive:{
			0:{
				items:4
			},
			990:{
				items:5
			},
			1200:{
				items:6
			}
		}
	});
	
	
	/*------------------
		Review Slider
	--------------------*/
	$('.review-slider').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        items: 1,
        dots: false,
        autoplay: true,
	});
	
	
	
})(jQuery);

