<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/datepicker-material/css/bootstrap-material-datetimepicker.css')?>"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style>
.form-nominal{
   width: 100%;
   border: 1px solid #ccc;
   border-radius: 5px;
   padding: 10px 10px 10px 35px;
}
#span-rp{
   position: absolute;
   top: 43px;
   left: 14px;
}
.sp-blog-item {
   box-shadow: none !important;
   border: 1px #ddd solid;
}
.btn-calc{
   background: #21438b;
   color: white;
   font-weight: bolder;
   border: none;
}
._label_pilih{
   background: #f6861f;
   color: white !important;
   padding: 16px;
   text-align: center;
   margin: 52px 0px !important;
   font-size: 21px !important;
}
.bg-foot{
   background: #e6e6e6;
}
.bg-cepiring{
   background: #21438b;
}

.simulasi_kredit{
   font-size: 25px;
   padding: 30px 0px;
   color: #21438b;
}
/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
  
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
  
  
}


/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  
   .box-grid{
      flex-direction: column;
      text-align: center;
   }
   .box-grid-n{
      flex-direction: column;
      text-align: center;
   }

   .box-grid-n .box-content {
      padding: 12px 5px 44px 5px !important;
      width: 100%;
   }
  
}

.box-grid-n .box-content {
   padding: 12px 5px 44px 5px !important;
   width: 100%;
}
.box-grid{
   border: 1px #ddd solid;
   display: flex;
   border-radius: 10px;
   margin-bottom: 20px;
}
.box-grid-n{
   /* border: 2px #ddd solid; */
   display: flex;
   border-radius: 10px;
   position: relative;
   min-height: 187px;
}
.act{
   position: absolute;
   right: 0px;
   bottom: 7px;
}
.act a{
   color: #fff !important;
   background: #a0a0a0;
   padding: 10px;
}
.act a:hover{
   color: #fff;
   background: #f6861f;
   padding: 10px;
   cursor: pointer;
}
.act  a:nth-child(1){
   border-top-left-radius: 10px;
}
.act  a:nth-child(2){
   border-bottom-right-radius: 10px;
}

.box-img{
   padding: 23px;
}
.box-img img{
   width: 100px;
}
.box-content{
   padding: 12px 5px 20px 5px;
}
.box-content .title{
   color: #21438b;
   font-weight: 500;
   font-size: 19px;
}
.box-content .content{
   font-size: 15px;
}
.box-grid-n .box-content .content{
   font-size: 15px;
   color: #838383;
}
.box-content .action{

}
.box-content .action a{
   font-size: 13px;
   background: #f6861f;
   color: #fff;
   padding: 5px;
   text-transform: uppercase;
   border-radius: 4px;
}
.box-content .action a:hover{
   cursor: pointer;
   color: #fff;
}
.spad {
    padding-top: 30px;
    padding-bottom: 40px !important;
}
.btn-orange{
   font-size: 13px;
   background: #f6861f;
   color: #fff;
   padding: 5px;
   text-transform: uppercase;
   border-radius: 4px;
}
.wrapper{
   border-radius: 10px;
   border: #ddd dashed 2px;
   min-height: 234px;
}
.wrap-title{
   color: #838383;
   padding: 10px 15px;
}
.box-content .bunga{
   color: #d65c29;
   font-size: 14px;
   padding: 6px 0px;
}
</style>
<section class="add-section spad">
   <div class="container">
      <div class="">
         <div class="row">
            <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
               <span class="simulasi_kredit" class="green"> Simulasi Produk Deposito <?php if($_profil){ echo $_profil['nama'];} ?></span>
            </div>
         </div>
         <div class="row add-text-warp">
            <div class="col-md-4">
               <div class="row" id="calc_scope">
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;">
                        <label>Silahkan Pilih Produk</label>
                        <select class="form-control select2" name="product" onchange="do_change_product(this)">
                           <option value="" selected>Pilih Disini</option>
                           <?php 
                              foreach($option_jenis_deposit as $data){
                                 echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
                              }
                           ?>
                        </select>
                        <input type="hidden" value="" name="jenis_bunga">
                        <input type="hidden" value="" name="bunga">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;"> 
                        <label>Jenis Bunga</label>
                        <select class="form-control select2" name="jenis_bunga" onchange="do_change_jenis_bunga(this)">
                           <option value="" disabled selected>Pilih Disini</option>
                           <option value="flat">Flat</option>
                           <!-- <option value="berjangka">Berjangka</option> -->
                        </select>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;"> 
                        <label>Tanggal Pembukaan</label>
                        <input type="text" class="datepicker form-control" name="tanggal_pembukaan">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;"> 
                        <label>Jangka Waktu (Dalam Bulan)</label>
                        <select class="form-control select2" name="jangka_waktu">
                           <option value="" disabled selected>Pilih Disini</option>

                        </select>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;position: relative">
                        <label>Nominal</label>
                        <!-- <span id="span-rp">Rp.</span> -->
                        <input class="form-control aRupiah" name="nominal">
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group" style="padding: 0px 4px;">
                        <button class="btn btn-calc" onclick="kalkulasi()">Kalkulasi</button>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-8" id="this-detail">
               <div class="wrapper">
                  <div class="wrap-title">
                     Detail Produk : 
                  </div>
                  <div class="wrap-body">
                     <input name="jenis_bunga" type="hidden">
                     <input name="bunga" type="hidden">
                     <div class="box-grid-n" style="display: none;">
                        <div class="box-img" >
                           <img id="detail_gambar" alt="Kredit Gratis" style="height: 90px; width: 90px; object-fit: cover;">
                        </div>
                        <div class="box-content">
                           <span class="title" id="nama_produk">-</span>
                           <div class="bunga" id="detail_bunga">-</div>
                           <div class="content" id="detail_produk">
                              -
                           </div>
                           <div class="act" id="btn-produk">
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Add section end -->
<!-- Add section end -->
<section class="add-section spad pt-0">
   <div class="container">
   <div class="">
   <div class="row">
      <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
         <span class="simulasi_kredit" class="green"> Hasil Simulasi </span><span class="simulasi_kredit" id="hasil_nama_produk"></span>
      </div>
   </div>
   <div class="row add-text-warp">
      <br><br><br>
      <div class="col-lg-12">
         <div class="table-responsive">
            <table class="table table-bordered" style="border-radius: 10px;">
				<thead class="bg-cepiring text-light">
					<tr>
						<th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Tanggal Deposito</span></th>
						<th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Tanggal Jatuh Tempo</span></th>
						<th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Nominal</span></th>
						<th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Jangka Waktu</span></th>
						<th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Suku Bunga Per Tahun</span></th>
						<th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Bunga</span></th>
						<th style="text-align:center; vertical-align: middle;"><span style="font-size: 14px;">Nominal Saat Jatuh Tempo</span></th>
					</tr>
				</thead>
				<tbody id="show_data">
					<tr>
						<td colspan="7" class="text-center">Belum Melakukan Simulasi</td>
					</tr>
				</tbody>
            </table>
         </div>
      </div>
   </div>
</section>
<section class="add-section spad pt-0">
   <div class="container">
      <div class="row">
         <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
            <span class="simulasi_kredit" class="green"> Produk Deposito yang Kami Miliki </span>
         </div>
      </div>
      <div class="row add-text-warp">
         <?php if($show) { ?>
            <?php foreach($show as $re) { ?>
               <div class="col-lg-6 col-sm-12">
                  <div class="box-grid">
                     <div class="box-img">
                        <?php
                           if(!isset($re['thumbnail']) || $re['thumbnail'] == ''){
                              $src = $theme_url.'/img_frontend/tangan.png';
                           }else{
                              $src = base_url('upload/thumbnail/'.$re['thumbnail']);
                           }
                        ?>
                        <img src="<?= $src ?>" alt="Kredit Gratis" style="height: 90px; width: 90px; object-fit: cover;">
                     </div>
                     <div class="box-content">
                        <span class="title"><?= $re['name']; ?></span>
                        <p class="content">
                           <?= spoiler($re['deskripsi'],96).'...' ?>
                        </p>
                        <div class="action">
                           <a href="<?= base_url().'public/home/show/'.$re['id'].'/'.$re['product_type_id']; ?>">Ajukan</a>
                           <a href="<?php echo base_url().'public/home/detail_/'.$re['id'].'/'.$re['product_type_id'].'/deposito'; ?>">Detail</a>
                        </div>
                     </div>
                  </div>
               </div>
            <?php } ?>
         <?php } ?>
      </div>
   </div>
</section>
<script type="text/javascript">
   var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
   (function(){
   var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
   s1.async=true;
   s1.src='https://embed.tawk.to/5d2620e87a48df6da243eaa0/default';
   s1.charset='UTF-8';
   s1.setAttribute('crossorigin','*');
   s0.parentNode.insertBefore(s1,s0);
   })();
</script>
<script src="<?= base_url().'assets/dist/js/moment.min.js'?>"></script>
<script src="<?= base_url('assets/plugins/datepicker-material/js/bootstrap-material-datetimepicker.js')?>"></script>
<script type="text/javascript">
	$('.datepicker').bootstrapMaterialDatePicker({format : 'DD/MM/YYYY', weekStart : 0, time: false });
</script>
<!--End of Tawk.to Script-->
<script type="text/javascript">
   $(document).ready(function() {
      // $('#example').DataTable();
   });
   
   function do_change_product(e){
      // console.log(e.value)
      let id = e.value;
      if(!id){
         alert('Pilih Produk');
         return false;
      }
      blockUI('Memuat ...');

      $.get(base_url+'public_service/get_product_detail_kredit/'+id).done((res) => {
         console.log(res)
         let action = `<a href="${base_url}/public/home/show/${res.id}/${res.product_type_id}">Ajukan</a>
         <a href="${base_url}/public/home/detail_/${res.id}/${res.product_type_id}">Detail</a>`;

         let src_gambar = '';
         $('#nama_produk').html(res.name)
         // $('#detail_bunga').html('Jenis Bunga : '+res.jenis_bunga+', Bunga : '+res.bunga+' %')
         $('#detail_produk').html(res.deskripsi)
         $('#detail_gambar').attr('src', res.gambar)
         $('#hasil_nama_produk').html(res.name)
         $('#btn-produk').html(action)

         $('.box-grid-n').show();

         $('[name="jenis_bunga"]').val(res.jenis_bunga)
         $('[name="bunga"]').val(res.bunga)
         
         $.unblockUI();
      }).fail((xhr) => {
         console.log(xhr)
         $.unblockUI();
      })
   }

   // function do_change_product(e){
   //    let id = e.value;
   //    blockUI('Memuat ...');
   //    $.get(base_url+'public_service/get_product_detail_deposito/'+id).done((res) => {
   //       console.log(res)
   //       let product = res.product;
   //       let html = '<button class="btn btn-orange" onclick="detail_deposito('+product.id+','+product.product_type_id+')">Selengkapnya</button>';
   //       let html_ajukan = '<button class="btn btn-orange" onclick="detail_ajukan('+product.id+','+product.product_type_id+')">Ajukan</button>';
         
   //       $('#nama_produk').html(product.name)
   //       $('#hasil_nama_produk').html(product.name)
   //       $('#detail_selengkapnya').html(html)
   //       $('#detail_ajukan').html(html_ajukan)
   //       // $('#detail_bunga').html('Jenis Bunga : '+product.jenis_bunga+', Bunga : '+product.bunga+' %')
   //       $('#detail_produk').html(product.deskripsi)
   //       $('._detail_produk').show();
   //       $('._label_pilih').hide();
   //       $.unblockUI();
   //    }).fail((xhr) => {
   //       console.log(xhr)
   //    })
   // }

   function detail_deposito(id,type_produk){
      // alert(id,type_produk);
      window.open(
         base_url+'public/home/detail_/'+id+'/'+type_produk+'/deposito',
         '_blank' 
      );
   }
   function detail_ajukan(id,type_produk){
      // alert(id,type_produk);
      window.open(
         base_url+'public/home/show/'+id+'/'+type_produk,
         '_blank' 
      );
   }

   function do_change_jenis_bunga(e){
      blockUI('Memuat ...');
      let id = $('[name="product"]').val();
      let jenis_bunga = e.value;

      if(!jenis_bunga){
         alert('Pilih Jenis Bunga');
         return false;
      }
      $.get(base_url+'public_service/get_product_detail_deposito/'+id+'/'+jenis_bunga).done((res) => {
         //Jangka Waktu
         let get_list = res.detail;
         let options = '<option value="" selected>Pilih Disini</option>';
         
         console.log(get_list);
         get_list.forEach((value) => {
            options += '<option value="'+value.bunga+'/'+value.bulan+'">'+value.bulan+'</option>';
         })
         $('[name="jangka_waktu"]').html(options);
         $.unblockUI();
      }).fail((xhr) => {
         console.log(xhr)
         $.unblockUI();
      })
   }

   function kalkulasi(){
      let tanggal = $('[name="tanggal_pembukaan"]').val();
      let jangka_waktu = $('[name="jangka_waktu"]').val();
      let nominal = $('[name="nominal"]').autoNumeric('get');

      console.log(tanggal, jangka_waktu, nominal);

      if(!tanggal || !jangka_waktu || !nominal){
         alert('Form Tidak Lengkap')
      }else{
         blockUI();
         getFlat(tanggal, jangka_waktu, nominal);
      }
   }

   function getFlat(tanggal, jangka_waktu, nominal){
      var tgl = tanggal;
      var jw = jangka_waktu;
      var nominal = nominal;
   
      // alert(0.08*100);
      var strArray = tgl.split("/");
      var strJW = jw.split("/");
      // alert(strArray);
      var thn = strArray[2];
      var bln = strArray[1];
      var tg = strArray[0];
      
      var bunga = strJW[0]/100; 
   
      var lmBln = strJW[1]; 
      // alert(bunga);
      
      tgl = thn+'-'+bln+'-'+tg;
      date1 = new Date (tgl);
      
      var y2k  = new Date(thn, 0 , tg);
      var blnn = parseInt(bln) + parseInt(lmBln) - 1 ;
      y2k.setMonth(blnn);
      var duedate = y2k.toDateString();
      
      
      
      date2 = new Date( duedate )
      var bdd = date2.getDate();
      var bmm = date2.getMonth()+1; //January is 0!
      var byyyy = date2.getFullYear();
      // alert(date2);
      var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      var firstDate = new Date(thn,bln,tg);
      console.log('Ini',lmBln)
      var secondDate = new Date(byyyy,bmm,bdd);
      console.log(secondDate)
      var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
      // var diffDays = 365;
      console.log(bunga)
      var deposito = parseFloat(nominal) * parseFloat(bunga) * (diffDays / 365);
      // deposito = deposito * lmBln;
      var akhir = parseFloat(parseFloat(deposito) + parseInt(nominal)) ;
      var bungah = bunga*100;
      var html = '';
      html = '<tr>'+
      '<td>'+formatDate(date1)+'</td>'+
      '<td>'+formatDate(date2)+'</td>'+
      '<td>Rp.</span><span class="float-right">'+formatRupiah(nominal)+'</span></td>'+
      '<td style="text-align:center">'+lmBln+' Bulan</td>'+
      '<td style="text-align:center">'+parseFloat(bungah.toFixed(2))+' %</td>'+
      '<td>Rp.</span><span class="float-right">'+ formatRupiah(parseFloat(deposito.toFixed(2))) +'</span></td>'+
      '<td>Rp.</span><span class="float-right">'+ formatRupiah(parseFloat(akhir.toFixed(2))) +'</span></td>'+
      '</tr>';
      $('#show_data').html(html);
      $.unblockUI();
   }

   function getBerjangka(){
      
   }
		
   function daysInMonth(month, year) {
      return new Date(year, month, 0).getDate();
   }

   function formatRupiah(num) {
      return num.toString().replace(/\./g, ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
   }
   
   function formatDate(date) {
      var monthNames = [
         "Januari", "Februari", "Maret",
         "April", "Mei", "Juni", "Juli",
         "Agustus", "September", "Oktober",
         "November", "Desember"
      ];
      
      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();
      
      return day + ' ' + monthNames[monthIndex] + ' ' + year;
   }

   function pad(num, size) {
      var s = num+"";
      while (s.length < size) s = "0" + s;
      return s;
   }
</script>
