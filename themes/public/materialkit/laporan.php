	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
	<script>
		$(document).ready(function() {
    		var table = $('#example').DataTable({
			"pagingType": "full_numbers"
			});
		} );
	</script>

	<script src = "https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
 	<script src = "https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
 	<style>
		.breadcrumb-item a{
			color: #f6861f !important;
		}
	</style>
	<section class="add-section spad">
		<div class="container">
			<div class="col-md-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php echo base_url('upload/photo/').$_slide['foto']  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
			</div>
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<ol class="breadcrumba _box">
							<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Laporan</li>
						</ol>
						<div class=" topnav " id="myTopnav" style="margin-top : 20px">
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
							
							<a href="<?= base_url('public/home/agenda') ?>">
								<button type="button" class="btn btn-primary g">
									Agenda
								</button>
							</a>
						
							<a href="<?= base_url('public/home/press_realese') ?>">
								<button type="button" class="btn btn-primary g">
									Press Realese
								</button>
							</a>
					
							<a href="<?= base_url('public/home/cerita_pelanggan') ?>">
								<button type="button" class="btn btn-primary g">
									Cerita Pelanggan
								</button>
							</a>
						
							<!-- <a href="<?= base_url('public/home/blog') ?>">
								<button type="button" class="btn btn-primary g">
									Blog
								</button>
							</a>
					 -->
							<a href="<?= base_url('public/home/karir') ?>">
								<button type="button" class="btn btn-primary g">
									Karir
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
					</div>
					
					<div class="col-lg-3">
						<br>
						<div class="yt">
							Kategori Laporan
						</div>
						<div class="ytb">
						</div>
						<br>
						<div class="list-group">
						<?php if($_kategori){	foreach($_kategori as $data) { ?>
							<a href="<?= base_url('public/home/laporanbykategori/'.$data['id']) ?>" class="list-group-item list-group-item-action"><?php echo $data['kategori_laporan'] ?></a>
						<?php }	} ?>
						</div>
					</div>
					<div class="col-lg-9">
						<br>
						<br>
						<br>
						<div class="yt">
						<?php if(!$_nm_kategori){ echo "Laporan";} ?> <?php if($_nm_kategori){ echo $_nm_kategori[0]['kategori_laporan']; } ?>
						</div>
						<div class="ytb">
						</div>
						<p style="text-align:left; font-size:15px">
							<?php if($_nm_kategori){ echo $_nm_kategori[0]['keterangan']; } ?>	
						</p>
						
					
						<table id="example" class="table table-striped table-bordered" style="width:100%">
							<thead class="g">
								<tr>
									<th>Tahun</th>
									<th>Keterangan</th>
									<th style="text-align:center">Download</th>
								</tr>
							</thead>
							<tbody>
								<?php if($_laporan){ foreach($_laporan as $_data){  ?>
									<tr>
										<td><?php echo $_data['tahun'] ?></td>
										<td><?php echo $_data['nama_file'] ?></td>
										<td style="text-align:center"><a href="<?php echo base_url('upload/file/').$_data['file']; ?>" download><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
									</tr>
								<?php } } ?>
								
							</tbody>
						</table>

						
					</div>
					
				</div>
			</div>
		</div>
	</section>

																																																																																																																																																																																																																																																																																																																																																																																																															