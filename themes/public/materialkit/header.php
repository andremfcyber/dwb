<?php defined( 'BASEPATH') OR exit( 'No direct script access allowed'); ?>
<!DOCTYPE html>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>
		<?php echo $this->settings->site_name; ?> |
		<?= $title ?>
	</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" sizes="192x192" href="<?php echo base_url(); ?>assets/image/bankeka.jpg">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/image/bankeka.jpg" type="image/svg">
	<link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/image/bankeka.jpg" type="image/svg">

	<link href="<?php echo base_url(); ?>assets/image/bankeka.jpg" rel="preload" as="image">
	<link href="<?php echo base_url(); ?>assets/image/bankeka.jpg" rel="preload" as="image">
	<link href="<?php echo base_url(); ?>assets/image/bankeka.jpg" rel="preload" as="image">
	<!-- Favicon -->
  
	<link href="<?php echo base_url('upload/logo/favicon1.png')?>" rel="shortcut icon" />
	<!-- Fonts -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" /> -->
		<!-- Bootstrap CSS -->
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/owl.carousel.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/animate.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/style.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?= base_url('assets/plugins/toastr/toastr.min.css')?>" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/select2.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="<?php echo $theme_url;?>/css_frontend/select2-bootstrap.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<link rel="preload" media="all" href="https://fonts.googleapis.com/css?family=Rubik&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'" >
		<noscript> 
			<link rel="stylesheet" media="all" href="<?php echo $theme_url;?>/css_frontend/bootstrap.min.css" > 
			<link rel="stylesheet" media="all" href="<?php echo $theme_url;?>/css_frontend/owl.carousel.css" >
			<link rel="stylesheet" media="all"  href="<?php echo $theme_url;?>/css_frontend/animate.css" >
			<link rel="stylesheet" media="all"  href="<?php echo $theme_url;?>/css_frontend/style.css" >
			<link rel="stylesheet" media="all"  type="text/css" href="<?= base_url('assets/plugins/toastr/toastr.min.css')?>">
			<link rel="stylesheet" media="all"  href="<?php echo $theme_url;?>/css_frontend/select2.min.css"   >
			<link rel="stylesheet" media="all"  href="<?php echo $theme_url;?>/css_frontend/select2-bootstrap.css" >
			<link rel="stylesheet" media="all"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" >
			<link rel="stylesheet" media="all" href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
			<link rel="stylesheet" media="all" href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
	 	</noscript>
		 
	<?php if ( isset( $css_files ) && is_array( $css_files ) ) : foreach ( $css_files as $css ) : ?>
	<!-- <link rel="stylesheet" href="<?= $css ?><?php echo '?r='.rand(1,1000);?>"> -->
	<?php echo "\n"; endforeach; endif; ?>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144983683-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		
		gtag('config', 'UA-144983683-1');
	</script> 
<style type="text/css">

</style>
	</head>
	<script>
		var site_url = '<?=site_url()?>';
		var base_url = '<?=base_url()?>';
	</script>
	<script src="<?= base_url('assets/dist/js/jquery.js') ?>"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha512-M5KW3ztuIICmVIhjSqXe01oV2bpe248gOxqmlcYrEzAvws7Pw3z6BK0iGbrwvdrUQUhi3eXgtxp5I8PDo9YfjQ==" crossorigin="anonymous"></script>
	<script src="<?= base_url('assets/dist/js/jquery.number.min.js') ?>"></script>
	<script src="<?= base_url('assets/dist/js/jquery.blockUI.js') ?>"></script>
	<script src="<?= base_url('assets/dist/js/sweetalert.min.js') ?>"></script>

	<style>
		a {
			color: #9c9fa9;
			text-decoration: underline;
		}
		 .dropdown-submenu{position:relative;}
		.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
		.dropdown-submenu:hover>.dropdown-menu{display:block;}
		.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
		.dropdown-submenu:hover>a:after{border-left-color:#ffffff;}
		.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
		.list p { 
			margin: 4px;
		}
		.list i {
			padding-right: 10px;
		}
		.card {
			min-height:auto;
		}
		.radius {
			border-radius:21px;
		}
		.breadcrumb { 
			background-color: #ffffff; 
		}
		.nav-item .fa {
			color:#DDD;
		}

		.nav-item,.dropdown-item>a {
			font-variant: all-petite-caps;  
			font-size: 17px;
		}

		.navbar .nav-item .dropdown-menu { 
			border-radius:0px;
			border-left: 4px solid #ffaf00;
		}
		h5.card-title.card-title-cs { 
			font-size: 16px;
		}
		ol {
			list-style: decimal;
			margin-left: 25px;
		}
		.cf {
			max-width: 92%!important;
			transition: 0.7s ease;
		}
		
		.cf:hover {
			max-width: 100%!important;
			border: 6px dashed #d9152b;
			transition: 0.7s ease;
			border-radius: 80px!important;
		}
		.btn-warning {
			color: #ffffff;
			background-color: #ffc107;
			border-color: #ffc107;
		}
	</style>
</head>

<body style="display:none;">
	<!-- Page Preloder -->
	<!-- <div id="preloder">
		<div class="loader"></div>
	</div> -->
	<!-- Header section -->
 
<header>
	<!-- <div class="header-top">
		<div class="container">
			<marquee style="font-size: 15px;color: white;">
				<?php //if($_profil){ echo $_profil['marque']; } ?>
			</marquee>
		</div>
	</div> -->
	<nav class="navbar navbar-expand-lg navbar-white bg-white">
    	<div class="container-xl">
    		<a class="navbar-brand site-logo" href="<?php echo base_url('') ?>"> 
              	<img style="padding: 6px;height: 68px;" src="<?php echo base_url('upload/logo/').$_profil['logo']  ?>" alt=""> 
    		</a>
    		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
    				<i class="fa fa-bars"></i>
    		</span>
    		</button> 
    		<div class="collapse navbar-collapse" id="navbarsExample07XL">
        		<ul class="navbar-nav navbar-right ml-auto">
        			<li class="nav-item active">
        				<a class="nav-link" href="<?php echo base_url('/') ?>">Beranda </a>
        			</li> 
					<li class="nav-item dropdown fade-down">
							<a class="nav-link dropdown-toggle" href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Profile
							</a>
							<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/selamatdatang">Selamat Datang</a></li>
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/sejarah">Sejarah</a></li>
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/visimisi">Visi misi</a></li>
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/identitasperusahaan">Identitas Perusahaan</a></li>
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/strukturorganisasi">Struktur Organisasi</a></li>
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/budayaperusahaan">Budaya Perusahaan</a></li>
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/kodeetikperusahaan">Kode Etik Perusahaan</a></li>
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/penghargaan">Awards</a></li>
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/mengapamemilihkami">Mengapa Memilih kami</a></li>
								<li class="dropdown-item"><a href="<?php echo base_url(); ?>public/home/whistleblowingsystem">Whistle Blowing System</a></li>
 
								<!-- <li class="dropdown-divider"></li>
								<li class="dropdown-submenu">
								<a  class="dropdown-item" tabindex="-1" href="#">Hover me for more options</a>
								<ul class="dropdown-menu">
									<li class="dropdown-item"><a tabindex="-1" href="#">Second level</a></li> 
									<li class="dropdown-item"><a href="#">Second level</a></li>
									<li class="dropdown-item"><a href="#">Second level</a></li>
								</ul>
								</li> -->
							</ul>
						</li>
						<!-- <li class="nav-item">
							<a class="nav-link" href="<?php echo base_url(); ?>public/home/management" > Management </a> 
						</li> -->
						
						<li class="nav-item dropdown ">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown07XL" data-toggle="dropdown" aria-expanded="true">Management </a>
							<div class="dropdown-menu " aria-labelledby="dropdown07XL">
								<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/dewankomisaris">Dewan Komisaris</a> 
								<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/direksi">Direksi</a> 
								<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/komiteperusahaan">Komite-komite Perusahaan</a>
							</div>
						</li>
						<!-- <li class="nav-item dropdown fade-down" >
							<a class="nav-link dropdown-toggle" href="#" id="dropdown07XL" data-toggle="dropdown" aria-expanded="true" >About  </a>
							<div class="dropdown-menu " aria-labelledby="dropdown07XL">
								<a class="dropdown-item" href="<?php #echo base_url('/profil') ?>"><i class="fa fa-circle-o"></i> Tentang Kami</a>
								<a class="dropdown-item" href="<?php #echo base_url('/visi_misi') ?>"><i class="fa fa-circle-o"></i> Visi Misi</a>
								<a class="dropdown-item" href="<?php #echo base_url('/struktur_organisasi') ?>"><i class="fa fa-circle-o"></i> Struktur Organisasi</a>
								<a class="dropdown-item" href="<?php #echo base_url('/budaya_perusahaan') ?>"><i class="fa fa-circle-o"></i> Budaya Perusahaan</a>
								<a class="dropdown-item" href="<?php #echo base_url('/awards') ?>"><i class="fa fa-circle-o"></i> Awards</a>
								<a class="dropdown-item" href="<?php #echo base_url('/kenapa_memilih_kami') ?>"><i class="fa fa-circle-o"></i> Kenapa memilih kami</a> 
							</div>
						</li> -->
        			<li class="nav-item dropdown ">
        				<a class="nav-link dropdown-toggle" href="#" id="dropdown07XL" data-toggle="dropdown" aria-expanded="true">Produk Layanan </a>
        				<div class="dropdown-menu " aria-labelledby="dropdown07XL">
        					<a class="dropdown-item" href="<?php echo base_url('/kredit') ?>">Kredit</a>
        					<a class="dropdown-item" href="<?php echo base_url('/deposito') ?>">Deposito</a>
        					<a class="dropdown-item" href="<?php echo base_url('/tabungan') ?>">Tabungan</a>
							<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/jaringanatm">Jaringan ATM</a>
							<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/smsbanking">SMS Banking</a>
							<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/edc">EDC</a>
							<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/mobilebanking">Mobile Banking</a>
							<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/syaratketentuan">Syarat & Ketentuan</a>
        				</div>
        			</li> 
        			<li class="nav-item dropdown ">
        				<a class="nav-link dropdown-toggle" href="#" id="dropdown07XL" data-toggle="dropdown" aria-expanded="true">Jaringan Kantor </a>
        				<div class="dropdown-menu " aria-labelledby="dropdown07XL">
						 	<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/kantorpusat">Kantor Pusat</a> 
							<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/kantorcabang">Kantor Cabang</a> 
							<a class="dropdown-item" href="<?php echo base_url(); ?>public/home/lokasiatm">Lokasi ATM</a>
        				</div>
        			</li>
        			<li class="nav-item dropdown ">
        				<a class="nav-link dropdown-toggle" href="#" id="dropdown07XL" data-toggle="dropdown" aria-expanded="true">Berita  </a>
        				<div class="dropdown-menu " aria-labelledby="dropdown07XL">
        					<a class="dropdown-item" href="<?php echo base_url('/agenda') ?>">Agenda</a>
        					<a class="dropdown-item" href="<?php echo base_url('/press_realese') ?>"> Berita</a>
        					<a class="dropdown-item" href="<?php echo base_url('/cerita_pelanggan') ?>">Testimoni</a>
        					<a class="dropdown-item" href="<?php echo base_url('/laporan') ?>">Laporan</a>
        				 
        					<a class="dropdown-item" href="<?php echo base_url('/karir') ?>">Karir</a>
        				</div>
        			</li> 
        			<li class="nav-item dropdown ">
        				<a class="nav-link dropdown-toggle" href="#" id="dropdown07XL" data-toggle="dropdown" aria-expanded="true">Media</a>
        				<div class="dropdown-menu " aria-labelledby="dropdown07XL">
        					<a class="dropdown-item" href="<?php echo base_url('/galeri') ?>">Foto</a>
        					<a class="dropdown-item" href="<?php echo base_url('/download') ?>"> Download</a> 
        				</div>
        			</li>
        			  
        			<li class="nav-item">
        				<a class="nav-link" href="<?php echo base_url('/faq') ?>">FAQ</a>
        			</li> 
        			
        			<!-- <li class="nav-item">
        				<a class="nav-link" href="<?php echo base_url('/artikel') ?>">Artikel</a>
        			</li>   -->
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('/hubungi_kami') ?>">Pengaduan</a>
					</li>  
        		</ul>
    		</div>
    	</div>
	</nav>
</header>


<!--- MAIN --->
	<section class="body-content">