<style>
	.spada_ {
		padding-top: 30px;
		padding-bottom: 30px;
	}

	._box{
		border-top-left-radius: 10px;
		border-bottom-right-radius: 10px;
		border-left: 3px #ddd solid;
		border-right: 3px #ddd solid;
		border-bottom: 1px #ddd solid;
		border-top: 1px #ddd solid;
		/* box-shadow: 0px 3px #eeee; */
	}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
</style>
<section class="add-section spada_">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
					<img src="<?php echo base_url('upload/photo/').get_baner_by_kode('1_visi_misi')  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
				</div>
			</div>
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<ol class="breadcrumba _box">
							<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Visi Misi</li>
						</ol>
						<div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
							<a href="<?php echo base_url('public/home/profil') ?>">
								<button type="button" class="btn btn-primary g">
									Tentang Kami
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/struktur') ?>">
								<button type="button" class="btn btn-primary g">
									Struktur Organisasi
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/budaya_perusahaan') ?>">
								<button type="button" class="btn btn-primary g">
									Budaya Perusahaan
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/awards') ?>">
								<button type="button" class="btn btn-primary g">
									Awards
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/kenapa_memilih_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Kenapa Memilih Kami
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/faq') ?>">
								<button type="button" class="btn btn-primary g">
									FAQ
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/hubungi_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Hubungi Kami
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="add-section spad pt-0">
		<div class="container">
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<h2 class="green" style="text-align:center; font-size:25px"><u>Visi</u></h2>
						<br>
							<b><i><?php if($_profil){ echo $_profil['visi'];} ?></b></i>
					</div>
					<br>
					<div class="col-lg-12">
						<h2 class="green" style="text-align:center; font-size:25px"><u>Misi</u></h2>
						<br>
						<?php if($_profil){ echo $_profil['misi'];} ?>		
					</div>
				</div>
			</div>
		</div>
	</section>
	
<!-- Footer section end -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d2620e87a48df6da243eaa0/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
	
</html>																																																																																																																																																																																																																																																																																																																																																																																																																																	