<style>
	.spada_ {
		padding-top: 30px;
		padding-bottom: 35px;
	}
	.right-bar {
		border: 2px #ddd dashed;
		border-radius: 5px;
		padding: 11px 15px;
		margin: 32px 0px;
	}
	.right-bar-title span{
		color: #366a34;
		font-size: 31px;
	}
	.right-bar-content {
		margin: 10px 0px;
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
	}
	a.kategori_btn {
		padding: 10px;
		background: #d95c2a;
		color: #fff;
		text-transform: uppercase;
		border-bottom-right-radius: 10px;
		border-top-left-radius: 10px;
		font-size: 14px;
		/* margin-bottom: 10px; */
		margin: 3px;
	}
	.recent {
		display: flex;
		flex-direction: row;
		margin: 11px 0px;
	}
	img.recent-img {
		height: 100px;
		object-fit: cover;
		width: 100px;
		border-radius: 11px;
		border: 2px #ddd solid;
	}
	.recent-cover-img {
		margin-right: 11px;
	}
	.recent-content {
		display: flex;
		flex-direction: column;
		justify-content: center;
	}
	a.recent-content-title {
		color: #366a34 !important;
	}
	.med-br {
		position: relative !important;
		margin: 8px 5px !important;
		/* border-radius: 3px !important; */
		box-shadow: 0px 5px 3px #ddd !important;
		border-top: 0.5px #eee solid;
		border-radius: 10px !important;
		min-height: 115px !important;
		display: flex;
		flex-direction: row;
		justify-content: center;
		align-items: center;
	}
	.med-br-img {
		margin: 10px 17px;
	}
	.med-br-img img{
		max-width: 70px;
	}
	.med-br-body {
		margin-right: 10px;
	}
	span.med-br-title {
		color: #366a34;
	}
	._box{
		border-top-left-radius: 10px;
		border-bottom-right-radius: 10px;
		border-left: 3px #ddd solid;
		border-right: 3px #ddd solid;
		border-bottom: 1px #ddd solid;
		border-top: 1px #ddd solid;
		/* box-shadow: 0px 3px #eeee; */
	}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
	
</style>

	<section class="add-section spada_">
		<div class="container">
			<div class="col-md-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php echo base_url('upload/photo/').get_baner_by_kode('1_kml')  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
			</div>
			<div class="add-warp">
				<div class="row add-text-warp">
			
					<div class="col-lg-12">
						<ol class="breadcrumba _box">
							<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Kenapa Memilih Kami</li>
						</ol>
						<div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
							<a href="<?php echo base_url('public/home/profil') ?>">
								<button type="button" class="btn btn-primary g">
									Tentang Kami
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/visi_misi') ?>">
								<button type="button" class="btn btn-primary g">
									Visi Misi
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/struktur') ?>">
								<button type="button" class="btn btn-primary g">
									Struktur Organisasi
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/budaya_perusahaan') ?>">
								<button type="button" class="btn btn-primary g">
									Budaya Perusahaan
								</button>
							</a>
						
							<a href="<?php echo base_url('public/home/awards') ?>">
								<button type="button" class="btn btn-primary g">
									Awards
								</button>
							</a>
								
							<a href="<?php echo base_url('public/home/faq') ?>">
								<button type="button" class="btn btn-primary g">
									FAQ
								</button>
							</a>
					
							<a href="<?php echo base_url('public/home/hubungi_kami') ?>">
								<button type="button" class="btn btn-primary g">
									Hubungi Kami
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="add-section spad pt-0">
		<div class="container">
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-4">
						<div class="yt">
							Kenapa Memilih Kami
						</div>
						<div class="ytb">
						</div>
					</div>
					<!-- <div class="col-lg-12">
						<br>
						<p style="text-align:left; font-size:12px"  class="gray">
						Nullam venenatis cursus efficitur. Aliquam in velit nisi. Nullam ut justo non erat faucibus aliquet. Nam hendrerit arcu a orci dignissim, ut ultrices augue elementum. Quisque ipsum dui, ultrices id semper quis, accumsan a enim. Mauris quis lectus vel tortor convallis malesuada. Quisque laoreet, elit a porttitor congue, metus nisi tristique velit, quis commodo est erat porttitor sapien. Nunc condimentum mattis dolor non ultrices. Aliquam non nisl nec arcu pellentesque dictum. Aenean auctor laoreet pellentesque. Pellentesque vulputate tortor id nisi commodo euismod. Cras vitae risus nec ipsum efficitur finibus. Vestibulum pulvinar at nisl vel rutrum. Donec facilisis, sapien nec congue venenatis, nisi metus viverra est, sit amet vestibulum massa nisi nec sem.</i>	
					</p> -->
				</div>
				<br>
				<!-- <div class="col-lg-12">
					
				
					<div class="right-bar-content">
						<?php if(count($_kml) > 0) { ?>
							<?php foreach($_kml as $_data) { ?>
							<div class="col-md-12">
								<div class="recent">
									<div class="recent-cover-img">
										<img class="recent-img" src="<?php echo base_url('upload/photo/').$_data['foto']  ?>" alt="Icon">
									</div>
									<div class="recent-content">
										<span class="green"><?= $_data['judul'] ?></span>
										<span class="recent-content-spoiler"><?= spoiler($_data['keterangan'], 68) ?></span>
									</div>
								</div>
							</div>
							<?php } ?>
						<?php }else{ ?>
							<div class="col-md-12">
								<div class="text-center">
									Belum Ada Konten	
								</div>
							</div>
						<?php } ?>
					</div>
				</div> -->
				<div class="col-lg-12">
					<div class="row">
						<?php if(count($_kml) > 0) { ?>
							<?php foreach($_kml as $data) { ?>
								<div class="col-lg-4" style="margin-bottom : 10px">
									<div class="med-br">
										<div class="med-br-img">
											<img src="<?php echo base_url('upload/photo/').$data['foto'] ?>" alt="Icon">
										</div>
										<div class="med-br-body">
											<span class="med-br-title"><?= $data['judul'] ?></span>
											<div class="med-br-content">
												<?= $data['keterangan'] ?>
											</div>
										</div>
									</div>	
								</div>
							<?php } ?>
						<?php }else{ ?>
							<div class="col-md-12">
								<div class="text-center">
									Belum Ada Konten	
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	

																																					