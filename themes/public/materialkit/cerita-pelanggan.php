	<style>
		.breadcrumb-item a{
			color: #f6861f !important;
		}
	</style>
	<section class="add-section spad">
		<div class="container">
			<div class="add-warp">
				<div class="row add-text-warp">
					<!-- <div class="col-lg-12">
						<img class="lazy" src="<?php // echo base_url(); ?>upload/noimg.jpg" data-src="<?php echo base_url('upload/photo/').get_baner_by_kode('1_cerpel')  ?>" class="rounded " style="width:100%; height: auto" alt="Responsive image">
					</div> -->
					<div class="col-md-12" style="margin: 20px 0px">
					<div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
							
							<a href="<?= base_url('public/home/agenda') ?>">
								<button type="button" class="btn btn-primary g">
									Agenda
								</button>
							</a>
						
							<a href="<?= base_url('public/home/press_realese') ?>">
								<button type="button" class="btn btn-primary g">
									Press Realese
								</button>
							</a>
						
							<a href="<?= base_url('public/home/laporan') ?>">
								<button type="button" class="btn btn-primary g">
									Laporan
								</button>
							</a>
						
							<!-- <a href="<?= base_url('public/home/blog') ?>">
								<button type="button" class="btn btn-primary g">
									Blog
								</button>
							</a> -->
					
							<a href="<?= base_url('public/home/karir') ?>">
								<button type="button" class="btn btn-primary g">
									Karir
								</button>
							</a>
							
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
						<div class="row" style="margin-top : 20px">
							<div class="col-md-12">
								<ol class="breadcrumba _box" style="margin-bottom: 0px !important;">
									<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home	') ?>">Beranda</a></li>
									<li class="breadcrumb-item active" aria-current="page">Cerita Pelanggan</li>
								</ol>
							</div>
						</div>

					</div>		
					
			
					<div class="col-lg-12" style="margin-top : 20px">
						<h2 class="green" style="text-align:left; font-size:25px">Cerita Pelanggan</h2>
						<br>
						<div class="row">
							<?php if($results['results'] ) { ?> 
								<?php 	foreach($results['results'] as $_data)	{ ?>
								<!--team-1-->
									<div class="col-lg-4">
										<div class="our-team-main">
											
											<div class="team-front">
												<img class="lazy" src="<?php echo base_url(); ?>upload/noimg.jpg" data-src="<?php echo base_url('upload/photo/').$_data['foto']  ?>" class="img-fluid" />
												<h3><?php echo $_data['nama'] ?></h3>
												<p><?php echo $_data['jabatan'] ?></p>
												<span>
												<?php star_render($_data['bintang']) ?>
												</span>
											</div>
											
											<div class="team-back">
												<span>
												<?php echo $_data['keterangan'] ?>
												</span>
											</div>
										</div>
									</div>

								<?php } ?>
								<?php 
									
									$next = $page + 1;
									$next_status = $next > $results['pagination']['more'] ? 'disabled' : '';	
									$prev =  $page - 1;
									$prev_status = $prev < 1 ? 'disabled' : '';							
									
								?>
									<!-- <div class = "col-md-12">
										<div class="text-center">
											<nav aria-label="Page navigation example">
												<ul class="pagination">
													<li class="page-item <?php echo $prev_status ?>"><a class="page-link" href="<?php echo base_url().'public/home/artikel?page='.$prev; ?>">Previous</a></li>
													<li class="page-item <?php echo $next_status ?>"><a class="page-link" href="<?php echo base_url().'public/home/artikel?page='.$next; ?>">Next</a></li>
												</ul>
											</nav> 
										</div>
									</div> -->
							<?php }else{ ?>
								<div class="col-sm-12">
										<div class="text-center">
											Belum Ada Artikel
										</div>
									</div>
							<?php } ?>
						</div>
						<div class="row">
							<div class = "col-md-12">
								<?php if($results['results']) { ?>
								<div class="container-btn">
									<a class="btn-galeri <?php echo isset($prev_status) ? $prev_status : '' ?>" href="<?php echo base_url().'public/home/cerita_pelanggan?page='.$prev; ?>">
										<i class="fa fa-arrow-left"></i>
									</a>
									<a class="btn-galeri <?php echo isset($next_status) ? $next_status : '' ?>" href="<?php echo base_url().'public/home/cerita_pelanggan?page='.$next; ?>">
										<i class="fa fa-arrow-right"></i>
									</a>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
