<style>
	.form-nominal{
		width: 100%;
		border: 1px solid #ccc;
		border-radius: 5px;
		padding: 10px 10px 10px 35px;
	}
	#span-rp{
		position: absolute;
		top: 43px;
		left: 14px;
	}
	.sp-blog-item {
		box-shadow: none !important;
		border: 1px #ddd solid;
	}
	.btn-calc{
		background: #366a34;
		color: white;
		font-weight: bolder;
		border: none;
	}
	._label_pilih{
		background: #ee832e;
		color: white !important;
		padding: 16px;
		text-align: center;
		margin: 52px 0px !important;
		font-size: 21px !important;
	}
	.bg-foot{
		background: #e6e6e6;
	}
	.bg-cepiring{
		background: #366a34;
	}

	.simulasi_kredit{
		font-size: 25px;
		padding: 30px 0px;
		color: #366a34;
	}
/* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  
	.flexi{
		display: flex;
	}
  
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  
	.flexi{
		display: flex;
	}
  
}


/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  
   .box-grid{
      flex-direction: column;
      text-align: center;
   }
   .flexi{
	display: block;
	}

}
	.box-grid{
		border: 1px #ddd solid;
		display: flex;
		border-radius: 10px;
		margin-bottom: 20px;
	}
	.box-img{
		padding: 23px;
	}
	.box-img img{
		width: 100px;
	}
	.box-content{
		padding: 12px 5px 20px 5px;
	}
	.box-content .title{
		color: #366a34;
		font-weight: 500;
		font-size: 19px;
	}
	.box-content .content{
		font-size: 13px;
	}
	.box-content .action{

	}
	.box-content .action a{
		font-size: 13px;
		background: #ee832e;
		color: #fff;
		padding: 5px;
		text-transform: uppercase;
		border-radius: 4px;
	}
	.box-content .action a:hover{
		cursor: pointer;
		color: #fff;
	}
	.spad {
		padding-top: 30px;
		padding-bottom: 40px !important;
	}

	.label-file {
		cursor: pointer;
		font-size: 14px;
		background: white;
		border: 2px #ddd solid;
		padding: 8px 15px;
		margin-bottom: 0px;
		border-radius: 4px;
		width: 50%;
		/* background: #28a745; */
		color: black;
		/* box-shadow: 1px 2px 3px 0px rgb(94, 94, 94); */
	}
	.file-custom {
		opacity: 0;
		position: relative;
		z-index: 0;
		cursor: pointer;
		top: -34px;
		width: 100%;
	}
	._box{
		border: 2px #ddd solid;
		padding: 28px 10px;
		margin: 17px 0px;
		border-radius: 8px;
		position: relative;
	}
	._box_title{
		position: absolute;
		top: -30px;
		left: 18px;
		text-align: center;
		font-size: 25px;
		background: white;
		/* height: 36px; */
		padding: 11px;
	}
	#kode_negara{
		position: absolute;
		top: 39px;
		left: 28px;
	}
	._aox{
			border-top-left-radius: 10px;
			border-bottom-right-radius: 10px;
			border-left: 3px #ddd solid;
			border-right: 3px #ddd solid;
			border-bottom: 1px #ddd solid;
			border-top: 1px #ddd solid;
			/* box-shadow: 0px 3px #eeee; */
		}
	.breadcrumb-item a{
		color: #f6861f !important;
	}
	.container-btn {
		margin-top: 31px;
		display: flex;
		flex-direction: row;
		justify-content: space-around;
	}
	a.recent-content-title {
		color: #21438b !important;
	}
	a.btn-artikel {
		background: white;
		color: #f6861f;
		padding: 3px 9px;
		border-radius: 70px;
		border: 2px solid #f6861f;
	}
	a.btn-artikel.disabled {
		background: white;
		color: #ddd;
		padding: 3px 9px;
		border-radius: 70px;
		border: 2px solid #ddd;
	}
	a.btn-artikel.disabled:hover {
		background: white;
		color: #ddd;
		cursor: not-allowed;
	}
	a.btn-artikel:hover {
		background: #f6861f;
		color: #fff;
	}
	.flex_center{
		display: flex;
		flex-direction: row;
		justify-content: center;
	}

	.modal { 
    z-index: 9999!important;
	
	}
	.modal-content { 
		z-index: 9999;
	}
	.modal-backdrop { 
	 display:none;
	}
	.card {
		z-index: 1!important; 
	}
	.modal-header,.modal-footer  {
		border:0px!important;
		border-radius:0px!important;
	}

	.card-title {
		font-size: 21px;
		margin-bottom: .75rem; 
	}

	.card-bodyd p { 
		line-height: 17px;
		font-size: 13px;
		padding: 11px;
	}

	.card-bodyd { 
    	padding: 23px;
	}

	@media (min-width: 992px){
		.col-lg-2 {
			-ms-flex: 0 0 16.666667%;
			flex: 4 16.666667%;
			max-width: 21.666667%;
		}
	}

	 
</style>
	<section class="add-section spad">
		<div class="container">
			<!-- <div class="col-lg-12"  style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php //echo base_url('upload/photo/').get_baner_by_kode('1_karir')  ?>" style="width : 100%; height : auto;"  alt="Responsive image">
			</div> -->
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<ol class="breadcrumba _aox">
							<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home') ?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Karir</li>
						</ol>
						<div class=" topnav " id="myTopnav" >
							<a>
								<button type="button" class="btn btn-primary g hide_daftar">
								Daftar Menu
								</button>
							</a>
							
							<a href="<?= base_url('public/home/agenda') ?>">
								<button type="button" class="btn btn-primary g">
									Agenda
								</button>
							</a>
						
							<a href="<?= base_url('public/home/press_realese') ?>">
								<button type="button" class="btn btn-primary g">
									Press Realese
								</button>
							</a>
					
							<a href="<?= base_url('public/home/cerita_pelanggan') ?>">
								<button type="button" class="btn btn-primary g">
									Cerita Pelanggan
								</button>
							</a>
						
							<a href="<?= base_url('public/home/laporan') ?>">
								<button type="button" class="btn btn-primary g">
									Laporan
								</button>
							</a>
						
							<!-- <a href="<?= base_url('public/home/blog') ?>">
								<button type="button" class="btn btn-primary g">
									Blog
								</button>
							</a> -->
					
							<div class="text-center">
							<a href="javascript:void(0);" style="font-size:20px; color: white; text-align: right; padding-right: 5px;" class="icon" onclick="myFunction()"><i class="fa fa-bars"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<form action="<?php echo site_url('public/home/karir?search') ?>" class="flexi" method="POST">
							<div class="col-lg-8">
								<br>
								<div class="text-center">
									<input type="text" class="form-control" name="job" placeholder="Cari Pekerjaan">
								</div>
							</div>
							
							<div class="col-lg-4">
								<br>
								<div class="text-center">
									<button type="submit" class="btn btn-primary elips-btn"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
								</div>
							</div>
						</form>
					</div>
					<div class="col-lg-12">
						<br><br>
						
						<div class="row">
							<?php if($_kategori_career){ foreach($_kategori_career as $_data){?>

								<div class="col-lg-2 col-md-2 col-6 col-sm-6 col-xs-6" style="margin-bottom : 10px	">
									<div class="card">
										<div class="card-body" style="background: #21438b;" >
											<div class="text-center">
												<a style="font-size:50px; color:white;"><i class="<?php echo $_data['icon']; ?>" aria-hidden="true"></i></a><br>
												<a style="font-size:15px; color:white;"><?php echo $_data['kategori_career']; ?></a>
												<?php $a = get_total_loker($_data['id']) ?>
												<p style="font-size:15px; color:white;"><?php echo $a[0]['count']; ?></p>
											</div>
										</div>
									</div>
								</div>
							<?php } } ?>
		
						</div>
					</div>
					
					<div class="col-lg-3">
						<br>
						<div class="yt">
							Pekerjaan
						</div>
						<div class="ytb">
						</div>
					</div>
					<div class="col-lg-3">
					</div>
					<div class="col-lg-6">
						<br>
						<nav aria-label="Page navigation example ">
							<ul class="pagination justify-content-end">
								
								<?php 
									if($_tipe_career){
										foreach($_tipe_career as $_data){

								?>
								<li class="page-item"><a class="page-link fgh" href="<?= base_url('public/home/karir?tipe=').$_data['id']; ?>"><?php  echo $_data['tipe_career']; ?></a></li>
								<?php 
									}
								}
								?>
							</ul>
						</nav>
					</div>
					<br><br><br><br><br>

					
						<?php if($results['results']){ ?>
							<?php $no = 1; foreach($results['results'] as $_data){ ?>
								<div class="col-lg-4">
									<div class="card-deck">
										<div class="card mb-3" style="max-width: 18rem;">	
											<div class="card-bodyd text-center">
												<div class="container">
													<h3 class="card-title"><?php echo $_data['nama']?></h3>
													<a style="font-size:12px; color:#d95c2a;"><?php echo $_data['tipe_career']?></a><br>
													<p> <?php echo spoiler($_data['keterangan'], 69); ?></p>
													
													
													 <button type="button" class="btn btn-sm btn-primarysd" value='<?php $_data['id']?>' data-toggle="modal" data-target="#exampleModal<?php echo $no; ?>">
													Selengkapnya
													</button>  

												</div>
											</div>
										</div>
									</div>


									<div class="modal fade" id="exampleModal<?php echo $no; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="height: 100%; padding-top: 74px;  background: #1e35de8a!important; ">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel"><?php echo $_data['nama']?></h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
											<?php echo $_data['keterangan']; ?>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-remove"></i></button> 
											</div>
											</div>
										</div>
									</div>
																 
									
								</div>
							<?php $no++; } ?> 
						
							<?php 
						
								$next = $page + 1;
								$next_status = $next > $results['pagination']['more'] ? 'disabled' : '';	
								$prev =  $page - 1;
								$prev_status = $prev < 1 ? 'disabled' : '';							
								
							?>
								
							<!-- <div class = "col-md-12">
								<div class="text-center">
									<nav aria-label="Page navigation example">
										<ul class="pagination pull-right">
											<li class="page-item <?php echo $prev_status ?>"><a class="page-link" href="<?php echo base_url().'public/home/galeri?page='.$prev; ?>">Previous</a></li>
											<li class="page-item <?php echo $next_status ?>"><a class="page-link" href="<?php echo base_url().'public/home/galeri?page='.$next; ?>">Next</a></li>
										</ul>
									</nav> 
								</div>
							</div> -->
						<?php }else{ ?>
								<div class="col-sm-12">
									<div class="text-center">
										Belum Ada Lowongan Pekerjaan
									</div>
								</div>
						<?php } ?>
				
					
				</div>
				<div class="row">
						<div class = "col-md-12">
							<?php if($results['results']) { ?>
							<div class="container-btn">
								<a class="btn-artikel <?php echo isset($prev_status) ? $prev_status : '' ?>" href="<?php echo base_url().'public/home/artikel?page='.$prev; ?>">
									<i class="fa fa-arrow-left"></i>
								</a>
								<a class="btn-artikel <?php echo isset($next_status) ? $next_status : '' ?>" href="<?php echo base_url().'public/home/artikel?page='.$next; ?>">
									<i class="fa fa-arrow-right"></i>
								</a>
							</div>
							<?php } ?>
						</div>
					</div>
				<div class="col-lg-12">
						<br><br>
						<div class="_box">
							<form action="<?php echo site_url('public/home/lamaran') ?>" method="POST" enctype="multipart/form-data">
								<span class="_box_title green">Formulir Lamaran</span>
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Posisi yang dilamar</label>
										<select name="id_career" class="form-control select2" id="exampleFormControlSelect1" required>
											<option value="" disabled>Pilih Posisi</option>
											<?php foreach($results['results'] as $_data){ ?>
												<option value="<?php echo $_data['id'] ?>"><?php echo $_data['nama'] ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group" style="padding: 0px 4px;"> 
										<label class="green">Nama Lengkap Sesuai KTP</label>
										<input class="form-control" name="nama" required> 
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group" style="padding: 0px 4px;"> 
										<label class="green">No.Hp</label>
										<span id="kode_negara">+62</span>
										<input class="form-control no_hp" name="no_hp" style="padding-left: 48px;" required>  
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group" style="padding: 0px 4px;"> 
										<label class="green">Email</label>
										<input class="form-control" name="email" required> 
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Pesan</label>
										<textarea name="pesan" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Alamat</label>
										<textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah Pas Foto Terbaikmu</label>
										<div>
											<label class="label-file" style="width : 100%">Pilih File</label>
											<input type="file" name="foto" class="file-custom" accept="image/*" onchange="tampilkanPreview(this,'preview')" required />
											<div style="border: 2px #ddd dashed; margin-top: -10px; height: auto; padding: 10px;">
												<img id="preview" src="" alt="" style="width: 200px" />
											</div>
										</div>
									</div>
								</div>
								<!-- <div class="col-md-12">
									<div class="alert alert-info">
											<strong>Info</strong> Untuk dokumen semuanya menggunakan format PDF
									</div>
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah Pas Foto Terbaikmu</label>
										<div class="custom-file">
											<input type="file" name="foto" class="custom-file-input" onchange="tampilkanPdf(this)" required>
											<label class="custom-file-label" for="customFile">Pilih file</label>
										</div>
									</div> 
								</div> -->
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah CV atau portofolio</label>
										<div class="custom-file">
											<input type="file" name="cv" class="custom-file-input" onchange="tampilkanPdf(this)" required>
											<label class="custom-file-label" for="customFile">Pilih file</label>
										</div>
									</div> 
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah Ijazah</label>
										<div class="custom-file">
											<input type="file" name="ijazah" class="custom-file-input" onchange="tampilkanPdf(this)" required>
											<label class="custom-file-label" for="customFile">Pilih file</label>
										</div>
									</div> 
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah Transkrip Nilai</label>
										<div class="custom-file">
											<input type="file" name="transkrip" class="custom-file-input" onchange="tampilkanPdf(this)" >
											<label class="custom-file-label" for="customFile">Pilih file</label>
										</div>
									</div> 
								</div>
								<!-- <div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah KTP</label>
										<div>
											<label class="label-file" style="width : 100%">Pilih File</label>
											<input type="file" name="ktp" class="file-custom" accept="image/*" onchange="tampilkanPreview(this,'preview_ktp')" required />
											<div style="border: 2px #ddd dashed; margin-top: -10px; height: auto; padding: 10px;">
												<img id="preview_ktp" src="" alt="" style="width: 200px" />
											</div>
										</div>
									</div>
								</div> -->
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah KTP </label>
										<div class="custom-file">
											<input type="file" name="ktp" class="custom-file-input" onchange="tampilkanPdf(this)" required>
											<label class="custom-file-label" for="customFile">Pilih file</label>
										</div>
									</div> 
								</div>
								<!-- <div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah Kartu Keluarga</label>
										<div>
											<label class="label-file" style="width : 100%">Pilih File</label>
											<input type="file" name="kk" class="file-custom" accept="image/*" onchange="tampilkanPreview(this,'preview_kk')" required />
											<div style="border: 2px #ddd dashed; margin-top: -10px; height: auto; padding: 10px;">
												<img id="preview_kk" src="" alt="" style="width: 200px" />
											</div>
										</div>
									</div>
								</div> -->
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah Kartu Keluarga</label>
										<div class="custom-file">
											<input type="file" name="kk" class="custom-file-input" onchange="tampilkanPdf(this)" required>
											<label class="custom-file-label" for="customFile">Pilih file</label>
										</div>
									</div> 
								</div>
								<!-- <div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah SKCK</label>
										<div>
											<label class="label-file" style="width : 100%">Pilih File</label>
											<input type="file" name="skck" class="file-custom" accept="image/*" onchange="tampilkanPreview(this,'preview_skck')" required />
											<div style="border: 2px #ddd dashed; margin-top: -10px; height: auto; padding: 10px;">
												<img id="preview_skck" src="" alt="" style="width: 200px" />
											</div>
										</div>
									</div>
								</div> -->
								<div class="col-md-12">
									<div class="form-group">
										<label class="green" style="padding: 0px 4px;">Unggah SKCK</label>
										<div class="custom-file">
											<input type="file" name="skck" class="custom-file-input" onchange="tampilkanPdf(this)" required>
											<label class="custom-file-label" for="customFile">Pilih file</label>
										</div>
									</div> 
								</div>
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary g"><i class="fa fa-save"></i> Kirim</button>
								</div>
							</form>
						</div> 
						
					</div>
					
				
			</div>
		</div>
	</section>

	<script type="text/javascript">
		$(form).on('submit', () => {
			blockUI('Memuat ...');
			return true;
		})
    function tampilkanPreview(gambar, idpreview) {
        var gb = gambar.files;
        for (var i = 0; i < gb.length; i++) {
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview = document.getElementById(idpreview);
            var reader = new FileReader();

            if (gbPreview.type.match(imageType)) {
                preview.file = gbPreview;
                reader.onload = (function(element) {
                    return function(e) {
                        element.src = e.target.result;
                    };
                })(preview);
                reader.readAsDataURL(gbPreview);
            } else {
                alert("file yang anda upload tidak sesuai. Khusus mengunakan image.");
            }

        }
    }
    function tampilkanPdf(pdf) {
        var gb = pdf.files;
        for (var i = 0; i < gb.length; i++) {
            var gbPreview = gb[i];
            var pdfType = /pdf/;
            var reader = new FileReader();

            if (gbPreview.type.match(pdfType)) {
                preview.file = gbPreview;
                // reader.readAsDataURL(gbPreview);
            } else {
                alert("file yang anda upload tidak sesuai. Khusus mengunakan pdf.");
            }

        }
    }

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
$(document).ready(function(){
	$('.view_data').click(function(){
		// Get the id of selected phone and assign it in a variable called phoneData
		var karir = $(this).attr('id');
     	 
		blockUI('Memuat ...');

		$.get(base_url+'public/home/get_search_karir/'+karir).done((res) => {
			$('#detail_bunga').html('Kategori : '+res.kategori_career+', Tipe Pekerjaan : '+res.tipe_career)
			console.log(res)
			$('#nama_loker').html(res.nama)
			$('#detail_loker').html(res.keterangan)
			$.unblockUI();
			$('#exampleModal').modal('show');
		}).fail((xhr) => {
			console.log(xhr)
		})
             // End AJAX function
	});
});  
</script>

																																																																																																																																																																																																																																																																																																																																																																																																																				