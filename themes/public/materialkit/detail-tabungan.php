<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bank Nusamba BPR</title>
		<meta charset="UTF-8">
		<meta name="description" content="Logo">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->   
		<link href="<?php echo $theme_url;?>/img_frontend/favicon.ico" rel="shortcut icon"/>
		
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
		
		<!-- Stylesheets -->
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/bootstrap.min.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/owl.carousel.css"/>
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/animate.css"/>
		<link rel="stylesheet" href="<?php echo $theme_url;?>/css_frontend/style.css"/>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
		<link href="<?php echo $theme_url;?>/css_frontend/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

		<style type="text/css">
            .green {
                 background-color: white !important; 
            }
            .grey {
                 background-color: white !important; 
            }
        </style>
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144983683-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144983683-1');
</script>	
	</head>
	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>
		
		<!-- Header section -->
		<header class="header-section">
			<div class="header-top">
				<div class="container">
					<marquee color="white" style="font-size: 12px;"><font color="white" >HUBUNGI LAYANAN PELANGGAN TLP:021-84931416 EMAIL:INFO@BPRNUSAMBACEPIRING.COM</font></marquee>
				</div>
			</div>
			<div class="header-bottom h-b">
				<div class="container">
					<a href="index.html"  class="site-logo">
						<img style="padding-top: 10px; padding-bottom:10px;" src="<?php echo $theme_url;?>/img_frontend/logo.png" alt="">
					</a>
					<div class="nav-switch">
						<i class="fa fa-bars"></i>
					</div>
					<ul class="main-menu">
						<li class='submenu'><a href="#">Profil <i class="fa fa-caret-down"></i></a>
							<ul class='dropdown'>
								<li><a href="<?php echo base_url('public/home/profil') ?>"><i class="fa fa-chevron-right"></i> Tentang Kami</a></li>
								<li><a href="<?php echo base_url('public/home/visi_misi') ?>"><i class="fa fa-chevron-right"></i> Visi Misi</a></li>
								<li><a href="<?php echo base_url('public/home/struktur') ?>"><i class="fa fa-chevron-right"></i> Struktur Organisasi</a></li>
								<li><a href="<?php echo base_url('public/home/budaya_perusahaan') ?>"><i class="fa fa-chevron-right"></i> Budaya Perusahaan</a></li>
								<li><a href="<?php echo base_url('public/home/awards') ?>"><i class="fa fa-chevron-right"></i> Awards</a></li>
								<li><a href="<?php echo base_url('public/home/kenapa_memilih_kami') ?>"><i class="fa fa-chevron-right"></i> Kenapa memilih kami</a></li>
								<li><a href="<?php echo base_url('public/home/faq') ?>"><i class="fa fa-chevron-right"></i>FAQ</a></li>
								<li><a href="<?php echo base_url('public/home/hubungi_kami') ?>"><i class="fa fa-chevron-right"></i> Hubungi Kami</a></li>
							</ul>
						</li>
						<li class='submenu'><a href="#">Berita <i class="fa fa-caret-down"></i> </a>
							<ul class='dropdown'>
								<li><a href="<?php echo base_url('public/home/agenda') ?>"><i class="fa fa-chevron-right"></i> Agenda</a></li>
								<li><a href="<?php echo base_url('public/home/press_realese') ?>"><i class="fa fa-chevron-right"></i> Press Realese</a></li>
								<li><a href="<?php echo base_url('public/home/cerita_pelanggan') ?>"><i class="fa fa-chevron-right"></i> Cerita Pelanggan</a></li>
								<li><a href="<?php echo base_url('public/home/laporan') ?>"><i class="fa fa-chevron-right"></i> Laporan</a></li>
								<li><a href="<?php echo base_url('public/home/blog') ?>"><i class="fa fa-chevron-right"></i> Blog</a></li>
								<li><a href="<?php echo base_url('public/home/karir') ?>"><i class="fa fa-chevron-right"></i> Karir</a></li>
							</ul>
						</li>
						<li class='submenu'><a href="#">Media <i class="fa fa-caret-down"></i></a>
							<ul class='dropdown'>
								<li><a href="<?php echo base_url('public/home/galeri') ?>"><i class="fa fa-chevron-right"></i> Galeri</a></li>
								<li><a href="<?php echo base_url('public/home/download') ?>"><i class="fa fa-chevron-right"></i> Download &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
							</ul>
						</li>
						<li><a href="<?php echo base_url('public/home/kredit') ?>">Kredit</a></li>
                        <li><a href="<?php echo base_url('public/home/deposito') ?>">Deposito</a></li>
                        <li><a href="<?php echo base_url('public/home/tabungan') ?>">Tabungan</a></li>
						<li><a href="<?php echo base_url('public/home/hubungi_kami') ?>">Kontak</a></li>
						<li><a href="<?php echo base_url('public/home/artikel') ?>">Artikel</a></li>
						
						<a class="gray" href="#">IND</a> <a class="gray" href="#"> | ENG</a>
						
					</ul>
				</div>
			</div>
		</header>
		<!-- Header section end -->
		
		<!-- Add section end -->
		<section class="add-section spad">
			<div class="container">
				<div class="add-warp">
					
					<div class="row add-text-warp">
						<div class="col-lg-4">
						</div>
						<div class="col-lg-8">
							<h2 class="green" style="text-align:left; font-size:20px">Kredit Modal Kerja</h2>
						</div>
						<br><br><br>
						<div class="col-lg-4 text-center">
							<div class="rounded-circle">
								<br>
								<img src="<?php echo $theme_url;?>/img_frontend/tangan.png" class="rounded" width="30%" heigth="40%" alt="...">
							</div>
						</div>
						<div class="col-lg-8">
							<p class="gray">
								Bank BPR Membantu anda memberikan pelayanan untuk pembelian maupun take over KPR dengan plafon hingga 80% dari harga properti dengan pilihan jangka waktu tenor hingga 25 tahun. Dapatkan juga perlindungan asuransi jiwa, kebakaran, serta gempa bumi.
							</p>
						</div>
						<div class="col-lg-4">
							<div class="text-center">
								
							</div>
						</div>
						<div class="col-lg-4">
							<p class="gray"><i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Tenor sampai dengan <b>25 Tahun</b> 
							</p>
							<p class="gray"><i class="fa fa-check-circle-o" aria-hidden="true"></i>
								<b>70%</b> Margin pembayaran 
							</p>
						</div>
						
						<div class="col-lg-4">
							<p class="gray"><i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Fix and Floating Suku Bunga
							</p>
							<p class="gray"><i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Pencairan<b>7 Hari</b>  
							</p>
						</div>
						<div class="col-lg-4">
						</div>
						<div class="col-lg-8">
							<table class="table table-striped table-bordered">
								<thead class="gt">
									<tr>
										<th style="text-align:center" >Jumlah Pinjaman</th>
										<th style="text-align:center" >Suku Bunga</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="text-align:center">Rp 150 Juta - 3.50 Milyar</td>
										<td style="text-align:center">10.25% Per Tahun</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Add section end -->
		
		<section class="add-section spad pt-0">
			<div class="container">
				<div class="add-warp">
					<div class="row add-text-warp">
						<div class="col-lg-12">
							<br>
							<div class="accordion" id="accordionExample">
								<div class="cards">
									<div class="card-headers" id="headingOne">
										<h5>
											<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
												<p style="text-align:left; font-size:15px" class="green">	<i class="fa fa-angle-double-right" aria-hidden="true"></i>
													TABUNGAN HARMONI PLUS
												</p>
											</button>
										</h5>
									</div>
									<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
										<div class="card-bodyd">
											<p style="text-align:left; font-size:13px" class="grey">
												KEUNGGULAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Merupakan Produk Tabungan  BPR Nusamba Group</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Merupakan Tabungan yang berhadiah dengan sistem Undian
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dapat dijadikan jaminan kredit</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												HADIAH UNDIAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> 4 (empat) Unit Mobil Inova ( Untuk 4 Wilayah ) Jabar, Jateng & DIY, Jatim dan Bali</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> 20 (dua puluh) unit Sepeda Motor
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> 40 (empat puluh) Kulkas 2 Pintu</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> 60 (enam puluh) LED 32</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> 60 (enam puluh) Sepeda Gunung</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												KETENTUAN KHUSUS
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya administrasi Rp. 2.000 setiap bulan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung dari saldo terendah tiap akhir bulan
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya penutupan rekening sebesar Rp. 10.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran pertama Rp. 100.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran selanjutnya Rp. 50.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya administrasi Rp. 2.000 setiap bulan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya penutupan rekening sebesar Rp. 10.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung dari saldo terendah tiap akhir bulan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setiap kelipatan Rp. 100.000,- dari saldo setiap akhir bulan akan mendapatkan 1 (satu) nomor undian.</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Nomor Undian tidak diberikan kepada penabun	namun sudah tercatat dalam sistem Komputerisasi
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Yang berhak mengikuti undian adalah penabung yang saldo tabungannya pada akhir periode undian minimal Rp.100.000,-</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Undian dilaksanakan setiap bulan Februari
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Undian tidak berlaku bagi karyawan BPR Nusamba/ Mitra Harmoni dan keluarga.
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SUKU BUNGA TABUNGAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga tabungan Harmoni plus
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga tabungan Harmoni plus 
															
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th style="text-align:center" >Rupiah</th>
																		<th style="text-align:center" >Suku Bunga</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td style="text-align:center">0 s/d 100.000,-</td>
																		<td style="text-align:center">0 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">100.001 s/d 5.000.000,-</td>
																		<td style="text-align:center">2,50 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">5.000.001 s/d 10.000.000,-</td>
																		<td style="text-align:center">3,00 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">10.000.001 keatas
																		</td>
																		<td style="text-align:center">4,00 %
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SALDO
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Saldo awal belum dipotong Administrasi</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung dari saldo terendah tiap bulan
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="cards">
									<div class="card-headers" id="headingTwo">
										<h5>
											<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
												<p style="text-align:left; font-size:15px" class="green">	<i class="fa fa-angle-double-right" aria-hidden="true"></i>
													TABUNGAN HARMONI PREMIUM
												</p></button>
										</h5>
									</div>
									<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
										<div class="card-bodyd">
											<p style="text-align:left; font-size:13px" class="grey">
												KEUNGGULAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Menabung Serasa Deposito</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga maksimal sesuai ketentuan LPS
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dapat dijadikan jaminan Kredit</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												KETENTUAN KHUSUS
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran pertama minimal Rp. 2.500.000 selanjutnya minimal Rp. 500.000</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SUKU BUNGA TABUNGAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung dari saldo terendah tiap bulan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga Tabungan Harmoni Premium
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th style="text-align:center" >Rupiah</th>
																		<th style="text-align:center" >Suku Bunga</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td style="text-align:center"> 0 s/d       500.000,-</td>
																		<td style="text-align:center">0 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">500.001 s/d  25.000.000,-</td>
																		<td style="text-align:center">3,00 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center"> 25.000.001 s/d  50.000.000</td>
																		<td style="text-align:center">3,50 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">50.000.001 s/d 100.000.000
																		</td>
																		<td style="text-align:center">4,50 %
																		</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">100.000.001 keatas
																		</td>
																		<td style="text-align:center">LPS
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="cards">
									<div class="card-headers" id="headingThree">
										<h5>
											<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
												<p style="text-align:left; font-size:15px" class="green">	<i class="fa fa-angle-double-right" aria-hidden="true"></i>
													TABUNGANKU
												</p></button>
										</h5>
									</div>
									<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
										<div class="card-bodyd">
											<p style="text-align:left; font-size:13px" class="grey">
												KEUNGGULAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Tabungan Program Pemerintah</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Tanpa biaya administrasi bulanan
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dapat dijadikan jaminan Kredit</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												KETENTUAN KHUSUS
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran pertama minimal Rp. 10.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran selanjutnya tidak ada pembatasan
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Selama 6 bulan tidak ada transaksi, dikenakan
														Biaya pinalti sebesar Rp. 1.000  perbulan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Apabila saldo rekenig mencapai Rp. 10.000  maka 
														rekening akan ditutup oleh sistem</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Minimum penarikan di Counter Rp. 50.000, kecuali 
														pada saat nasabah ingin menutup rekening</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Saldo mengendap setelah penarikan minimum Rp.10.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung dari Saldo Harian setiap bulan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya penutupan rekening Rp. 5.000</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SUKU BUNGA TABUNGAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga TABUNGANKU adalah 4.00 % saldo harian</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="cards">
									<div class="card-headers" id="heading4">
										<h5 >
											<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="heading4">
												<p style="text-align:left; font-size:15px" class="green">	<i class="fa fa-angle-double-right" aria-hidden="true"></i>
													TABUNGAN SEHATI
												</p></button>
										</h5>
									</div>
									<div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionExample">
										<div class="card-bodyd">
											<p style="text-align:left; font-size:13px" class="grey">
												KEUNGGULAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga lebih besar dibandingkan dengan bunga tabungan umum</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bank memberikan ISTW (Insentif Setoran Tepat Waktu).
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bebas Biaya Administrasi Bulanan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dapat dijadikan jaminan Kredit</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												KETENTUAN KHUSUS
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Peyetoran wajib dilakukan setiap bulannya minimal Rp. 50.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung berdasarkan Saldo terendah setiap Bulannya
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya penutupan rekening Rp.25.000</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SUKU BUNGA TABUNGAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga tabungan SEHATI adalah 6.00 % pa</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung dari saldo terendah tiap bulan</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="cards">
									<div class="card-headers" id="heading6">
										<h5 >
											<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="heading6">
												<p style="text-align:left; font-size:15px" class="green">	<i class="fa fa-angle-double-right" aria-hidden="true"></i>
													TABUNGAN SIJARI
												</p></button>
										</h5>
									</div>
									<div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionExample">
										<div class="card-bodyd">
											<p style="text-align:left; font-size:13px" class="grey">
												KEUNGGULAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Tabungan Berjangka</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bebas biaya administrasi bulanan
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dapat dijadikan jaminan kredit</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Tabungan dapat diambil sewaktu-waktu diluar perjanjian</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												KETENTUAN KHUSUS
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran hanya dilakukan sekali dalam satu periode minimal Rp. 1.000.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Penarikan dan penutupan rekening sesuai dengan Jangka Waktu yang ditentukan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya penutupan rekening Rp.50.000
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SUKU BUNGA TABUNGAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga tabungan SIJARI adalah 6.00 % pa</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung dari saldo terendah tiap bulan</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												ISTW (Insentif Setoran Tepat Waktu)
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ISTW Sebesar 1,6 pa, dihitung dari saldo terendah tiap bulan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ISTW diberikan tiap tahun sepanjang tabungan masih aktif dan langsung di kredit ke rekening tabungan</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ISTW akan hangus jika tabungan ditutup rekening</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="cards">
									<div class="card-headers" id="heading7e">
										<h5>
											<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse7d" aria-expanded="false" aria-controls="heading7e">
												<p style="text-align:left; font-size:15px" class="green">	<i class="fa fa-angle-double-right" aria-hidden="true"></i>
													TABUNGAN EMAS
													
												</p></button>
										</h5>
									</div>
									<div id="collapse7d" class="collapse" aria-labelledby="heading7" data-parent="#accordionExample">
										<div class="card-bodyd">
											<p style="text-align:left; font-size:13px" class="grey">
												KEUNGGULAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Tabungan Berhadiah Langsung</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dapat dijadikan jaminan Kredit
														</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												KETENTUAN KHUSUS
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran pertama minimal Rp. 10.000,-</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran selanjutnya minimum Rp. 10.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Saldo tabungan yang harus disisakan setelah pengambilan sekurang-kurangnya Rp. 10.000
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung atas Saldo Terendah tiap bulan.
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setiap kelipatan Rp. 100.000 dari saldo terendah, tiap bulan akan memperoleh 1 poin Senilai Rp. 50			</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Poin tersebut digunakan untuk memperoleh Hadiah Langsung</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya Adiministrasi Rp. 1.500 setiap  bulan
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya Penutupan rekening sebesar Rp. 5.000
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Batas Waktu penukaran poin paling akhir tanggal 24 Desember, bila lewat batas waktu penukaran, poin hangus.-
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SUKU BUNGA TABUNGAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga tabungan emas
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th style="text-align:center" >Rupiah</th>
																		<th style="text-align:center" >Suku Bunga</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td style="text-align:center">0 s/d         25.000,-</td>
																		<td style="text-align:center">0 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">25.001 s/d    5.000.000,-</td>
																		<td style="text-align:center">2,50 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center"> 5.000.001 s/d 10.000.000,-</td>
																		<td style="text-align:center">3,00 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">10.000.001 keatas
																		</td>
																		<td style="text-align:center">4,00 %
																		</td>
																	</tr>
																	
																</tbody>
															</table>
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SALDO
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Saldo awal belum dipotong biaya administrasi</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung dari saldo terendah tiap bulan
														</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="cards">
									<div class="card-headers" id="heading">
										<h5>
											<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo6" aria-expanded="false" aria-controls="collapseTwo6">
												<p style="text-align:left; font-size:15px" class="green">	<i class="fa fa-angle-double-right" aria-hidden="true"></i>
													TABUNGAN SIMPEL
												</p></button>
										</h5>
									</div>
									<div id="collapseTwo6" class="collapse" aria-labelledby="headingTwo6" data-parent="#accordionExample">
										<div class="card-bodyd">
											<p style="text-align:left; font-size:13px" class="grey">
												KEUNGGULAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Tabungan Berhadiah Langsung</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dapat dijadikan jaminan Kredit
														</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												KETENTUAN KHUSUS
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran pertama minimal Rp. 10.000,-</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setoran selanjutnya minimum Rp. 10.000</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Saldo tabungan yang harus disisakan setelah pengambilan sekurang-kurangnya Rp. 10.000
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung atas Saldo Terendah tiap bulan.
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setiap kelipatan Rp. 100.000 dari saldo terendah, tiap bulan akan memperoleh 1 poin Senilai Rp. 50			</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Poin tersebut digunakan untuk memperoleh Hadiah Langsung</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya Adiministrasi Rp. 1.500 setiap  bulan
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Biaya Penutupan rekening sebesar Rp. 5.000
														</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Batas Waktu penukaran poin paling akhir tanggal 24 Desember, bila lewat batas waktu penukaran, poin hangus.-
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SUKU BUNGA TABUNGAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga Deposito Nusamba	
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th style="text-align:center" >Jangka Waktu</th>
																		<th style="text-align:center" >Suku Bunga</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td style="text-align:center">1 Bulan</td>
																		<td style="text-align:center">6.50 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">3 Bulan</td>
																		<td style="text-align:center">6.75 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">6 Bulan</td>
																		<td style="text-align:center">7.00 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">12 Bulan
																			
																		</td>
																		<td style="text-align:center">6.00 %
																		</td>
																	</tr>
																	
																</tbody>
															</table>
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											<p style="text-align:left; font-size:13px" class="grey">
												SALDO
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Saldo awal belum dipotong biaya administrasi</td>
														<td style="text-align:center"> </td>
													</tr>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Bunga dihitung dari saldo terendah tiap bulan
														</td>
														<td style="text-align:center"> </td>
													</tr>
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
								<div class="cards">
									<div class="card-headers" id="heading">
										<h5>
											<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo6d" aria-expanded="false" aria-controls="collapseTwo6d">
												<p style="text-align:left; font-size:15px" class="green">	<i class="fa fa-angle-double-right" aria-hidden="true"></i>
													DEPOSITO NUSAMBA
												</p></button>
										</h5>
									</div>
									<div id="collapseTwo6d" class="collapse" aria-labelledby="headingTwo6d" data-parent="#accordionExample">
										<div class="card-bodyd">
											
											<p style="text-align:left; font-size:13px" class="grey">
												SUKU BUNGA TABUNGAN
											</p>
											<table class="table">
												<tbody>
													<tr>
														<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga tabungan emas
															<table class="table table-striped table-bordered">
																<thead>
																	<tr>
																		<th style="text-align:center" >Rupiah</th>
																		<th style="text-align:center" >Suku Bunga</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td style="text-align:center">0 s/d         25.000,-</td>
																		<td style="text-align:center">0 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">25.001 s/d    5.000.000,-</td>
																		<td style="text-align:center">2,50 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center"> 5.000.001 s/d 10.000.000,-</td>
																		<td style="text-align:center">3,00 %</td>
																	</tr>
																	<tr>
																		<td style="text-align:center">10.000.001 keatas
																		</td>
																		<td style="text-align:center">4,00 %
																		</td>
																	</tr>
																	
																</tbody>
															</table>
														</td>
														<td style="text-align:center"> </td>
													</tr>
												</tbody>
											</table>
											
										</div>
									</div>
									
									<div class="cards">
										<div class="card-headers" id="heading">
											<h5>
												<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo6dd" aria-expanded="false" aria-controls="collapseTwo6dd">
													<p style="text-align:left; font-size:15px" class="green">	<i class="fa fa-angle-double-right" aria-hidden="true"></i>
														DEPOSITO SUPER PLUS
													</p></button>
											</h5>
										</div>
										<div id="collapseTwo6dd" class="collapse" aria-labelledby="headingTwo6d" data-parent="#accordionExample">
											<div class="card-bodyd">
												<p style="text-align:left; font-size:13px" class="grey">
													KEUNGGULAN
												</p>
												<table class="table">
													<tbody>
														<tr>
															<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Deposito berhadiah langsung tanpa diundi</td>
															<td style="text-align:center"> </td>
														</tr>
														<tr>
															<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Hadiah Sesuai keinginan Deposan
															</td>
															<td style="text-align:center"> </td>
														</tr>
														<tr>
															<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Dapat dijadikan jaminan Kredit
															</td>
															<td style="text-align:center"> </td>
														</tr>
														
													</tbody>
												</table>
												<p style="text-align:left; font-size:13px" class="grey">
													KETENTUAN KHUSUS
												</p>
												<table class="table">
													<tbody>
														<tr>
															<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Minimal Deposito Rp. 5.000.000</td>
															<td style="text-align:center"> </td>
														</tr>
														<tr>
															<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Setiap kelipatan Rp. 5.000.000 mendapatkan 1 poin perbulan senilai Rp. 20.000</td>
															<td style="text-align:center"> </td>
														</tr>
														<tr>
															<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Jangka Waktu Deposito minimal 3 (tiga) bulan, Maksimal 60 bulan
															</td>
															<td style="text-align:center"> </td>
														</tr>
														
													</tbody>
												</table>
												<p style="text-align:left; font-size:13px" class="grey">
													SUKU BUNGA TABUNGAN
												</p>
												<table class="table">
													<tbody>
														<tr>
															<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya suku bunga tabungan emas
																
															</td>
															<td style="text-align:center"> </td>
														</tr>
													</tbody>
												</table>
												<p style="text-align:left; font-size:13px" class="grey">
													SALDO
												</p>
												<table class="table">
													<tbody>
														<tr>
															<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Saldo awal belum dipotong biaya administrasi</td>
															<td style="text-align:center"> </td>
														</tr>
														<tr>
															<td style="text-align:left"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Besarnya Suku bunga Deposito Super Plus adalah 3.75 % pa
															</td>
															<td style="text-align:center"> </td>
														</tr>
														
													</tbody>
												</table>
											</div>
										</div>
									</div>
									
									
									
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
			</section>
			
			
			
			
			<!-- Gallery section end -->
			
			
			<!-- Footer section  -->
			<footer class="footer-section set-bg" >
				
				<div class="row">
					<div class="col-lg-3">
						<h3 style="color:white; font-size:18px">Kontak Kami</h3><br>
						<font style="color:white; font-size:11px">Jl. Raya Gondang No.30, Pencarikan, Gondang, Kec. Cepiring, Kabupaten Kendal, Jawa Tengah 51352</font><br>
						<font style="color:white; font-size:11px"><i class="fa fa-phone"></i>  Telepon :  (0294) 382234</font><br>
						<font style="color:white; font-size:11px"><i class="fa fa-fax"></i> Fax :  (0294) 382234</font><br>
						<font style="color:white; font-size:11px"><i class="fa fa-envelope"></i> Email :  (0294) 382234</font><br>
					</div>
					<div class="col-lg-3">
						<h3 style="color:white; font-size:18px">Tentang Kami</h3><br>
					<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Cara Pengajuan dan Pembayaran</a></font></a><br>
					<a href="404.html">	<font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Tanya Jawab</font></a><br>
					<a href="404.html">	<font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Ajukan Lagi</font></a><br>
					<a href="404.html">	<font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Referral</font></a><br>
					
				</div>
				<div class="col-lg-3">
					<h3 style="color:white; font-size:18px">Kebijakan</h3><br>
					<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Syarat dan Ketentuan</font></a><br>
					<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Kebijakan Privasi</font></a><br>
					<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Kebijakan Cookies</font></a><br>
					<a href="404.html"><font style="color:white; font-size:11px"><i class="fa fa-chevron-right"></i> Referral</font></a><br>
					
				</div>
				<div class="col-lg-3">
					<div class="col-lg-12">
						<h3 style="color:white; font-size:18px">Ikuti Kami</h3><br>
						<a style="color:white; font-size:11px" href="#"><i class="fa fa-facebook"></i></a>
						<a style="color:white; font-size:11px" href="#"><i class="fa fa-twitter"></i></a>
						<a style="color:white; font-size:11px" href="#"><i class="fa fa-instagram"></i></a>
						<a style="color:white; font-size:11px" href="#"><i class="fa fa-google-plus-square"></i></a>
					</div>
					<div class="col-lg-12">
						<div class="text-left">
							<div class="form-group">
								<input type="text" class="form-control"  placeholder="">
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="text-left">
							<button type="submit" class="btn btn-primary elips-btn">Subscribe</button>
						</div>
					</div>
					
				</div>
			</div>
			
		</footer>
		<!-- Footer section end -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d2620e87a48df6da243eaa0/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
		
		<script type="text/javascript">
			$(document).ready(function() {
				$('#example').DataTable();
			} );
			</script>
			<script>
				// Open the Modal
				function openModal() {
					document.getElementById("myModal").style.display = "block";
				}
				
				// Close the Modal
				function closeModal() {
					document.getElementById("myModal").style.display = "none";
				}
				
				var slideIndex = 1;
				showSlides(slideIndex);
				
				// Next/previous controls
				function plusSlides(n) {
					showSlides(slideIndex += n);
				}
				
				// Thumbnail image controls
				function currentSlide(n) {
					showSlides(slideIndex = n);
				}
				
				function showSlides(n) {
					var i;
					var slides = document.getElementsByClassName("mySlides");
					var dots = document.getElementsByClassName("demo");
					var captionText = document.getElementById("caption");
					if (n > slides.length) {slideIndex = 1}
					if (n < 1) {slideIndex = slides.length}
					for (i = 0; i < slides.length; i++) {
						slides[i].style.display = "none";
					}
					for (i = 0; i < dots.length; i++) {
						dots[i].className = dots[i].className.replace(" active", "");
					}
					slides[slideIndex-1].style.display = "block";
					dots[slideIndex-1].className += " active";
					captionText.innerHTML = dots[slideIndex-1].alt;
				}
				
				
			</script>
			
			<!--====== Javascripts & Jquery ======-->
			
			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
			<script src="<?php echo $theme_url;?>/js_frontend/bootstrap.min.js"></script>
			<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
			<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
			<script src="<?php echo $theme_url;?>/js_frontend/owl.carousel.min.js"></script>
			<script src="<?php echo $theme_url;?>/js_frontend/main.js"></script>
			
			
		</body>
	</html>																																																																																																																																																																																																																																																																						