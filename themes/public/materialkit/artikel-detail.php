 


<meta property="og:title" content="Bank Eka">
<meta property="og:description" content="Pembaca website yang budiman, Sudah hampir setengah abad Bank Eka memberikan pelayanan perbankan kepada masyarakat. Belum banyak BPR yang mampu bertahan melakukan aktivitasnya dalam kurun waktu yang cukup lama">
<meta property="og:image" content="<?php echo base_url(); ?>assets/image/bankeka.jpg">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="400">
<meta property="og:url" content="<?php echo base_url(); ?>assets/image/bankeka.jpg">
<meta property="og:site_name" content="Bank Eka">
<meta property="og:type" content="website">
<meta name="fb_admins_meta_tag" content="Bank Eka">
<meta property="fb:admins" content="Bank Eka">

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Bank Eka">
<meta name="twitter:description" content="Pembaca website yang budiman, Sudah hampir setengah abad Bank Eka memberikan pelayanan perbankan kepada masyarakat. Belum banyak BPR yang mampu bertahan melakukan aktivitasnya dalam kurun waktu yang cukup lama">
<meta name="twitter:image" content="<?php echo base_url(); ?>assets/image/bankeka.jpg">

	<section class="add-section spad">
		<div class="container">
            <!-- <div class="col-md-12" style="text-align: center; margin-bottom: 20px; padding : 0px">
				<img src="<?php //echo base_url('upload/photo/').get_baner_by_kode('1_artikel')  ?>" style="width : 100%; height : auto;" class="rounded" alt="Responsive image">
			</div> -->
			<div class="add-warp">
				<div class="row add-text-warp">
					<div class="col-lg-12">
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumba _box" style="margin-bottom: 0px !important;">
									<li class="breadcrumb-item"><a class="gray" href="<?php echo base_url('public/home	') ?>">Beranda</a></li>
									<li class="breadcrumb-item active" aria-current="page">Artikel</li>
								</ol>
							</div>
						</div>
					</div>
					<br>
					<div class="col-lg-8">
						<br>
						<div class="card-deck">
							<div class="card">
							<img class="card-img-top lazy" src="<?php echo base_url(); ?>upload/noimg.jpg" data-src="<?php if($_artikel_detail){ echo base_url('upload/photo/').$_artikel_detail[0]['foto'];}  ?>" alt="Card image cap">
								<div class="card-bodys">
									<div class="container">
										<p style="text-align:left; font-size:9px"><i class="fa fa-calendar" aria-hidden="true"></i> <?php if($_artikel_detail){ echo $_artikel_detail[0]['created_at']; }?> <i class="fa fa-user" aria-hidden="true"></i> <?php if($_artikel_detail){ echo $_artikel_detail[0]['created_by']; }?></p>
										<h5 class="card-title" style="text-align:left; color:#21438b; font-size:20px"> <?php if($_artikel_detail){ echo $_artikel_detail[0]['judul']; }?> </h5>
										<p style="font-size : 15px;"><?php if($_artikel_detail){ echo $_artikel_detail[0]['isi']; }?> </p>
										
									</div>
								</div>
								
							</div>
						</div>
						
						<br>
						<div id="disqus_thread"></div>
					</div>
					<div class="col-md-4">
						<div class="right-bar">
							<div class="right-bar-title">
								<span>Kategori</span>
							</div>
							<div class="right-bar-content">
								<?php if(count($_kategori) > 0) { ?>
									<?php foreach($_kategori as $_data) { ?>
										<a class="kategori_btn" href="<?= base_url('public/home/artikel?kategori=').$_data['id']; ?>"><?php echo $_data['nama'] ?></a>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
						<div class="right-bar">
							<div class="right-bar-title">
								<span>Artikel Lainnya</span>
							</div>
							<div class="right-bar-content">
								<?php if(count($_artikel_lainnya) > 0) { ?>
									<?php foreach($_artikel_lainnya as $_data) { ?>
										<div class="recent">
											<div class="recent-cover-img">
												<img class="recent-img lazy" src="<?php echo base_url(); ?>upload/noimg.jpg" data-src="<?php echo base_url('upload/photo/').$_data['foto']  ?>" alt="Kredit Bebas Bunga">
											</div>
											<div class="recent-content">
												<a class="recent-content-title" href="<?php echo base_url().'public/home/artikel_detail/'.$_data['id']; ?>"><?= $_data['judul'] ?></a>
												<span class="recent-content-spoiler"><?= spoiler($_data['isi'], 68) ?></span>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> 
	<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://bprnusambacepiring.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>