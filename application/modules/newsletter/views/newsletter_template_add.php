<div class="row" id="form_wrapper">
    <div id="preloader" style="display: none;"><h2>Saving ....</h2></div>
    <form role="form" id="form">
        <input type="hidden" name="content" id="post_description">

        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New Newsletter Template</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" maxlength="50" name="title" class="form-control validate[required]" id="name" placeholder="Template Name (Required)">
                    </div>

                    <div class="form-group">
                        <label for="description">Content</label>
                        <textarea class="form-control" id="description"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="0">Draft</option>
                            <option value="1">Publish</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    &nbsp;<button type="submit" class="btn btn-primary">Submit</button>
                    &nbsp;<button type="button" onclick="cancelForm();" class="btn btn-default">Back</button>
                </div>

            </div>
            <!-- /.box -->

            <!-- /.box -->

        </div>
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Preview</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="template_preview" id="template_preview" style="padding:50px;"><h1 style="color: #eee;text-align: center;padding: 50px;">Your Template Preview</h1></div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">

                </div>

            </div>
            <!-- /.box -->

            <!-- /.box -->

        </div>
    </form>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

    $(document).ready(function(){


        CKEDITOR.replace('description', ckfinder_config);

        for (var i in CKEDITOR.instances) {

            CKEDITOR.instances[i].on('change', function() {

                $("#post_description").val(CKEDITOR.instances[i].getData());

                if(CKEDITOR.instances[i].getData()!=""){
                    $("#template_preview").html(CKEDITOR.instances[i].getData());
                }else{
                    $("#template_preview").html('<h1 style="color: #eee;text-align: center;padding: 100px;">Your Template Preview</h1>');
                }


            });

        }

        $("#form").submit(function(){

            if($(this).validationEngine('validate')){

                showLoading();

                setTimeout('saveFormData();',3000);
            }

            return false;

        });

        $("#form").validationEngine();

    });

    function saveFormData(){

        var target = base_url+"newsletter/newsletter_template/addnew";
        var data = $("#form").serialize();
        $.post(target, data, function(res){

            hideLoading();

            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');
            }else{
                toastr.error(res.msg, 'Response Server');
            }

            resetForm();

        },'json');

    }
    function hideLoading(){

        $("body,html").animate({ scrollTop: 0 }, 600);
        $("#form_wrapper").removeClass("js");
        $("#preloader").hide();

    }
    function showLoading(){

        $("#form_wrapper").addClass("js");
        $("#preloader").show();

    }
    function cancelForm(){

        window.history.back();

    }
    function resetForm(){

        $('#form')[0].reset();

    }
</script>