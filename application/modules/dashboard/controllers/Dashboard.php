<?php
/**
 * Created by IntelliJ IDEA.
 * User: indra
 * Date: 9/16/16
 * Time: 23:20
 */

 class Dashboard extends Admin_Controller{

    function __construct(){

        parent::__construct();
        $base_url = base_url();
        // if($base_url == 'bprnusambacepiring.com'){
        //     redirect('public/home');
        // }
        $this->load->library("Aauth");
        $this->load->library('email');
        $this->load->model("MDashboard");
        $this->load->model("mcore");
        $this->theme_module = "dashboard";
        $this->load->library('session');
        $this->load->library('Gapi');
        $this->time_range = array('start'=>date('Y-m-d H:i:s', strtotime("-1 month")), 'end'=>date('Y-m-d H:i:s'));
        
        $_profil = $this->front_profilerow();
        $this->_theme_vars['_profil'] = $_profil;
    }

    function index(){
        // map_y('HAI');
        $base_url = base_url();
        // map_y($base_url);
        // if($base_url == 'http://bprnusambacepiring.com/' || $base_url == 'https://bprnusambacepiring.com/'){
        //     redirect('public/home');
        // }
        if($this->aauth->is_loggedin()){
            
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

            $this->_theme_vars['page_title'] = 'Dashboard';
            $this->_theme_vars['page_subtitle'] = 'Data Summary';
            $this->_theme_vars['page'] = 'dashboard';
            $this->_theme_vars['parent_menu'] = 'dashboard';
            $this->_theme_vars['submenu'] = '';
            $this->_theme_vars['lastest_registered_users'] = $this->MDashboard->getLatestRegisteredUsers();

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('dashboard');
           
         }else{
            
            

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"login");
            $this->_theme_vars['current_page'] = "login";
            $this->_theme_vars['current_theme'] = $this->theme;
            $this->render_admin_view('login');
         
         }
    }

    function front_profilerow(){
        $front_profil = $this->db->query("SELECT nama,logo FROM front_profil ")->row_array(); 
        $this->_theme_vars['front_profil'] = $front_profil;
        return $front_profil;
    }

    function wa_page(){

        if($this->aauth->is_loggedin()){
            
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

            $this->_theme_vars['page_title'] = 'Wa Setup';
            $this->_theme_vars['page_subtitle'] = 'Mengelola device';
            $this->_theme_vars['page'] = 'wa_setup';
            $this->_theme_vars['parent_menu'] = 'wa_setup';
            $this->_theme_vars['submenu'] = '';

            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('wa_page');
        
        }else{
            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"login");
            $this->_theme_vars['current_page'] = "login";
            $this->_theme_vars['current_theme'] = $this->theme;
            $this->render_admin_view('login');
        }
    }

    function check_status_wa(){
        $get = cek_wablas();
        if(isset($get->status) && $get->status = 1){
            // map_y($get);
            $html = '<table class="table table-bordered">';
            $html .= '<tr>';
            $html .= '<tr>';
            $html .= '<td>Kuota</td>';
            $html .= '<td>'.$get->data->whatsapp->quota.' Pesan</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td>Status</td>';
            $html .= '<td>'.$get->data->whatsapp->status.'</td>';
            $html .= '</tr>';
            $html .= '</table>';

            echo $html;
        }else{
            echo '<h1> GAGAL MENGAMBIL DATA</h1>';
        }
    }

    function send_wa(){
        $post = $this->input->post();
        // map_y($post);
        $no_hp = $post['no_hp'];
        $pesan = $post['pesan'];

        send_wa($no_hp, $pesan);

        return response_json([
            'message' => 'oke'
        ], 200);
    }

    private function _get_themes($type = null){

        if($type!=null){

            if($type=="admin"){

                $parent_dir = "admin";
                $themes_path = $_SERVER['DOCUMENT_ROOT'].'/themes/'.$parent_dir;
                $current_theme = $this->_theme_vars['active_admin_theme'];
                

            }else{

                $parent_dir = "public";
                $themes_path = $_SERVER['DOCUMENT_ROOT'].'/themes/'.$parent_dir;
                $current_theme = $this->_theme_vars['active_theme'];
               
            }

            $themes = array();

            $scan_path = $this->listFolderFiles($themes_path);

            $themes_dir = array_keys($scan_path);

            for($i=0;$i < count($themes_dir);$i++){

                $json = $themes_path.'/'.$themes_dir[$i].'theme.json';         

                if(file_exists($json)){

                    $meta_theme = file_get_contents($json);
                    $info = json_decode($meta_theme,true);
                    
                    $info['detail'] = base_url().'dashboard/theme/detail/'.base64_encode($json.'|'.trim(str_replace("/","",$themes_dir[$i])));
                    $info['dir'] = 'themes/'.$parent_dir.'/'.trim(str_replace("/","",$themes_dir[$i]));
                    $info['type'] = $parent_dir;

                    $thumb_path = $_SERVER['DOCUMENT_ROOT'].'/'.$info['dir'].'/'.$info['thumbnail'];
                    
                    if(file_exists($thumb_path)){
                        $info['thumb_url'] = base_url().'themes/'.$parent_dir.'/'.$themes_dir[$i].'/'.$info['thumbnail'];
                    }else{
                        $info['thumb_url'] = base_url().'assets/dist/img/blank_theme.png';
                    }
                    

                    if($current_theme == trim(str_replace("/","",$themes_dir[$i]))){

                        $info['active'] = true;

                    }else{

                        $info['active'] = false;

                    }
                    
                    $themes[] = $info;
                    
                }

            }

            return $themes;

        }

    }

    function theme($param){

        $decoded = base64_decode($param);

        $ex = explode("|", $decoded);
        $json = @$ex[0];
        $theme_dir = @$ex[1];

        $meta_theme = file_get_contents($json);
        $info = json_decode($meta_theme,true);
        $info['meta_info_file'] = $json;
        if($info['theme']=="admin"){

            $thumb_path = $_SERVER['DOCUMENT_ROOT'].'/'.'themes/admin/'.$theme_dir.'/'.$info['thumbnail'];
                    
            if(file_exists($thumb_path)){
                $info['thumb_url'] = base_url().'themes/admin/'.$theme_dir.'/'.$info['thumbnail'];
            }else{
                $info['thumb_url'] = base_url().'assets/dist/img/blank_theme.png';
            }
                    
            
            $current_theme = $this->_theme_vars['active_admin_theme'];
            $type = "admin";

        }else{

            $thumb_path = $_SERVER['DOCUMENT_ROOT'].'/'.'themes/public/'.$theme_dir.'/'.$info['thumbnail'];
                    
            if(file_exists($thumb_path)){
                $info['thumb_url'] = base_url().'themes/public/'.$theme_dir.'/'.$info['thumbnail'];
            }else{
                $info['thumb_url'] = base_url().'assets/dist/img/blank_theme.png';
            }
                    
            $current_theme = $this->_theme_vars['active_theme'];
            $type = "public";
            
        }

        if($current_theme == $theme_dir){

            $info['active'] = true;

        }else{

            $info['active'] = false;

        }     

        $info['dir'] = 'themes/'.$info['theme'].'/'.$theme_dir;
        $info['type'] = $type;


        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->read_perm = 'theme_view';

        $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

        $this->_theme_vars['page_title'] = $info['name'];
        $this->_theme_vars['page_subtitle'] = 'Theme Detail';
        $this->_theme_vars['parent_menu'] = 'configuration';
        $this->_theme_vars['submenu'] = 'theme';
        $this->_theme_vars['theme_detail'] = $info;
        $this->_theme_vars['permission'] = $this->mcore->getPermission();
        $this->_theme_vars['menus'] = $this->mcore->getMenus();
        $this->_theme_vars['add_perm'] = $this->mcore->checkPermission($this->user_group, 'theme_add');
        $this->_theme_vars['edit_perm'] = $this->mcore->checkPermission($this->user_group, 'theme_update');
        $this->_theme_vars['delete_perm'] = $this->mcore->checkPermission($this->user_group, 'theme_delete');

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('theme_detail');

    }

    function activate_theme(){


        if(count($_POST)>0){

            $themes = $this->_get_themes($_POST['type']);

            foreach ($themes as $t) {
                
                $meta_info_file = $_SERVER['DOCUMENT_ROOT'].'/'.$t['dir'].'/theme.json';
                $meta_theme = file_get_contents($meta_info_file);
                $info = json_decode($meta_theme,true);

                $info['enabled'] = false;

                $json = json_encode($info);

                $file_handle = fopen($meta_info_file, 'w'); 
                fwrite($file_handle, $json);
                fclose($file_handle);

            }


            $meta_info_file = $_POST['meta_file'];
            $meta_theme = file_get_contents($meta_info_file);
            $info = json_decode($meta_theme,true);

            $info['enabled'] = true;

            $json = json_encode($info);

            $file_handle = fopen($meta_info_file, 'w'); 
            $setactive = fwrite($file_handle, $json);
            fclose($file_handle);

            if($setactive === false){
                
                $res = array("status" => "0", "msg" => "Oop! Something went wrong");

            }else{

                $res = array("status" => "1", "msg" => "Theme successfully activated!");
            }

            echo json_encode($res);

        }


    }


    function delete_theme(){

        if(count($_POST)>0){

            $t_dir = $_SERVER['DOCUMENT_ROOT'].'/'.$_POST['dir'];

            $this->recursive_rmdir($t_dir);

            $res = array("status" => "1", "msg" => "Theme successfully uninstalled!");

            echo json_encode($res);

        }

    }

    function recursive_rmdir($dir) { 
        if( is_dir($dir) ) { 
          $objects = array_diff( scandir($dir), array('..', '.') );
          foreach ($objects as $object) { 
            $objectPath = $dir."/".$object;
            if( is_dir($objectPath) )
              $this->recursive_rmdir($objectPath);
            else
              unlink($objectPath); 
          } 
          rmdir($dir); 
        } 
    }

    function manage_themes(){


         $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
         $this->read_perm = 'theme_view';

         $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

         $this->_theme_vars['page_title'] = 'Themes';
         $this->_theme_vars['page_subtitle'] = 'Manage Themes';
         $this->_theme_vars['parent_menu'] = 'configuration';
         $this->_theme_vars['submenu'] = 'theme';
         $this->_theme_vars['admin_themes'] = $this->_get_themes('admin');
         $this->_theme_vars['website_themes'] = $this->_get_themes('website');
         $this->_theme_vars['permission'] = $this->mcore->getPermission();
         $this->_theme_vars['menus'] = $this->mcore->getMenus();
         $this->_theme_vars['add_perm'] = $this->mcore->checkPermission($this->user_group, 'theme_add');
         $this->_theme_vars['edit_perm'] = $this->mcore->checkPermission($this->user_group, 'theme_update');
         $this->_theme_vars['delete_perm'] = $this->mcore->checkPermission($this->user_group, 'theme_delete');

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('manage_theme');


    }

    function manage_menus(){

         $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
         $this->read_perm = 'cms_menus_view';

         $this->mcore->checkPermission($this->user_group, $this->read_perm, true);

         $this->_theme_vars['page_title'] = 'CMS Menus';
         $this->_theme_vars['page_subtitle'] = 'Manage Backend Menus';
         $this->_theme_vars['parent_menu'] = 'developer';
         $this->_theme_vars['submenu'] = 'cms_menu';
         $this->_theme_vars['permission'] = $this->mcore->getPermission();
         $this->_theme_vars['menus'] = $this->mcore->getMenus();
         $this->_theme_vars['add_perm'] = $this->mcore->checkPermission($this->user_group, 'cms_menus_add');
         $this->_theme_vars['edit_perm'] = $this->mcore->checkPermission($this->user_group, 'cms_menus_update');
         $this->_theme_vars['delete_perm'] = $this->mcore->checkPermission($this->user_group, 'cms_menus_delete');

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('manage_menu');


    }

     function rearrange_menu_position(){

         if(count($_POST)>0){

             $a = array_keys($_POST['menuItem']);
             $b = count($a);

             $parent_seq = 1;
             $child_seq = 1;

             for($i=0;$i < $b;$i++){

                 $menu_id = $a[$i];

                 $menu_parent = $_POST['menuItem'][$menu_id];

                 if($menu_parent!="null"){

                     $this->mcore->setParentMenu($menu_id, $menu_parent, $child_seq);

                     $child_seq++;

                 }else{

                     $child_seq = 1;

                     $this->mcore->setParentMenu($menu_id, $menu_parent, $parent_seq);

                     $parent_seq++;

                 }
             }


             echo json_encode(array("status" => 1, "msg" => "Menus position successfully updated!"));
         }

     }

     function insert_menu(){

         if(count($_POST)>0){


             if($_POST['parent']!=""){

                 $last_squence = $this->mcore->getMenuLastSquence($_POST['parent']);

                 $seq = $last_squence + 1;

                 $insert = array(

                     "name" => $_POST['name'],
                     "display_name" => $_POST['display_name'],
                     "parent" => $_POST['parent'],
                     "fa_icon" => $_POST['fa_icon'],
                     "sequence" => $seq,
                     "path" => $_POST['path'],
                     "slug" => $_POST['slug'],
                     "permission" => $_POST['permission'],
                     "status" => $_POST['status']

                 );


             }else{

                 $last_squence = $this->mcore->getMenuLastSquence();

                 $seq = $last_squence + 1;

                 $insert = array(

                     "name" => $_POST['name'],
                     "display_name" => $_POST['display_name'],
                     "fa_icon" => $_POST['fa_icon'],
                     "sequence" => $seq,
                     "path" => $_POST['path'],
                     "slug" => $_POST['slug'],
                     "permission" => $_POST['permission'],
                     "status" => $_POST['status']

                 );


             }

             $result = $this->db->insert("menus", $insert);

             if($result){

                 $res = array("status" => 1, "msg" => "Successfully add menu!");

             }else{

                 $res = array("status" => 0, "msg" => "Oop! Something went wrong.");

             }

             echo json_encode($res);

         }

     }

     function update_menu(){

         if(count($_POST)>0){


             $update = array(

                 "name" => $_POST['name'],
                 "display_name" => $_POST['display_name'],
                 "fa_icon" => $_POST['fa_icon'],
                 "path" => $_POST['path'],
                 "permission" => $_POST['permission'],
                 "status" => $_POST['status']

             );

             $this->db->where("menu_id", $_POST['menu_id']);
             $result = $this->db->update("menus", $update);

             if($result){

                 $res = array("status" => 1, "msg" => "Successfully update menu!");

             }else{

                 $res = array("status" => 0, "msg" => "Oop! Something went wrong.");

             }

             echo json_encode($res);

         }

     }

     function delete_menu(){

         if(count($_POST)>0){

             $this->db->where("menu_id", $_POST['menu_id']);
             $result = $this->db->delete("menus");

             if($result){

                 $res = array("status" => 1, "msg" => "Successfully delete menu!");

             }else{

                 $res = array("status" => 0, "msg" => "Oop! Something went wrong.");

             }

             echo json_encode($res);

         }

     }

     function change_skin(){

         if(count($_POST)>0){

             $this->db->where("scope","LAYOUT");
             $this->db->where("key","SKIN");
             $this->db->update("configurations",array(
                 "value" => $_POST['skin']
             ));

         }

     }

     function getAllVisited(){
        $time_range = array('start'=>date('Y-m-d', strtotime("-1 month")), 'end'=>date('Y-m-d'));
        $metric = "ga:sessions";
        $result = $this->gapi->getResult($time_range, $metric);
        $data = $result->getRows();
        echo $result[0][0];
     }
    
    function getSubByTypeProduct(){
        $time_range = array('start'=>date('Y-m-d H:i:s', strtotime("-30 day")), 'end'=>date('Y-m-d H:i:s'));
        $result = $this->MDashboard->getSubByTypeProduct($time_range);
        $data = [];
        foreach ($result as $value ) {
            $data[$value['nama_type_product']] = $value['count']; 
        }
        echo json_encode($data);
    }

    function getSubByStatus(){
        $time_range = array('start'=>date('Y-m-d H:i:s', strtotime("-30 day")), 'end'=>date('Y-m-d H:i:s'));
        $result = $this->MDashboard->getSubByStatus($time_range);
        // map_y($result);
        $data = [];
        foreach ($result as $value ) {
            $data[trim($value['value_status'])] = (int) $value['count']; 
        }
        echo json_encode($data);
    }

    function getVisitByDevice(){
        $time_range = array('start'=>date('Y-m-d', strtotime("-30 day")), 'end'=>date('Y-m-d'));
        $metric = "ga:sessions";
        $dimensions = array (
            'dimensions' => 'ga:date, ga:deviceCategory'
        );
        $result = $this->gapi->getResult($time_range, $metric, $dimensions);
        $begin = new DateTime($time_range['start']);
        $end = new DateTime($time_range['end']);

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $series = array(); 
        foreach ($result->getRows() as $ga) {
            if (!(in_array((object)['name'=>$ga[1]], $series))){
                $series[] = (object)['name' => $ga[1]];
            }
        }
        foreach ($series as $seri) {
            foreach ($period as $key => $dt) {
                $seri->data[$key] = 0;
                foreach ($result->getRows() as $ga) {
                    if ($dt->format('Ymd') === $ga[0] && $seri->name === $ga[1])
                        $seri->data[$key] = (int)$ga[2]; 
                }
                $date[$key] = $dt->format('d-m-Y');
            }
        }
        $data = array('date' => $date , 'series' => $series);
        echo json_encode($data);
    }

    function getSubByProduct(){
        $time_range = array('start'=>date('Y-m-d H:i:s', strtotime("-30 day")), 'end'=>date('Y-m-d H:i:s'));
        $result_tipe = $this->MDashboard->getSubByTypeProduct($time_range);
        $result_product = $this->MDashboard->getSubByProduct($time_range);
        $temp_result_product = array();
        foreach ($result_tipe as $key => $value) {
            $result_tipe[$key] = array(
                'name' => $value['nama_type_product'], 
                'y' => floatval($value['count']),
                'drilldown' => $value['nama_type_product'],
            );
            $temp_result_product[$key] = array('name' => $value['nama_type_product'], 'id' => $value['nama_type_product']);
            foreach ($result_product as $key_prodcut => $value_product) {
                if ($temp_result_product[$key]['name'] === $value_product['nama_type_product']) {
                    $temp_result_product[$key]['data'][] = array(
                        $value_product['name'] ? $value_product['name'] : '-', (int)$value_product['count']
                    );
                }
            }
        }
        
        $result ['product'] = $temp_result_product;
        $result ['tipe_product'] = $result_tipe;
        echo json_encode($result);
    }

    function getSubBySex(){
        $time_range = array ('start'=> date('Y-m-d H:i:s', strtotime('-30 day')), 'end'=>date('Y-m-d H:i:s'));
        $result = $this->MDashboard->getSubBySex($time_range);
        foreach ($result as $key => $value) {
            $result[$key] = array(
                'name' => $value['jenis_kelamin'],
                'y' => intval($value['count']),
            );
        }
        echo json_encode($result);
    }

    function getSubByJob(){
        $time_range = array ('start'=> date('Y-m-d H:i:s', strtotime('-30 day')), 'end'=>date('Y-m-d H:i:s'));
        $result = $this->MDashboard->getSubByJob($time_range);
        foreach ($result as $key => $value) {
            $result[$key] = array(
                'name' => $value['pekerjaan'],
                'y' => intval($value['count']),
            );
        }
        echo json_encode($result);
    }

    function update_profile(){
        $table = "aauth_users";
        $pk = "id";
        
        $n_session = [];
        if(count($_POST)>0){
            $id = $this->session->userdata('id');
            /* upload cover */
            if($_FILES["photo"]["name"]!=""){
                if($_FILES["photo"]["type"]=="image/png" or
                    $_FILES["photo"]["type"]=="image/jpg" or
                    $_FILES["photo"]["type"]=="image/jpeg"){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                    $array = explode('.', $_FILES['photo']['name']);
                    $extension = end($array);
                    $photo = md5(uniqid(rand(), true)).".".$extension;
                    $old_photo = $upload_path.'/'.@$_POST['old_photo'];
                    if(file_exists($old_photo)){
                        @unlink($old_photo);
                    }
                    if(move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {
                        $n_session['picture'] = base_url().'upload/photo/'.$photo;
                    }else{
                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";
                        echo json_encode($res);
                        exit;
                    }
                }else{
                    $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                    $res['status'] = "0";

                    echo json_encode($res);
                    exit;
                }
            }else{
                $photo = $this->input->post("old_photo");
            }

            $data = array(
                "first_name" => $this->input->post("first_name"),
                "last_name" => $this->input->post("last_name"),
                "picture" => $photo,
            );
           
            $this->db->where($pk, $id);
            // map_y($this->db->update($table, $data));
            $update = $this->db->update($table, $data);
            
            if($update){

                $res = array("status" => "1", "msg" => "Successfully update data!");

            }else{

                $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
            }

            $_ses = array(
                "first_name" => $data['first_name'],
                "last_name" => $data['last_name'],
            );
            $_ses = array_merge($_ses, $n_session);
           
            $this->session->set_userdata($_ses);
            echo json_encode($res);
        }else{
            $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
            $this->render_admin_view('dashboard');

        }
    }
    
    function update_password(){
        $request = $_REQUEST;
        $res = [];
        
        if(count($request) > 0){
            $is_old_pw = check_pw($request['old_password']);
            if($is_old_pw){
                $user_id = $request['id'];
                $new_pw = $request['new_password'];
                $repeat_new_pw = $request['confirm_password'];
                if($new_pw == $repeat_new_pw){
                    $pass = hash_pw($new_pw, $user_id);
                    $data['pass'] = $pass;
                    $this->db->where('id', $user_id);
                    $this->db->update('aauth_users', $data);
                    
                    $res = [
                        'status' => '1',
                        'msg' => 'Password diperbaharui'
                    ];
                }else{
                    $res = [
                        'status' => '0',
                        'msg' => 'Pengulangan password tidak sesuai'
                    ];
                }
            }else{
                $res = [
                    'status' => '0',
                    'msg' => 'Password Lama Tidak Sesuai'
                ];
            }
        }else{
            $res = [
                'status' => '-',
                'msg' => 'Param tidak ditemukan'
            ];
        }

        return response_json($res, 200);
    }
    
    function info(){
        echo phpinfo();
    }

    function test_vpn(){
        $client = new GuzzleHttp\Client([
            'base_uri' => base_url(),
        ]);

        try {
            //code...
            $response = $client->request('GET','/dashboard/ip_checker',[
                'proxy' => [
                    'https'  => 'https://tcpvpn.com-argan:argan123@103.47.208.106:443',
                ],
            ]);
            
            $check_status = $response->getStatusCode();
            $res_body = $response->getBody()->getContents();
            $res_body = json_decode($res_body);

            return response_json($res_body);
           
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            //throw $e;
            $res_body = $e->getResponse()->getBody()->getContents();
            $res_body = json_decode($res_body);

            return response_json($res_body);
        }
    }

    function test_normal(){
        $client = new GuzzleHttp\Client([
            'base_uri' => base_url('test_ip'),
        ]);

        try {
            //code...
            $response = $client->request('GET','/dashboard/ip_checker');
            
            $check_status = $response->getStatusCode();
            $res_body = $response->getBody()->getContents();
            $res_body = json_decode($res_body);
            
            return response_json($res_body);
           
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            //throw $e;
            $res_body = $e->getResponse()->getBody()->getContents();
            $res_body = json_decode($res_body);

            return response_json($res_body);
        }
    }

    function ip_checker(){
        $ip = $this->input->ip_address();
        return response_json($ip, 200);
    }
 }