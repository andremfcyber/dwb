<?php

class Pengajuan2 extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mpengajuan2");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "pengajuan";
        $this->dttModel = "Mdpengajuan2";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "List Pengajuan";
        $data['page_subtitle'] = "Modul Pengajuan";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();
    
    }

    // function show($id = null){

    //     // echo $id;
    //     // echo $id_product;
    //     // exit();
    //     if($id != null){
    //         $_SESSION['id_nasabah'] = $id;

    //     $data['theme'] = $this->_theme_vars['active_admin_theme'];
    //     $data['page_title'] = "List Pengajuan";
    //     $data['page_subtitle'] = "Modul Pengajuan";
    //     $data['current_class_dir'] = $this->router->fetch_directory();
    //     $data['current_class'] = $this->router->fetch_class();
    //     $data['permissions'] = $this->_get_permissions();
    //     $data['active_menu'] = $this->_get_active_menu();  
    //     $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
    //     $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
    //     $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
    //     $this->load->library("Cinta",$data);
    //     $this->cinta->browse();
    // }
    // }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Developer Option', 
            'submenu' => 'Pengajuan' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "pengajuan2_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "pengajuan2_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "pengajuan2_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "pengajuan2_delete"),
            "detail_perm" => $this->mcore->checkPermission($this->user_group, "pengajuan2_detail"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."pengajuan/pengajuan/add';}"

            // ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nama" => array(

                "data" => "c.nama",
                "searchable" => true,
                "orderable" => true,

            ),

            // "product" => array(

            //     "data" => "d.name",
            //     "searchable" => true,
            //     "orderable" => true,

            // ),

            "status" => array(

                "data" => "b.status",
                "searchable" => true,
                "orderable" => true,
                "render" => "function ( data,type,row ){if(data == 'pengajuan'){return '<label class=status-green>' + data + '</label>';}else{return '<label class=status-grey>' + data + '</label>';}}",
            ),

            "tgl_pengajuan" => array(

                "data" => "b.tgl_pengajuan",
                "searchable" => true,
                "orderable" => true,

            ),

           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "status" => array(

                "label" => "Status",
                "type" => "text",
                "placeholder" => "Status",
                "class" => "form-control validate[required]",
               

            ),

        );

// 
    }

    public function add(){

        if(count($_POST)>0){

            $_POST['id_nasabah'] = $_SESSION['id_nasabah'];
            // $_POST['id_product'] = $_SESSION['id_product'];

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Pengajuan Key";
            $data['page_subtitle'] = "Modul Pengajuan Key";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Pengajuan";
            $data['page_subtitle'] = "Modul Pengajuan";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}