<?php

class Dukcapil_data2 extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mdukcapil_data2");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "dukcapil_data";
        $this->dttModel = "Mddukcapil_data2";
        $this->pk = "id";

    }

    function index(){
        // if($id_nasabah != null){
        //     $_SESSION['id_nasabah'] = $id_nasabah;
        
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Verifikasi Data";
        $data['page_subtitle'] = "Verifikasi Data";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        // $data['data'] = json_decode($this->guzzle_get('/gammu/insert'));        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null, $nik = null){

        if($id != null){
            $_SESSION['id_pengajuan'] = $id;

        
            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "List Form";
            $data['page_subtitle'] = "Modul From";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons'] = $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();

            $data['data'] = json_decode($this->guzzle_get('/gammu/insert'));


            // var_dump($data['data']);die();

            $this->load->library("Cinta",$data);
            $this->cinta->browse();
        }
    }

    public function guzzle_get($id,$no_tlp){
        $adms_server = 'http://172.16.100.13:5969';
        $client = new GuzzleHttp\Client(['base_uri' => $adms_server]);
        $response = $client->request('POST','/gammu/insert',[
                'headers' => [
                    'Authorization' => 'modex',
                    'User-Agent' => '777',
                    'Content-Type' => 'application/json'
                ],
                GuzzleHttp\RequestOptions::JSON => [
                    'id' => $id,
                    'dirnumber' => $no_tlp,
                    'message' => 'TES SMS REKSI',
                ]
            ]);
            $res_body = $response->getBody()->getContents();

            $res_body = json_decode($res_body);
            var_dump($res_body);die();
            if($res_body->status != "10"){

                echo "<script type='text/javascript'>alert('SMS tidak valid!');window.history.back();</script>";exit;

            }else{
                echo "<script type='text/javascript'>alert('SMS Sudah Terkirim!');window.history.back();</script>";exit;                
             }  
    }
        

    function show_dukcapil($id){

        if($id != null){
            $_SESSION['id_nasabah'] = $id;

        $wa = $this->Mdukcapil_data2->getDukcapil($this->session->userdata("id_nasabah"),$id,null);
        // var_dump($wa);die();
        $this->_theme_vars['wa'] = $wa;

        $this->_theme_vars['page_title'] = "Detail Nasabah";
        $this->_theme_vars['page_subtitle'] = "Dukcapil Data";
        $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
        $this->render_admin_view('v_show_dukcapil');
    }
}

    function show_detail($id){

        if($id != null){
            $_SESSION['id_nasabah'] = $id;

        $wa = $this->Mdukcapil_data2->getPhoto($this->session->userdata("id_nasabah"),$id,null);
        // var_dump($wa);die();
        $this->_theme_vars['wa'] = $wa;

        $this->_theme_vars['page_title'] = "Detail Media";
        $this->_theme_vars['page_subtitle'] = "Dukcapil Data";
        $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
        $this->render_admin_view('v_show_photo');
    }
}

    function whatsapp($id){

        if($id != null){
            $_SESSION['id_nasabah'] = $id;

        $wa = $this->Mdukcapil_data2->getWa($this->session->userdata("id_nasabah"),$id,null);

        $wa_number = '62' . substr($wa['no_tlp'], 1);
        $wa['no_tlp'] = $wa_number;

        $this->_theme_vars['wa'] = $wa;

        $this->_theme_vars['page_title'] = "Kirim Pesan Verifikasi";
        $this->_theme_vars['page_subtitle'] = "Dukcapil Data";
        $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
        // $data['page_title'] = "List Form";
        // $data['page_subtitle'] = "Modul From";
        // $this->load->library("Cinta",$data);
        $this->render_admin_view('v_whatsapp');
    }
    }

    function sms($id){

        if($id != null){
            $_SESSION['id_nasabah'] = $id;

        $wa = $this->Mdukcapil_data2->getWa($this->session->userdata("id_nasabah"),$id,null);

        $wa_number = '62' . substr($wa['no_tlp'], 1);
        $wa['no_tlp'] = $wa_number;

        $this->_theme_vars['wa'] = $wa;

        $this->_theme_vars['page_title'] = "Kirim Pesan Verifikasi";
        $this->_theme_vars['page_subtitle'] = "Dukcapil Data";
        $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
        // $data['page_title'] = "List Form";
        // $data['page_subtitle'] = "Modul From";
        // $this->load->library("Cinta",$data);
        $this->render_admin_view('v_whatsapp');
    }
    }



    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Developer Option', 
            'submenu' => 'Dukcapil Data' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "dukcapil2_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "dukcapil2_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "dukcapil2_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "dukcapil2_delete"),
            "detail_perm" => $this->mcore->checkPermission($this->user_group, "dukcapil2_detail"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),

            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."dukcapil_data/dukcapil_data/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nik_nasabah" => array(

                "data" => "c.nik",
                "searchable" => true,
                "orderable" => true,

            ),

            "nik_dukcapil" => array(

                "data" => "b.nik",
                "searchable" => true,
                "orderable" => true,

            ),

            // "no_kk" => array(

            //     "data" => "b.no_kk",
            //     "searchable" => true,
            //     "orderable" => true,

            // ),

            "nama_nasabah" => array(

                "data" => "c.nama",
                "searchable" => true,
                "orderable" => true,

            ),

            "nama_dukcapil" => array(

                "data" => "b.nama_lgkp",
                "searchable" => true,
                "orderable" => true,

            ),

            // "nama_lgkp_ibu" => array(

            //     "data" => "b.nama_lgkp_ibu",
            //     "searchable" => true,
            //     "orderable" => true,

            // ),

            // "tgl_lhr" => array(

            //     "data" => "b.tgl_lhr",
            //     "searchable" => true,
            //     "orderable" => true,

            // ),

            // "tempat_lahir" => array(

            //     "data" => "b.tmpt_lhr",
            //     "searchable" => true,
            //     "orderable" => true,

            // ),            

           //  "alamat" => array(

           //      "data" => "b.alamat",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),

           //  "kelurahan" => array(

           //      "data" => "b.kel_name",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),            

           // "kabupaten" => array(

           //      "data" => "b.kab_name",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),

           // "kecamatan" => array(

           //      "data" => "b.kec_name",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),

           // "provinsi" => array(

           //      "data" => "b.prop_name",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),

           // "kode_pos" => array(

           //      "data" => "b.kode_pos",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),

           // "no_rt" => array(

           //      "data" => "b.no_rt",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),

           // "no_rw" => array(

           //      "data" => "b.no_rw",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),

           // "agama" => array(

           //      "data" => "b.agama",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),

           // "jenis_pkrjn" => array(

           //      "data" => "b.jenis_pkrjn",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),

           // "status_kawin" => array(

           //      "data" => "b.status_kawin",
           //      "searchable" => true,
           //      "orderable" => true,

           //  ),
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "nik" => array(

                "label" => "NIK",
                "type" => "text",
                "placeholder" => "NIK",
                "class" => "form-control validate[required]",
               

            ),

            // "id_nasabah" => array(

            //     "label" => "Id Nasabah",
            //     "type" => "text",
            //     "placeholder" => "Id Nasabah",
            //     "class" => "form-control validate[required]",
               

            // ),

        );

// 
    }

    public function add(){

        if(count($_POST)>0){

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Blog Key";
            $data['page_subtitle'] = "Modul Blog Key";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Form";
            $data['page_subtitle'] = "Modul Form";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}