<?php

class Mddukcapil_data2 extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'dukcapil2_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'dukcapil2_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'dukcapil2_detail');
        $this->allow_detail_sms = $this->mcore->checkPermission($this->user_group, 'dukcapil2_detail_foto');
        $this->allow_detail_nasabah = $this->mcore->checkPermission($this->user_group, 'dukcapil2_detail_nasabah');


    }
    public function appendToSelectStr() {
        $detail_nasabah = '';
        $edit = '';
        $detail = '';
        $detail_sms = '';
        $delete = '';
        $str = '';

        if($this->allow_detail_nasabah){
            $detail_nasabah = '<a class="btn btn-sm btn-default" href="'.base_url('dukcapil_data2/show_dukcapil/').'\',c.id,\'"><i class="fa fa-user"></i></a>&nbsp;';
        }

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-default" href="'.base_url('dukcapil_data2/show_detail/').'\',c.id,\'"><i class="fa fa-eye"></i></a>&nbsp;';
        }

        if($this->allow_detail){
            $detail = '<a class="btn btn-sm btn-success" target="blank" href="'.base_url('dukcapil_data2/whatsapp/').'\',c.id,\'"><i class="fa fa-whatsapp"></i></a>&nbsp;';
        }

        if($this->allow_detail_sms){
            $detail_sms = '<a class="btn btn-sm btn-primary" href="'.base_url('dukcapil_data2/guzzle_get/').'\',c.id,\'/\',c.no_tlp,\'"><i class="fa fa-envelope-o"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

        if($detail_nasabah!='' || $edit!='' || $detail!='' || $detail_sms!='' || $delete!=''){

            $op = "concat('".$detail_nasabah.$edit.$detail.$detail_sms.$delete."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "dukcapil_data b";
    }

    public function joinArray(){
        // return null;
        return array(
            "nasabah c|left" => "b.id_nasabah = c.id"
        );
            // "product d|left" => "b.id_product = d.id",
            // "product_type e|left" => "e.id = d.product_type_id"
    }

    public function whereClauseArray(){
        return null;
        // return array(
        //     "b.id_nasabah" => $_SESSION['id_nasabah']
        // );
    }


}