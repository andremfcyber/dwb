
<?php

class Mdukcapil_data2 extends CI_Model
{
    function __construct(){
        parent::__construct();
        // $this->load->library("Aauth");
        // $this->load->model("mcore");

        // if($this->aauth->is_loggedin()) {
        //     $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        // }

    }
    function getKeyById($id){

        $result = $this->db->query("SELECT * FROM dukcapil_data WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getKey(){

        $result = $this->db->query("SELECT * FROM dukcapil_data")->result_array();
        return $result;

    }

    function getWa($id){

        $result = $this->db->query("SELECT c.name,a.nama, a.id_product, a.nik, a.no_tlp, a.email, b.id_nasabah, b.id_product, b.status, b.tgl_pengajuan, b.link_verifikasi FROM nasabah a, pengajuan b, product c WHERE a.id='".$id."' AND a.id = b.id_nasabah AND a.id_product = c.id")->result_array();
        return @$result[0];

    }

    function getSms($id){

        $result = $this->db->query("SELECT a.nama, a.id_product, a.nik, a.no_tlp, a.email, b.id_nasabah, b.id_product, b.status, b.tgl_pengajuan, b.link_verifikasi FROM nasabah a, pengajuan b WHERE a.id='".$id."' AND a.id = b.id_nasabah ")->result_array();
        return @$result[0];

    }

    function getPhoto($id){

        $result = $this->db->query("SELECT a.photo_selfie, a.photo_ktp FROM nasabah a, dukcapil_data b WHERE a.id='".$id."' AND a.id = b.id_nasabah ")->result_array();
        return @$result[0];

    }

    function getDukcapil($id){

        $result = $this->db->query("SELECT a.id, a.nama, a.nik,b.nik, a.no_tlp, a.email, a.photo_selfie, a.photo_ktp,
            b.tmpt_lhr, b.kel_name, b.no_kk, b.no_rt, b.alamat, b.kab_name, b.jenis_klmin, b.no_rw, 
            b.prop_name, b.nama_lgkp, b.kec_name, b.agama, b.jenis_pkrjn, b.status_kawin, b.kode_pos, b.tgl_lhr, b.nama_lgkp_ibu FROM nasabah a, dukcapil_data b WHERE a.id='".$id."' AND a.id = b.id_nasabah ")->result_array();
        return @$result[0];

    }

}