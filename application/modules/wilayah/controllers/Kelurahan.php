<?php

class Kelurahan extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Mkelurahan");
        $this->request = request_handler();
    }

    private function _get_active_menu(){
        return [
            'parent_menu' => 'master', 
            'submenu' => 'wilayah_kelurahan_view' 
        ];
    }

    function ajax_select2_jateng_only(){
        $get_list = $this->Mkelurahan->list_jateng_only();
        return response_json($get_list, 200);
    }

    function index(){
        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->_theme_vars['current_page'] = "struktur";
        $this->_theme_vars['current_theme'] = $this->theme;
        $this->_theme_vars['page_title'] = 'Wilayah';
        $this->_theme_vars['page_subtitle'] = 'Kelurahan';
        $this->_theme_vars['parent_menu'] = $this->_get_active_menu()['parent_menu'];
        $this->_theme_vars['submenu'] = $this->_get_active_menu()['submenu'];

        $this->render_admin_view('view/wilayah/kelurahan');
    }

    function create(){
        /**
         * Validation
         */
        if(!isset($this->request->kelurahan) || $this->request == ''){
            response_json([
                'msg' => 'Kolom kelurahan Wajib Diisi'
            ], 422);
        }

        /**
         * Proses
         */
        $data = $this->request;
        $save = $this->Mkelurahan->create($data);
        if($save){
            response_json([
                'msg' => 'Berhasil Data Baru Ditambahkan'
            ], 200);
        }
    }

    function update($id){
        /**
         * Validation
         */
        if(!isset($this->request->kelurahan) || $this->request == ''){
            response_json([
                'msg' => 'Kolom kelurahan Wajib Diisi'
            ], 422);
        }

        /**
         * Proses
         */
        $data = $this->request;
        $save = $this->Mkelurahan->update($id, $data);
        if($save){
            response_json([
                'msg' => 'Berhasil Data Diperbaharui'
            ], 200);
        }
        
    }

    function show($id){
        $get = $this->Mkelurahan->show($id);
        return response_json($get);
    }

    function delete($id){
        /**
         * Proses
         */
        $del = $this->Mkelurahan->delete($id);
        if($del){
            response_json([
                'msg' => 'Berhasil Data Dihapus'
            ], 200);
        }
    }
}