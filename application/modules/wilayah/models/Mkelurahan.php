<?php
class Mkelurahan extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        
        $this->dbase = $this->load->database('default', TRUE);
        $this->table = '_tbl_kelurahan';
    }
    
    /**
     * For select2
     */
    function list_jateng_only(){
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        $keyword = isset($request->term) ? $request->term : '';
        $resultCount = 10;

        $offset = ($page - 1) * $resultCount;

        $columns = [
            'id as id',
            'kelurahan as text',
            'kd_pos as kodepos',
        ];

        $kabkota_all_jateng = $this->dbase->query("SELECT id FROM _tbl_kabkot WHERE provinsi_id = '13'")->result_array();
        $kabkota_all_jateng = map_id($kabkota_all_jateng);

        $kecamatan_all_jateng = $this->dbase->query("SELECT id FROM _tbl_kecamatan WHERE kabkot_id IN ($kabkota_all_jateng)")->result_array();
        $kecamatan_all_jateng = map_id($kecamatan_all_jateng);

        $conditions = [
            "WHERE kecamatan_id IN ($kecamatan_all_jateng)",
            "AND (kelurahan::text ILIKE '%".$keyword."%' OR kd_pos::text ILIKE '%".$keyword."%')"
        ];
        
        $query = "SELECT ".implode(', ', $columns)." FROM ".$this->table." ".implode(' ', $conditions)." ";

        /*Total Count */
        $count = count($this->dbase->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= 'OFFSET '.$offset;

        $results = $this->dbase->query($query)->result_array();
        /* EndCount */
        $endCount = $offset + $resultCount;
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        return $results;
    }

    /**
     * Show
     */
    function show($id){
        $get = $this->dbase->query("SELECT * FROM ".$this->table." WHERE id = '$id' LIMIT '1'")->row();
        return @$get;
    }

    /**
     * Create
     */
    function create($data){
        $this->dbase->insert($this->table, $data);
        return true;
    }

    /**
     * Update
     */
    function update($id, $data){
        $this->dbase->where(['id' => $id]);
        $this->dbase->update($this->table, $data);
        return true;
    }

    /**
     * Delete
     */
    function delete($id){
        $this->dbase->where(['id' => $id]);
        $this->dbase->delete($this->table);
        return true;
    }
}