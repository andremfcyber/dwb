<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpub extends CI_Model {

    public function data_home($id = null)
    {
        $this->db->select('a.*');
        $this->db->from('detail_form a');
        $query = $this->db->get();
        return $query;
    }

    public function insert($data)
  	{
    	$this->db->insert('nasabah', $data);
  	}

    public function insert_pengajuan($data1)
    {
        $this->db->insert('pengajuan', $data1);
    }

    public function insert_data_formulir($data2)
    {
        $this->db->insert('data_formulir_nasabah', $data2);
    }

    // public function dukcapil_data()
    // {
    //     $query = $this -> db -> get ('dukcapil_data');
    //     if ($query -> num_rows() > 0){
    //         return $query -> result();
    //     }else{
    //         return false;
    //     }
    // }

    function getShowProduct($id){

        $result = $this->db->query("SELECT a.name, b.nama_type_product FROM product a, product_type b" )->result_array();
        return @$result[0];

    }

    public function data_product(){
        $query = $this -> db -> get ('product_type');
        if ($query -> num_rows() > 0){
            return $query -> result();
        }else{
            return false;
        }
    }

    public function data_product_kredit()
    {
        $this->db->select('a.id,
                           b.nama_type_product,
                           a.name,
                           a.deskripsi,
                           a.thumbnail,
                           a.product_type_id');
        $this->db->from('product a');
        $this->db->join('product_type b','b.id = a.product_type_id');
        $this->db->where('a.product_type_id = 5');
        $query = $this->db->get();
        // map_y($query->result_array());
        return $query->result_array();      
    }

    public function data_product_deposito()
    {
        $this->db->select('a.id,
                           b.nama_type_product,
                           a.name,
                           a.deskripsi,
                           a.thumbnail,
                           a.product_type_id');
        $this->db->from('product a');
        $this->db->join('product_type b','b.id = a.product_type_id');
        $this->db->where('a.product_type_id = 7');
        $query = $this->db->get();
        return $query->result_array();      
    }

    public function data_product_tabungan()
    {
        $this->db->select('a.id,
                           b.nama_type_product,
                           a.name,
                           a.deskripsi,
                           a.thumbnail,
                           a.product_type_id');
        $this->db->from('product a');
        $this->db->join('product_type b','b.id = a.product_type_id');
        $this->db->where('a.product_type_id = 8');
        $query = $this->db->get();
        return $query->result_array();      
    }

    function getVerifikasi($id_product,$id_nasabah){

        $result = $this->db->query("SELECT a.id_product, a.id, b.name FROM nasabah a LEFT JOIN  product b ON a.id_product = b.id
            WHERE a.id_product = $id_product AND a.id = $id_nasabah ")->result_array();
        return @$result[0];

    }

    function getNasabah($id){

        $result = $this->db->query("SELECT * FROM nasabah WHERE id = $id ")->row_array();
        return @$result;

    }

    function getDukcapil($id){

        $result = $this->db->query("SELECT * FROM dukcapil_data")->result_array();
        return @$result[0];

    }

    function getValidasi($nik,$nama_lgkp_ibu,$id){

        $result = $this->db->query("SELECT * FROM dukcapil_data WHERE nik = '$nik' 
            AND nama_lgkp_ibu = '$nama_lgkp_ibu'")->result_array();
        return @$result[0];

    }

    function get_header_form($id_product){
        $result = $this->db->query("SELECT * FROM form_header where product_id = $id_product ORDER BY urutan");
        return @$result->result_array();
    }

    function get_detail_nasabah($id_nasabah){
        $result = $this->db->query("SELECT * FROM nasabah n LEFT JOIN detail_nasabah dn ON n.id = dn.id_nasabah WHERE n.id = $id_nasabah");
        return @$result->result_array()[0];
    }

    function getsales () {
        $result = $this->db->query("SELECT * FROM sales ORDER BY random() LIMIT 1");
        return @$result->result_array()[0];
    }
    function getprofil () {
        $result = $this->db->query("SELECT * FROM front_profil");
        return @$result->result_array()[0];
    }
    function getkontak () {
        $result = $this->db->query("SELECT * FROM front_kontak");
        return @$result->result_array()[0];
    }
    function getstruktur () {
        $result = $this->db->query("SELECT * FROM front_struktur_organisasi");
        return @$result->result_array()[0];
    }
    function getbudaya () {
        $result = $this->db->query("SELECT * FROM front_budaya_kerja");
        return @$result->result_array();
    }
    function getcerpel () {
        $result = $this->db->query("SELECT * FROM front_cerita_pelanggan");
        return @$result->result_array();
    }
    function getgaleri () {
        $result = $this->db->query("SELECT * FROM front_galeri");
        return @$result->result_array();
    }
    function getfaq () {
        $result = $this->db->query("SELECT * FROM front_fax");
        return @$result->result_array();
    }
    function getawards () {
        $result = $this->db->query("SELECT * FROM front_awards");
        return @$result->result_array();
    }
    function getkml () {
        $result = $this->db->query("SELECT * FROM front_kml");
        return @$result->result_array();
    }
    function getagenda () {
        $result = $this->db->query("SELECT * FROM front_agenda");
        return @$result->result_array();
    }
    function getslide () {
        $result = $this->db->query("SELECT * FROM front_slide");
        return @$result->result_array();
    }
    function getartikel () {
        $result = $this->db->query("SELECT * FROM front_artikel");
        return @$result->result_array();
    }
    function getkategori () {
        $result = $this->db->query("SELECT * FROM front_kategori");
        return @$result->result_array();
    }
    function getpressrealese () {
        $result = $this->db->query("SELECT * FROM front_press_realese");
        return @$result->result_array();
    }
    function getpressrealese_limit () {
        $result = $this->db->query("SELECT * FROM front_press_realese ORDER BY random() limit 3 ");
        return @$result->result_array();
    }
    function getblog () {
        $result = $this->db->query("SELECT * FROM cms_blog");
        return @$result->result_array();
    }
    function getblog_limit () {
        $result = $this->db->query("SELECT * FROM cms_blog ORDER BY random() limit 3 ");
        return @$result->result_array();
    }
    function getcareer () {
        $result = $this->db->query("SELECT a.id, a.nama, a.id_kategori, b.kategori_career,
        a.type_kerja, 
        c.tipe_career,
        a.keterangan, 
        a.created_at, 
        a.created_by, 
        a.updated_at, 
        a.updated_by
      FROM public.front_career a 
      left join front_kategori_career b on a.id_kategori = b.id
      left join front_tipe_career c on a.type_kerja = c.id
      ");
        return @$result->result_array();
    }
    function getkategori_career () {
        $result = $this->db->query("SELECT * FROM front_kategori_career");
        return @$result->result_array();
    }
    function gettipe_career () {
        $result = $this->db->query("SELECT * FROM front_tipe_career");
        return @$result->result_array();
    }
    function get_blog_detail($id){
        $result = $this->db->query("SELECT * FROM cms_blog where id = $id");
        return @$result->result_array();
    }
    function get_artikel_detail($id){
        $result = $this->db->query("SELECT * FROM front_artikel where id = $id");
        return @$result->result_array();
    }
    function get_pressrealese_detail($id){
        $result = $this->db->query("SELECT * FROM front_press_realese where id = $id");
        return @$result->result_array();
    }
    function get_kategori_laporan(){
        $result = $this->db->query("SELECT * FROM front_kategori_laporan");
        return @$result->result_array();
    }
    function get_tahun_download(){
        $result = $this->db->query("SELECT tahun FROM front_download GROUP BY tahun order by tahun desc");
        return @$result->result_array();
    }
    function get_download_by_tahun($tahun){
        $result = $this->db->query("SELECT * FROM front_download where tahun = '$tahun'");
        return @$result->result_array();
    }
    function detail_product($id){
        
        $this->db->select('a.id,
        b.nama_type_product,
        a.name,
        a.deskripsi,
        a.thumbnail,
        a.product_type_id');
        $this->db->from('product a');
        $this->db->join('product_type b','b.id = a.product_type_id');
        $this->db->where('a.id ='.$id);
        $query = $this->db->get();
        return $query->result_array();      
    }
    function getbaner($kode){
        $result = $this->db->query("SELECT * FROM front_baner where kode = '$kode'");
        return @$result->result_array();
    }
    public function insert_pelamar($data)
  	{
    	$this->db->insert('front_pelamar', $data);
    }
    function get_laporan(){
        $result = $this->db->query("SELECT * FROM front_laporan order by tahun desc");
        return @$result->result_array();
    }
    function get_laporan_kategori($id){
        $result = $this->db->query("SELECT * FROM front_laporan where id_kategori_laporan = '$id' order by tahun desc");
        return @$result->result_array();
    }
    function get_career_keywoard($keywoard) {
        $result = $this->db->query("SELECT a.id, a.nama, a.id_kategori, b.kategori_career,
        a.type_kerja, 
        c.tipe_career,
        a.keterangan, 
        a.created_at, 
        a.created_by, 
        a.updated_at, 
        a.updated_by
      FROM public.front_career a 
      left join front_kategori_career b on a.id_kategori = b.id
      left join front_tipe_career c on a.type_kerja = c.id
      where a.nama LIKE '%$keywoard%'");
        return @$result->result_array();
    }
    function get_artikel_list($limit, $start){
        $result = $this->db->query("SELECT * FROM front_artikel", $limit, $start);
        return @$result->result_array();
    }
    function get_karir_search($id){
        $result = $this->db->query("SELECT a.id, a.nama, a.id_kategori, b.kategori_career,
        a.type_kerja, 
        c.tipe_career,
        a.keterangan, 
        a.created_at, 
        a.created_by, 
        a.updated_at, 
        a.updated_by
      FROM public.front_career a 
      left join front_kategori_career b on a.id_kategori = b.id
      left join front_tipe_career c on a.type_kerja = c.id where a.id = '$id'");
        return $result->row();
    }
    function get_artikel_kategori($id){
        $result = $this->db->query("SELECT * FROM front_artikel where id_kategori = '$id'");
        return @$result->result_array();
    }
    function get_press_realese_kategori($id){
        $result = $this->db->query("SELECT * FROM front_press_realese where id_kategori = '$id'");
        return @$result->result_array();
    }
    function get_blog_kategori($id){
        $result = $this->db->query("SELECT * FROM cms_blog where id_kategori = '$id'");
        return @$result->result_array();
    }
    function get_kategori_laporan_by($id){
        $result = $this->db->query("SELECT * FROM front_kategori_laporan where id = '$id'");
        return @$result->result_array();
    }
    function getRecent(){
        $get = $this->db->query("SELECT * FROM front_artikel ORDER BY random() limit 3");
        return @$get->result_array();
    }
    function get_mitra(){
        $get = $this->db->query("SELECT * FROM front_mitra");
        return @$get->result_array();
    }
    function get_badan_pengawas(){
        $get = $this->db->query("SELECT * FROM front_badan_pengawas");
        return @$get->result_array();
    }
    public function insert_pesan($data)
  	{
    	$this->db->insert('front_hubungi_kami', $data);
    }


    public function get_detail_surat_tugas($id){
        $result = $this->db->query("SELECT 
                   a.id, 
                   a.id_collector, 
                   b.nama_collector,
                   b.photo,
                   a.id_pinjaman,
                   d.nama as nama_nasabah,
                   e.name as nama_produk,
                   a.surat_tugas,
                   a.id_angsuran,
                   f.tgl_penagihan,
                   f.angsuran_ke,
                   f.bayar_bulan,
                   date_part('day'::text, now() - f.tgl_penagihan::timestamp with time zone) AS telat,
                    ( SELECT count(*) AS sisa_tagihan
                      FROM c_angsuran
                      WHERE c_angsuran.id_pinjaman = c.id AND c_angsuran.status = 0) AS sisa_tagihan,
                    ( SELECT sum(c_angsuran.bayar_bulan) AS total_sisa_tagihan
                      FROM c_angsuran
                      WHERE c_angsuran.id_pinjaman = c.id AND c_angsuran.status = 0) AS total_sisa_tagihan
                FROM public.c_inventory_collect a
                left join c_collector b on a.id_collector = b.id
                left join c_pinjaman c on a.id_pinjaman = c.id
                left join c_nasabah d on c.id_nasabah = d.id
                LEFT JOIN product e ON c.id_product = e.id
                left join c_angsuran f on a.id_angsuran = f.id WHERE a.id = '$id'");
        return @$result->result_array()[0];
    }

    function get_icon($key){
        $result = $this->db->query("SELECT * FROM front_depan where key = '$tahun'");
        return @$result->result_array();
    }
    function getCorevalue () {
        $result = $this->db->query("SELECT * FROM front_core_value");
        return @$result->result_array();
    }
}