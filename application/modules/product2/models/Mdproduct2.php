<?php

class Mdproduct2 extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'product2_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'product2_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'product2_detail');
        $this->allow_nasabah = $this->mcore->checkPermission($this->user_group, 'product2_nasabah');


    }
    public function appendToSelectStr() {
        $detail = '';
        $nasabah = '';
        $edit = '';
        $delete = '';
        $str = '';

        if($this->allow_nasabah){
            $nasabah = '<a class="btn btn-sm btn-success" href="'.base_url('nasabah/show/').'\',b.id,\'"><i class="fa fa-send"></i></a>&nbsp;';
        }

        // if($this->allow_detail){
        //     $detail = '<a class="btn btn-sm btn-success" href="'.base_url('form_header/show/').'\',b.id,\'"><i class="fa fa-file"></i></a>&nbsp;';
        // }

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($nasabah!='' ||$detail!='' || $edit!='' || $delete!=''){

            $op = "concat('".$nasabah.$detail.$edit.$delete."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "product b";
    }

    public function joinArray(){
        // return null;
        return array(
            "product_type c |left" => "b.product_type_id = c.id" 
        );
    }

    public function whereClauseArray(){
        return null;
        // return array(
        //     "b.product_type_id" => $_SESSION['id_type_product']
        // // );
    }


}