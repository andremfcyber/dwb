<?php

class Mproduct extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getProductKeyById($id){

        $result = $this->db->query("SELECT * FROM product WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getProduct(){

        $result = $this->db->query("SELECT * FROM product")->result_array();
        return $result;

    }

    function getProductByType($type){
        $result = $this->db->query("SELECT * FROM product WHERE product_type_id='".$type."'")->result_array();
        return @$result;
    }

    function getProductType(){
        $result = $this->db->query("SELECT * FROM product_type ")->result_array();
        return @$result;
    }

}