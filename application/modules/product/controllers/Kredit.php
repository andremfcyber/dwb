<?php
class Kredit extends Admin_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mproduct");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }

        $this->table = "product";
        $this->dttModel = "Mdkredit";
        $this->pk = "id";
	}

	function index(){
        
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "List Nasabah Kredit";
        $data['page_subtitle'] = "Modul Nasabah Kredit";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();
    }

    public function dataTable() {
        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'p.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Product', 
            'submenu' => '' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "product_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "product_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "product_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "product_delete"),
            "detail_prem" => $this->mcore->checkPermission($this->user_group, "product_detail"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."product/kredit/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(
            "Gambar" => array(
                "data" => "p.thumbnail",
                "searchable" => false,
                "orderable" => false,
                "render" => "function(data, type, row){ let src = ''; if(!data){ src = base_url+'/assets/dist/img/default_img.jpg'; }else{ src = base_url+'/upload/thumbnail/'+data;} let html = `<img src='\${src}' style='height: 100px; width: 100px; object-fit: cover;'>`; return html;}"

            ),

            "Nama Product" => array(

                "data" => "p.name",
                "searchable" => true,
                "orderable" => true,
                "className" => 'dt-lb',
              

            ),

            "Tipe Product" => array(

                "data" => "pt.nama_type_product",
                "searchable" => true,
                "orderable" => true,
                "className" => 'dt-lb',
              

            ),

            "Deskripsi" => array(

                "data" => "p.deskripsi",
                "searchable" => true,
                "orderable" => true,
                "className" => 'dt-desc',
                "render" => "function(data, type, row){ return data.substr(0,70)+'...'; }"

            ),
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                
            )

        );

    }
    
    private function _get_fields_edit($id = ''){
        $form = [
            "name" => array(

                "label" => "Nama Product",
                "type" => "text",
                "placeholder" => "Nama Product",
                "class" => "form-control validate[required]",
               

            ),

            "product_type_id" => array(

                "label" => "Tipe Product",
                "type" => "sourcequery",
                "source" => $this->Mproduct->getProductType(),
                "keydt" => "id",
                "valuedt" => "nama_type_product",
                "class" => "form-control select2 validate[required]",
               
            ),

            "deskripsi" => array(
            	"label" => "Deskripsi",
            	"type" => "editor",
            	"placeholder" => "Deskripsi",
            	"class" => "form-control validate[required]",
            ),
        ];
        if($id){
            $form["thumbnail"] = array (
                "label" => "Thumbnail <span style='color: red'>* Ukuran Proporsional (Eg, 500px x 500px)</span>",
                "type" => "upload_file",
                "placeholder" => "thumbnail",
                "class" => "form-control",
                "file_path" => 'upload/thumbnail/'
            );
        }else{
            $form["thumbnail"] = array (
                "label" => "Thumbnail <span style='color: red'>* Ukuran Proporsional (Eg, 500px x 500px)</span>",
                "type" => "upload_file",
                "placeholder" => "thumbnail",
                "class" => "form-control validate[required]",
                "file_path" => 'upload/thumbnail/'
            );
        }

        return $form;
    }

    public function add(){

        if(count($_POST)>0){
            unset($_POST['tmp_name']);
            unset($_POST['old_thumbnail']);

            if(count($_FILES)>0){
                if($_FILES["thumbnail"]["name"]!=""){

                    if($_FILES["thumbnail"]["type"]=="image/png" or
                        $_FILES["thumbnail"]["type"]=="image/jpg" or
                        $_FILES["thumbnail"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/thumbnail/';
                        $array = explode('.', $_FILES['thumbnail']['name']);
                        $extension = end($array);
                        $photo = generate_name_random().".".$extension;

                        if (move_uploaded_file($_FILES["thumbnail"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['thumbnail'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }
                }
            }

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;
            $data['params']['log'] = 'Tambah product ' . $_POST['name'];

            $this->load->library("Cinta",$data);
            $insert = $this->cinta->process();
            
            if($insert){
               
                $res = array("status" => "1", "msg" => "Data berhasil terupdate!");
                
            }else{

                $res = array("status" => "1", "msg" => "Oops! Telah terjadi kesalahan.");

            }

            echo json_encode($res);

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Product Kredit";
            $data['page_subtitle'] = "Modul Nasabah";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            unset($_POST['tmp_name']);
            unset($_POST['old_thumbnail']);

            if(count($_FILES)>0){
                if($_FILES["thumbnail"]["name"]!=""){

                    if($_FILES["thumbnail"]["type"]=="image/png" or
                        $_FILES["thumbnail"]["type"]=="image/jpg" or
                        $_FILES["thumbnail"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/thumbnail/';
                        $array = explode('.', $_FILES['thumbnail']['name']);
                        $extension = end($array);
                        $photo = generate_name_random().".".$extension;
                        $path = 'upload/thumbnail/'.get_file_name('product','id',$id,'thumbnail');
                        delete_file($path);

                        if (move_uploaded_file($_FILES["thumbnail"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['thumbnail'] = $photo;  
                            //Delete old FOto
                           
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{

                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit product ' . $_POST['name'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();


        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Product Kredit";
            $data['page_subtitle'] = "Modul Nasabah";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit($id);
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            $data['params']['file_path'] = 'upload/thumbnail';
            $data['params']['file_field'] = 'thumbnail';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }
    /**
     * View Conf Bunga  
     */
    public function conf_bunga($id){
        // map_y($id);
        $this->theme_module = '';
        $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
        $this->_theme_vars['theme'] = $this->_theme_vars['active_admin_theme'];
        $this->_theme_vars['page_title'] = 'Kredit';
        $this->_theme_vars['page_subtitle'] = 'Aturan Bunga';
        $this->_theme_vars['current_class_dir'] = $this->router->fetch_directory();
        $this->_theme_vars['current_class'] = $this->router->fetch_class();
        $this->_theme_vars['permissions'] = $this->_get_permissions();
        $this->_theme_vars['parent_menu'] = 'product2';
        $this->_theme_vars['submenu'] = 'product2';
        
        $check = $this->db->query("SELECT * FROM conf_bunga_kredit WHERE id_kredit_produk = $id")->result_array();
        $get_bulan = $this->db->query("SELECT * FROM conf_bulan_kredit WHERE id_produk_kredit = '$id'  ORDER BY bulan ASC")->result_array();
        $get_musiman = $this->db->query("SELECT * FROM conf_musiman_kredit WHERE id_kredit_produk = '$id'")->result_array();
        $id_conf = '';
        if(count($check) == 0){
            $insert['bunga'] = '0';
            $insert['jenis_bunga'] = '';
            $insert['id_kredit_produk'] = $id;

            $this->db->insert('conf_bunga_kredit', $insert);

            $id_conf = $id;
        }else{
            $id_conf = $id;
        }
        $data['_bulan'] = $get_bulan;
        $data['_musiman'] = $get_musiman;
        $data['detail'] = $this->db->query("SELECT * FROM conf_bunga_kredit WHERE id_kredit_produk = $id_conf")->result_array()[0];
        // map_y($data);
        $this->render_admin_view('view/kredit/conf_bunga','', $data);
    }

    /**
     * Save Conf Bunga
     */
    public function save_conf_bunga($id){
        if(count($_POST) > 0){
            $this->db->where('id_kredit_produk', $id);
            $this->db->update('conf_bunga_kredit', $_POST);
            return response_json([
                'msg' => 'Aturan Bunga Dirperbaharui'
            ]);
        }else{
            return response_json([
                'msg' => 'Terjadi Kesalahan'
            ], 400);
        }
    }

    /**
     * Save Conf Bunga
     */
    public function save_conf_musiman($id){
        if(count($_POST) > 0){
            $data['musiman'] = $_POST['musiman'];
            $data['id_kredit_produk'] = $id;
            $check_exist = $this->db->query("SELECT * FROM conf_musiman_kredit WHERE id_kredit_produk = $id")->result_array();
            $check_exist = count($check_exist);
            if($check_exist > 0){
                $this->db->where('id_kredit_produk', $id);
                $this->db->update('conf_musiman_kredit', $_POST);
            }else{
                $insert = $this->db->insert('conf_musiman_kredit', $data);
            }
            return response_json([
                'msg' => 'Aturan Musiman Dirperbaharui'
            ]);
        }else{
            return response_json([
                'msg' => 'Terjadi Kesalahan'
            ], 400);
        }
    }

    /**
     * add Conf Bulan
     */
    public function add_conf_bulan($id_produk){
        $request = $_REQUEST;
        $data['id_produk_kredit'] = $id_produk;
        $data['bulan'] = $request['bulan'];

        $bulan = $request['bulan'];
        $jenis_bunga = $request['jenis_bunga'];
        
        $check_exist = $this->db->query("SELECT * FROM conf_bulan_kredit WHERE bulan = '$bulan' AND id_produk_kredit = $id_produk")->result_array();
        $check_exist = count($check_exist);
        if($check_exist){
            return response_json([
                'msg' => 'Bulan Sudah Ada'
            ], 400);
        }

        // if($jenis_bunga == 'flat' && $bulan%12 != 0){
        //     return response_json([
        //         'msg' => 'Bulan Harus Kelipatan 12'
        //     ], 400);
        // }

        $insert = $this->db->insert('conf_bulan_kredit', $data);
        if($insert){
            
            return response_json([
                'msg' => 'Bulan baru ditambahkan'
            ]);
        }else{
            return response_json([
                'msg' => 'Terjadi Kesalahan'
            ], 400);
        }
    }

    /**
     * delete Conf Bunga
     */
    public function delete_conf_bulan($id_conf){
        $this->db->where('id', $id_conf);
        $delete = $this->db->delete('conf_bulan_kredit');
        if($delete){
            return response_json([
                'msg' => 'Bulan dihapus',
            ]);
        }else{
            return response_json([
                'msg' => 'Terjadi Kesalahan',
            ], 200);
        }
    }
    
    /**
     * edit Conf Bunga
     */
    public function edit_conf_bulan($id_conf){
      
        $request = $_REQUEST;
        $id_produk = $request['id_produk'];
        $bulan = $request['bulan'];
        $data['bulan'] = $request['bulan'];
        
        $check_exist = $this->db->query("SELECT * FROM conf_bulan_kredit WHERE bulan = '$bulan' AND id_produk_kredit = '$id_produk'")->result_array();
        // $check_exist = count($check_exist);
        if(count($check_exist)){
            return response_json([
                'msg' => 'Bulan Sudah Ada'
            ], 400);
        }
        $this->db->where('id', $id_conf);
        $update = $this->db->update('conf_bulan_kredit', $data);

        if($update){
            return response_json([
                'msg' => 'Bulan diperbaharui'
            ]);
        }else{
            return response_json([
                'msg' => 'Terjadi Kesalahan'
            ], 200);
        }
    }
   
}