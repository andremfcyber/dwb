<?php

class Mdetail_nasabah extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function gejuliantaratKeyById($id){

        $result = $this->db->query("SELECT * FROM detail_nasabah WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getKey($id){

        $result = $this->db->query("SELECT a.* FROM pengajuan a LEFT JOIN nasabah b ON a.id_nasabah = b.id WHERE a.id_nasabah='".$id."'")->result_array();
        return @$result[0];

    }

    function getShowDetail($id_nasabah){

        $result = $this->db->query("SELECT a.id, a.id_nasabah, a.id_product, a.status, a.tgl_pengajuan, b.nama, b.nik, c.name, d.nama_type_product FROM pengajuan a, nasabah b, product c, product_type d WHERE a.id_nasabah = b.id" )->result_array();
        return @$result[0];

    }

}