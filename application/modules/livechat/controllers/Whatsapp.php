<?php
/**
 * Created by IntelliJ IDEA.
 * User: indra
 * Date: 9/16/16
 * Time: 23:20
 */

 class Whatsapp extends Admin_Controller{

    function __construct(){

         parent::__construct();
         $this->load->library("Aauth");
         $this->load->library('email');
         // $this->load->model("MDashboard");
         $this->load->model("mcore");
         $this->theme_module = "livechat";
         $this->load->library('session');
    }

    function index(){
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $this->_theme_vars['page_title'] = 'Whatsapp';
        $this->_theme_vars['page_subtitle'] = 'Modul livechat';
        $this->_theme_vars['page'] = 'whatsapp';
        $this->_theme_vars['parent_menu'] = 'livechat';
        $this->_theme_vars['submenu'] = 'Whatsapp';

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('whatsapp');
           
         
    }
}