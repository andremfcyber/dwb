<?php

/**
 * Created by PhpStorm.
 * User: indra
 * Date: 29/03/17
 * Time: 16:43
 */
class MPermission extends CI_Model
{
    function getPermissionById($id){

        $result = $this->db->query("SELECT * FROM aauth_perms WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getPermission(){

        $result = $this->db->query("SELECT * FROM aauth_perms")->result_array();
        return $result;

    }
    function getPermissionModule(){

        $result = $this->db->query("SELECT DISTINCT ON (module) module, id FROM aauth_perms ORDER BY module")->result_array();
        return $result;

    }
    function getPermissionByModule($module){

        $result = $this->db->query("SELECT * FROM aauth_perms WHERE module='".$module."'")->result_array();
        return $result;

    }
    function getPermissionByGroupId($id){

        $result = $this->db->query("SELECT b.* FROM aauth_perm_to_group a
                                    LEFT JOIN aauth_perms b ON b.id=a.perm_id WHERE group_id='".$id."'")->result_array();
        return $result;

    }

}