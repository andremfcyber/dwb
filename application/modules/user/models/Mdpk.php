<?php

class Mdpk extends CI_Model
{
	function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getDPKById($id){

        $result = $this->db->query("SELECT * FROM dpk WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getDPK(){

        $result = $this->db->query("SELECT * FROM dpk")->result_array();
        return $result;

    }
    function getDPKByName($name){

        $result = $this->db->query("SELECT * FROM dpk WHERE name = '$name'")->result_array();
        return $result;

    }
}