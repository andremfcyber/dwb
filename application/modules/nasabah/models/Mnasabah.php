<?php

class Mnasabah extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getnasabahbyid($id){

        $result = $this->db->query("SELECT * FROM nasabah n LEFT JOIN product p on n.id_product = p.id LEFT JOIN product_type pt ON pt.id = p.product_type_id LEFT JOIN master_status s on n.status = s.id WHERE n.id='".$id."'")->result_array();
        return @$result[0];

    }

    function detailNasabah($id){
        $result = $this->db->query("SELECT 
            n.id as id,
            n.nik,
            n.nama,
            n.email,
            n.no_tlp,
            n.photo_ktp,
            n.photo_selfie,
            n.photo_ktp,
            n.alamat_sama_dengan_ktp as is_same_ktp,
            n.sales_id as sales_id,
            n.kode_pos as kode_pos_sekarang,
            n.kode_pos_ktp as kode_pos_ktp,
            n.alamat_lengkap as alamat_lengkap_sekarang,
            n.alamat_lengkap_ktp as alamat_lengkap_ktp,
            n.id_product as id_product,
            n.link_tracking as link_tracking,
            n.status as status,
            s.value_status,
            p.name,
            pt.nama_type_product,
            pr1.provinsi as provinsi_sekarang,
            pr2.provinsi as provinsi_ktp,
            kb1.kabupaten_kota as kabkot_sekarang,
            kb2.kabupaten_kota as kabkot_ktp,
            kc1.kecamatan as kecamatan_sekarang,
            kc2.kecamatan as kecamatan_ktp,
            kl1.kelurahan as kelurahan_sekarang,
            kl2.kelurahan as kelurahan_ktp
        FROM nasabah n 
        LEFT JOIN product p on n.id_product = p.id 
        LEFT JOIN product_type pt ON pt.id = p.product_type_id 
        LEFT JOIN master_status s on n.status = s.id
        LEFT JOIN _tbl_provinsi pr1 on pr1.id = n.provinsi_id
        LEFT JOIN _tbl_kabkot kb1 on kb1.id = n.kabupaten_id
        LEFT JOIN _tbl_kecamatan kc1 on kc1.id = n.kecamatan_id
        LEFT JOIN _tbl_kelurahan kl1 on kl1.id = n.kelurahan_id
        LEFT JOIN _tbl_provinsi pr2 on pr2.id = n.provinsi_ktp
        LEFT JOIN _tbl_kabkot kb2 on kb2.id = n.kabupaten_ktp
        LEFT JOIN _tbl_kecamatan kc2 on kc2.id = n.kecamatan_ktp
        LEFT JOIN _tbl_kelurahan kl2 on kl2.id = n.kelurahan_ktp where n.id = '$id' LIMIT 1")->result_array();

        return @$result[0];
    }

    function get_product($id){
        $result = $this->detailNasabah($id);
        return $result['name'];
    }

    function getnasabahUID($id){
        $result = $this->db->query("SELECT uid FROM nasabah WHERE id = '$id'")->row();
        return @$result->uid;
    }

    function getdetailnasabahbyid($id){
        $result = $this->db->query("SELECT * FROM nasabah n LEFT JOIN detail_nasabah dn ON n.id = dn.id_nasabah WHERE n.id = $id")->result_array();
        return @$result[0];
    }
    function nasabah_by_id($id){
        $result = $this->db->query("SELECT * FROM nasabah n LEFT JOIN detail_nasabah dn ON n.id = dn.id_nasabah WHERE n.id = $id")->result_array();
        return @$result[0];
    }

    function getheaderform($id){
        $result = $this->db->query("SELECT * FROM form_header where product_id = $id ORDER BY urutan");
        return @$result->result_array();
    }

    function getAppointment($id){
        $result = $this->db->query("SELECT * FROM appoiment a LEFT JOIN sales s ON a.id_sales = s.id where id_nasabah = $id");
        return @$result->result_array()[0];   
    }
    function getKey(){
        $result = $this->db->query("SELECT * FROM nasabah")->result_array();
        return $result;
    }

    function getDukcapilByUidNasabah($uid){
        $result = $this->db->query("SELECT * FROM dukcapil_history WHERE uid_nasabah = '$uid'")->result_array();
        return count($result) > 0 ? json_decode($result[0]['res_body']) : false; 
    }

    function laporanPertemuan($id){
        $status = $this->getdetailnasabahbyid($id)['status'];
        $appoinment = $this->getAppointment($id);
        $appoinment['pertemuan'] = $status == 5 || $status == 2 ? 'Sudah' : 'Belum';
        return $appoinment;
    }

    function laporanDetailNasabah($id){
        $nasabah = $this->Mnasabah->getnasabahbyid($id);
        $detail_nasabah = $this->Mnasabah->getdetailnasabahbyid($id);
        $header_form = $this->Mnasabah->getheaderform($nasabah['id_product']);
        $appointment = $this->Mnasabah->getAppointment($id);
        
        $id_nasabah = $nasabah['id'];
        $id_product = $nasabah['id_product'];
        // map_y($header_form);
        $data['nasabah'] = $nasabah;
        $data['detail_nasabah'] = $detail_nasabah;
        $data['detail_dinamis_nasabah'] = [];
        if (count($header_form) > 0){
            foreach ($header_form as $key => $value) {
                $query = "SELECT * FROM form_fileds WHERE form_header_id = ".$value['id']." ORDER BY urutan";
                $form_attr = $this->db->query($query)->result_array();
                foreach ($form_attr as $fa_key => $fa_value) {
                    $input_type = $fa_value['input_type'];
                    switch ($input_type) {
                    case 'text':                                      
                        $field_name = strtolower(str_replace(' ', '_', $fa_value['label']));
                        $field_name = (preg_replace('/[^A-Za-z0-9\_]/', '', $field_name));
                        $value_detail = $this->db->get_where('data_formulir_nasabah', array('id_nasabah' => $id_nasabah, 'id_product'=>$id_product, 'id_form_field'=>$fa_value['id']))->row_array();
                        if (isset($value_detail)){
                            $data_value = $value_detail['value'];
                        } else if (isset($detail_nasabah[$field_name])){
                            $data_value = $detail_nasabah[$field_name];
                        } else {
                            $data_value = null;
                        }
                        $data['detail_dinamis_nasabah'][$value['name_header']][$fa_key] = [
                            'label' => $fa_value['label'],
                            'value' => $data_value,
                        ];
                        // generateTextInput($fa_value['label'],$fa_value['id'],$data_value);
                    break;
                    case 'checkbox' :
                        $form_attr_source = $this->db->get_where("form_attribut",array("form_filed_id" => $fa_value['id']))->result_array();
                        $data['detail_dinamis_nasabah'][$value['name_header']][$fa_key] = [
                            'label' => $fa_value['label'],
                            'value' => $data_value,
                        ];
                        // generateCheckbox($fa_value['label'],$fa_value['id'],$form_attr_source,$id_nasabah,$id_product);
                    break;
                    default: 
                    break;
                    }
                }
            }
        }
        return $data;
        
    }

    // function getAppointment($id){
    //     $result = $this->db->query("SELECT * FROM appiment WHERE id = '$id'")->result_array();
    //     return $result[0];
    // }

}