<?php

class Deposito extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mnasabah");
        $this->load->model("product/Mproduct");
        $this->theme_module = "nasabah";

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }

        $this->table = "nasabah";
        $this->dttModel = "Mdddeposito";
        $this->pk = "id";
    }

    function index(){
        // map_y($this->_get_permissions());
        //Get All Status From Master Except id => 4? Verifikasi
        $d['select'] = $this->db->query("SELECT id, value_status FROM master_status WHERE id <> 4")->result_array();
        // map_y($d);
        // map_y($select);  
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "List Nasabah Deposito";
        $data['page_subtitle'] = "Modul Nasabah Deposito";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        $data['params']['datatable']['filter_custom'] = $this->load->view('v_filter_select',$d, TRUE);
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();
    }

    // function show($id = null){
    //     if($id != null){

    //         $_SESSION['id_product'] = $id;
        
    //     $data['theme'] = $this->_theme_vars['active_admin_theme'];
    //     $data['page_title'] = "List Nasabah";
    //     $data['page_subtitle'] = "Modul Nasabah";
    //     $data['current_class_dir'] = $this->router->fetch_directory();
    //     $data['current_class'] = $this->router->fetch_class();
    //     $data['permissions'] = $this->_get_permissions();
    //     $data['active_menu'] = $this->_get_active_menu();  
    //     $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
    //     $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
    //     $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
    //     $this->load->library("Cinta",$data);
    //     $this->cinta->browse();
    // }
    // }

    public function dataTable() {
        //Reindex Status
        $index = count($_POST['columns']);
        
        $_POST['columns'][$index] = [
            'name' => '',
            'data' => 'status',
            'orderable' => 'false',
            'searchable' => 'false',
            'search' => [
                'value' => '',
                'regex' => false,       
            ],
        ];

        // /**
        //  * Multiple Filter Select
        //  */
        // if(isset($_POST['search']['value'])){
        //     $this->
        // }
        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return [
            'parent_menu' => 'nasabah', 
            'submenu' => 'deposito' 
        ];

    }

    private function _get_permissions(){
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(
            "add_perm" => $this->mcore->checkPermission($this->user_group, "deposito_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "deposito_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "deposito_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "deposito_delete"),
            "validasi_perm" => $this->mcore->checkPermission($this->user_group, "deposito_validasi"),
            "verifikasi_perm" => $this->mcore->checkPermission($this->user_group, "deposito_verifikasi"),
            "detail_perm" => $this->mcore->checkPermission($this->user_group, "deposito_detail"),
            "print_perm" => $this->mcore->checkPermission($this->user_group, "deposito_print"),
            "approval_perm" => $this->mcore->checkPermission($this->user_group, "deposito_approval"),
            "pertemuan_perm" => $this->mcore->checkPermission($this->user_group, "deposit_pertemuan"),
            "send_wa_perm" => $this->mcore->checkPermission($this->user_group, "deposit_wa_send"),
            "assign_perm" => $this->mcore->checkPermission($this->user_group, "deposito_pindah_sales"),
        );
    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        $button = array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            'colvis'
        );

        if($this->mcore->checkPermission($this->user_group, "deposito_add")){
            $button[] =   array(
                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."nasabah/kredit/add';}"
            );
        }

        return $button;
    }


    private function _get_datatable_columns(){


        return [

            "NIK" => [
                "data" => "b.nik",
                "searchable" => true,
                "orderable" => true,
            ],

            "Nama" => [
                "data" => "b.nama",
                "searchable" => true,
                "orderable" => true,
            ],

            "Product" => [
                "data" => "c.name",
                "searchable" => true,
                "orderable" => true,
            ],

            "Alamat" => [
                "data" => "b.alamat_lengkap",
                "searchable" => true,
                "orderable" => true,
            ],

            "Phone Number" => [
                "data" => "b.no_tlp",
                "searchable" => true,
                "orderable" => true,
            ],

            "Email" => [
                "data" => "b.email",
                "searchable" => true,
                "orderable" => true,
            ],

            "Status" => [
                "data" => "sta.value_status",
                "searchable" => true,
                "orderable" => true,
                "className" => "text-center",
                "render" => "function(data, type, row){
                    let html = '';
                    let pengajuan = 'bg-default';
                    let validasi = 'bg-default';
                    let appointment = 'bg-default';
                    let lengkapi = 'bg-default';
                    let diterima = 'bg-default';
                    let end_status = '';
                    let end_status_label = '';

                    if(row.status == '6'){
                        html = `<span class='badge bg-red'> Invalid </span>`;
                    }else{
                        if(row.status == '1'){
                            pengajuan = 'bg-green';
                        }else if(row.status == '3'){
                            pengajuan = 'bg-green';
                            validasi = 'bg-green';
                        }else if(row.status == '7'){
                            pengajuan = 'bg-green';
                            validasi = 'bg-green';
                            appointment = 'bg-green';
                        }else if(row.status == '5'){
                            pengajuan = 'bg-green';
                            validasi = 'bg-green';
                            appointment = 'bg-green';
                            lengkapi = 'bg-green';
                            end_status = 'Menunggu approval';
                            end_status_label = 'bg-default';        
                        }else if(row.status == '2'){
                            pengajuan = 'bg-green';
                            validasi = 'bg-green';
                            appointment = 'bg-green';
                            lengkapi = 'bg-green';
                            end_status = 'Diterima';
                            end_status_label = 'bg-green';
                        }
                        html = `<span class='badge \${pengajuan}'>Pengajuan</span> <br> `;
                        html += `<span class='badge \${validasi}'>Validasi</span> <br> `;
                        html += `<span class='badge \${appointment}'>Pertemuan</span> <br> `;
                        html += `<span class='badge \${lengkapi}'>Dilengkapi</span>`;
                        html += `<span class='badge \${end_status_label}'>\${end_status}</span>`;
                        if(row.status == '8'){
                            html = `<span class='badge bg-red'>Ditolak</span>`;
                        }
                        if(row.status == '2'){
                            html = `<span class='badge bg-green'>Diterima</span>`;
                        }
                        if(row.status == '6'){
                            html = `<span class='badge bg-red'>Tidak Valid</span>`;
                        }
                    }
                    return html;
                }"
            ],

            "Tanggal Pengajuan" => [
                "data" => "b.tgl_pengajuan",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, type, row){
                    let tgl = moment(data,'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY');
                    let time = moment(data,'YYYY-MM-DD HH:mm:ss').format('HH:mm:ss');

                    let html = `<span><span class='badge bg-purple'>\${tgl}</span>`;
                        html += `<span class='badge bg-blue'>\${time} WIB</span></span>`;
                    return html;
                }"
            ],
           
            "action" => [
                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                "className" => "text-center",
                "render" => "function(data, type, row){

                    data = $(data);
                    if(row.status == '1'){
                        data.find('#pertemuan_'+row.b.id).hide()
                        data.find('#print_'+row.b.id).hide()
                        data.find('#accept_'+row.b.id).hide()
                        data.find('#reject_'+row.b.id).hide()
                        data.find('#detail_'+row.b.id).hide()
                    }

                    if(row.status == '3'){
                        // data.find('#pertemuan_'+row.b.id).hide()
                        data.find('#print_'+row.b.id).hide()
                        data.find('#accept_'+row.b.id).hide()
                        data.find('#reject_'+row.b.id).hide()
                        data.find('#detail_'+row.b.id).hide()
                    }

                    if(row.status == '5'){
                        data.find('#pertemuan_'+row.b.id).hide()
                        // data.find('#print_'+row.b.id).hide()
                    }

                    if(row.status == '6'){
                        data.find('#pertemuan_'+row.b.id).hide()
                        data.find('#print_'+row.b.id).hide()
                        data.find('#accept_'+row.b.id).hide()
                        data.find('#reject_'+row.b.id).hide()
                        data.find('#detail_'+row.b.id).hide()
                        data.find('#validasi_'+row.b.id).hide()
                    }

                    if(row.status == '7'){
                        data.find('#pertemuan_'+row.b.id).hide()
                        data.find('#accept_'+row.b.id).hide()
                        data.find('#reject_'+row.b.id).hide()
                        data.find('#print_'+row.b.id).hide()
                    }

                    if(row.status == '8' || row.status == '2'){
                        data.find('#pertemuan_'+row.b.id).hide()
                        // data.find('#print_'+row.b.id).hide()
                        data.find('#accept_'+row.b.id).hide()
                        data.find('#reject_'+row.b.id).hide()
                        data.find('#detail_'+row.b.id).hide()
                        data.find('#validasi_'+row.b.id).hide()
                    }
                    // console.log(data.html())
                    return data[0]['outerHTML'];
                }"
            ]

        ];

    }
    
    private function _get_fields_edit($id = ''){


        $data = array(

            "nik" => array(

                "label" => "NIK <span style='color: red'>* Tidak boleh ada huruf harus angka </span>",
                "type" => "text",
                "placeholder" => "NIK",
                "class" => "form-control validate[required]",
               

            ),

            "nama" => array(

                "label" => "Nama Nasabah",
                "type" => "text",
                "placeholder" => "Nama Nasabah",
                "class" => "form-control validate[required]",
               

            ),

            "no_tlp" => array(

                "label" => "No.HP <span style='color: red'>* No Hp Wajib (62), contoh: 62837482789 </span>",
                "type" => "text",
                "placeholder" => "Nomor HP",
                "class" => "form-control validate[required]",
               

            ),

            "email" => array(

                "label" => "Email <span style='color: red'>* Format Email Harus Benar, contoh: email@sample.com </span>",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required]",
               

            ),

            "id_product" => array(

                "label" => "Product",
                "type" => "sourcequery",
                "source" => $this->Mproduct->getProductByType(7),
                "keydt" => "id",
                "valuedt" => "name",
                "class" => "form-control select2 validate[required]",
               
            ),
            "photo_selfie" => array (
                "label" => "Foto Selfi",
                "type" => "upload_file",
                "placeholder" => "Foto Selfi",
                "class" => "form-control validate[required]",
                "file_path" => 'upload/photo/'
            ),
            "photo_ktp" => array (
                "label" => "Foto KTP",
                "type" => "upload_file",
                "placeholder" => "Foto KTP",
                "class" => "form-control validate[required]",
                "file_path" => 'upload/photo/'
            ),

             
            "provinsi_sekarang" => array(
            	"label" => "Provinsi Sekarang",
                "type" => "sourcearray",
            	"placeholder" => "Provinsi Sekarang",
                "class" => "form-control select2 validate[required] select2",
                "event" => [
                    'onchange' => "change_provinsi('sekarang')",
                ],
            ),

            "kota_sekarang" => array(
            	"label" => "Kab/Kota Sekarang",
                "type" => "sourcearray",
            	"placeholder" => "Kab/Kota Sekarang",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kabupaten('sekarang')",
                ],
            ),

            "kecamatan_sekarang" => array(
            	"label" => "Kecamatan Sekarang",
                "type" => "sourcearray",
            	"placeholder" => "Kecamatan Sekarang",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kecamatan('sekarang')",
                ],
            ),

            "kelurahan_sekarang" => array(
            	"label" => "Kelurahan Sekarang",
                "type" => "sourcearray",
            	"placeholder" => "Kelurahan Sekarang",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kelurahan('sekarang')",
                ],
            ),

            "kodepos_sekarang" => array(
                "label" => "Kode Pos Sekarang",
                "type" => "text",
                "placeholder" => "Kode Pos Sekarang",
                "class" => "form-control validate[required]",
                "readonly" => true,
            ),

            "alamat_lengkap_sekarang" => array(
            	"label" => "Alamat Sekarang",
            	"type" => "textarea",
            	"placeholder" => "Alamat Sekarang",
            	"class" => "form-control validate[required]",
            ),

            "alamat_sama_dengan_ktp" => array(
            	"label" => "Alamat Sama Sekarang Sama Dengan KTP",
                "type" => "sourcearray",
                "source" => [
                    't' => 'Ya',
                    'f' => 'Tidak',
                ],
                // "value" => 'true',
            	"placeholder" => "Pilih",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    "onchange" => "is_same_ktp(this)",
                ],
            ),
             
            "provinsi_ktp" => array(
            	"label" => "Provinsi Ktp",
                "type" => "sourcearray",
            	"placeholder" => "Provinsi Ktp",
                "class" => "form-control select2 validate[required] select2",
                "event" => [
                    'onchange' => "change_provinsi('ktp')",
                ],
            ),

            "kota_ktp" => array(
            	"label" => "Kab/Kota Ktp",
                "type" => "sourcearray",
            	"placeholder" => "Kab/Kota Ktp",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kabupaten('ktp')",
                ],
            ),

            "kecamatan_ktp" => array(
            	"label" => "Kecamatan Ktp",
                "type" => "sourcearray",
            	"placeholder" => "Kecamatan Ktp",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kecamatan('ktp')",
                ],
            ),

            "kelurahan_ktp" => array(
            	"label" => "Kelurahan Ktp",
                "type" => "sourcearray",
            	"placeholder" => "Kelurahan Ktp",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kelurahan('ktp')",
                ],
            ),

            "kodepos_ktp" => array(
                "label" => "Kode Pos Ktp",
                "type" => "text",
                "placeholder" => "Kode Pos Ktp",
                "class" => "form-control validate[required]",
                "readonly" => true,
            ),

            "alamat_lengkap_ktp" => array(
            	"label" => "Alamat Ktp",
            	"type" => "textarea",
            	"placeholder" => "Alamat Ktp",
            	"class" => "form-control validate[required]",
            ),


        );

        if($id){
            unset($data['photo_selfie']);
            unset($data['photo_ktp']);

            $data["photo_selfie"] = array (
                "label" => "Foto Selfi",
                "type" => "upload_file",
                "placeholder" => "Foto Selfi",
                "class" => "form-control",
                "file_path" => 'upload/photo/'
            );
            $data["photo_ktp"] = array (
                "label" => "Foto KTP",
                "type" => "upload_file",
                "placeholder" => "Foto KTP",
                "class" => "form-control",
                "file_path" => 'upload/photo/'
            );
        }

        return $data;

// 
    }

    public function add(){
        // map_y('d');
        if(count($_POST)>0){
            // map_y($_POST);
            $uid = uuid();
            $link_tracking = base_url().'public/home/tracking/'.$uid;
            $_POST['link_tracking'] = $link_tracking;
            $_POST['provinsi_id'] = $_POST['provinsi_sekarang'];
            $_POST['kabupaten_id'] = $_POST['kota_sekarang'];
            $_POST['kecamatan_id'] = $_POST['kecamatan_sekarang'];
            $_POST['kelurahan_id'] = $_POST['kelurahan_sekarang'];
            $_POST['kode_pos'] = $_POST['kodepos_sekarang'];
            $_POST['alamat_lengkap'] = $_POST['alamat_lengkap_sekarang'];
            
            if($_POST['alamat_sama_dengan_ktp'] == 't'){
                // map_y($_POST);
                $_POST['provinsi_ktp'] = $_POST['provinsi_id'];
                $_POST['kabupaten_ktp'] = $_POST['kabupaten_id'];
                $_POST['kecamatan_ktp'] = $_POST['kecamatan_id'];
                $_POST['kelurahan_ktp'] = $_POST['kelurahan_id'];
                $_POST['kode_pos_ktp'] = $_POST['kode_pos'];
                $_POST['alamat_lengkap_ktp'] = $_POST['alamat_lengkap'];
            }else{
                $_POST['kabupaten_ktp'] = $_POST['kota_ktp'];
                $_POST['kode_pos_ktp'] = $_POST['kodepos_ktp'];
            }
            // map_y($_POST);
            $role_id = get_role();
            
            if($role_id == '18'){
                $_POST['sales_id'] = get_sales_id();
            }

            $_POST['create_by_sales'] = 1;
            // map_x('-');
            // map_y($_POST);
            $no_tlp = $_POST['no_tlp'];
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_tlp'] = $no_tlp;
            $_POST ['status'] = "1";
            $_POST ['cr_time'] = date('Y-m-d H:i:s');
            $_POST ['cr_user'] = $this->session->userdata('username');
            $_POST ['up_time'] = date('Y-m-d H:i:s');
            $_POST ['tgl_pengajuan'] = date('Y-m-d H:i:s');
            unset($_POST['tmp_name']);
            unset($_POST['old_photo_selfie']);
            unset($_POST['old_photo_ktp']);
            unset($_POST['provinsi_sekarang']);
            unset($_POST['kota_sekarang']);
            unset($_POST['kecamatan_sekarang']);
            unset($_POST['kelurahan_sekarang']);
            unset($_POST['kodepos_sekarang']);
            unset($_POST['alamat_lengkap_sekarang']);
            unset($_POST['kota_ktp']);
            unset($_POST['kodepos_ktp']);

            
            if(count($_FILES)>0){
                if($_FILES["photo_selfie"]["name"]!=""){

                    if($_FILES["photo_selfie"]["type"]=="image/png" or
                        $_FILES["photo_selfie"]["type"]=="image/jpg" or
                        $_FILES["photo_selfie"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo_selfie']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo_selfie"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo_selfie'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }

                if($_FILES["photo_ktp"]["name"]!=""){

                    if($_FILES["photo_ktp"]["type"]=="image/png" or
                        $_FILES["photo_ktp"]["type"]=="image/jpg" or
                        $_FILES["photo_ktp"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo_ktp']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo_ktp"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo_ktp'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;
            $data['params']['log'] = 'Tambah pengajuan deposito dari dashboard atas nama ' . $_POST['nama'];

            $this->load->library("Cinta",$data);
            $insert = $this->cinta->process();
            
            if($insert){
               
                $res = array("status" => "1", "msg" => "Data berhasil Ditambah!");
                
            }else{

                $res = array("status" => "1", "msg" => "Oops! Telah terjadi kesalahan.");

            }

            echo json_encode($res);

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Pengajuan Deposito";
            $data['page_subtitle'] = "Modul Nasabah";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";
            $data['ikeh_ikeh_js'] = base_url('assets/dist/js/pages/nasabah.js');
            // map_y($data);
            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){
        // map_y($id);
        if(count($_POST)>0){
            $_POST['provinsi_id'] = $_POST['provinsi_sekarang'];
            $_POST['kabupaten_id'] = $_POST['kota_sekarang'];
            $_POST['kecamatan_id'] = $_POST['kecamatan_sekarang'];
            $_POST['kelurahan_id'] = $_POST['kelurahan_sekarang'];
            $_POST['kode_pos'] = $_POST['kodepos_sekarang'];
            $_POST['alamat_lengkap'] = $_POST['alamat_lengkap_sekarang'];
            
            if($_POST['alamat_sama_dengan_ktp'] == 't'){
                $_POST['provinsi_ktp'] = $_POST['provinsi_id'];
                $_POST['kabupaten_ktp'] = $_POST['kabupaten_id'];
                $_POST['kecamatan_ktp'] = $_POST['kecamatan_id'];
                $_POST['kelurahan_ktp'] = $_POST['kelurahan_id'];
                $_POST['kode_pos_ktp'] = $_POST['kode_pos'];
                $_POST['alamat_lengkap_ktp'] = $_POST['alamat_lengkap'];
            }else{
                $_POST['kabupaten_ktp'] = $_POST['kota_ktp'];
                $_POST['kode_pos_ktp'] = $_POST['kodepos_ktp'];
            }
            
            $no_tlp = $_POST['no_tlp'];
            // $pos = strpos($no_tlp, '0');
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
                unset($_POST['kota_ktp']);
                unset($_POST['kodepos_ktp']);
            }
            $_POST['no_tlp'] = $no_tlp;
            $_POST ['up_time'] = date('Y-m-d H:i:s');
            $_POST ['up_user'] = $this->session->userdata('username');
            unset($_POST['tmp_name']);
            unset($_POST['old_photo_selfie']);
            unset($_POST['old_photo_ktp']);
            unset($_POST['provinsi_sekarang']);
            unset($_POST['kota_sekarang']);
            unset($_POST['kecamatan_sekarang']);
            unset($_POST['kelurahan_sekarang']);
            unset($_POST['kodepos_sekarang']);
            unset($_POST['alamat_lengkap_sekarang']);
            
            if(count($_FILES)>0){
                if($_FILES["photo_selfie"]["name"]!=""){

                    if($_FILES["photo_selfie"]["type"]=="image/png" or
                        $_FILES["photo_selfie"]["type"]=="image/jpg" or
                        $_FILES["photo_selfie"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo_selfie']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo_selfie"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo_selfie'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }

                if($_FILES["photo_ktp"]["name"]!=""){

                    if($_FILES["photo_ktp"]["type"]=="image/png" or
                        $_FILES["photo_ktp"]["type"]=="image/jpg" or
                        $_FILES["photo_ktp"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo_ktp']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo_ktp"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo_ktp'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit pengajuan deposito dari dashboard atas nama ' . $_POST['nama'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();


        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Pengajuan Deposito";
            $data['page_subtitle'] = "Modul Nasabah";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit($id);
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $detail_nasabah = $this->Mnasabah->nasabah_by_id($id);
            // map_y($detail_nasabah);
            $data['ikeh_ikeh_js'] = base_url('assets/dist/js/pages/nasabah.js');
            $data['excute_kimochi'] = "
            $(`[name='alamat_lengkap_sekarang']`).val(`".$detail_nasabah['alamat_lengkap']."`)
            $(`[name='alamat_sama_dengan_ktp']`).val(`".$detail_nasabah['alamat_sama_dengan_ktp']."`).trigger('change');
            setTimeout(() => {
                set_wilayah(".$detail_nasabah['provinsi_id'].",
                ".$detail_nasabah['kabupaten_id'].",
                ".$detail_nasabah['kecamatan_id'].",
                ".$detail_nasabah['kelurahan_id'].",'sekarang')

                set_wilayah(".$detail_nasabah['provinsi_ktp'].",
                ".$detail_nasabah['kabupaten_ktp'].",
                ".$detail_nasabah['kecamatan_ktp'].",
                ".$detail_nasabah['kelurahan_ktp'].",'ktp')
            }, 500)
            ";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

    public function do_save_detail($id_product = 0, $id_nasabah = 0){
        if (count($_POST) > 0){
            $delete = $this->db->delete('data_formulir_nasabah', array('id_nasabah' => $id_nasabah));
            if ($delete) {
                foreach ($_POST as $key => $value) {
                    $name = explode("_", $key);
                    $input_type = @$name[0];
                    $id_field = @$name[1];
                    $field = $this->db->get_where("form_fileds", array("id" => $id_field))->result_array();
                    if ($input_type === 'text'){
                        $data = array(
                            "id_form_field" => $id_field,
                            "id_header_form" => @$field[0]['form_header_id'],
                            "id_nasabah" => $id_nasabah,
                            "id_product" => $id_product,
                            "input_type" => "text",
                            "value" => $value,
                        );
                        $this->db->insert("data_formulir_nasabah",$data);
                    } else if ($input_type === 'checkbox'){
                        $data = array(
                            "id_form_field" => $id_field,
                            "id_header_form" => @$field[0]['form_header_id'],
                            "id_nasabah" => $id_nasabah,
                            "id_product" => $id_product,
                            "input_type" => "checkbox",
                            "value" => $id_field,
                        );

                        $this->db->insert("data_formulir_nasabah",$data);
                    }
                }
                $newnasabah['status'] = '5';
                $newnasabah['up_time'] = date('Y-m-d H:i:s');
                $newnasabah['up_user'] = 'web public';
                $this->db->update('nasabah', $newnasabah, array('id' => $id_nasabah));
                $this->_insertlog('nasabah dengan id '.$id_nasabah.' melakukan kelengkapan data pengajuan untuk produk '. $id_product);
                echo json_encode(array("status" => "1", "msg" => "Data berhasil tersimpan!"));
            } else {
                echo json_encode(array("status" => "1", "msg" => "Kelengkapan data tidak tersimpan"));
            }
        }
    }

    public function detail($id){
        $nasabah = $this->Mnasabah->getnasabahbyid($id);
        $detail_nasabah = $this->Mnasabah->getdetailnasabahbyid($id);
        $header_form = $this->Mnasabah->getheaderform($nasabah['id_product']);
        $appointment = $this->Mnasabah->getAppointment($id);
        // print_r($detail_nasabah);
        // print_r($header_form);

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $this->_theme_vars['page_title'] = 'Detail Deposito';
        $this->_theme_vars['page_subtitle'] = 'Modul Nasabah';
        $this->_theme_vars['page'] = 'nasabah';
        $this->_theme_vars['parent_menu'] = 'Nasabah';
        $this->_theme_vars['submenu'] = 'Deposito';
        $this->_theme_vars['nasabah'] = $nasabah;
        $this->_theme_vars['detail_nasabah'] = $detail_nasabah;
        $this->_theme_vars['header_form'] = $header_form;
        $this->_theme_vars['appointment'] = $appointment;

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('detail');
    }

    public function verify($id){
        $nasabah = $this->Mnasabah->getnasabahbyid($id);
        
        if ($nasabah > 0) {
            $dataview ['nasabah'] = $nasabah;
            $this->load->view('v_donevalid_nasabah', $dataview);
        }else {
            $this->load->view("v_notfound_nasabah");
        }
    }

    public function cek_status($id){
        $nasabah = $this->Mnasabah->getnasabahbyid($id);
        
        if ($nasabah > 0) {
            $dataview['nasabah'] = $nasabah;
            if($nasabah['status'] == '7'){
                $dataview['appointment'] = $this->Mnasabah->getAppointment($id);
            }
            $this->load->view('v_donevalid_nasabah', $dataview);
        }else {
            $this->load->view("v_notfound_nasabah");
        }
    }

    public function do_verify($id){
        $nasabah = json_decode($_POST['nasabah'], true);
        $detail_nasabah = json_decode($_POST['detail_nasabah'], true);
        $newnasabah['status'] = '4';
        $newnasabah['up_time'] = date('Y-m-d H:i:s');
        $newnasabah['up_user'] = $this->session->userdata('username');
        $update = $this->db->update('nasabah', $newnasabah, array('id' => $detail_nasabah['id_nasabah']));
        if ($update){
            $this->_insertlog("validasi data atas nama : ". $nasabah['nama'] ."melalui Dashboard Admin BERHASIL");
            echo json_encode(array("status" => "1", "msg" => "Verifikasi berhasil. <br/> silahkan data calon nasabah"));
        } else {
            echo json_encode(array("status" => "1", "msg" => "Verifikasi Gagal. Kesalahan Sistem"));
        }

    }


    public function validate($id){
        $nasabah = $this->Mnasabah->getnasabahbyid($id);
        $newnasabah = array();
        $res = "";
        if ($nasabah > 0){
            if ($nasabah['status'] === '1') {
                $dukcapil = $this->checkdukcapil($nasabah['nik']);
                $dataview ['dukcapil'] = $dukcapil;
                $data = array (
                    "cr_time" => date('Y-m-d H:i:s'),
                    "cr_user" => $this->session->userdata('username'),
                    "up_time" => date('Y-m-d H:i:s'),
                    "rc" => $dukcapil->rc,
                    "rm" => $dukcapil->rm,
                    "definition" => json_encode($dukcapil)
                );
                $this->db->insert('dukcapil_log', $data);
                if ($dukcapil->rc === '00'){
                    $dataview ['nasabah'] = $nasabah;
                    if (strtolower($dukcapil->NAMA_LGKP) === strtolower($nasabah['nama'])){
                        $dataview ['isName'] = true;
                        $dataview ['id_nasabah'] = $id;
                        // $this->_do_validate($nasabah, $dukcapil, $id);
                        // $newnasabah['status'] = '3';
                        // $newnasabah['link_verifikasi'] = 'dashboard.bprnusambacepiring.com/public/home/verifikasi/'.$nasabah['id_product'].'/'.$id.'';
                    } else {
                        $dataview ['isName'] = false;
                        $dataview ['id_nasabah'] = $id;
                        // $newnasabah['status'] = '6';
                        // $newnasabah['up_time'] = date('Y-m-d H:i:s');
                        // $newnasabah['up_user'] = $this->session->userdata('username');
                        // $this->db->update('nasabah', $newnasabah, array('id' => $id));
                    }
                    $this->load->view("v_valid_nasabah", $dataview);
                } else {
                    $dataview ['nasabah'] = $nasabah;
                    $this->load->view("v_notvalid_nasabah", $dataview);
                    if ($dukcapil->rc !== '50'){
                        $nasabah['status'] = '6';
                        $nasabah['up_time'] = date('Y-m-d H:i:s');
                        $nasabah['up_user'] = $this->session->userdata('username');
                        $this->db->update('nasabah', $nasabah, array('id' => $id));    
                    }
                }
            } else {
                $dataview ['nasabah'] = $nasabah;
                $this->load->view('v_donevalid_nasabah', $dataview);
            }
        } else {
            $this->load->view("v_notfound_nasabah");
        }
        
    }

    // public function do_validate($id_nasabah){
    //     // map_y($id_nasabah);
    //     $nasabah = json_decode($_POST['nasabah'], true);
    //     // print_r($nasabah);
    //     $dukcapil = json_decode($_POST['dukcapil']);
    //     // print_r($dukcapil);
    //     $data = array(
    //         'id_nasabah' => isset($id_nasabah) ? $id_nasabah : 0,
    //         'kk' => isset($dukcapil->NO_KK) ? $dukcapil->NO_KK : null ,
    //         'tempat_lahir' => isset($dukcapil->TMPT_LHR) ? $dukcapil->TMPT_LHR : null ,
    //         'tanggal_lahir' => isset($dukcapil->TGL_LHR) ? date_ind_to_eng($dukcapil->TGL_LHR) : null,
    //         'jenis_kelamin' => isset($dukcapil->JENIS_KLMIN) ? $dukcapil->JENIS_KLMIN : null ,
    //         'pekerjaan' => isset($dukcapil->JENIS_PKRJN) ? $dukcapil->JENIS_PKRJN : null ,
    //         'status_kawin' => isset($dukcapil->STATUS_KAWIN) ? $dukcapil->STATUS_KAWIN : null ,
    //         'agama' => isset($dukcapil->AGAMA) ? $dukcapil->AGAMA : null ,
    //         'nama_ibu' => isset($dukcapil->NAMA_LGKP_IBU) ? $dukcapil->NAMA_LGKP_IBU : null ,
    //         'alamat' => isset($dukcapil->ALAMAT) ? $dukcapil->ALAMAT : null ,
    //         'rt' => isset($dukcapil->NO_RT) ? $dukcapil->NO_RT : null ,
    //         'rw' => isset($dukcapil->NO_RW) ? $dukcapil->NO_RW : null ,
    //         'kecamatan' => isset($dukcapil->KEC_NAME) ? $dukcapil->KEC_NAME : null ,
    //         'kelurahan' => isset($dukcapil->KEL_NAME) ? $dukcapil->KEL_NAME : null ,
    //         'kota' => isset($dukcapil->KAB_NAME) ? $dukcapil->KAB_NAME : null ,
    //         'provinsi' => isset($dukcapil->PROP_NAME) ? $dukcapil->PROP_NAME : null ,
    //         'kode_pos' => isset($dukcapil->KODE_POS) ? $dukcapil->KODE_POS : null ,
    //         'cr_time' => date('Y-m-d H:i:s'),
    //         'cr_user' => $this->session->userdata('username'),
    //         'up_time' => date('Y-m-d H:i:s'),
    //     );
    //     $this->db->insert('detail_nasabah', $data);
    //     $this->_insertlog("validasi data atas nama : ". $nasabah['nama']);

    //     $no_hp = $nasabah['no_tlp'];
    //     $link_pertemuan = base_url().'public/home/new_appointment/'.$nasabah['id_product'].'/'.$id_nasabah;
    //     $link_tracking = $nasabah['link_tracking'];

    //     /**
    //      * Send SMS
    //      */
    //     $sms_message = "Bpk/Ibu ".$nasabah['nama'].", Pengajuan ".$nasabah['name']." di BPR NUSAMBA CEPIRING sudah divalidasi, lengkapi formulir pertemuan, cek link di WA dan EMAIL. Terima Kasih";
    //     send_sms($no_hp, $sms_message);
        
    //     /**
    //      * Send WA
    //      */
    //     $wa_message = "Pengajuan atas nama ".$nasabah['nama']." untuk produk ".$nasabah['name']." berhasil di validasi, mudahnya ketemu dengan tim kami dengan klik link lalu isi : ".$link_pertemuan."
    //     Anda bisa menentukan jadwal ketemu dengan tim kami untuk melengkapi data & mempermudah proses pengajuan Anda.

    //     BPR Nusamba Cepiring, sistem DIGITAL WEB BRANCH kami memudahkan Anda.
    //     - Pengajuan Deposito Pinjaman Cepat Cair
    //     - Pengajuan Deposito Berjangka Mantap
    //     - Pengajuan pembukaan Tabungan menguntungkan

    //     Tim kami siap melayani Anda via:
    //     - Livechat di website
    //     - WhatsApp wa.me/62294382234
    //     - Telp 0294-382234
    //     - SMS  08562050083
    //     - Email info@bprnusambacepiring.com

    //     Rasakan kemudahan, kenyamanan, dan keamanan sistem kami Digital Web Branch

    //     Digital Web Branch 
    //     *BPR NUSAMBA CEPIRING* 
    //     ".base_url()."";
        
    //     send_wa($no_hp, $wa_message);
    //     /**
    //      * Send Email
    //      */
    //     $email_message = "<p style='text-indent: 20px' >Pengajuan atas nama ".$nasabah['nama']." untuk produk ".$nasabah['name']." berhasil di validasi, mudahnya ketemu dengan tim kami dengan klik:<p style='text-align: center;'><a href='".$link_pertemuan."' style='border:none; color: white; background: green; padding: 10px'>BUAT JANJI PERTEMUAN</a></p> .<br></p><p style='text-indent: 20px'>Anda bisa menentukan jadwal ketemu dengan tim kami untuk melengkapi data & mempermudah proses pengajuan Anda.</p>";
    //     $email_message .= "<br><p>Klik untuk melihat status pengajuan anda:</p> <p style='text-align: center;'><a href='".$link_tracking."' style='border:none; color: white; background: green; padding: 10px'>TRACKING</a></p>";
        
    //     // $email_message .= " <p>BPR Nusamba Cepiring, sistem DIGITAL WEB BRANCH kami memudahkan Anda.</p>
    //     // <p>- Pengajuan Deposito Pinjaman Cepat Cair</p>
    //     // <p>- Pengajuan Deposito Berjangka Mantap</p>
    //     // <p>- Pengajuan pembukaan Tabungan menguntungkan</p>

    //     // <p>Tim kami siap melayani Anda via:</p>
    //     // <p>- Livechat di website</p>
    //     // <p>- WhatsApp wa.me/62294382234</p>
    //     // <p>- Telp 0294-382234</p>
    //     // <p>- SMS  08562050083</p>
    //     // <p>- Email info@bprnusambacepiring.com</p>

    //     // <p>Rasakan kemudahan, kenyamanan, dan keamanan sistem kami Digital Web Branch</p>

    //     // <p>Digital Web Branch </p>
    //     // <p>*BPR NUSAMBA CEPIRING*</p>
    //     // ".base_url();

    //     $data['content'] = $email_message;
    //     $html_message = $this->load->view('email_template', $data, TRUE);
        
    //     /**
    //      * Send Mail
    //      */
    //     send_email($nasabah['email'], $html_message);

    //     $updateNasabah['status'] = '3';
    //     // $updateNasabah['link_verifikasi'] = $link_verifikasi;
    //     $updateNasabah['up_time'] = date('Y-m-d H:i:s');
    //     $updateNasabah['up_user'] = $this->session->userdata('username');
    //     $insert = $this->db->update('nasabah', $updateNasabah, array('id' => $id_nasabah));
        
    //     /**
    //      * Send Response
    //      */
    //     if($insert){
    //         echo json_encode(array("status" => "1", "msg" => "Validasi berhasil dilakukan. <br/> Silahkan periksa SMS, Email, dan Whatsapp untuk informasi lebih lanjut."));
    //     }else{
    //         echo json_encode(array("status" => "1", "msg" => "Validasi gagal dilakukan. <br/>"));
    //     }
    // }

    private function getMacAddress (){
        $ipAddress=$_SERVER['REMOTE_ADDR'];
        ob_start();
        system('arp -an');
        $mycom=ob_get_contents();
        ob_clean();
         
        $findme = "Physical";
        $pmac = strpos($mycom, $findme);
         
        $mac=substr($mycom,($pmac+19),17);
        return $mac ? $mac : $ipAddress;
    }

    public function checkdukcapil($nik){
        $username = $this->session->userdata('username');
        $macAddress = $this->getMacAddress();
        $res_body = request_dukcapil($nik, $username, $macAddress);
        return $res_body;
    }

    // private function smssend($link_verifikasi, $nasabah){
    //     $userkey = '9kn7pv';
    //     $passkey = 'aw00e9tjfa';
    //     $message = "Bpk/Ibu ".$nasabah['nama'].", Pengajuan ".$nasabah['name']." di BPR NUSAMBA CEPIRING sdh divalidasi, lengkapi appointment, cek link di WA dan EMAIL. Trims";
    //     $client = new GuzzleHttp\Client([
    //         'base_uri' => URL_SMSAPI,
    //     ]);
    //     $response = $client->request('POST', 'apps/smsapi.php',[
    //         'query' => [
    //             'userkey' => $userkey,
    //             'passkey' => $passkey,
    //             'nohp' => '+'.$nasabah['no_tlp'],
    //             'pesan' => $message
    //         ]
    //     ]);

    //     $results = $response->getBody()->getContents();
    //     $XMLdata = new SimpleXMLElement($results);
    //     $data = array(
    //         'cr_time' => date('Y-m-d H:i:s'),
    //         'cr_user' => $this->session->userdata('username'),
    //         'up_time' => date('Y-m-d H:i:s'),
    //         'status' => $XMLdata->message[0]->text,
    //         'statuscode' => $XMLdata->message[0]->status,
    //         'notlp' => $nasabah['no_tlp'],
    //         'message' => $message,
    //         'definition' => $results
    //     );
    //     $this->db->insert('sms_log', $data);
    // }

    // private function emailsend($link_verifikasi, $nasabah){
    //     $from_email = "bprnusambacepiring.com@gmail.com"; 
    //     $to_email = $nasabah['email']; 

    //      $config = Array(
    //             'protocol' => 'sendmail',
    //             // 'protocol' => 'smtp',
    //             'smtp_host' => 'ssl://smtp.googlemail.com',
    //             'smtp_user' => $from_email,
    //             'smtp_pass' => 'cepiring123',
    //             'smtp_port' => 465,
    //             'wordwrap' => TRUE,
    //             'wrapchars' => 76,
    //             'mailtype'  => 'html',
    //             'charset'   => 'iso-8859-1',
                
    //     );
    //      $data_email = array(
    //         'nasabah' => $nasabah,
    //         'link_verifikasi' => $link_verifikasi
    //      );
    //     $message = $this->load->view('v_send_email', $data_email, TRUE);

    //     $this->load->library('email', $config);
    //     $this->email->set_newline("\r\n");   

    //     $this->email->from($from_email, 'BPR Nusamba Cepiring'); 
    //     $this->email->to($nasabah['email']);
    //     $this->email->subject('Validasi data pengajuan'); 
    //     $this->email->message($message); 
        
    //     if ($this->email->send()) {
    //         $data = array(
    //             'cr_time' => date('Y-m-d H:i:s'),
    //             'cr_user' => $this->session->userdata('username'),
    //             'up_time' => date('Y-m-d H:i:s'),
    //             'status' => 'sukses',
    //             'statuscode' => '0',
    //             'email' => $to_email,
    //             'message' => $message,
    //             // 'definition' => $results
    //         );
    //     }else {
    //         $data = array(
    //             'cr_time' => date('Y-m-d H:i:s'),
    //             'cr_user' => $this->session->userdata('username'),
    //             'up_time' => date('Y-m-d H:i:s'),
    //             'status' => "gagal",
    //             'statuscode' => '1',
    //             'email' => $to_email,
    //             'message' => $message,
    //             // 'definition' => $results
    //         );
    //     }
    //     $this->db->insert('mail_log', $data);

    // }

    // public function wasend($link_verifikasi, $nasabah){
    //     $nohp = $nasabah['no_tlp'];
    //     $message = "Pengajuan atas nama ".$nasabah['nama']." untuk product ".$nasabah['name']." berhasil di validasi, mudahnya ketemu dengan tim kami dengan isi klik link : $link_verifikasi 
    //     Anda bisa menentukan jadwal ketemu dengan tim kami untuk melengkapi data & mempermudah proses pengajuan Anda.

    //     BPR Nusamba Cepiring, sistem DIGITAL WEB BRANCH kami memudahkan Anda.
    //     - Pengajuan Deposito Pinjaman Cepat Cair
    //     - Pengajuan Deposito Berjangka Mantap
    //     - Pengajuan pembukaan Tabungan menguntungkan

    //     Tim kami siap melayani Anda via:
    //     - Livechat di website
    //     - WhatsApp wa.me/62294382234
    //     - Telp 0294-382234
    //     - SMS  08562050083
    //     - Email info@bprnusambacepiring.com

    //     Rasakan kemudahan, kenyamanan, dan keamanan sistem kami Digital Web Branch

    //     Digital Web Branch 
    //     *BPR NUSAMBA CEPIRING*
    //     dev.nusambacepiring.com";
        
    //     $token = "GCEbwTjUrZTN8DlH6dHA7VJpAKFf1jFKg5qqbxYq9rdYZk4hQX2znRuQWm1PS4sj"; //from wablas.com
    //     $client = new GuzzleHttp\Client([
    //         'base_uri' => URL_WA,
    //     ]);
    //     $response = $client->request('POST','api/send-message',[
    //         'headers' => [
    //             'Authorization' => $token
    //         ],
    //         'query' => [
    //             'phone' => $nohp,
    //             'message' => $message,
    //         ]
    //     ]);

    //     $res_body = $response->getBody()->getContents();
    //     $res_body = json_decode($res_body);
    //     $data = array(
    //         'cr_time' => date('Y-m-d H:i:s'),
    //         'cr_user' => $this->session->userdata('username'),
    //         'up_time' => date('Y-m-d H:i:s'),
    //         'status' => $res_body->message,
    //         'statuscode' => $res_body->status,
    //         'notlp' => $nasabah['no_tlp'],
    //         'message' => $message,
    //         'definition' => json_encode($res_body)
    //     );
    //     $this->db->insert('wa_log', $data);
    //     // print_r($res_body);
    // }

    public function printSub($id){
        $nasabah = $this->Mnasabah->getnasabahbyid($id);
        $detail_nasabah = $this->Mnasabah->getdetailnasabahbyid($id);
        $header_form = $this->Mnasabah->getheaderform($nasabah['id_product']);
        $appointment = $this->Mnasabah->getAppointment($id);
        $data = array(
            'nasabah' => $nasabah,
            'detail_nasabah' => $detail_nasabah,
            'header_form' => $header_form,
            'appointment' => $appointment 
        );
        $print = $this->load->view("v_print_pengajuan", $data, true);

        $this->load->library('Html2pdf');
        // $this->html2pdf->initialize();
        $this->html2pdf->paper('A4','potrait');
        $this->html2pdf->filename($nasabah['name'].'-'.$detail_nasabah['id_nasabah'].".pdf");
        $this->html2pdf->html($print);
        $this->html2pdf->folder($_SERVER['DOCUMENT_ROOT'].'/upload/pdf/');
        $this->html2pdf->create();
        return null;
    }

    private function _insertlog($definition){
        $data = array (
            'definition' => $definition,
            'cr_time' => date('Y-m-d H:i:s'),
            'up_time' => date('Y-m-d H:i:s'),
            'cr_user' => $this->session->userdata('username'),
        );
        $insert = $this->db->insert('trans_log', $data);
    }

    /**
     * View Validate   
     */
    public function validasi($id){
        // map_y($id);
        $this->theme_module = '';
        $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
        $this->_theme_vars['theme'] = $this->_theme_vars['active_admin_theme'];
        $this->_theme_vars['page_title'] = 'Nasabah';
        $this->_theme_vars['page_subtitle'] = 'Validasi';
        $this->_theme_vars['current_class_dir'] = $this->router->fetch_directory();
        $this->_theme_vars['current_class'] = $this->router->fetch_class();
        $this->_theme_vars['permissions'] = $this->_get_permissions();
        $this->_theme_vars['parent_menu'] = 'nasabah';
        $this->_theme_vars['submenu'] = 'deposito';
        
        $data['dukcapil_url_map'] = base_url('nasabah/deposito/dukcapil_map/'.$id);
        $data['url_reject'] = base_url('nasabah/deposito/form_reject/'.$id);
        $data['url_validate'] = base_url('nasabah/deposito/do_validate/'.$id);
        // map_y($data);
        $data['detail_nasabah'] = $this->Mnasabah->detailNasabah($id);
        // map_y($data);
        $this->render_admin_view('view/validasi','', $data);
    }

    public function do_validate($id_nasabah){
        // map_y($_REQUEST);
        // map_y($id_nasabah);
        $nasabah = json_decode($_REQUEST['nasabah']);
        $dukcapil = isset($_REQUEST['dukcapil']) ? json_decode($_REQUEST['dukcapil']) : '';
        // map_y($nasabah);

        $data = [
            'id_nasabah' => isset($id_nasabah) ? $id_nasabah : 0,
            'kk' => isset($dukcapil->NO_KK) ? $dukcapil->NO_KK : null ,
            'tempat_lahir' => isset($dukcapil->TMPT_LHR) ? $dukcapil->TMPT_LHR : null ,
            'tanggal_lahir' => isset($dukcapil->TGL_LHR) ? date_ind_to_eng($dukcapil->TGL_LHR) : null,
            'jenis_kelamin' => isset($dukcapil->JENIS_KLMIN) ? $dukcapil->JENIS_KLMIN : null ,
            'pekerjaan' => isset($dukcapil->JENIS_PKRJN) ? $dukcapil->JENIS_PKRJN : null ,
            'status_kawin' => isset($dukcapil->STATUS_KAWIN) ? $dukcapil->STATUS_KAWIN : null ,
            'agama' => isset($dukcapil->AGAMA) ? $dukcapil->AGAMA : null ,
            'nama_ibu' => isset($dukcapil->NAMA_LGKP_IBU) ? $dukcapil->NAMA_LGKP_IBU : null ,
            'alamat' => isset($dukcapil->ALAMAT) ? $dukcapil->ALAMAT : isset($nasabah->alamat_lengkap_ktp) ? $nasabah->alamat_lengkap_ktp : null ,
            'rt' => isset($dukcapil->NO_RT) ? $dukcapil->NO_RT : null ,
            'rw' => isset($dukcapil->NO_RW) ? $dukcapil->NO_RW : null ,
            'kecamatan' => isset($dukcapil->KEC_NAME) ? $dukcapil->KEC_NAME : isset($nasabah->kecamatan_ktp) ? $nasabah->kecamatan_ktp : null  ,
            'kelurahan' => isset($dukcapil->KEL_NAME) ? $dukcapil->KEL_NAME : isset($nasabah->kelurahan_ktp) ? $nasabah->kelurahan_ktp : null  ,
            'kota' => isset($dukcapil->KAB_NAME) ? $dukcapil->KAB_NAME : isset($nasabah->kabkot_ktp) ? $nasabah->kabkot_ktp : null  ,
            'provinsi' => isset($dukcapil->PROP_NAME) ? $dukcapil->PROP_NAME : isset($nasabah->provinsi_ktp) ? $nasabah->provinsi_ktp : null  ,
            'kode_pos' => isset($dukcapil->KODE_POS) ? $dukcapil->KODE_POS : isset($nasabah->kode_pos_ktp) ? $nasabah->kode_pos_ktp : null  ,
            'cr_time' => date('Y-m-d H:i:s'),
            'cr_user' => $this->session->userdata('username'),
            'up_time' => date('Y-m-d H:i:s'),
        ];

        // map_y($data);
        $nasabah = (Array) $nasabah;

        $this->db->insert('detail_nasabah', $data);
        $this->_insertlog("validasi data atas nama : ". $nasabah['nama']);

        $no_hp = $nasabah['no_tlp'];
        $base_url = base_url();
        
        if($base_url == "http://dashboard.bprnusambacepiring.com/" || $base_url == 'https://dashboard.bprnusambacepiring.com/'){
            $base_url = "http://bprnusambacepiring.com/";
        }

        $link_pertemuan = $base_url.'public/home/new_appointment/'.$nasabah['id_product'].'/'.$id_nasabah;
        $link_tracking = $nasabah['link_tracking'];

        /**
         * Send SMS
         */
        $sms_message = "Bpk/Ibu ".$nasabah['nama'].", Pengajuan ".$nasabah['name']." di BPR NUSAMBA CEPIRING sudah divalidasi, lengkapi formulir pertemuan, cek link di WA dan EMAIL. Terima Kasih";
        send_sms($no_hp, $sms_message);
        
        /**
         * Send WA
         */
        $wa_message = "Pengajuan atas nama ".$nasabah['nama']." untuk produk ".$nasabah['name']." berhasil di validasi, mudahnya ketemu dengan tim kami dengan klik link lalu isi : ".$link_pertemuan."

        Anda bisa menentukan jadwal ketemu dengan tim kami untuk melengkapi data & mempermudah proses pengajuan Anda.


        BPR Nusamba Cepiring, sistem DIGITAL WEB BRANCH kami memudahkan Anda.
        - Pengajuan Deposito Pinjaman Cepat Cair
        - Pengajuan Deposito Berjangka Mantap
        - Pengajuan pembukaan Tabungan menguntungkan


        Tim kami siap melayani Anda via:
        - Livechat di website
        - WhatsApp wa.me/62294382234
        - Telp 0294-382234
        - SMS  08562050083
        - Email info@bprnusambacepiring.com

        Rasakan kemudahan, kenyamanan, dan keamanan sistem kami Digital Web Branch

        Digital Web Branch 
        *BPR NUSAMBA CEPIRING* 
        ".$base_url;
        
        send_wa($no_hp, $wa_message);
        /**
         * Send Email
         */
        $email_message = "<p style='text-indent: 20px' >Pengajuan atas nama ".$nasabah['nama']." untuk produk ".$nasabah['name']." berhasil di validasi, mudahnya ketemu dengan tim kami dengan klik:<p style='text-align: center;'><a href='".$link_pertemuan."' style='border:none; color: white; background: green; padding: 10px'>BUAT JANJI PERTEMUAN</a></p> .<br></p><p style='text-indent: 20px'>Anda bisa menentukan jadwal ketemu dengan tim kami untuk melengkapi data & mempermudah proses pengajuan Anda.</p>";
        $email_message .= "<br><p>Klik untuk melihat status pengajuan anda:</p> <p style='text-align: center;'><a href='".$link_tracking."' style='border:none; color: white; background: green; padding: 10px'>TRACKING</a></p>";

        $data['content'] = $email_message;
        $html_message = $this->load->view('email_template', $data, TRUE);
        
        /**
         * Send Mail
         */
        send_email($nasabah['email'], $html_message);

        $updateNasabah['status'] = '3';
        // $updateNasabah['link_verifikasi'] = $link_verifikasi;
        $updateNasabah['up_time'] = date('Y-m-d H:i:s');
        $updateNasabah['up_user'] = $this->session->userdata('username');
        $insert = $this->db->update('nasabah', $updateNasabah, array('id' => $id_nasabah));
        
        /**
         * Send Response
         */
        if($insert){
            echo json_encode(array("status" => "1", "msg" => "Validasi berhasil dilakukan. <br/> Silahkan periksa SMS, Email, dan Whatsapp untuk informasi lebih lanjut."));
        }else{
            echo json_encode(array("status" => "0", "msg" => "Validasi gagal dilakukan. <br/>"));
        }
    }

    /**
     * Check Dukcapil
     */
    public function dukcapil_map($id){
        $nasabah = $this->Mnasabah->getnasabahbyid($id);
        $uid_nasabah = $this->Mnasabah->getnasabahUID($id);
        
        $newnasabah = array();
        $res = "";
        if ($nasabah > 0){
            if ($nasabah['status'] === '1') {
                /* Get Data Dukcapil */
                $dukcapil = $this->Mnasabah->getDukcapilByUidNasabah($uid_nasabah);
                if(!$dukcapil){
                    $dukcapil = $this->checkdukcapil($nasabah['nik']);

                    /**
                     * Save Dukcapil Data to LOG
                     */
                    $data = [
                        "cr_time" => date('Y-m-d H:i:s'),
                        "cr_user" => $this->session->userdata('username'),
                        "up_time" => date('Y-m-d H:i:s'),
                        "rc" => $dukcapil->rc,
                        "rm" => $dukcapil->rm,
                        "definition" => json_encode($dukcapil),
                    ];
                    $this->db->insert('dukcapil_log', $data);

                    if($dukcapil->rc === '00'){
                        /* Save to History */
                        $_dhis['uid_nasabah'] = $uid_nasabah;
                        $_dhis['res_body'] = json_encode($dukcapil);
                        $this->db->insert('dukcapil_history', $_dhis);
                    }
                }

                $dataview['dukcapil'] = $dukcapil;

                if($dukcapil->rc === '00'){
                    $dataview['nasabah'] = $nasabah;
                    
                    if(strtolower($dukcapil->NAMA_LGKP) === strtolower($nasabah['nama'])){
                        $dataview['isName'] = true;
                        $dataview['id_nasabah'] = $id;
                    }else{
                        $dataview['isName'] = false;
                        $dataview['id_nasabah'] = $id;
                    }

                    $this->load->view("v_validasi_nasabah", $dataview);
                } else {
                    $dataview ['nasabah'] = $nasabah;
                    $this->load->view("v_notvalid_nasabah", $dataview);
                    if ($dukcapil->rc !== '50'){
                        $nasabah['status']= '6';
                        $nasabah['up_time'] = date('Y-m-d H:i:s');
                        $nasabah['up_user'] = $this->session->userdata('username');
                        $this->db->update('nasabah', $nasabah, array('id' => $id));    
                    }
                }
            } else {
                $dataview ['nasabah'] = $nasabah;
                if($nasabah['status'] == '7'){
                    $dataview['appointment'] = $this->Mnasabah->getAppointment($id);
                }
                $this->load->view('v_donevalid_nasabah', $dataview);
            }
        } else {
            $this->load->view("v_notfound_nasabah");
        }
        
    }

    /**
     * Form Reject
     */
    public function form_reject($id){
        $data['url_reject'] = base_url('nasabah/deposito/reject/'.$id);
        $data['produk'] = $this->Mnasabah->get_product($id);
        
        $this->load->view('v_penolakan', $data);
    }
    /**
     * Form Accept
     */
    public function form_accept($id){
        $data['url_reject'] = base_url('nasabah/deposito/accept/'.$id);
        $data['produk'] = $this->Mnasabah->get_product($id);

        $this->load->view('v_penerimaan', $data);
    }
    /**
     * Form Ketemuan
     */
    public function  form_pertemuan($id){
        $data['url_pertemuan'] = base_url('nasabah/deposito/pertemuan/'.$id);
        $this->load->view('v_pertemuan', $data);
    }
    /**
     * Reject Handler
     */
    public function reject($id){
        $request = $_REQUEST;
        $data['status'] = '8';
        $data['keterangan'] = $request['keterangan'];
        $data['up_time'] = date('Y-m-d H:i:s');
        $data['up_user'] = $_SESSION['username'];

        $this->db->where('id', $id);
        $update = $this->db->update('nasabah', $data);
        if($update){
            $nasabah = $this->Mnasabah->detailNasabah($id);
            $message = 'Maaf Pengajuan anda di BPR Nusamba Cepiring tidak di dapat setuju dengan alasan ';
            $message .= $data['keterangan'];
            $message .= ' Silahkan datang ke kantor BPR Nusamba Cepiring kantor kas (sesuai dengan kantor AO yang memproses) / Lakukan pengajuan ulang dengan data yang valid';
            
            $data['content'] = '<p>'.$message.'</p>';

            $html_message = $this->load->view('email_template', $data, TRUE);

            $no_hp = $nasabah['no_tlp'];
            $email = $nasabah['email'];

            send_wa($no_hp, $message);
            send_sms($no_hp, $message);
            send_email($email, $html_message );

            $res =  [
                'status' => '1',
                'msg' => "Nasabah Tidak Disetujui"
            ];
        }else{
            $res =  [
                'status' => '0',
                'msg' => "Maaf Terjadi Kesalahan"
            ];
        }
        return response_json($res, 200);
    }

    /**
     * Approve 
     */
    public function accept($id){
        $request = $_REQUEST;
        $data['status'] = '2';
        $data['keterangan'] = $request['keterangan'];
        $data['up_time'] = date('Y-m-d H:i:s');
        $data['up_user'] = $_SESSION['username'];

        $this->db->where('id', $id);
        $update = $this->db->update('nasabah', $data);
        if($update){
            $nasabah = $this->Mnasabah->detailNasabah($id);
            $message = 'Selamat Pengajuan anda di BPR Nusamba Cepiring dapat setuju dengan ';
            $message .= $data['keterangan'];
            $message .= ' Silahkan datang ke kantor BPR Nusamba Cepiring kantor kas (sesuai dengan kantor AO yang memproses)';
            
            $data['content'] = '<p>'.$message.'</p>';

            $html_message = $this->load->view('email_template', $data, TRUE);

            $no_hp = $nasabah['no_tlp'];
            $email = $nasabah['email'];

            send_wa($no_hp, $message);
            send_sms($no_hp, $message);
            send_email($email, $html_message );
            
            $res =  [
                'status' => '1',
                'msg' => "Nasabah Disetujui"
            ];
        }else{
            $res =  [
                'status' => '0',
                'msg' => "Maaf Terjadi Kesalahan"
            ];
        }
        return response_json($res, 200);
    }
    
    /**
     * Pertemuan 
     */
    public function pertemuan($id){
        $request = $_REQUEST;

        $detail_nasabah = $this->Mnasabah->detailNasabah($id);

        $data['status'] = '7';
        $data['up_time'] = date('Y-m-d H:i:s');
        $data['up_user'] = $_SESSION['username'];
        
        $tanggal = $_POST['tanggal_appoiment'];
        $waktu = $_POST['waktu_appoiment'];

        $new_appo['cr_time'] = date('Y-m-d H:i:s');
        $new_appo['no_pertemuan'] = gen_no_pertemuan($id, $detail_nasabah['id_product']);
        $new_appo['cr_user'] = $_SESSION['username'];
        $new_appo['up_time'] = date('Y-m-d H:i:s');
        $new_appo['id_nasabah'] = $id;
        $new_appo['lat'] = $_POST['lat'];
        $new_appo['lng'] = $_POST['lng'];
        $new_appo['tanggal_appoiment'] = $tanggal;
        $new_appo['waktu_appoiment'] = $waktu;
        $new_appo['lokasi'] = $_POST['lokasi'];
        $new_appo['id_sales'] = $detail_nasabah['sales_id'];

        $this->db->where('id', $id);
        $update = $this->db->update('nasabah', $data);
        if($update){
            $this->db->where('id', $id);
            $insert = $this->db->insert('appoiment', $new_appo);
            $message = 'Tim kami telah membuat jadwal pertemuan dengan Anda, silahkan cek menu tracking untuk melihat detailnya.';
            
            $data['content'] = '<p>'.$message.'</p>';
            $data['content'] .= "<br><p>Klik untuk melihat status pengajuan anda:</p> <p style='text-align: center;'><a href='".$detail_nasabah['link_tracking']."' style='border:none; color: white; background: green; padding: 10px'>TRACKING</a></p>";  
            $html_message = $this->load->view('email_template', $data, TRUE);

            $no_hp = $detail_nasabah['no_tlp'];
            $email = $detail_nasabah['email'];

            $message .= $detail_nasabah['link_tracking'];

            send_wa($no_hp, $message);
            send_sms($no_hp, $message);
            send_email($email, $html_message );
            
            $res =  [
                'status' => '1',
                'msg' => "Perjanjian telah dibuat"
            ];
        }else{
            $res =  [
                'status' => '0',
                'msg' => "Maaf Terjadi Kesalahan"
            ];
        }
        return response_json($res, 200);
    }
    
    /**
     * Form Pindah Sales
     */
    public function form_assign($id){
        $data['url_assign'] = base_url('nasabah/deposito/assign/'.$id);
        $this->load->view('v_assign', $data);
    }

    /**
     * Pindah Sales
     */
    public function assign($id){
        $request = $_REQUEST;

        $detail_nasabah = $this->Mnasabah->detailNasabah($id);
    
        $data['sales_id'] = $request['sales_id'];
        $data['give_from_sales'] = 1;
        $this->db->where('id', $id);
        $update = $this->db->update('nasabah', $data);
        // map_y($update);
        if($update){
            $new_['from_sales_id'] = $detail_nasabah['sales_id'];
            $new_['to_sales_id'] = $request['sales_id'];
            $new_['assign_by_user_id'] = $_SESSION['id'];
            $new_['assign_date'] = date('Y-m-d H:i:s');/*  */
            $new_['keterangan'] = $request['keterangan'];/*  */
            $this->db->insert('n_assign_history', $new_);

            $message = 'Perubahan Tim Bisnis.';
            
            $data['content'] = '<p>'.$message.'</p>';
            $data['content'] .= "<br><p>Klik untuk melihat status pengajuan anda:</p> <p style='text-align: center;'><a href='".$detail_nasabah['link_tracking']."' style='border:none; color: white; background: green; padding: 10px'>TRACKING</a></p>";  
            $html_message = $this->load->view('email_template', $data, TRUE);

            $no_hp = $detail_nasabah['no_tlp'];
            $email = $detail_nasabah['email'];

            $message .= $detail_nasabah['link_tracking'];

            send_wa($no_hp, $message);
            send_sms($no_hp, $message);
            send_email($email, $html_message );
            
            $res =  [
                'status' => '1',
                'msg' => "Berhasil"
            ];
        }else{
            $res =  [
                'status' => '0',
                'msg' => "Maaf Terjadi Kesalahan"
            ];
        }

        return response_json($res, 200);
    }

    /**
     * Form Report
     */
    public function form_report($id){
        $data['url_report'] = base_url('nasabah/deposito/report/'.$id);
        $this->load->view('v_report', $data);
    }

    /**
     * Report
     */
    public function report($id){
        $html = '';
        
        if($_GET['type'] == 'pertemuan'){
            $data['appointment'] = $this->Mnasabah->laporanPertemuan($id);
            $html = $this->load->view('v_report_pertemuan', $data, TRUE);
            // map_y($data);
        }else if($_GET['type'] == 'detail_nasabah'){
            $data = $this->Mnasabah->laporanDetailNasabah($id);
            $html = $this->load->view('v_report_detail', $data, TRUE);
            // map_y($data);
        }

        /**
         * Start Print
         */
        return print_pdf('Hai', $html);
    }
    
}