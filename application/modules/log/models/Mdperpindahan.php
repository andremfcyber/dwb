<?php

class Mdperpindahan extends MY_Model implements DatatableModel{

    function __construct(){
        parent::__construct();
        $this->load->library('mcore');
    }
    
    public function appendToSelectStr() {

    }

    public function fromTableStr() {
        return "n_assign_history b";
    }

    public function joinArray(){
        // return null;
        return array(
            "sales c |left" => "b.from_sales_id = c.id",
            "sales d |left" => "b.to_sales_id = d.id",
            "aauth_users e |left" => "b.assign_by_user_id = e.id",  
        );
    }

    public function whereClauseArray(){
        return null;
    }


}