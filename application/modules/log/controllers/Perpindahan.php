<?php

class Perpindahan extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mperpindahan");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "n_assign_history";
        $this->dttModel = "MdPerpindahan";
        $this->pk = "id";

    }

    function index(){
        
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Log";
        $data['page_subtitle'] = "Riwayat Perpindahan";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();
    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){
        return array(
            'parent_menu' => 'log_view', 
            'submenu' => 'perpindahan_nasabah_view' 
        );
    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_delete"),
            "detail_perm" => $this->mcore->checkPermission($this->user_group, "nasabah2_detail"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(
                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(
                    "columns" => ':visible'
                )
            ),
            array(
                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(
                    "columns" => ':visible'
                )
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."nasabah/nasabah/add';}"

            // ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(
            "nama_ao_dari" => array(
                "data" => "c.nama_sales",
                "searchable" => true,
                "orderable" => true,
            ),
            "nama_ao_ke" => array(
                "data" => "d.nama_sales",
                "searchable" => true,
                "orderable" => true,
            ),
            "nama_user" => array(
                "data" => "d.nama_sales",
                "searchable" => true,
                "orderable" => true,
            ),
            "keterangan" => array(
                "data" => "b.keterangan",
                "searchable" => true,
                "orderable" => true,
            ),
        );

    }

}