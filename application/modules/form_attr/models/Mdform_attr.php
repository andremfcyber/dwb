<?php

class Mdform_attr extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'form_attr_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'form_attr_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'form_attr_detail');


    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';

        // if($this->allow_detail){
        //     $detail = '<a class="btn btn-sm btn-success" href="'.base_url('form_fields/show/').'\',b.id,\'"><i class="fa fa-file"></i></a>&nbsp;';
        // }

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if($detail!='' || $edit!='' || $delete!=''){

            $op = "concat('".$detail.$edit.$delete."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "form_attribut b";
    }

    public function joinArray(){
        return array(
            "form_fileds c |left" => "b.form_filed_id=c.id"  
        );
    }

    public function whereClauseArray(){
        return array(
            "b.form_filed_id" => $_SESSION['form_filed_id']
        );
    }


}