<?php

class Msales extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getsalesById($id){

        $result = $this->db->query("SELECT * FROM sales WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getKey(){
        $result = $this->db->query("SELECT * FROM sales")->result_array();
        return $result;
    }

    function update_wilayah($id_sales, $wilayahs){
        if(count($wilayahs) > 0){
            $this->db->query("DELETE FROM wilayah_sales WHERE id_sales = '$id_sales'");
            $this->db->insert_batch('wilayah_sales', $wilayahs);
            return true;
        }
    }
    function getSales_by_kantor($id_kantor){
        $result = $this->db->query("SELECT * FROM sales where id_kantor = '$id_kantor'")->result_array();
        return @$result;
    }
    function getAllSales(){
        $result = $this->db->query("SELECT * FROM sales ")->result_array();
        return @$result;
    }

    function get_detail_wilayah($id_kelurahan){
        $result = $this->db->query("SELECT provinsi_id, kabkot_id, kecamatan_id, id as kelurahan_id FROM v_wilayah where id = '$id_kelurahan'")->row();
        return @$result;
    }

    /**
     * Select Sales
     */

    function list_sales(){
        $table = 'v_sales_user';
        
        $request = request_handler(); 
        $page = (!empty( (Array) $request) && isset($request->page)) ? $request->page : 1;
        $keyword = isset($request->term) ? $request->term : '';
        $resultCount = 10;

        $offset = ($page - 1) * $resultCount;

        $columns = [
            '*'
        ];


        $conditions = [
            "WHERE (nama_sales::text ILIKE '%".$keyword."%' 
                OR no_telp::text ILIKE '%".$keyword."%' 
                OR alamat::text ILIKE '%".$keyword."%' 
                OR kantor::text ILIKE '%".$keyword."%' )"
        ];
        
        $query = "SELECT ".implode(', ', $columns)." FROM ".$table." ".implode(' ', $conditions)." ";
        // map_y($query);
        /*Total Count */
        $count = count($this->db->query($query)->result_array());
        // map_y($count);
        $query .= 'LIMIT '.$resultCount;
        $query .= 'OFFSET '.$offset;

        $results = $this->db->query($query)->result_array();
        /* EndCount */
        $endCount = $offset + $resultCount;
        /* More Page */
        $morePages = $count > $endCount;

        $results = array(
          "results" => $results,
          "pagination" => array(
            "more" => $morePages
		  ),
          "per_page" => $resultCount,
          "total_count" => $count,
          "end_count" => $endCount, 
        );
        return $results;
    }

}