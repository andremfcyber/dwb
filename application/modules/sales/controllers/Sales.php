<?php

class Sales extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Msales");
        $this->load->model("Master/Mjabatan");
        $this->load->model("Master/Mkantor");
        $this->theme_module = "sales";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "sales";
        $this->dttModel = "Mdsales";
        $this->pk = "id";

    }

    public function wilayah_update($id_sales, $wilayah){
        
        // map_y($request);

        $arr_wilayah = [];
        $wilayah = isset($wilayah) ? $wilayah: '';
        if($wilayah && count($wilayah) > 0){
            $i = 0;
            foreach($wilayah as $id_kelurahan){
                $detail = $this->Msales->get_detail_wilayah($id_kelurahan);
                $arr_wilayah[$i]['id_sales'] = $id_sales;
                $arr_wilayah[$i]['id_kelurahan'] = $id_kelurahan;
                $arr_wilayah[$i]['id_kecamatan'] = $detail->kecamatan_id;
                $arr_wilayah[$i]['id_kabkot'] = $detail->kabkot_id;
                $arr_wilayah[$i]['id_provinsi'] = $detail->provinsi_id;
                $i++;
            }
        }
        $update = $this->Msales->update_wilayah($id_sales, $arr_wilayah);
    }
    
    function index(){
        // map_y(FCPATH);
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Daftar Marketing";
        $data['page_subtitle'] = "Modul Marketing";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Marketing";
            $data['page_subtitle'] = "Modul Marketing";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

    }

    public function dataTable() {
        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    private function _get_active_menu(){

        return array(
            'parent_menu' => 'master', 
            'submenu' => 'sales' 
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "sales_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "sales_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "sales_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "sales_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."sales/sales/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            // "label" => array(

            //     "data" => "b.label",
            //     "searchable" => true,
            //     "orderable" => true,
              

            // ),

            "Nama Sales" => array(

                "data" => "b.nama_sales",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "Nomor Telepon" => array(

                "data" => "b.no_telp",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "Jabatan" => array(

                "data" => "c.nama",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "Alamat" => array(

                "data" => "b.alamat",
                "searchable" => true,
                "orderable" => true,
              

            ),

            // "Referal" => array(
            //     "data" => "$.referal",
            //     "searchable" => true,
            //     "orderable" => false,
            // ),

           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){

        $form = array(
            "nama_sales" => array(
                "label" => "Nama Pegawai",
                "type" => "text",
                "placeholder" => "Nama Pegawai",
                "class" => "form-control validate[required]",
            ),

            "no_telp" => array(
                "label" => "Nomor Telepon",
                "type" => "text",
                "placeholder" => "Nomor Telepon",
                "class" => "form-control validate[required] no_hp",
            ),

            "id_jabatan" => array(
                "label" => "Jabatan",
                "type" => "sourcequery",
                "source" => $this->Mjabatan->getJabatan(),
                "keydt" => "id",
                "valuedt" => "nama",
                "class" => "form-control select2 validate[required]",
            ),
            "id_kantor" => array(
                "label" => "Kantor",
                "type" => "sourcequery",
                "source" => $this->Mkantor->getKantor(),
                "keydt" => "id",
                "valuedt" => "nm_kantor",
                "class" => "form-control select2 validate[required]",
            ),
            "wilayah[]" => array(
                "label" => "Wilayah",
                "type" => "sourcearray",
                "multiple" => true,
                "source" => [],
                "keydt" => "",
                "valuedt" => "",
                "class" => "form-control validate[required]",
                "id" => "wilayah_ids",
            ),

            "alamat" => array(

                "label" => "Alamat Tinggal",
                "type" => "textarea",
                "placeholder" => "Alamat Tinggal",
                "class" => "form-control validate[required]"

            ),
            
            "email" => array(

                "label" => "Email",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required]"

            ),

            "username" => array(

                "label" => "Username (Nama Pengguna)",
                "type" => "text",
                "placeholder" => "Username",
                "class" => "form-control validate[required]"

            ),

        );

        if($id){
            $id_kelurahan = [];
            $get_kelurahan_sales = $this->db->query("SELECT id_kelurahan FROM wilayah_sales WHERE id_sales='$id'")->result_array();
            foreach($get_kelurahan_sales as $data){
                $id_kelurahan[] = $data['id_kelurahan'];
            }
            $id_kelurahan = implode(',', $id_kelurahan);
            $get_kelurahan_name = $this->db->query("SELECT kelurahan FROM _tbl_kelurahan WHERE id IN ($id_kelurahan)")->result_array();
            
            $nama_kelurahan = [];
            foreach($get_kelurahan_name as $data){
                $nama_kelurahan[] = $data['kelurahan'];
            }
            $nama_kelurahan = implode(',', $nama_kelurahan);
            // map_y($nama_kelurahan);
            $form['disp_wilayah'] =  [
                'label' => 'Wilayah Sebelumnya',
                'type' => 'text',
                'disabled' => 'true',
                'value' => $nama_kelurahan,
                'class' => 'form-control validate[required]',
            ];
            
            $user = get_user_by_sales($id);

            $form["email" ] = array(
                "label" => "Email",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required]",
                "value" => $user->email
            );

            $form["username" ] = array(
                "label" => "Username",
                "type" => "text",
                "placeholder" => "Username",
                "class" => "form-control validate[required]",
                "value" => $user->username
            );

            
        }
        return $form;

// 
    }

    public function add(){
        
        if(count($_POST)>0){
            $no_tlp = $_POST['no_telp'];
            // $pos = strpos($no_tlp, '0');
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_telp'] = $no_tlp;

            $email = $_POST['email'];
            $username = $_POST['username'];
            $user_group = 'Sales';
            $pass = "user123";

            /**
             * Create User
             */
            $user_id = $this->aauth->create_user($email, $pass, $username, $user_group);
            
            if($user_id){
                set_user_status($user_id, 1);
                $wilayah = $_POST['wilayah'];
                $data = [
                    'nama_sales' => $_POST['nama_sales'],
                    'no_telp' => $_POST['no_telp'],
                    'alamat' => $_POST['alamat'],
                    'id_jabatan' => $_POST['id_jabatan'],
                    'user_id' => $user_id,
                    'id_kantor' => $_POST['id_kantor'],
                    'kode_referal' => generate_referal(),
                ];
    
                $this->db->insert('sales', $data); 
    
                $last_id = $this->db->insert_id('sales_seq');
    
                /**
                 * Log
                 */
                insert_log('Tambah Sales dari dashboard atas nama ' . $_POST['nama_sales']);
                if($last_id){
                    $this->wilayah_update($last_id, $wilayah);
                    $res = array("status" => "1", "msg" => "Data successfully inserted!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong!");
                }
                return response_json($res, 200);
            }else{
                $msg = $_SESSION['errors_auth'];
                $html = '';
                foreach($msg as $_dt){
                    $html .= $_dt.', ';
                }
                $res = array("status" => "0", "msg" => $html);
                unset($_SESSION['errors_auth']);
                
                return response_json($res, 200);
            }
           

        }else{
            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Marketing";
            $data['page_subtitle'] = "Modul Marketing";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();
        }

    }

    public function edit($id){

        if(count($_POST)>0){
            $no_tlp = $_POST['no_telp'];
            // $pos = strpos($no_tlp, '0');
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_telp'] = $no_tlp;
            
            $email = $_POST['email'];
            $username = $_POST['username'];

            $user_sales = get_user_by_sales($id);
            /**
             * Update User
             */
            // map_y($user_sales);
            $user_id = sales_user_id($id);

            $update_user = $this->aauth->update_user($user_id, $email, $pass = FALSE, $username);

            if($update_user){
            
                $wilayah = $_POST['wilayah'];
                $data = [
                    'nama_sales' => $_POST['nama_sales'],
                    'no_telp' => $_POST['no_telp'],
                    'alamat' => $_POST['alamat'],
                    'id_jabatan' => $_POST['id_jabatan'],
                    'id_kantor' => $_POST['id_kantor'],
                    'kode_referal' => generate_referal(),
                ];

                
                // 'kode_referal' => generate_referal(),

                $where = [
                    'id' => $id,
                ];
                $_ikeh = $this->db->where($where)->get('sales')->result_array()[0];
                // map_y($_ikeh);
                if($_ikeh['kode_referal'] == '' || $_ikeh['kode_referal'] == null){
                    $data['kode_referal'] = generate_referal();
                }

                $this->db->where($where);
                $this->db->update('sales',$data);

                /**
                 * Log
                 */
                insert_log('Edit Sales dari dashboard atas nama ' . $_POST['nama_sales']);

                if($id){
                    $this->wilayah_update($id, $wilayah);
                    $res = array("status" => "1", "msg" => "Data successfully inserted!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong!");
                }
                return response_json($res, 200);
            }else{
                $msg = $_SESSION['errors_auth'];

                $html = '';
                foreach($msg as $_dt){
                    $html .= $_dt.', ';
                }
                $res = array("status" => "0", "msg" => $html);

                unset($_SESSION['errors_auth']);
                
                return response_json($res, 200);
                
            }
            

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Marketing";
            $data['page_subtitle'] = "Modul Marketing";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit($id);
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $id = $_POST['id'];
            
            $detail_kabid = $this->Msales->getsalesById($id);
            $user_id = $detail_kabid['user_id'];

            $delete_user = $this->db->query("DELETE FROM aauth_users WHERE id = '$user_id' ");
            $delete_kabid = $this->db->query("DELETE FROM sales WHERE id = '$id' ");
            $delete_wilayah = $this->db->query("DELETE FROM wilayah_sales WHERE id_sales = '$id' ");
            if($delete_kabid){
                if($id){
                    $res = array("status" => "1", "msg" => "Data successfully Deleted!");
                }else{
                    $res = array("status" => "0", "msg" => "Oop! Something went wrong!");
                }
                return response_json($res, 200);
            }

        }

    }

    public function clipboard($referal = ''){
        $result = $this->db->query("SELECT * FROM product")->result_array();
        $base_url = base_url();
        if($base_url == 'http://dashboard.bprnusambacepiring.com/' || $base_url == 'https://dashboard.bprnusambacepiring.com/');
        $ikeh_ikeh = [];
        foreach($result as $data){
            $ikeh_ikeh[$data['id']]['nama_product'] = $data['name'];
            $ikeh_ikeh[$data['id']]['link_referal'] = base_url('/public/home/show/'.$data['id'].'/'.$data['product_type_id'].'?ref='.$referal);
        }
        $data['all_links'] = $ikeh_ikeh;
        $this->load->view('v_clipboard', $data);
        // map_y($ikeh_ikeh);
    }

    public function list_sales(){
        return response_json($this->Msales->list_sales());
    }

    
    public function reaset_password($user_id){
        
        $update_user = $this->aauth->update_user($user_id, $email = FALSE , $pass = 'user123', $username = FALSE);
        if($update_user){
            if($user_id){
                $res = array("status" => "1", "msg" => "Data successfully Reaset Password!");
            }else{
                $res = array("status" => "0", "msg" => "Oop! Something went wrong!");
            }
            return response_json($res, 200);
        }

    }

    public function  form_reaset_sales($id){
        $data['url_reaset_sales'] = base_url('sales/sales/reaset_password/'.$id);
        $this->load->view('v_reaset_sales', $data);
      
    }
}