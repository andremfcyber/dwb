<?php
/**
 * Created by IntelliJ IDEA.
 * User: indra
 * Date: 9/16/16
 * Time: 23:20
 */

 class Dashboard extends Admin_Controller{

    function __construct(){

        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->theme_module = "dashboard";
        $this->load->library('session');
    }

    function index(){
            
        // $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        // $time_range = array('start'=>date('Y-m-d H:i:s', strtotime("-30 day")), 'end'=>date('Y-m-d H:i:s'));
        // $dt_pinjaman = $this->Mdashboard->getSubByProduct($time_range);
        // map_y($result);
        // $this->_theme_vars['dt_pinjaman'] =$dt_pinjaman;
        $this->_theme_vars['page_title'] = 'Dashboard Collector';
        $this->_theme_vars['page_subtitle'] = 'Data Summary';
        $this->_theme_vars['page'] = 'dashboard';
        $this->_theme_vars['parent_menu'] = 'dashboard';
        $this->_theme_vars['submenu'] = '';
        // $this->_theme_vars['lastest_registered_users'] = $this->Mdashboard->getLatestRegisteredUsers();

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('dashboard_collector');
         
         
    }

 }