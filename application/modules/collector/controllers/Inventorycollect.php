<?php

class Inventorycollect extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 123456);
        $this->load->library("Aauth");
        $this->load->model("mcore");
        // $this->load->model("Mpenagihan");
        $this->theme_module = "collector";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "c_inventory_collect";
        $this->dttModel = "Mdinventory";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Inventory Collect";
        $data['page_subtitle'] = "Modul Collector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }


    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Emause', 
            'submenu' => 'collector.inventory_collect' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "kantor_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "inventory_collect_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "kantor_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "kantor_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(
            
            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nama_collector" => array(

                "data" => "c.nama_sales",
                "searchable" => true,
                "orderable" => true,
            
            ),
            "nama" => array(

                "data" => "e.nama",
                "searchable" => true,
                "orderable" => true,
            
            ),
            "name" => array(

                "data" => "f.name",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "tgl_pembayaran" => array(

                "data" => "h.tgl_pembayaran_terbaru",
                "searchable" => true,
                "orderable" => true,
            
            ),
             "stat_bayar" => array(

                "data" => "$.stat_bayar",
                "searchable" => true,
                "orderable" => true,
            
            ),

           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){

        return null;

// 
    }

    public function  form_penagihan($id){
        $data['url_c_penagihan'] = base_url('collector/penagihan/send_email_penagihan/'.$id);
        $data['pesan'] = "Apakah anda ingin mengirimkan tagihan lewat email?";
        $this->load->view('v_c_penagihan', $data);
      
    }

    public function send_email_penagihan($id){
    
        $_data = $this->Mpenagihan->getTagihanByIdAngsuran($id);
        $message = 'Kepada Babak/Ibu yang terhormat,';
        
        $data['content'] = '<p>'.$message.'</p>';
        $data['content'] .= "<br><p>Sedang test software Maaf</p> <p style='text-align: center;'></p>";  
        $html_message = $this->load->view('email_template', $data, TRUE);
        $email = $_data['email'];

        $send_email = send_email($email, $html_message );

        $res =  [
            'status' => '1',
            'msg' => "Email tagihan telah dikirim"
        ];
        return response_json($res, 200);
    }

    public function detail_tagihan($id){
        // map_y($id);
        $this->theme_module = '';
        $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
        $this->_theme_vars['theme'] = $this->_theme_vars['active_admin_theme'];
        $this->_theme_vars['page_title'] = 'Hasil Penagihan';
        $this->_theme_vars['page_subtitle'] = 'Collector';
        $this->_theme_vars['current_class_dir'] = $this->router->fetch_directory();
        $this->_theme_vars['current_class'] = $this->router->fetch_class();
        $this->_theme_vars['permissions'] = $this->_get_permissions();
        $this->_theme_vars['parent_menu'] = 'a';
        $this->_theme_vars['submenu'] = 'a';
        $icolect = $this->db->query("SELECT a.id, 
                a.alamat_sesuai, 
                a.alasan_telat_bayar, 
                a.bertemu_dengan, 
                a.bukti_bayar, 
                a.created_at, 
                a.created_by, 
                a.karakter_nasabah, 
                a.lokasi_bertemu, 
                a.nominal_janji_bayar, 
                a.resume_singkat_nasabah, 
                a.surat_tugas, 
                a.tanggal_janji_bayar, 
                a.tgl_pembayaran_terbaru, 
                a.tgl_penagihan, 
                a.updated_at, 
                a.updated_by, 
                a.id_inventory_collect,
                a.nominal_bayar,
                b.id_pinjaman,
                b.id_collector,
                b.id_angsuran,
                d.nama,
                d.no_tlp,
                d.email,
                d.alamat_lengkap,
                e.name,
                f.nama_collector,
                f.no_telp as no_telp_collector,
                f.email as email_collector,
                g.tgl_penagihan,
                g.angsuran_ke,
                g.bayar_bulan,
                g.status as status_angsuran
                
            FROM public.c_penagihan a
            left join c_inventory_collect b on a.id_inventory_collect = b.id
            left join c_pinjaman c on b.id_pinjaman = c.id
            left join c_nasabah d on c.id_nasabah = d.id
            left join product e on c.id_product = e.id
            left join c_collector f on b.id_collector = f.id
            left join c_angsuran g on b.id_angsuran = g.id
            where a.id_inventory_collect = $id
            ")->row();
        $data['detail'] =$icolect;
        // map_y($icolect);
        $this->render_admin_view('view/collector/hasil_penagihan','', $data);
    }

    public function approve_detail_tagihan($id){

        $data = array('status' => 1);

        $this->db->where('id', $id);
        $result = $this->db->update('c_angsuran',$data);

        if($result){
          return response_json([
                'message' => 'Berhasil di Approve'
            ]);
        }else{
            return response_json([
                'message' => 'Terjadi Kesalahan'
            ], 200);
        }
    }
}