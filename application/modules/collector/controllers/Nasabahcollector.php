<?php

class Nasabahcollector extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 123456);
        $this->load->library("Aauth");
        $this->load->library('excel');
        $this->load->model("mcore");
        $this->load->model("product/Mproduct");
        $this->load->model("Mnasabahcollector");
        $this->theme_module = "collector";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "c_nasabah";
        $this->dttModel = "Mdnasabahcollector";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Nasabah Collector";
        $data['page_subtitle'] = "Modul Collector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Fax";
        $data['page_subtitle'] = "Modul Coolector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Emause', 
            'submenu' => 'collector.nasabahcollector' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "nasabah_colector_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "nasabah_collector_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "nasabah_collector_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "nasabah_collector_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(
            array(

                "text" => '<i class="fa fa-file-excel-o "></i> Download Template',
                "action" => "function ( e, dt, node, config ) { window.location.href = base_url + '/upload/templates/nasabah.xlsx'}"

            ),
            array(

                "text" => '<i class="fa fa-file-excel-o "></i> Import Data',
                "action" => "function ( e, dt, node, config ) { $('#excel').trigger('click');}"
            ), 
            
            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."collector/nasabahcollector/add';}"

            ),
            
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nik" => array(

                "data" => "b.nik",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "nama" => array(

                "data" => "b.nama",
                "searchable" => true,
                "orderable" => true,
            
            ),
            "no_hp" => array(

                "data" => "b.no_tlp",
                "searchable" => true,
                "orderable" => true,
            
            ),
            "email" => array(

                "data" => "b.email",
                "searchable" => true,
                "orderable" => true,
            
            ),
        
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){

        $data = array(

            "nik" => array(

                "label" => "NIK <span style='color: red'>* Tidak boleh ada huruf harus angka </span>",
                "type" => "text",
                "placeholder" => "NIK",
                "class" => "form-control validate[required]",
               

            ),

            "nama" => array(

                "label" => "Nama Nasabah",
                "type" => "text",
                "placeholder" => "Nama Nasabah",
                "class" => "form-control validate[required]",
               

            ),

            "jenis_kelamin" => array(

                "label" => "jenis_kelamin",
                "type" => "sourcearray",
                "source" => array("L" => "Laki-Laki", "P" => "Perempuan"),
                "class" => "form-control select2 validate[required]"
            ),

            "no_tlp" => array(

                "label" => "No.HP <span style='color: red'>* No Hp Wajib (62), contoh: 62837482789 </span>",
                "type" => "text",
                "placeholder" => "Nomor HP",
                "class" => "form-control validate[required]",

            ),
            "no_tlp2" => array(

                "label" => "No.HP2",
                "type" => "text",
                "placeholder" => "Nomor HP Ke 2",
                "class" => "form-control",

            ),
            "no_tlp_saudara" => array(

                "label" => "No.HP Saudara",
                "type" => "text",
                "placeholder" => "Nomor HP Saudara",
                "class" => "form-control",

            ),

            "email" => array(

                "label" => "Email <span style='color: red'>* Format Email Harus Benar, contoh: email@sample.com </span>",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required]",
               

            ),

           
            "photo_selfie" => array (
                "label" => "Foto Selfi",
                "type" => "upload_file",
                "placeholder" => "Foto Selfi",
                "class" => "form-control validate[required]",
                "file_path" => 'upload/photo/'
            ),
            "photo_ktp" => array (
                "label" => "Foto KTP",
                "type" => "upload_file",
                "placeholder" => "Foto KTP",
                "class" => "form-control validate[required]",
                "file_path" => 'upload/photo/'
            ),

             
            "provinsi_sekarang" => array(
            	"label" => "Provinsi Sekarang",
                "type" => "sourcearray",
            	"placeholder" => "Provinsi Sekarang",
                "class" => "form-control select2 validate[required] select2",
                "event" => [
                    'onchange' => "change_provinsi('sekarang')",
                ],
            ),

            "kota_sekarang" => array(
            	"label" => "Kab/Kota Sekarang",
                "type" => "sourcearray",
            	"placeholder" => "Kab/Kota Sekarang",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kabupaten('sekarang')",
                ],
            ),

            "kecamatan_sekarang" => array(
            	"label" => "Kecamatan Sekarang",
                "type" => "sourcearray",
            	"placeholder" => "Kecamatan Sekarang",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kecamatan('sekarang')",
                ],
            ),

            "kelurahan_sekarang" => array(
            	"label" => "Kelurahan Sekarang",
                "type" => "sourcearray",
            	"placeholder" => "Kelurahan Sekarang",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kelurahan('sekarang')",
                ],
            ),

            "kodepos_sekarang" => array(
                "label" => "Kode Pos Sekarang",
                "type" => "text",
                "placeholder" => "Kode Pos Sekarang",
                "class" => "form-control validate[required]",
                "readonly" => true,
            ),

            "alamat_lengkap_sekarang" => array(
            	"label" => "Alamat Sekarang",
            	"type" => "textarea",
            	"placeholder" => "Alamat Sekarang",
            	"class" => "form-control validate[required]",
            ),

            "alamat_sama_dengan_ktp" => array(
            	"label" => "Alamat Sama Sekarang Sama Dengan KTP",
                "type" => "sourcearray",
                "source" => [
                    't' => 'Ya',
                    'f' => 'Tidak',
                ],
                // "value" => 'true',
            	"placeholder" => "Pilih",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    "onchange" => "is_same_ktp(this)",
                ],
            ),
             
            "provinsi_ktp" => array(
            	"label" => "Provinsi Ktp",
                "type" => "sourcearray",
            	"placeholder" => "Provinsi Ktp",
                "class" => "form-control select2 validate[required] select2",
                "event" => [
                    'onchange' => "change_provinsi('ktp')",
                ],
            ),

            "kota_ktp" => array(
            	"label" => "Kab/Kota Ktp",
                "type" => "sourcearray",
            	"placeholder" => "Kab/Kota Ktp",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kabupaten('ktp')",
                ],
            ),

            "kecamatan_ktp" => array(
            	"label" => "Kecamatan Ktp",
                "type" => "sourcearray",
            	"placeholder" => "Kecamatan Ktp",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kecamatan('ktp')",
                ],
            ),

            "kelurahan_ktp" => array(
            	"label" => "Kelurahan Ktp",
                "type" => "sourcearray",
            	"placeholder" => "Kelurahan Ktp",
                "class" => "form-control select2 validate[required]",
                "event" => [
                    'onchange' => "change_kelurahan('ktp')",
                ],
            ),

            "kodepos_ktp" => array(
                "label" => "Kode Pos Ktp",
                "type" => "text",
                "placeholder" => "Kode Pos Ktp",
                "class" => "form-control validate[required]",
                "readonly" => true,
            ),

            "alamat_lengkap_ktp" => array(
            	"label" => "Alamat Ktp",
            	"type" => "textarea",
            	"placeholder" => "Alamat Ktp",
            	"class" => "form-control validate[required]",
            ),

            
            "searchmap" => array(
                "type" => "custom",
                "html" => " <div class='form-group col-md-12' style='padding: 0px 4px;'>
                                <label for='lokasi'>
                                    Lokasi
                                </label>
                                <input type='text' class='form-control textbox' name='lokasi' id='searchmap'>
                                <div id='map-canvas'></div>
                                <div    class='col-md-6'>
                                    <input type='text' class='form-control' id='latitude' name='latitude' readonly>
                                </div>
                                <div    class='col-md-6'>
                                    <input type='text' class='form-control' id='longitude' name='longitude' readonly>
                                </div>
                            </div>"
            ),

        );

        if($id){
            unset($data['photo_selfie']);
            unset($data['photo_ktp']);

            $data["photo_selfie"] = array (
                "label" => "Foto Selfi",
                "type" => "upload_file",
                "placeholder" => "Foto Selfi",
                "class" => "form-control",
                "file_path" => 'upload/photo/'
            );
            $data["photo_ktp"] = array (
                "label" => "Foto KTP",
                "type" => "upload_file",
                "placeholder" => "Foto KTP",
                "class" => "form-control",
                "file_path" => 'upload/photo/'
            );
        }

        return $data;

// 
    }

    public function add(){

        if(count($_POST)>0){
        //    map_y($_POST);
            $_POST['provinsi_id'] = $_POST['provinsi_sekarang'];
            $_POST['kabupaten_id'] = $_POST['kota_sekarang'];
            $_POST['kecamatan_id'] = $_POST['kecamatan_sekarang'];
            $_POST['kelurahan_id'] = $_POST['kelurahan_sekarang'];
            $_POST['kode_pos'] = $_POST['kodepos_sekarang'];
            $_POST['alamat_lengkap'] = $_POST['alamat_lengkap_sekarang'];
            
            if($_POST['alamat_sama_dengan_ktp'] == 't'){
                // map_y($_POST);
                $_POST['provinsi_ktp'] = $_POST['provinsi_id'];
                $_POST['kabupaten_ktp'] = $_POST['kabupaten_id'];
                $_POST['kecamatan_ktp'] = $_POST['kecamatan_id'];
                $_POST['kelurahan_ktp'] = $_POST['kelurahan_id'];
                $_POST['kode_pos_ktp'] = $_POST['kode_pos'];
                $_POST['alamat_lengkap_ktp'] = $_POST['alamat_lengkap'];
            }else{
                $_POST['kabupaten_ktp'] = $_POST['kota_ktp'];
                $_POST['kode_pos_ktp'] = $_POST['kodepos_ktp'];
            }
         
            $no_tlp = $_POST['no_tlp'];
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_tlp'] = $no_tlp;
            $_POST ['status'] = "1";
            $_POST ['created_at'] = date('Y-m-d H:i:s');
            $_POST ['created_by'] = $this->session->userdata('username');
            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            // $_POST ['tgl_pengajuan'] = date('Y-m-d H:i:s');
            unset($_POST['tmp_name']);
            unset($_POST['old_photo_selfie']);
            unset($_POST['old_photo_ktp']);
            unset($_POST['provinsi_sekarang']);
            unset($_POST['kota_sekarang']);
            unset($_POST['kecamatan_sekarang']);
            unset($_POST['kelurahan_sekarang']);
            unset($_POST['kodepos_sekarang']);
            unset($_POST['alamat_lengkap_sekarang']);
            unset($_POST['kota_ktp']);
            unset($_POST['kodepos_ktp']);
            unset($_POST['lokasi']);

            
            if(count($_FILES)>0){
                if($_FILES["photo_selfie"]["name"]!=""){

                    if($_FILES["photo_selfie"]["type"]=="image/png" or
                        $_FILES["photo_selfie"]["type"]=="image/jpg" or
                        $_FILES["photo_selfie"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo_selfie']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo_selfie"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo_selfie'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }

                if($_FILES["photo_ktp"]["name"]!=""){

                    if($_FILES["photo_ktp"]["type"]=="image/png" or
                        $_FILES["photo_ktp"]["type"]=="image/jpg" or
                        $_FILES["photo_ktp"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo_ktp']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo_ktp"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo_ktp'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;
            $data['params']['log'] = 'Tambah nasabah collector dari dashboard atas nama ' . $_POST['nama'];

            $this->load->library("Cinta",$data);
            $insert = $this->cinta->process();
            
            if($insert){
               
                $res = array("status" => "1", "msg" => "Data berhasil Ditambah!");
                
            }else{

                $res = array("status" => "1", "msg" => "Oops! Telah terjadi kesalahan.");

            }

            echo json_encode($res);

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Nasabah";
            $data['page_subtitle'] = "Modul Collector";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";
            $data['ikeh_ikeh_js'] = base_url('assets/dist/js/pages/nasabah.js');
            // map_y($data);
            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){
        // map_y($id);
        if(count($_POST)>0){
            $_POST['provinsi_id'] = $_POST['provinsi_sekarang'];
            $_POST['kabupaten_id'] = $_POST['kota_sekarang'];
            $_POST['kecamatan_id'] = $_POST['kecamatan_sekarang'];
            $_POST['kelurahan_id'] = $_POST['kelurahan_sekarang'];
            $_POST['kode_pos'] = $_POST['kodepos_sekarang'];
            $_POST['alamat_lengkap'] = $_POST['alamat_lengkap_sekarang'];
            
            if($_POST['alamat_sama_dengan_ktp'] == 't'){
                $_POST['provinsi_ktp'] = $_POST['provinsi_id'];
                $_POST['kabupaten_ktp'] = $_POST['kabupaten_id'];
                $_POST['kecamatan_ktp'] = $_POST['kecamatan_id'];
                $_POST['kelurahan_ktp'] = $_POST['kelurahan_id'];
                $_POST['kode_pos_ktp'] = $_POST['kode_pos'];
                $_POST['alamat_lengkap_ktp'] = $_POST['alamat_lengkap'];
            }else{
                $_POST['kabupaten_ktp'] = $_POST['kota_ktp'];
                $_POST['kode_pos_ktp'] = $_POST['kodepos_ktp'];
            }
            
            $no_tlp = $_POST['no_tlp'];
            // $pos = strpos($no_tlp, '0');
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
                unset($_POST['kota_ktp']);
                unset($_POST['kodepos_ktp']);
            }
            $_POST['no_tlp'] = $no_tlp;
            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            $_POST ['updated_by'] = $this->session->userdata('username');
            unset($_POST['tmp_name']);
            unset($_POST['old_photo_selfie']);
            unset($_POST['old_photo_ktp']);
            unset($_POST['provinsi_sekarang']);
            unset($_POST['kota_sekarang']);
            unset($_POST['kecamatan_sekarang']);
            unset($_POST['kelurahan_sekarang']);
            unset($_POST['kodepos_sekarang']);
            unset($_POST['alamat_lengkap_sekarang']);
            unset($_POST['kodepos_ktp']);
            unset($_POST['lokasi']);
            
            if(count($_FILES)>0){
                if($_FILES["photo_selfie"]["name"]!=""){

                    if($_FILES["photo_selfie"]["type"]=="image/png" or
                        $_FILES["photo_selfie"]["type"]=="image/jpg" or
                        $_FILES["photo_selfie"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo_selfie']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo_selfie"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo_selfie'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }

                if($_FILES["photo_ktp"]["name"]!=""){

                    if($_FILES["photo_ktp"]["type"]=="image/png" or
                        $_FILES["photo_ktp"]["type"]=="image/jpg" or
                        $_FILES["photo_ktp"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo_ktp']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo_ktp"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo_ktp'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Nasabah collector dari dashboard atas nama ' . $_POST['nama'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();


        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Pengajuan Kredit";
            $data['page_subtitle'] = "Modul Nasabah";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit($id);
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $detail_nasabah = $this->Mnasabahcollector->nasabah_by_id($id);
            // map_y($detail_nasabah);
            $data['ikeh_ikeh_js'] = base_url('assets/dist/js/pages/nasabah.js');
            $latitude = $detail_nasabah['latitude'];
            $longitude = $detail_nasabah['longitude'];
            $data['excute_kimochi'] = "
                $(`[name='alamat_lengkap_sekarang']`).val(`".$detail_nasabah['alamat_lengkap']."`)
                $(`[name='alamat_sama_dengan_ktp']`).val(`".$detail_nasabah['alamat_sama_dengan_ktp']."`).trigger('change');
                $(`[name='latitude']`).val(`".$detail_nasabah['latitude']."`);
                $(`[name='longitude']`).val(`".$detail_nasabah['longitude']."`);
                setTimeout(() => {
                    set_wilayah(".$detail_nasabah['provinsi_id'].",
                    ".$detail_nasabah['kabupaten_id'].",
                    ".$detail_nasabah['kecamatan_id'].",
                    ".$detail_nasabah['kelurahan_id'].",'sekarang')

                    set_wilayah(".$detail_nasabah['provinsi_ktp'].",
                    ".$detail_nasabah['kabupaten_ktp'].",
                    ".$detail_nasabah['kecamatan_ktp'].",
                    ".$detail_nasabah['kelurahan_ktp'].",'ktp')

                    set_map(".$latitude.",
                    ".$longitude.")
                }, 500)
            ";
            // map_y($data);
            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

    function import(){

        if($_FILES["excel"]["name"]!=""){

            if($_FILES["excel"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" or
                $_FILES["excel"]["type"]=="application/vnd.ms-excel" or
                $_FILES["excel"]["type"]=="application/x-msexcel" or
                $_FILES["excel"]["type"]=="application/x-ms-excel" or
                $_FILES["excel"]["type"]=="application/x-excel" or
                $_FILES["excel"]["type"]=="application/x-dos_ms_excel" or
                $_FILES["excel"]["type"]=="application/xls" or
                $_FILES["excel"]["type"]=="application/wps-office.xlsx" or
                $_FILES["excel"]["type"]=="application/x-xls" or
                $_FILES["excel"]["type"]=="application/msexcel"){

                $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/excel';
                // echo $upload_path;exit();
                $array = explode('.', $_FILES['excel']['name']);
                $extension = end($array);
                $file = md5(uniqid(rand(), true)).".".$extension;

                if (move_uploaded_file($_FILES["excel"]["tmp_name"], $upload_path."/".$file)) {

                    $import = $this->import_from_excel($upload_path."/".$file); 

                    if($import){

                        @unlink($upload_path."/".$file);

                    }

                    $res = array("status" => "1", "msg" => "Data imported Successfully");

                }else{

                    $res = array("status" => "0", "msg" => "Oops! Something went wrong while uploading file");

                }

            }else{

                $res = array("status" => "0", "msg" => "Invalid file extention. Must be .xls or .xlsx");

            }

            echo json_encode($res);

        }

    }

    function import_from_excel($file){

        //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
        $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007  

        $objReader->setReadDataOnly(true);        
  
        $objPHPExcel=$objReader->load($file);      
        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);     
        // $nama_nasabah   = $objWorksheet->getCellByColumnAndRow(1,3)->getValue();
        // map_y($totalrows);
        // $i=2;
       
        for($i=2; $i <= $totalrows ; $i++){

            $produk         = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
            $nama_nasabah   = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
            $jenis_kelamin  = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
            $no_hp1         = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
            $no_hp2         = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
            $no_hp_saudara  = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
            $email          = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
            $tagihan_ke     = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
            $total_tenor    = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
            $tunggakan      = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
            $sisa_cicilan   = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
            $nominal_tagihan= $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
            $tgl_penagihan  = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($objWorksheet->getCellByColumnAndRow(12,$i)->getValue()));
            // $collector      = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();

            $usercreated= $this->session->userdata("username"); 
            // if ($produk == '' || $produk==' ') {
            //     break;
            // }
            $produk_id = $this->Mnasabahcollector->get_id_produk($produk);
            // $cproduk_id = count($produk_id);
            if($produk_id == '' || $produk==' '){
                $res = array("status" => "0", "msg" => "Produk Tidak Ada");
                echo json_encode($res);
                break;
            }

            // $collector_id = $this->Mnasabahcollector->get_id_collector($collector);
            // if($collector_id == '' || $collector==' '){
            //     $res = array("status" => "0", "msg" => "Collector Tidak Ada");
            //     echo json_encode($res);
            //     break;
            // }

            // map_y($produk_id['id']);
            $data_nasabah = array(

                'nama'          => $nama_nasabah,
                'jenis_kelamin' => $jenis_kelamin,
                'no_tlp'        => $no_hp1,
                'no_tlp2'       => $no_hp2,
                'no_tlp_saudara'=> $no_hp_saudara,
                'email'         => $email,
                'created_at'    => date("Y-m-d h:i:s"),
                'created_by'    => $usercreated,
                
            );
            $this->db->insert($this->table,$data_nasabah);
            $last_id_nasabah =$this->db->insert_id('c_nasabah_id_seq'); 

            $data_pinjaman = array(

                'id_product'      => $produk_id['id'],
                'id_nasabah'      => $last_id_nasabah,
                'tenor'           => $total_tenor,
                'tgl_penagihan'   => $tgl_penagihan,
                'tagihan_bulanan' => $nominal_tagihan,
                'created_at'      => date("Y-m-d h:i:s"),
                'created_by'      => $usercreated,
                
            );
            $this->db->insert('c_pinjaman',$data_pinjaman);
            $last_id_pinjaman =$this->db->insert_id('c_pinjaman_id_seq');
           
            $date = $tgl_penagihan;
            $_diff = $total_tenor; // jumlah bulan yang dibayarkan
            $dateFormat = 'Y-m-j';
            if($tagihan_ke > 0 ){
                 $tagihan_ke = $tagihan_ke-1;
            }
          
            for($ix=0;$ix<$_diff;$ix++){ // pengulangan sampai batas cicilan
                
                $status = 0;
                $dDiff = $tgl_penagihan;

                if ($tagihan_ke > $ix ){
                    $status = 1;
                }
                if ($ix > 0){
                    $dDiff = substractMonth($date, +1, $dateFormat);
                    $date = date ($dateFormat , strtotime ( $dDiff) );
                }
                $angsuran_ke = 1 + $ix;
                $data = array(
                    'id_pinjaman' => $last_id_pinjaman,
                    'tgl_penagihan' => $dDiff ,
                    'bayar_bulan' => $nominal_tagihan,
                    'angsuran_ke' => $angsuran_ke,
                    'status' => $status,                        
                );
                $this->db->insert('c_angsuran',$data);
            }

        }   
        return true;
    }

    public function detail_nasabah($id){
        // map_y($id);
        $this->theme_module = '';
        $this->set_admin_theme($this->_theme_vars['active_admin_theme']);
        $this->_theme_vars['theme'] = $this->_theme_vars['active_admin_theme'];
        $this->_theme_vars['page_title'] = 'Detail Nasabah';
        $this->_theme_vars['page_subtitle'] = 'Collector';
        $this->_theme_vars['current_class_dir'] = $this->router->fetch_directory();
        $this->_theme_vars['current_class'] = $this->router->fetch_class();
        $this->_theme_vars['permissions'] = $this->_get_permissions();
        $this->_theme_vars['parent_menu'] = 'product2';
        $this->_theme_vars['submenu'] = 'product2';
        
        $data['detail'] = $this->db->query("SELECT * FROM c_nasabah  WHERE id = $id")->result_array()[0];
        $data['pinjaman'] = $this->db->query("SELECT a.*,b.name FROM c_pinjaman a left join product b on a.id_product = b.id WHERE a.id_nasabah = $id")->result_array();
        // map_y($data);
        // map_y($data);
        $this->render_admin_view('view/collector/nasabah','', $data);
    }
}