<?php

class Surattugas extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        // $this->load->model("Mpenagihan");
        $this->theme_module = "collector";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "c_inventory_collect";
        $this->dttModel = "Mdsurattugas";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Surat Tugas";
        $data['page_subtitle'] = "Modul Collector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }


    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Emause', 
            'submenu' => 'collector.surat_tugas' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "kantor_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "surat_tugas_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "kantor_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "surat_tugas_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(
            
            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."collector/collector/add';}"

            // ),
            
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nama" => array(

                "data" => "e.nama",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "name" => array(

                "data" => "f.name",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "nama_collector" => array(

                "data" => "c.nama_collector",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "tenor" => array(

                "data" => "d.tenor",
                "searchable" => true,
                "orderable" => true,
            
            ),
        
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){

        return null;

// 
    }

    public function  form_upload_surat($id){
        $data['url_upload_surat'] = base_url('collector/surattugas/upload_surat/');
        $data['id'] = $id;
        $this->load->view('v_surat_tugas', $data);
      
    }

    public function upload_surat($id){
    // map_y($_FILES);
        if($_FILES["surat_tugas"]["name"]!=""){

            $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/surat_tugas/';
            $array = explode('.', $_FILES['surat_tugas']['name']);
            $extension = end($array);
            $photo = md5(uniqid(rand(), true)).".".$extension;

            if (move_uploaded_file($_FILES["surat_tugas"]["tmp_name"], $upload_path."/".$photo)) {
                $surat_tugas = $photo;  
            }else{


                $res['msg'] = "Oops! Something went wrong!";
                $res['status'] = "0";

                echo json_encode($res);
                exit;

            }
        }

        $data = array(
            "surat_tugas" => $surat_tugas,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('username'),
        );
        
        $this->db->where('id', $id);
        // map_y($this->db->update($table, $data));
        $update = $this->db->update($this->table, $data);
        
        if($update){

            $res = array("status" => "1", "msg" => "Successfully Upload Surat Tugas!");

        }else{

            $res = array("status" => "0", "msg" => "Oop! Something went wrong. Please try again.");
        }

        echo json_encode($res);
        
    }
    
    public function print_surattugas($id){
        if($id){
            $nasabah = $this->db->query("SELECT 
                   a.id, 
                   a.id_collector, 
                   b.nama_collector,
                   a.id_pinjaman,
                   d.nama as nama_nasabah,
                   e.name as nama_produk,
                   a.surat_tugas,
                   a.id_angsuran,
                   f.tgl_penagihan,
                   f.angsuran_ke
                FROM public.c_inventory_collect a
                left join c_collector b on a.id_collector = b.id
                left join c_pinjaman c on a.id_pinjaman = c.id
                left join c_nasabah d on c.id_nasabah = d.id
                LEFT JOIN product e ON c.id_product = e.id
                left join c_angsuran f on a.id_angsuran = f.id WHERE a.id = '$id'")->result_array()[0];
            $data['nasabah'] = $nasabah;
            $html = $this->load->view('v_pdf_surat_tugas', $data, TRUE);
            $name = md5($nasabah['id']);
            $path = "upload/surat_tugas/".$name;
            // map_y($path);
            return print_pdf('surat_tugas', $html,$path);
        }   
    }
    

}