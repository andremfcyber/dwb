<?php

class Detailpinjaman extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mdetailpinjaman");
        $this->theme_module = "collector";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "c_angsuran";
        $this->dttModel = "Mddetailpinjaman";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Detail Pinjaman";
        $data['page_subtitle'] = "Modul Collector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show(){

        unset($_SESSION['idpinjaman']);
        $_SESSION['idpinjaman'] = $_GET['idpinjaman'];
        
        $this->index();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Emause', 
            'submenu' => 'collector.nasabahcollector' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "angsuran_colector_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "angsuran_colector_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "angsuran_colector_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "angsuran_colector_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."collector/detailpinjaman/add';}"

            // ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "angsuran_ke" => array(

                "data" => "b.angsuran_ke",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "tgl_penagihan" => array(

                "data" => "b.tgl_penagihan",
                "searchable" => true,
                "orderable" => true,
            
            ),
            "telat" => array(

                "data" => "$.telat",
                "searchable" => true,
                "orderable" => true,
            
            ),
            "telaat" => array(

                "data" => "$.stat_bayar",
                "searchable" => true,
                "orderable" => true,
            
            ),
            // "jml_bayar" => array(

            //     "data" => "b.tagihan_bulanan",
            //     "searchable" => true,
            //     "orderable" => true,
            
            // ),

            // "status" => array(

            //     "data" => "b.status",
            //     "searchable" => true,
            //     "orderable" => true,
            
            // ),
        
            "action" => array(

                // $hasil_penagihan = cek_hasil_penagihan("$.op"),

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
                // "render" => "function(data, type, row){ data = $(data); if($hasil_penagihan ==true){data.find('#hs_p_'+row.b.id).hide()} return data[0]['outerHTML'];}"
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){


        return array(


            "jumlah_bayar" => array (
                "label" => "Jumlah Bayar",
                "type" => "text",
                "placeholder" => "Jumlah Bayar",
                "class" => "form-control",
            ),
            
            "tgl_bayar" => array (
                "label" => "Tanggal Bayar",
                "type" => "datepicker",
                "placeholder" => "Tanggal Bayar",
                "class" => "form-control",
            ),

            "alasan_telat" => array (
                "label" => "Alasan telat bayar",
                "type" => "textarea",
                "placeholder" => "Alasan telat bayar",
                "class" => "form-control",
            ),

        );

// 
    }

    public function add(){
        // map_y('d');
        if(count($_POST)>0){
           
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;
            // $data['params']['log'] = 'Tambah pinjaman collector dari dashboard atas nama ';

            $this->load->library("Cinta",$data);
            $insert = $this->cinta->process();
            $id_last = $this->db->insert_id();
            // loop detail pinjaman
                $date = $_POST['tgl_penagihan'];
                $_diff = $_POST['tenor']; // jumlah bulan yang dibayarkan
                $dateFormat = 'Y-m-j';
                $data = array(
                    'id_pinjaman' => $id_last,
                    'tgl_penagihan' => $_POST['tgl_penagihan'] ,
                    'bayar_bulan' => $_POST['tagihan_bulanan'],
                    'angsuran_ke' => 1,
                    'status' => 0,                        
                );
                $this->db->insert('c_angsuran',$data);
                for($i=1;$i<$_diff;$i++){ // pengulangan sampai batas cicilan
                    $dDiff = substractMonth($date, +1, $dateFormat);
                    $date = date ($dateFormat , strtotime ( $dDiff) );
                    $angsuran_ke = 1 + $i;
                        $data = array(
                            'id_pinjaman' => $id_last,
                            'tgl_penagihan' => $dDiff ,
                            'bayar_bulan' => $_POST['tagihan_bulanan'],
                            'angsuran_ke' => $angsuran_ke,
                            'status' => 0,                        
                        );
                   
                    $this->db->insert('c_angsuran',$data);
                }
            
            if($insert){

                $res = array("status" => "1", "msg" => "Data berhasil Ditambah!");

            }else{

                $res = array("status" => "1", "msg" => "Oops! Telah terjadi kesalahan.");

            }

            echo json_encode($res);

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Pinjaman";
            $data['page_subtitle'] = "Modul Collector";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";
            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            $_POST ['updated_by'] = $this->session->userdata('username');
            
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Detail Pinjaman id = ' . $id;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Detail Pinjaman";
            $data['page_subtitle'] = "Modul Coolector";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

    

}