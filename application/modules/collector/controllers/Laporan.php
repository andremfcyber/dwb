<?php

class Laporan extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        // $this->load->model("Mnasabah");
        // $this->load->model("product/Mproduct");
        $this->theme_module = "nasabah";

        if(!$this->aauth->is_loggedin()) {
            redirect('admin');
        }

        $this->table = "nasabah";
        $this->dttModel = "Mdlaporan";
        $this->pk = "id";
    }

    function index(){
        // map_y($this->_get_permissions());
        //Get All Status From Master Except id => 4? Verifikasi
        // $d['select'] = $this->db->query("SELECT id, value_status FROM master_status WHERE id <> 4")->result_array();
        $d['select'] = array (['value_status' => 'Sudah Bayar'],['value_status' => 'Belum Bayar']);
        // map_y($d);
        // map_y($select);  
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Laporan";
        $data['page_subtitle'] = "Laporan";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        $data['params']['datatable']['filter_custom'] = $this->load->view('v_filter_select',$d, TRUE);
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();
    }

    // function show($id = null){
    //     if($id != null){

    //         $_SESSION['id_product'] = $id;
        
    //     $data['theme'] = $this->_theme_vars['active_admin_theme'];
    //     $data['page_title'] = "List Nasabah";
    //     $data['page_subtitle'] = "Modul Nasabah";
    //     $data['current_class_dir'] = $this->router->fetch_directory();
    //     $data['current_class'] = $this->router->fetch_class();
    //     $data['permissions'] = $this->_get_permissions();
    //     $data['active_menu'] = $this->_get_active_menu();  
    //     $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
    //     $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
    //     $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
    //     $this->load->library("Cinta",$data);
    //     $this->cinta->browse();
    // }
    // }

    public function dataTable() {
        //Reindex Status
        $index = count($_POST['columns']);
        
        $_POST['columns'][$index] = [
            'name' => '',
            'data' => 'e.status',
            'orderable' => 'false',
            'searchable' => 'false',
            'search' => [
                'value' => '',
                'regex' => false,       
            ],
        ];

        // /**
        //  * Multiple Filter Select
        //  */
        // if(isset($_POST['search']['value'])){
        //     $this->
        // }
        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return [
            'parent_menu' => 'nasabah', 
            'submenu' => 'kredit' 
        ];

    }

    private function _get_permissions(){
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(
            "add_perm" => $this->mcore->checkPermission($this->user_group, "kredit_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "laporan_emause_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "kredit_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "kredit_delete"),
        );
    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        $button = array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            'colvis'
        );

        if($this->mcore->checkPermission($this->user_group, "kredit_add")){
            $button[] =   array(
                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."nasabah/kredit/add';}"
            );
        }

        return $button;
    }

    private function _get_datatable_columns(){


        return array(

            "nama_collector" => array(

                "data" => "c.nama_collector",
                "searchable" => true,
                "orderable" => true,
            
            ),
            "nama" => array(

                "data" => "e.nama",
                "searchable" => true,
                "orderable" => true,
            
            ),
            "name" => array(

                "data" => "f.name",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "tgl_penagihan" => array(

                "data" => "g.tgl_penagihan",
                "searchable" => false,
                "orderable" => true,
            
            ),

            "tgl_pembayaran" => array(

                "data" => "h.tgl_pembayaran_terbaru",
                "searchable" => false,
                "orderable" => false,
            
            ),

            "Status" => [
                "data" => "$.stat_bayar",
                "searchable" => true,
                "orderable" => true,
            ],

        );

    }

}