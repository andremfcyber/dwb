<?php

class Collector extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 123456);
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mcollector");
        $this->theme_module = "collector";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "c_collector";
        $this->dttModel = "Mdcollector";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Collector";
        $data['page_subtitle'] = "Modul Collector";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }


    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Emause', 
            'submenu' => 'collector.collector' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "collector_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "collector_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "collector_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "collector_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(
            
            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."collector/collector/add';}"

            ),
            
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "nama_collector" => array(

                "data" => "b.nama_collector",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "no_hp" => array(

                "data" => "b.no_telp",
                "searchable" => true,
                "orderable" => true,
            
            ),

            "alamat" => array(

                "data" => "b.alamat",
                "searchable" => true,
                "orderable" => true,
            
            ),
        
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit($id = ''){

        $data = array(

            "nama_collector" => array(

                "label" => "Nama Collector",
                "type" => "text",
                "placeholder" => "Nama Collector",
                "class" => "form-control validate[required]",
               
            ),

            "no_telp" => array(

                "label" => "No.HP <span style='color: red'>* No Hp Wajib (62), contoh: 62837482789 </span>",
                "type" => "text",
                "placeholder" => "Nomor HP",
                "class" => "form-control validate[required] no_hp",
               
            ),

            "email" => array(

                "label" => "Email <span style='color: red'>* Format Email Harus Benar, contoh: email@sample.com </span>",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required]",
               
            ),

            "alamat" => array (
                "label" => "Alamat",
                "type" => "textarea",
                "placeholder" => "Alamat Nasabah",
                "class" => "form-control",
            ),  

            "username" => array (
                "label" => "Username",
                "type" => "text",
                "placeholder" => "Username",
                "class" => "form-control",
            ),  

            "photo" => array (
                "label" => "Foto",
                "type" => "upload_file",
                "placeholder" => "Foto",
                "class" => "form-control",
                "file_path" => 'upload/photo/'
            ),  

        );
        if($id){
            unset($data['photo']);

            $data["photo"] = array (
                "label" => "Foto",
                "type" => "upload_file",
                "placeholder" => "Foto",
                "class" => "form-control",
                "file_path" => 'upload/photo/'
            );
        }

        return $data;

// 
    }

    public function add(){
        // map_y('d');
       
        if(count($_POST)>0){
            $username = $_POST['username'];
            $check_exist = $this->db->query("SELECT * FROM c_collector WHERE username = '$username' ")->result_array();
            $check_exist = count($check_exist);
            if($check_exist){
                $res['msg'] = "Username tidak boleh sama!";
                $res['status'] = "0";

                echo json_encode($res);
                exit;
            } 
            $pass = "user123";
            $_POST['password'] = md5($pass);
            $no_tlp = $_POST['no_telp'];
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_telp'] = $no_tlp;
            $_POST ['status'] = "1";
            $_POST ['created_at'] = date('Y-m-d H:i:s');
            $_POST ['created_by'] = $this->session->userdata('username');
            // $_POST ['updated_at'] = date('Y-m-d H:i:s');
            // $_POST ['tgl_pengajuan'] = date('Y-m-d H:i:s');
            unset($_POST['tmp_name']);
            unset($_POST['old_photo']);
            
            if(count($_FILES)>0){
                if($_FILES["photo"]["name"]!=""){

                    if($_FILES["photo"]["type"]=="image/png" or
                        $_FILES["photo"]["type"]=="image/jpg" or
                        $_FILES["photo"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }

               
            }
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;
            $data['params']['log'] = 'Tambah collector dari dashboard atas nama ' . $_POST['nama_collector'];

            $this->load->library("Cinta",$data);
            $insert = $this->cinta->process();
            
            if($insert){
               
                $res = array("status" => "1", "msg" => "Data berhasil Ditambah!");
                
            }else{

                $res = array("status" => "1", "msg" => "Oops! Telah terjadi kesalahan.");

            }

            echo json_encode($res);

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Nasabah";
            $data['page_subtitle'] = "Modul Collector";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";
            $data['ikeh_ikeh_js'] = base_url('assets/dist/js/pages/nasabah.js');
            // map_y($data);
            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){
        // map_y($id);
        if(count($_POST)>0){
            $username = $_POST['username'];
            $check_exist = $this->db->query("SELECT * FROM c_collector WHERE username = '$username' AND id <> $id ")->result_array();
            $check_exist = count($check_exist);
            if($check_exist){
                $res['msg'] = "Username tidak boleh sama!";
                $res['status'] = "0";

                echo json_encode($res);
                exit;
            } 
            $pass = "user123";
            $_POST['password'] = md5($pass);
            $no_tlp = $_POST['no_telp'];
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_telp'] = $no_tlp;
            $_POST ['status'] = "1";
            $_POST ['created_at'] = date('Y-m-d H:i:s');
            $_POST ['created_by'] = $this->session->userdata('username');
            // $_POST ['updated_at'] = date('Y-m-d H:i:s');
            // $_POST ['tgl_pengajuan'] = date('Y-m-d H:i:s');
            unset($_POST['tmp_name']);
            unset($_POST['old_photo']);
            
            if(count($_FILES)>0){
                if($_FILES["photo"]["name"]!=""){

                    if($_FILES["photo"]["type"]=="image/png" or
                        $_FILES["photo"]["type"]=="image/jpg" or
                        $_FILES["photo"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['photo']['name']);
                        $extension = end($array);
                        $photo = md5(uniqid(rand(), true)).".".$extension;

                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['photo'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }

               
            }
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit collector dari dashboard atas nama ' . $_POST['nama_collector'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();


        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Pengajuan Kredit";
            $data['page_subtitle'] = "Modul Nasabah";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit($id);
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}