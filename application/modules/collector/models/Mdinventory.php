<?php

class Mdinventory extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'kantor_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'inventory_collect_detail');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'inventory_collect_detail');

    }
    public function appendToSelectStr() {
        $detail = '';
        $str = '';
        // if($this->allow_detail){
            $detail = '<a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="detail dan bukti bayar" href="'.base_url('collector/inventorycollect/detail_tagihan/').'\',b.id,\'"><i class="fa fa-eye"></i></a>&nbsp;';
        // }

        if($detail!=''){
            $op = "concat('".$detail."')";
            $str = array(
                "op" => $op,
                "stat_bayar" => "CASE WHEN  h.nominal_bayar = g.bayar_bulan then 'Sudah Bayar'     
                                ELSE 'BelumBayar ' END",
            );
        }

        return $str;

    }

    public function fromTableStr() {
        return "c_inventory_collect b";
    }

    public function joinArray(){
        return array(
            "sales c |left" => "b.id_collector = c.id",
            "c_pinjaman d |left" => "b.id_pinjaman = d.id",
            "c_nasabah e |left" => "d.id_nasabah = e.id",
            "product f |left" => "d.id_product = f.id",
            "c_angsuran g |left" => "b.id_angsuran = g.id",
            "c_penagihan h |right" => "h.id_inventory_collect = b.id"  
        );
    }

    public function whereClauseArray(){
        return [
            'g.status' => 0,
        ];
    }


}