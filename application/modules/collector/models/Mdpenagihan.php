<?php

class Mdpenagihan extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'penagihan_send');
        
    }
    public function appendToSelectStr() {
        $penagihan = '';
        $str = '';
        $url_l = base_url('collector/penagihan/form_penagihan');
        $url_wa = base_url('collector/penagihan/form_penagihan_wa');
        $header = 'Kirim Penagihan';

        $penagihan = '<a class="btn btn-sm btn-info" href="javascript:send_penagihan(\',b.id_angsuran,\', `'.$url_l.'`,`'.$header.'`);" data-toggle="tooltip" data-placement="left" title="Kirim Email Penagihan" ><i class="fa fa-send"></i></a>&nbsp;';
        $wa = '<a class="btn btn-sm btn-success" href="javascript:send_penagihan(\',b.id_angsuran,\', `'.$url_wa.'`,`'.$header.'`);" data-toggle="tooltip" data-placement="left" title="Kirim WA Penagihan" ><i class="fa fa-send"></i></a>&nbsp;';
        
        if($penagihan!=''){
            $op = "concat('".$penagihan.$wa."')";
            $str = array(
                "op" => $op
            );
        }

        return $str;

    }

    public function fromTableStr() {
        return "v_c_tagihan b";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return null;
    }


}