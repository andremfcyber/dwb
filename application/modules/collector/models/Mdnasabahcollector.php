<?php

class Mdnasabahcollector extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'nasabah_collector_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'nasabah_collector_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'nasabah_collector_detail');

    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';


        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

        $detail = '<a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="List Pinjaman" href="'.base_url('collector/produknasabah/show?idnasabah=').'\',b.id,\'"><i class="fa fa-list-alt"></i></a>&nbsp;';
        $detail_nasabah = '<a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Detail nasabah" href="'.base_url('collector/nasabahcollector/detail_nasabah/').'\',b.id,\'"><i class="fa fa-eye"></i></a>&nbsp;';

        if($edit!='' || $detail!='' || $delete!='' || $detail_nasabah!=''){
            $op = "concat('".$edit.$detail.$delete.$detail_nasabah."')";
            $str = array(
                "op" => $op
            );
        }

        return $str;

    }

    public function fromTableStr() {
        return "c_nasabah b";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return null;
    }


}