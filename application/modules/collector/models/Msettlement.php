<?php

class Msettlement extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getSettlementbyAO($ao){
        $result = $this->db->query("SELECT * FROM emaus.v_settlement where ao= $ao")->row_array();
        return @$result;
    }

    function getDetailSettlementbyAO($ao){
        $result = $this->db->query("SELECT * FROM emaus.v_detail_settlement where ao = $ao")->result_array();
        return @$result;
    }

}