<?php

class Mddetailpinjaman extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'angsuran_colector_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'angsuran_colector_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'angsuran_colector_detail');

    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';


        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }
        // map_y();

        // $detail = '<a class="btn btn-sm btn-info" id="hs_p_\',b.id,\'" href="'.base_url('collector/detailpinjaman/detail_tagihan/').'\',b.id,\'"><i class="fa fa-eye"></i></a>&nbsp;}';

    

        if($edit!=''|| $detail!=''){
            $op = "concat('".$edit.$detail."')";
            $date = date("Y-m-d");
            $str = array(

                "op" => $op,
                "telat" => "CASE WHEN  (now() > b.tgl_penagihan) AND  (b.status =0) then '".$date."' - b.tgl_penagihan ELSE '0' END",
                "stat_bayar" => "CASE WHEN  (now() > b.tgl_penagihan) AND  (b.status =0) then 
                                CASE WHEN ('".$date."' - b.tgl_penagihan  > 6) AND ('".$date."' - b.tgl_penagihan  < 14) then 'collect 1'
                                    WHEN ('".$date."' - b.tgl_penagihan > 14) AND ('".$date."' - b.tgl_penagihan < 21) then 'collect 2'
                                    WHEN ('".$date."' - b.tgl_penagihan > 20) then 'collect 3'
                                    ELSE 'telat' END
                                WHEN (now() > b.tgl_penagihan) AND (b.status =1) then 'Sudah Bayar'   
                                ELSE 'Belum Bayar' END",
                "id_ang" => "b.id",
            
            );
        }

        return $str;

    }

    public function fromTableStr() {
        return "c_angsuran b";
    }

    public function joinArray(){
        return array(
            "c_pinjaman c |left" => "b.id_pinjaman = c.id",
        );
    }

    public function whereClauseArray(){
    
        return [
            'b.id_pinjaman' => $_SESSION['idpinjaman'],
            
        ];

    }

    public function orderBy(){
        return [
            'b.created_at' => 'DESC',
        ];
    }


}