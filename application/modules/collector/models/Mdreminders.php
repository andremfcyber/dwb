<?php

class Mdreminders extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'kantor_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'kantor_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'kantor_detail');

    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';
        $url_l = base_url('collector/penagihan/form_penagihan');

        // if($this->allow_delete){
        //     $delete = '<a class="btn btn-sm btn-info" href="javascript:send_penagihan(\',b.id,\', `'.$url_l.'`);" data-toggle="tooltip" data-placement="left" title="Kirim Email Penagihan" ><i class="fa fa-send"></i></a>&nbsp;';
        // }

        // $detail = '<a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="List Pinjaman" href="'.base_url('collector/produknasabah/show/').'\',b.id,\'"><i class="fa fa-list-alt"></i></a>&nbsp;';

        // if($edit!=''){
        //     $op = "concat('".$edit."')";
        //     $str = array(
        //         "op" => $op
        //     );
        // }

        // return $str;

    }

    public function fromTableStr() {
        return "c_reminders b";
    }

    public function joinArray(){
        return array(
            "c_nasabah c |left" => "b.id_nasabah = c.id",
            "c_angsuran d |left" => "b.id_angsuran = d.id",
            "c_pinjaman e |left" => "d.id_pinjaman = e.id",
        );
    }

    public function whereClauseArray(){
        return null;
    }


}