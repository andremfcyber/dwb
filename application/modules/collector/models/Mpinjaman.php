<?php

class Mpinjaman extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->table = 'emaus."Nasabah"';
        $this->table2 = 'emaus."Pinjaman"';

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }
    }

    function getPinjaman(){

        $result = $this->db->query("SELECT * FROM $this->table2 a JOIN $this->table b ON a.no_rek = b.no_rek")->result_array();
        return $result;

    }

    function getNasabahByNorek($no_rek){

        $result = $this->db->query("SELECT * FROM $this->table WHERE no_rek='".$no_rek."'")->result_array();
        return @$result[0];

    }

    function getPinjamanbyId($id){
        
        $result = $this->db->query(
            "SELECT * FROM $this->table2 a JOIN $this->table b ON a.no_rek = b.no_rek
            WHERE a.id_pinjaman = '$id'"
        )->result_array();
        return @$result[0];

    }

}