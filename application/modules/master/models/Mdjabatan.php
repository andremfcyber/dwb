<?php

class Mdjabatan extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'jabatan_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'jabatan_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'jabatan_detail');
        $this->allow_rule_produk = $this->mcore->checkPermission($this->user_group, 'jabatan_produk_rule');


    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';


        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

        if($this->allow_rule_produk){
            $rule = '<a class="btn btn-sm btn-success" href="'.base_url().'master/jabatan/rule_produk/\',b.id,\'"><i class="fa fa-check"></i></a>';
        }

        if($edit!='' || $delete!='' || $rule     !=''){
            $op = "concat('".$edit.$delete.$rule."')";
            $str = array(
                "op" => $op
            );
        }

        return $str;

    }

    public function fromTableStr() {
        return "jabatan_agen b";
    }

    public function joinArray(){
        return null;
    }

    public function whereClauseArray(){
        return null;
    }


}