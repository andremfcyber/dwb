<?php

class Mbulan extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getBulan(){
        $result = $this->db->query("SELECT * FROM master_bulan")->result_array();
        return @$result;
    }

    function getBulanById($id){

        $result = $this->db->query("SELECT * FROM master_bulan WHERE id='".$id."'")->result_array();
        return @$result[0];

    }

}