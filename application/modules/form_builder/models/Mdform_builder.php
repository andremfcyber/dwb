<?php

class Mdform_builder extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'form_builder_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'form_builder_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'form_builder_detail');

    }

    public function appendToSelectStr() {
        $edit = '';
        $delete = '';
        $detail = "";
        $str = '';


        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit" href="javascript:edit(\',fh.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus" href="javascript:remove(\',fh.id,\');"><i class="fa fa-remove"></i></a>&nbsp;';
        }

        if($this->allow_detail){
            $detail = '<a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail Form" href="'.base_url('form_fields/show/').'\',fh.id,\'"><i class="fa fa-list-alt"></i></a>&nbsp;';
        }



        if($edit!='' || $delete!='' || $detail != ''){

            $op = "concat('".$edit.$delete.$detail."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "form_header fh";
    }

    public function joinArray(){
        // return null;
        return array(
            "product p |left" => "fh.product_id = p.id",
        );
    }

    public function whereClauseArray(){
        // return null;
        return array(
            "p.id" => $_SESSION['id_product']
        );
    }


}