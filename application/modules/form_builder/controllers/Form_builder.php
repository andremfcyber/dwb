<?php

class Form_builder extends Admin_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mform_builder");
        $this->load->model("product/Mproduct");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "form_header";
        $this->dttModel = "Mdform_builder";
        $this->pk = "id";
	}
	function index(){
        
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "List Form Header";
        $data['page_subtitle'] = "Form";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();
    }

    public function show($id){
        $_SESSION['id_product'] = $id;
        $this->index();
    }

    public function dataTable() {
        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'fh.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Product', 
            'submenu' => '' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "form_builder_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "form_builder_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "form_builder_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "form_builder_delete"),
            "detail_prem" => $this->mcore->checkPermission($this->user_group, "form_builder_detail"),

        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."form_builder/form_builder/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "Nama Header Form" => array(

                "data" => "fh.name_header",
                "searchable" => true,
                "orderable" => true,
              

            ),


            "Product" => array(

                "data" => "p.name",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "Urutan" => array(

                "data" => "fh.urutan",
                "searchable" => true,
                "orderable" => true,
              

            ),
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "name_header" => array(

                "label" => "Nama Header Form",
                "type" => "text",
                "placeholder" => "Nama Header Form",
                "class" => "form-control validate[required]",
               

            ),

            "product_id" => array(

                "label" => "Product",
                "type" => "sourcequery",
                "source" => $this->Mproduct->getProduct(),
                "keydt" => "id",
                "valuedt" => "name",
                "class" => "form-control select2 validate[required]",
               
            ),

            "urutan" => array(
            	"label" => "Urutan",
            	"type" => "text",
            	"placeholder" => "Urutan",
            	"class" => "form-control validate[required]",
            ),
            

        );

// 
    }

    public function add(){

        if(count($_POST)>0){
           
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['data_return'] = true;
            $data['params']['log'] = 'Tambah Form Header ' . $_POST['name_header'];

            $this->load->library("Cinta",$data);
            $insert = $this->cinta->process();
            
            if($insert){
               
                $res = array("status" => "1", "msg" => "Data berhasil terupdate!");
                
            }else{

                $res = array("status" => "1", "msg" => "Oops! Telah terjadi kesalahan.");

            }

            echo json_encode($res);

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Pengajuan Form Header";
            $data['page_subtitle'] = "Modul Form";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Form Header ' . $_POST['name_header'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();


        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Form Builder";
            $data['page_subtitle'] = "Modul Form";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }
}