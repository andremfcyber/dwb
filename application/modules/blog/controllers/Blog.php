<?php

class Blog extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mblog");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "cms_blog";
        $this->dttModel = "Mdblog";
        $this->pk = "id";

    }

    function index(){

        
        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "List Blog Key";
        $data['page_subtitle'] = "Modul Blog Key";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'Developer Option', 
            'submenu' => 'Blog' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "blog_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "blog_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "blog_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "blog_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => DATATABLE_DOM_CONF

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."blog/Blog/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "flag" => array(

                "data" => "b.flag",
                "searchable" => true,
                "orderable" => true,
              

            ),

            "judul" => array(

                "data" => "b.judul",
                "searchable" => false,
                "orderable" => true,
             

            ),

            "isi" => array(

                "data" => "b.isi",
                "searchable" => false,
                "orderable" => true,
             

            ),

            "input_oleh" => array(

                "data" => "b.input_oleh",
                "searchable" => false,
                "orderable" => true,
             

            ),

            "update_oleh" => array(

                "data" => "b.update_oleh",
                "searchable" => false,
                "orderable" => true,
             

            ),

            "input_tgl" => array(

                "data" => "b.input_tgl",
                "searchable" => false,
                "orderable" => true,
             

            ),

            "update_tgl" => array(

                "data" => "b.update_tgl",
                "searchable" => false,
                "orderable" => true,
             

            ),

            // "file_foto" => array(

            //     "data" => "b.file_foto",
            //     "searchable" => false,
            //     "orderable" => true,
             

            // ),

            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "id" => array(

                "label" => "Id",
                "type" => "text",
                "placeholder" => "Id",
                "class" => "form-control validate[required]",
               

            ),

            "flag" => array(

                "label" => "Flag",
                "type" => "text",
                "value" => "2",
                "placeholder" => "Flag",
                "class" => "form-control validate[required]",
               

            ),

            "judul" => array(

                "label" => "Judul",
                "type" => "text",
                "placeholder" => "Judul",
                "class" => "form-control validate[required]",
             

            ),     

            "isi" => array(

                "label" => "Isi",
                "type" => "editor",
                "placeholder" => "Isi",
                "class" => "form-control validate[required]",
             

            ),     

            "input_oleh" => array(

                "label" => "Input Oleh",
                "type" => "text",
                "placeholder" => "Input Oleh",
                "class" => "form-control validate[required]",
             

            ), 

            "update_oleh" => array(

                "label" => "Update Oleh",
                "type" => "text",
                "placeholder" => "Update Oleh",
                "class" => "form-control validate[required]",
             

            ),   

            "input_tgl" => array(

                "label" => "Input Tanggal",
                "type" => "datepicker",
                "placeholder" => "Input Tanggal",
                "class" => "form-control validate[required]",
             

            ),     

            "update_tgl" => array(

                "label" => "Update Tanggal",
                "type" => "datepicker",
                "placeholder" => "Update Tanggal",
                "class" => "form-control validate[required]",
             

            ),

            // "file_foto" => array(

            //     "label" => "Upload File",
            //     "type" => "upload_file",
                    // "upload_path" => "upload/ipload"
            //     "placeholder" => "Update File",
            //     "class" => "form-control validate[required]",
             

            // ),
            
            

        );


    }

    public function add(){

        if(count($_POST)>0){

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Blog Key";
            $data['page_subtitle'] = "Modul Blog Key";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Blog Key";
            $data['page_subtitle'] = "Modul Blog Key";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}