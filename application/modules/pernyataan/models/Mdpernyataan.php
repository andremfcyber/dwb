<?php

class Mdpernyataan extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_print = $this->mcore->checkPermission($this->user_group, 'pernyataan_print');
     
    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $str = '';

        if($this->allow_print){
            $pernyataan = '<a target="_blank" class="btn btn-sm btn-success" href="'.base_url('/pernyataan/print/').'\',b.id,\'"><i class="fa fa-print"></i></a>&nbsp;';
        }

        if($pernyataan!=''){
            $op = "concat('".$pernyataan."')";
            $str = array(
                "op" => $op
            );
        }

        return $str;

    }

    public function fromTableStr() {
        return "nasabah b";
    }

    public function joinArray(){
        return [
            "sales c|left" => "b.sales_id = c.id",
        ];
    }

    public function whereClauseArray(){
        $role_id = get_role();
        $sales_id = sales_by_user_id()['id'];

        if($role_id == '18'){
            return [
                'b.sales_id' => $sales_id,
            ];
        }
    }


}