<?php

class Mdpelamar extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        // $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'pelamar_update');
        $this->allow_delete = $this->mcore->checkPermission($this->user_group, 'pelamar_delete');
        $this->allow_detail = $this->mcore->checkPermission($this->user_group, 'pelamar_detail');


    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';
 
         $detail = '<a class="btn btn-sm btn-success" href="'.base_url('frontend/pelamar/merge_pdf/').'\',b.id,\'" data-toggle="tooltip" data-placement="top" title="download all file" target="_blank" download><i class="fa fa-file"></i></a>&nbsp;';
      
        // if($this->allow_edit){
        //     $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        // }
        // $detail = '<a class="btn btn-sm btn-info" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>';
        if($this->allow_delete){
            $delete = '<a class="btn btn-sm btn-danger" href="javascript:remove(\',b.id,\');"><i class="fa fa-remove"></i></a>';
        }

        if( $delete!='' || $detail!=''){

            $op = "concat('".$detail.$delete."')";
            $str = array(

                "op" => $op,
                'pelamar' => 'b.nama'
            );

        }

        return $str;

    }

    public function fromTableStr() {
        return "front_pelamar b";
    }

    public function joinArray(){
        return array(
            "front_career c |left" => "b.id_career = c.id",
        );
    }

    public function whereClauseArray(){
        return null;
    }

    public function orderBy(){
        return [
            'b.created_at' => 'DESC',
        ];
    }
}