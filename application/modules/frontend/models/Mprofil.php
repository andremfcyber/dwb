<?php

class Mprofil extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");

        if($this->aauth->is_loggedin()) {
            $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        }

    }
    function getprofilById($id){

        $result = $this->db->query("SELECT * FROM front_profil WHERE id='".$id."'")->result_array();
        return @$result[0];

    }
    function getKey(){

        $result = $this->db->query("SELECT * FROM front_profil")->result_array();
        return $result;

    }
    function getkategori(){

        $result = $this->db->query("SELECT * FROM front_kategori")->result_array();
        return $result;

    }
    function getkategori_kerja(){

        $result = $this->db->query("SELECT * FROM front_kategori_career")->result_array();
        return $result;

    }
    function gettipekerja(){

        $result = $this->db->query("SELECT * FROM front_tipe_career")->result_array();
        return $result;

    }
    function getkategori_laporan(){

        $result = $this->db->query("SELECT * FROM front_kategori_laporan")->result_array();
        return $result;

    }

    function getkategori_galeri(){

        $result = $this->db->query("SELECT * FROM front_kategori_galeri")->result_array();
        return $result;

    }

}