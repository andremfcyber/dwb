<?php

class Mddepan extends MY_Model implements DatatableModel{

    function __construct(){

        parent::__construct();
        $this->load->library('mcore');
        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));
        $this->allow_edit = $this->mcore->checkPermission($this->user_group, 'baner_update');
       


    }
    public function appendToSelectStr() {
        $detail = '';
        $edit = '';
        $delete = '';
        $str = '';

        // if($this->allow_detail){
        //     $detail = '<a class="btn btn-sm btn-success" href="'.base_url('form_fields/show/').'\',b.id,\'"><i class="fa fa-file"></i></a>&nbsp;';
        // }

        if($this->allow_edit){
            $edit = '<a class="btn btn-sm btn-primary" href="javascript:edit(\',b.id,\');"><i class="fa fa-pencil"></i></a>&nbsp;';
        }

    

        if($edit!=''){

            $op = "concat('".$edit."')";
            $str = array(

                "op" => $op

            );


        }

        return $str;

    }

    public function fromTableStr() {
        return "front_depan b";
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return NULL;
    }


}