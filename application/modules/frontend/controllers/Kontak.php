<?php

class Kontak extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mprofil");
        $this->theme_module = "kontak";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "front_kontak";
        $this->dttModel = "Mdkontak";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Kontak";
        $data['page_subtitle'] = "Modul kontak";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Kontak";
        $data['page_subtitle'] = "Modul kontak";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'frontend', 
            'submenu' => 'frontend.kontak' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "kontak_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "kontak_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "kontak_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "kontak_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_logo.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."kontak/kontak/add';}"

            // ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            // "label" => array(

            //     "data" => "b.label",
            //     "searchable" => true,
            //     "orderable" => true,
              

            // ),

            "Alamat" => array(

                "data" => "b.alamat",
                "searchable" => true,
                "orderable" => true,
              

            ),
            "Phone Number" => array(

                "data" => "b.notelpon",
                "searchable" => true,
                "orderable" => true,
              

            ),
            "fax" => array(

                "data" => "b.fax",
                "searchable" => true,
                "orderable" => true,
              

            ),
            "Email" => array(

                "data" => "b.email",
                "searchable" => true,
                "orderable" => true,
              

            ),
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){


        return array(

            "alamat" => array(

                "label" => "Alamat",
                "type" => "textarea",
                "placeholder" => "Alamat",
                "class" => "form-control validate[required]",
            ),

            "notelpon" => array(

                "label" => "No. Telepon",
                "type" => "text",
                "placeholder" => "No. Telepon",
                "class" => "form-control validate[required]",

            ),
            "no_wa" => array(

                "label" => "No. WhatsApp",
                "type" => "text",
                "placeholder" => "No. WhatsApp",
                "class" => "form-control validate[required]",

            ),

            "fax" => array(

                "label" => "Fax",
                "type" => "text",
                "placeholder" => "Fax",
                "class" => "form-control validate[required]",

            ),
            
            "email" => array(

                "label" => "Email",
                "type" => "text",
                "placeholder" => "Email",
                "class" => "form-control validate[required]",

            ),
            "fb" => array(

                "label" => "Facebook",
                "type" => "text",
                "placeholder" => "Facebook",
                "class" => "form-control validate[required]",

            ),
            "twitter" => array(

                "label" => "Twitter",
                "type" => "text",
                "placeholder" => "Twitter",
                "class" => "form-control validate[required]",

            ),
            "ig" => array(

                "label" => "Instagram",
                "type" => "text",
                "placeholder" => "Instagram",
                "class" => "form-control validate[required]",

            ),
            "foto" => array (
                "label" => "Image",
                "type" => "upload_file",
                "placeholder" => "Image",
                "class" => "form-control",
                "file_path" => 'upload/photo/'
            ),
            
            "keterangan" => array(

                "label" => "Iframe Google Maps",
                "type" => "textarea",
                "placeholder" => "Iframe Google Maps",
                "class" => "form-control validate[required]",

            ),

            // "lat" => array(

            //     "label" => "Latitude",
            //     "type" => "text",
            //     "placeholder" => "Latitude",
            //     "class" => "form-control validate[required]",

            // ),
            
            // "lng" => array(

            //     "label" => "longitude",
            //     "type" => "text",
            //     "placeholder" => "Longitude",
            //     "class" => "form-control validate[required]",

            // ),
            
        );

// 
    }

    public function add(){

        if(count($_POST)>0){
            $no_tlp = $_POST['no_telp'];
            // $pos = strpos($no_tlp, '0');
            if ($no_tlp[0] === '0') {
                $no_tlp = substr_replace($no_tlp, '62', 0, 1);
            } else {
                $no_tlp = $no_tlp;
            }
            $_POST['no_telp'] = $no_tlp;

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Tambah kontak dari dashboard atas nama ' . $_POST['nama_kontak'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah kontak";
            $data['page_subtitle'] = "Modul kontak";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            $no_wa = $_POST['no_wa'];
            // $pos = strpos($no_wa, '0');
            if ($no_wa[0] === '0') {
                $no_wa = substr_replace($no_wa, '62', 0, 1);
            } else {
                $no_wa = $no_wa;
            }
            $_POST['no_wa'] = $no_wa;
            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            $_POST ['updated_by'] = $this->session->userdata('username');
            
            unset($_POST['tmp_name']);
            unset($_POST['old_foto']);

            if(count($_FILES)>0){
                if($_FILES["foto"]["name"]!=""){

                    if($_FILES["foto"]["type"]=="image/png" or
                        $_FILES["foto"]["type"]=="image/jpg" or
                        $_FILES["foto"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['foto']['name']);
                        $extension = end($array);
                        $photo = generate_name_random().".".$extension;
                        $path = 'upload/photo/'.get_file_name('front_kontak','id',$id,'foto');
                        delete_file($path);

                        if (move_uploaded_file($_FILES["foto"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['foto'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{

                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }

            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Kontak' . $_POST['alamat'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit kontak";
            $data['page_subtitle'] = "Modul kontak";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}