<?php

class Halaman extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mprofil");
        $this->theme_module = "dashboard";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "front_halaman";
        $this->dttModel = "MdHalaman";
        $this->pk = "id";

    }

    function index(){
        
        if(isset($_GET['page'])){
            $page = $_GET['page'];
            $_SESSION["kode"] = $page;
        }else{
            $page = "";
            $_SESSION["kode"] = "";
            // unset($_SESSION['kode']);
        } 

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Halaman ".ucwords($page);
        $data['page_subtitle'] = "Modul Halaman";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

     function pagemaster(){   
        $this->_theme_vars['page_title'] = 'Page Master';
        $this->_theme_vars['page_subtitle'] = 'Mengelola Halaman';
        $this->_theme_vars['page'] = 'page_master';
        $this->_theme_vars['parent_menu'] = 'page_master';
        $this->_theme_vars['submenu'] = '';

        $this->set_admin_theme($this->_theme_vars['active_admin_theme'],"main");
        $this->render_admin_view('v_page_master');
    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Halaman";
        $data['page_subtitle'] = "Modul Halaman";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'frontend', 
            'submenu' => 'frontend.Halaman' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "fax_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "fax_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "fax_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "fax_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."frontend/Halaman/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array( 
            
            "foto" => array(
                "data" => "b.foto",
                "searchable" => false,
                "orderable" => false,
                "render" => "function(data, type, row){ let src = ''; if(!data){ src = base_url+'upload/noimg.webp'; }else{ src = base_url+'upload/photo/'+data; } let html = `<img src='\${src}' style='height: 100px; width: 100px; object-fit: cover;'>`; return html; }"
            ),

            "judul" => array(

                "data" => "b.judul",
                "searchable" => true,
                "orderable" => true,
              

            ),
            "halaman" => array(

                "data" => "b.halaman",
                "searchable" => true,
                "orderable" => true,
              

            ),
            "type" => array(

                "data" => "b.type",
                "searchable" => true,
                "orderable" => true,
              

            ),
            // "isi" => array(

            //     "data" => "b.isi",
            //     "searchable" => true,
            //     "orderable" => true,
              

            // ),
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){

        if($_SESSION["kode"]!=""){
            $value = $_SESSION["kode"];
            $readonly = true;
        }else{ 
            $readonly = false;
            $value = "";
        }

        return array(
            "halaman" => array(
                
                "label" => "halaman (example:name-tag)",
                "type" => "text",
                "placeholder" => "halaman",
                "class" => "form-control validate[required]",
                "readonly" => $readonly,
                "value" => $value 

            ),     
            // "type" => array( 
            //     "label" => "type (example:name-tag)",
            //     "type" => "text",
            //     "placeholder" => "type",
            //     "class" => "form-control ", 
            // ),     
            "foto" => array (
                "label" => "Image",
                "type" => "upload_file",
                "placeholder" => "Image",
                "class" => "form-control",
                "file_path" => 'upload/photo/'
            ),

            "judul" => array(

                "label" => "Judul",
                "type" => "text",
                "placeholder" => "Judul",
                "class" => "form-control validate[required]",
             

            ),     

            "isi" => array(

                "label" => "Isi",
                "type" => "textarea",
                "placeholder" => "Isi",
                "id"    => "tiny",
                "class" => "form-control", 
            )     
            

        );

// 
    }

    public function add(){
        
        if(count($_POST)>0){

            // print_r($_POST); die;

            $_POST ['created_at'] = date('Y-m-d H:i:s');
            $_POST ['created_by'] = $this->session->userdata('username');
            unset($_POST['tmp_name']);
            unset($_POST['old_foto']);
            
            if(count($_FILES)>0){
                if($_FILES["foto"]["name"]!=""){

                    if($_FILES["foto"]["type"]=="image/png" or
                        $_FILES["foto"]["type"]=="image/jpg" or
                        $_FILES["foto"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['foto']['name']);
                        $extension = end($array);
                        $photo = generate_name_random().".".$extension;

                        if (move_uploaded_file($_FILES["foto"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['foto'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{


                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }
                }
            }
            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Tambah Halaman' . $_POST['judul'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Halaman";
            $data['page_subtitle'] = "Modul Halaman";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
 

            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            $_POST ['updated_by'] = $this->session->userdata('username');
            unset($_POST['tmp_name']);
            unset($_POST['old_foto']);

            if(count($_FILES)>0){
                if($_FILES["foto"]["name"]!=""){

                    if($_FILES["foto"]["type"]=="image/png" or
                        $_FILES["foto"]["type"]=="image/jpg" or
                        $_FILES["foto"]["type"]=="image/jpeg"){

                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/photo/';
                        $array = explode('.', $_FILES['foto']['name']);
                        $extension = end($array);
                        $photo = generate_name_random().".".$extension;
                        $path = 'upload/photo/'.get_file_name('front_halaman','id',$id,'foto');
                        delete_file($path);

                        if (move_uploaded_file($_FILES["foto"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['foto'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }

                    }else{

                        $res['msg'] = "Invalid cover file! Should be PNG/JPG/JPEG.";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }

                }
            }
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Halaman ' . $_POST['judul'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit Halaman";
            $data['page_subtitle'] = "Modul Halaman";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

            //File
            $data['params']['file_path'] = 'upload/photo';
            // $data['params']['file_field'] = get_file_name($this->table,$this->pk,$_POST['id'],'foto');
            $data['params']['file_field'] = 'foto';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

 

}