<?php

class Laporan extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->load->model("Mprofil");
        $this->theme_module = "laporan";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "front_laporan";
        $this->dttModel = "Mdlaporan";
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Laporan";
        $data['page_subtitle'] = "Modul laporan";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Fax";
            $data['page_subtitle'] = "Modul laporan";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
            
            
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'frontend', 
            'submenu' => 'frontend.laporan' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(

            "add_perm" => $this->mcore->checkPermission($this->user_group, "laporan_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "laporan_view"),
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "laporan_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "laporan_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            "lengthChange" => false,
            "dom" => "Bfrtip"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            array(

                "text" => '<i class="fa fa-plus"></i> Tambah',
                "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."frontend/laporan/add';}"

            ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            // "label" => array(

            //     "data" => "b.label",
            //     "searchable" => true,
            //     "orderable" => true,
              

            // ),

            "nama_file" => array(

                "data" => "b.nama_file",
                "searchable" => true,
                "orderable" => true,
              

            ),
        
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,
            

            )

        );

    }
    
    private function _get_fields_edit(){

       
        return array(

            "nama_file" => array (
                "label" => "Nama File",
                "type" => "text",
                "placeholder" => "Nama File",
                "class" => "form-control",
            ),
            "tahun" => array (
                "label" => "Tahun",
                "type" => "text",
                "placeholder" => "Tahun",
                "class" => "form-control",
            ),  
            
            "id_kategori_laporan" => array(

                "label" => "Kategori Laporan",
                "type" => "sourcequery",
                "source" => $this->Mprofil->getkategori_laporan(),
                "keydt" => "id",
                "valuedt" => "kategori_laporan",
                "class" => "form-control select2 validate[required]",
               
            ),
            "file" => array (
                "label" => "File",
                "type" => "upload_file",
                "placeholder" => "Image",
                "class" => "form-control",
                "file_path" => 'upload/file/'
            ),  

        );

// 
    }

    public function add(){

        if(count($_POST)>0){
            $_POST ['created_at'] = date('Y-m-d H:i:s');
            $_POST ['created_by'] = $this->session->userdata('username');
           
            unset($_POST['tmp_name']);
            unset($_POST['old_file']);
            
            if(count($_FILES)>0){
                if($_FILES["file"]["name"]!=""){

                    $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/file/';
                    $array = explode('.', $_FILES['file']['name']);
                    $extension = end($array);
                    $photo = generate_name_random().".".$extension;

                    if (move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path."/".$photo)) {
                        $_POST['file'] = $photo;  
                    }else{


                        $res['msg'] = "Oops! Something went wrong!";
                        $res['status'] = "0";

                        echo json_encode($res);
                        exit;

                    }
                }
            }

            $data['params']['action'] = "save";
            $data['params']['table'] = $this->table;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Tambah Laporan' . $_POST['nama_file'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Tambah Laporan";
            $data['page_subtitle'] = "Modul laporan";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();

            $data['params']['form']['action'] = "add";

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function edit($id){

        if(count($_POST)>0){
            $_POST ['updated_at'] = date('Y-m-d H:i:s');
            $_POST ['updated_by'] = $this->session->userdata('username');
            unset($_POST['tmp_name']);
            unset($_POST['old_file']);

            if(count($_FILES)>0){
                if($_FILES["file"]["name"]!=""){

                 
                        $upload_path = $_SERVER['DOCUMENT_ROOT'].'/upload/file/';
                        $array = explode('.', $_FILES['file']['name']);
                        $extension = end($array);
                        $photo = generate_name_random().".".$extension;
                        $path = 'upload/file/'.get_file_name('front_laporan','id',$id,'file');
                        delete_file($path);

                        if (move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path."/".$photo)) {
                            $_POST['file'] = $photo;  
                        }else{


                            $res['msg'] = "Oops! Something went wrong!";
                            $res['status'] = "0";

                            echo json_encode($res);
                            exit;

                        }


                }
            }
            $data['params']['action'] = "update";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;
            $data['params']['post'] = $_POST;
            $data['params']['log'] = 'Edit Laporan ' . $_POST['nama_file'];

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }else{

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Edit laporan";
            $data['page_subtitle'] = "Modul laporan";
            $data['active_menu'] = $this->_get_active_menu(); 
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['params']['form']['fields'] = $this->_get_fields_edit();
            $data['params']['form']['action'] = "edit";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $id;

            $this->load->library("Cinta",$data);
            $this->cinta->render_form();

        }

    }

    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];
            //File
            $data['params']['file_path'] = 'upload/file';
            // $data['params']['file_field'] = get_file_name($this->table,$this->pk,$_POST['id'],'foto');
            $data['params']['file_field'] = 'file';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

}