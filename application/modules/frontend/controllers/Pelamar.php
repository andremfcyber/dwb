<?php

class Pelamar extends Admin_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->model("mcore");
        $this->theme_module = "pelamar";

        if(!$this->aauth->is_loggedin()) {

            redirect('admin');

        }

        $this->table = "front_pelamar";
        $this->dttModel = "Mdpelamar";
        
        $this->pk = "id";

    }

    function index(){

        $data['theme'] = $this->_theme_vars['active_admin_theme'];
        $data['page_title'] = "Pelamar";
        $data['page_subtitle'] = "Modul pelamar";
        $data['current_class_dir'] = $this->router->fetch_directory();
        $data['current_class'] = $this->router->fetch_class();
        $data['permissions'] = $this->_get_permissions();
        $data['active_menu'] = $this->_get_active_menu();  
        $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
        $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
        $data['params']['datatable']['options'] = $this->_get_datatable_option();
        
        
        $this->load->library("Cinta",$data);
        $this->cinta->browse();

    }

    function show($id = null){

        if($id != null){

            $_SESSION['form_filed_id'] = $id;

            $data['theme'] = $this->_theme_vars['active_admin_theme'];
            $data['page_title'] = "Fax";
            $data['page_subtitle'] = "Modul pelamar";
            $data['current_class_dir'] = $this->router->fetch_directory();
            $data['current_class'] = $this->router->fetch_class();
            $data['permissions'] = $this->_get_permissions();
            $data['active_menu'] = $this->_get_active_menu();  
            $data['params']['datatable']['buttons']= $this->_get_datatable_button();   
            $data['params']['datatable']['columns'] = $this->_get_datatable_columns();
            $data['params']['datatable']['options'] = $this->_get_datatable_option();
            
            
            $this->load->library("Cinta",$data);
            $this->cinta->browse();

        }

    }

    public function dataTable() {

        $this->load->library('Datatable', array('model' => $this->dttModel, 'rowIdCol' => 'b.'.$this->pk));
        $json = $this->datatable->datatableJson();
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("Cache-Control: no-store, no-cache");
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    private function _get_active_menu(){

        return array(

            'parent_menu' => 'frontend', 
            'submenu' => 'frontend.pelamar' 
        
        );

    }

    private function _get_permissions(){

        $this->user_group = $this->mcore->getUserGroupName($this->session->userdata('id'));

        return array(
           
            "add_perm" => $this->mcore->checkPermission($this->user_group, "pelamar_add"),
            "read_perm" => $this->mcore->checkPermission($this->user_group, "pelamar_view"),
            
            "edit_perm" => $this->mcore->checkPermission($this->user_group, "pelamar_update"),
            "delete_perm" => $this->mcore->checkPermission($this->user_group, "pelamar_delete"),
        );

    }

    private function _get_datatable_option(){

        $current_class_dir = $this->router->fetch_directory();
        $x = explode("/", $current_class_dir);
        $module = $x[2];
        $current_class = $this->router->fetch_class();

        return array(

            "processing" => true,
            "serverSide" => true,
            "ajax" => array(

                "url" => base_url().$module.'/'.$current_class.'/dataTable',
                "type" => "POST"
            ),
            // "lengthChange" => false,
            "lengthMenu" => [[10, 25, 50, -1], [10, 25, 50, 'All']],
            "dom" => "<lB<t>ip>"

        );

    }

    private function _get_datatable_button(){

        return array(

            array(

                "extend" => 'copyHtml5',
                "text" => '<i class="fa fa-files-o"></i>',
                "titleAttr" => 'Copy',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'excelHtml5',
                "text" => '<i class="fa fa-file-excel-o"></i>',
                "titleAttr" => 'Excel',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'csvHtml5',
                "text" => '<i class="fa fa-file-text-o"></i>',
                "titleAttr" => 'CSV',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                )
            
            ),
            array(

                "extend" => 'pdfHtml5',
                "text" => '<i class="fa fa-file-pdf-o"></i>',
                "titleAttr" => 'PDF',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => 'Daftar API KEY'
            
            ),
            array(

                "extend" => 'print',
                "text" => '<i class="fa fa-print"></i>',
                "titleAttr" => 'Print',
                "exportOptions" => array(

                    "columns" => ':visible'
                
                ),
                "title" => '<img src="'.base_url().'assets/dist/img/app_foto.png" style="width:50px;height:50px" /> <span style="color:#ddd !important">Daftar API KEY</span>'
            
            ),
            // array(

            //     "text" => '<i class="fa fa-plus"></i> Tambah',
            //     "action" => "function ( e, dt, node, config ){window.location.href = '".base_url()."frontend/pelamar/add';}"

            // ),
            'colvis'


        );

    }

    private function _get_datatable_columns(){


        return array(

            "foto_" => array(

                "data" => "b.foto",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, meta, row){
                    let html = '';
                    if(data != null && data != '' && data){
                        let ext = data.split('.');
                        html = `<a href='\${base_url}/upload/photo/\${data}' target='_blank'>Versi Pdf</a>`;
                        if(ext[1] != 'pdf'){
                            html = `<img src='\${base_url}/upload/photo/\${data}' class='scale' data-scale='best-fill' data-align='center' style='width:100px; height:130px;object-fit: cover;'></img>`; 
                        }
                    }
                    return html;
                }"

            ),
            "pelamar" => array(

                "data" => "$.pelamar",
                "searchable" => true,
                "orderable" => true,
              
            ),
            "pekerjaan" => array(

                "data" => "c.nama",
                "searchable" => true,
                "orderable" => true,
              
            ),
            "no_tlp" => array(

                "data" => "b.nohp",
                "searchable" => true,
                "orderable" => true,
              
            ),
            "email" => array(

                "data" => "b.email",
                "searchable" => true,
                "orderable" => true,
              
            ),
            "pekerjaan" => array(

                "data" => "c.nama",
                "searchable" => true,
                "orderable" => true,
              
            ),
            "cv" => array(

                "data" => "b.cv",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, meta, row){ let loc_file = `upload/cv/\${data}`; cek_file(loc_file,'cv'+row.b.id); return `<span id='cv\${row.b.id}'></span>` }"
            ),
            "ijazah" => array(

                "data" => "b.ijazah",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, meta, row){ let loc_file = `upload/file/\${data}`; cek_file(loc_file,'iz'+row.b.id); return `<span id='iz\${row.b.id}'></span>` }"  

            ),
            "transkrip" => array(

                "data" => "b.transkrip",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, meta, row){ let loc_file = `upload/file/\${data}`; cek_file(loc_file,'tr'+row.b.id); return `<span id='tr\${row.b.id}'></span>` }"

            ),
            "ktp" => array(

                "data" => "b.ktp",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, meta, row){ let loc_file = `upload/file/\${data}`; cek_file(loc_file,'ktp'+row.b.id); return `<span id='ktp\${row.b.id}'></span>` }"

            ),
            "kk" => array(

                "data" => "b.kk",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, meta, row){ let loc_file = `upload/file/\${data}`; cek_file(loc_file,'kk'+row.b.id); return `<span id='kk\${row.b.id}'></span>` }"

            ),
            "skck" => array(

                "data" => "b.skck",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, meta, row){ let loc_file = `upload/file/\${data}`; cek_file(loc_file,'skck'+row.b.id); return `<span id='skck\${row.b.id}'></span>` }"

            ),
            "alamat" => array(

                "data" => "b.alamat",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, meta, row){ return `<span style='font-size: 80%'>\${data ? data : '-'}</span>` }"

            ),

            "created_at" => array(
                "data" => "b.created_at",
                "searchable" => true,
                "orderable" => true,
                "render" => "function(data, type, row){ let tgl = moment(data,'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY'); let time = moment(data,'YYYY-MM-DD HH:mm:ss').format('HH:mm:ss'); let html = `<span><span class='badge bg-purple'>\${tgl}</span>`; html += `<span class='badge bg-blue'>\${time} WIB</span></span>`; return html;}"
            ),
            
           
            "action" => array(

                "data" => "$.op",
                "searchable" => false,
                "orderable" => false,

            )


        );

    }
    
//     private function _get_fields_edit(){


//         return array(
            
//             "nama" => array(

//                 "label" => "Nama Pelanggan",
//                 "type" => "text",
//                 "placeholder" => "Nama Pelanggan",
//                 "class" => "form-control validate[required]",
        
//             ),     

//             "cv" => array (
//                 "label" => "Image",
//                 "type" => "upload_file",
//                 "placeholder" => "Image",
//                 "class" => "form-control",
//                 "file_path" => 'upload/photo/'
//             ),

//             "id_career" => array(

//                 "label" => "Pekerjaan",
//                 "type" => "text",
//                 "placeholder" => "Pekerjaan",
//                 "class" => "form-control validate[required]",
        
//             ), 

//             "keterangan" => array(

//                 "label" => "Keterangan",
//                 "type" => "editor",
//                 "placeholder" => "Keterangan",
//                 "class" => "form-control validate[required]",
             

//             ),     
            

//         );

// // 
//     }


    public function remove(){

        if(count($_POST)>0){

            $data['params']['action'] = "delete";
            $data['params']['table'] = $this->table;
            $data['params']['pk'] = $this->pk;
            $data['params']['id'] = $_POST['id'];

              //File
              $data['params']['file_path'] = 'upload/cv';
              // $data['params']['file_field'] = get_file_name($this->table,$this->pk,$_POST['id'],'foto');
              $data['params']['file_field'] = 'cv';

            $this->load->library("Cinta",$data);
            $this->cinta->process();

        }

    }

    public function merge_pdf($id){
        $root = $_SERVER['DOCUMENT_ROOT'];   
        $pelamar = $this->db->query("SELECT * FROM front_pelamar where id = '$id'")->row();
        // map_y($pelamar);
        $file = array(
            [
                'dir' => $root.'/upload/cv/'.$pelamar->cv,
                'type' => 'pdf'
            ],
            [
                'dir' => $root.'/upload/file/'.$pelamar->ijazah,
                'type' => 'pdf'
            ],
            [
                'dir' => $root.'/upload/file/'.$pelamar->transkrip,
                'type' => 'pdf'
            ],
            [
                'dir' => $root.'/upload/file/'.$pelamar->ktp,
                'type' => 'image'
            ],
            [
                'dir' => $root.'/upload/file/'.$pelamar->kk,
                'type' => 'image'
            ],
            [
                'dir' => $root.'/upload/file/'.$pelamar->skck,
                'type' => 'image'
            ],
        );
        // map_y($file);       
        $filename = preg_replace('/\s+/', '_', $pelamar->nama);
        mergePDFFiles($file,$filename);
        
    }

}