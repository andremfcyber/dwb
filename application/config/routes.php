<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $route['default_controller'] = 'tehemes/admin/adminlte/layout/login';
$route['default_controller'] = 'public/home';
// $route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['admin'] = 'dashboard';
$route['dashboard/theme/detail/(:any)'] = 'dashboard/theme/$1';
$route['translate_uri_dashes'] = FALSE;

/**
 * Website Company
 */

$route['kredit'] = 'public/home/kredit';
$route['deposito'] = 'public/home/deposito';
$route['tabungan'] = 'public/home/tabungan';
$route['profil'] = 'public/home/profil';
$route['visi_misi'] = 'public/home/visi_misi';
$route['struktur_organisasi'] = 'public/home/struktur';
$route['budaya_perusahaan'] = 'public/home/budaya_perusahaan';
$route['awards'] = 'public/home/awards';
$route['kenapa_memilih_kami'] = 'public/home/kenapa_memilih_kami';
$route['agenda'] = 'public/home/agenda';
$route['press_realese'] = 'public/home/press_realese';
$route['cerita_pelanggan'] = 'public/home/cerita_pelanggan';
$route['laporan'] = 'public/home/laporan';
$route['karir'] = 'public/home/karir';
$route['galeri'] = 'public/home/galeri';
$route['download'] = 'public/home/download';
$route['artikel'] = 'public/home/artikel';
$route['faq'] = 'public/home/faq';
$route['hubungi_kami'] = 'public/home/hubungi_kami';
$route['restrukturisasi'] = 'public/home/restrukturisasi';
$route['cron_reminder_telat_bayar'] = 'public/home/cron_reminder_telat_bayar';
$route['cron_reminder_menuju_bayar'] = 'public/home/cron_reminder_menuju_bayar';
$route['core-value'] = 'public/home/core_value';