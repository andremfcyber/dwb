    <?php
    // Include the composer autoload file
    include_once "vendor/autoload.php";

    // Import the necessary classes
    use Xaamin\Whatsapi\Facades\Native\Whatsapi;
    use Xaamin\Whatsapi\Facades\Native\Registration;

    // Or, if you want you can add a class alias
    // class_alias('Xaamin\Whatsapi\Facades\Native\Whatsapi', 'Whatsapi');
    // class_alias('Xaamin\Whatsapi\Facades\Native\Registration', 'Registration');