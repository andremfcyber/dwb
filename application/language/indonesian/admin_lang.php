<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$lang['key'] = 'Key';
$lang['level'] = 'Level';

/* E-mail Messages */
/* brp branch */

$lang['branch_name'] = 'Nama Cabang';
$lang['branch_address'] = 'Alamat';
$lang['branch_telp'] = 'Telp';


/* brp contact person */

$lang['contact_name'] = 'Nama Kontak';
$lang['contact_address'] = 'Alamat';
$lang['contact_telp'] = 'Telp';
$lang['contact_mobile_phone'] = 'Hp';
$lang['contact_email'] = 'Email';


/* brp director position */

$lang['director_position_name'] = 'Nama Posisi';
$lang['director_position_parent'] = 'Induk';

/* brp director */

$lang['director_name'] = 'Nama';
$lang['director_position'] = 'Posisi';

/* brp employee */

$lang['employee_photo'] = 'Foto';
$lang['employee_name'] = 'Nama Pegawai';
$lang['employee_gender'] = 'Jenis Kelamin';
$lang['employee_job_position'] = 'Jabatan';
$lang['employee_email'] = 'Email';

/* brp mutation */

$lang['mutation_member'] = 'Nama';
$lang['mutation_from_position'] = 'Dari Posisi';
$lang['mutation_to_position'] = 'Ke Posisi';
$lang['mutation_date'] = 'Tanggal Mutasi';

/* brp mutation */

$lang['stockholder_name'] = 'Nama';
$lang['stockholder_percentage'] = 'Nilai Saham';

/* brp article category */

$lang['category_name'] = 'Nama Kategori';
$lang['category_parent'] = 'Induk Kategori';
$lang['category_status'] = 'Status';
$lang['category_sequence'] = 'Urutan';

/* brp article */

$lang['post_title'] = 'Judul';
$lang['post_status'] = 'Status';
$lang['post_category'] = 'Kategori';
$lang['post_created_at'] = 'Tanggal';

/* brp article */

$lang['product_category_name'] = 'Nama Kategori';
$lang['product_category_parent'] = 'Induk Kategori';
$lang['product_category_status'] = 'Status';
$lang['product_category_sequence'] = 'Urutan Kategori';


/* brp article */

$lang['product_name'] = 'Judul';
$lang['product_status'] = 'Status';
$lang['product_category'] = 'Kategori';
$lang['product_created_at'] = 'Tanggal';

/* brp article */

$lang['asset_type_name'] = 'Nama Tipe Asset';


/* brp article */

$lang['asset_year'] = 'Tahun';
$lang['asset_value'] = 'Nilai Asset';
$lang['finance_asset_type'] = 'Tipe Asset';


/* brp article */

$lang['report_name'] = 'Judul Laporan';
$lang['report_year'] = 'Tahun';
$lang['report_attachement'] = 'File Lampiran';

/* brp article */

$lang['auction_code'] = 'Kode Asset';
$lang['auction_status'] = 'Status Asset';
$lang['auction_category'] = 'Kategori';
$lang['auction_date'] = 'Jadwal Lelang';

// DPD Module
$lang['master_data'] = 'Data Master';
$lang['dpd_list'] = 'Daftar DPD';
$lang['dpd_add'] = 'Tambah DPD';
$lang['dpd_edit'] = 'Edit data DPD';
$lang['dpd'] = 'DPD';
$lang['name'] = 'Nama';
$lang['description'] = 'Deskripsi';
$lang['dpd_search'] = 'Cari DPD';

// BPR Module
$lang['bpr_list'] = 'Daftar BPR';
$lang['bpr_add'] = 'Tambah BPR';
$lang['bpr_edit'] = 'Edit data BPR';
$lang['address'] = 'Alamat';
$lang['telp'] = 'No Telp';
$lang['fax'] = 'Fax';
$lang['email'] = 'Email';
$lang['website'] = 'Website';
$lang['corporate'] = 'PT/PD';

// Corporate
$lang['corporate_list'] = 'Daftar PT/PD';
$lang['corporate_add'] = 'Tambah PT/PD';
$lang['corporate_edit'] = 'Edit data PT/PD';
$lang['corporate_search'] = 'Cari PT/PD';

// Corporate
$lang['corporate_list'] = 'Daftar PT/PD';
$lang['corporate_add'] = 'Tambah PT/PD';
$lang['corporate_edit'] = 'Edit data PT/PD';
$lang['corporate_search'] = 'Cari PT/PD';

$lang['action'] = 'Aksi';
$lang['aksi'] = 'Aksi';
$lang['modal_delete_title'] = 'konfirmasi';
$lang['modal_delete_body'] = 'Data pada baris ini akan dihapus. Anda yakin?';
$lang['yes'] = 'Ya';
$lang['no'] = 'Tidak';
$lang['submit'] = 'Submit';
$lang['back'] = 'Kembali';
$lang['add'] = 'Tambah';
$lang['search_data'] = 'Cari Data';

$lang['flag'] = 'Flag';
$lang['judul'] = 'Judul';
$lang['isi'] = 'Isi';
$lang['input_oleh'] = 'Input Oleh';
$lang['update_oleh'] = 'Update Oleh';
$lang['input_tgl'] = 'Input Tanggal';
$lang['update_tgl'] = 'Update Tanggal';
$lang['file_foto'] = 'Photo';

$lang['nama'] = 'Nama Nasabah';
$lang['name'] = 'Nama Product';
$lang['input_type'] = 'Input Type';
$lang['label'] = 'Label';
$lang['value'] = 'Value';
$lang['nik'] = 'NIK';
$lang['alamat'] = 'Alamat';
$lang['status'] = 'Status Pengajuan';
$lang['id_nasabah'] = 'ID Nasabah';
$lang['type_product'] = 'Tipe Produk';
$lang['name_header'] = 'Nama Header';
$lang['product'] = 'Nama Product';
$lang['kelurahan'] = 'Kabupaten';
$lang['kecamatan'] = 'Kecamatan';
$lang['provinsi'] = 'Provinsi';
$lang['nama_product_type'] = 'Nama Product Type';
$lang['nik_dukcapil'] = 'NIK Di Dukcapil';
$lang['nik_nasabah'] = 'NIK Nasabah';
$lang['no_tlp'] = 'Nomor HP';
$lang['tgl_pengajuan'] = 'Tanggal Pengajuan';
$lang['cekdukcapil'] = 'Cek Data Dukcapil';
$lang['nama_nasabah'] = 'Nama Nasabah';
$lang['nama_dukcapil'] = 'Nama Di Dukcapil';
$lang['emaill'] = 'Email';
$lang['urutan'] = 'Urutan';
$lang['deskripsi'] = 'Deskripsi';
$lang['photo_selfie'] = 'Photo Selfie';
$lang['photo_ktp'] = 'Photo KTP';
$lang['tempat_lahir'] = 'Tempat Lahir';
$lang['kelurahan'] = 'Kelurahan';
$lang['no_kk'] = 'No Kartu Keluarga';
$lang['kabupaten'] = 'Kabupaten';
$lang['jenis_kelamin'] = 'Jenis Kelamin';
$lang['no_rt'] = 'No RT';
$lang['no_rw'] = 'No RW';
$lang['no_rt'] = 'No RT';
$lang['prop_name'] = 'Provinsi';
$lang['no_rt'] = 'No RT';
$lang['kec_name'] = 'Kecamatan';
$lang['agama'] = 'Agama';
$lang['jenis_pkrjn'] = 'Pekerjaan';
$lang['no_rt'] = 'No RT';
$lang['status_kawin'] = 'Status Kawin';
$lang['kode_pos'] = 'Kode Pos';
$lang['tgl_lhr'] = 'Tanggal Lahir';
$lang['nama_lgkp_ibu'] = 'Nama Ibu';

$lang['NIK'] = 'No. Identitas';
$lang['Nama'] = 'Nama';
$lang['Product'] = 'Produk';
$lang['Type Product'] = 'Tipe Produk';
$lang['Phone Number'] = 'No. Telepon';
$lang['Email'] = 'Alamat Email';
$lang['Status'] = 'Status';
$lang['Tanggal Pengajuan'] = 'Tanggal Pengajuan';
$lang['Nama Product'] = 'Nama Product';
$lang['Tipe Product'] = 'Tipe Product';
$lang['Deskripsi'] = 'Deskripsi';

$lang['Nama Header Form'] = 'Nama Header Form';
$lang['Urutan'] = 'Urutan';

$lang['Nama Sales'] = 'Nama';
$lang['Nomor Telepon'] = 'Nomor Telepon';
$lang['Alamat'] = 'Alamat';
$lang['Jabatan'] = 'Jabatan';

$lang['Bulan'] = 'Bulan';
$lang['nama_pelamar'] = 'Nama Pelamar';
$lang['nama_halaman'] = 'Nama Halaman';
$lang['foto_slide'] = 'Baner';
$lang['nm_mitra'] = 'Mitra';
$lang['nm_badan_pengawas'] = 'Badan Pengawas';
$lang['keterangan'] = 'Keterangan';
$lang['nm_kantor'] = 'Kantor';
$lang['nm_kabid'] = 'Nama Kepala Bidang';
$lang['tgl_cuti'] = 'Tanggal Cuti';
$lang['status_cuti'] = 'Status';
$lang['kategori'] = 'Kategori';
$lang['username_mailjet'] = 'Userkey Mailjet';
$lang['username_zenziva'] = 'Userkey Zenziva';
$lang['nama_pengirim'] = 'Nama Pengirim';
$lang['pesan'] = 'Pesan';
$lang['galeri'] = 'Galeri';
$lang['nama_cerpel'] = 'Nama';
$lang['pekerjaan_cerpel'] = 'Pekerjaan';
$lang['nama_budaya'] = 'Budaya Kerja';
$lang['nama_agenda'] = 'Agenda';
$lang['no_hp'] = 'No. HP';

$lang['Referal'] = 'Referal link';
$lang['nama_file'] = 'Nama File Laporan';
$lang['namafile'] = 'Nama File';

$lang['cv'] = 'CV';
$lang['ijazah'] = 'Ijazah';
$lang['transkrip'] = 'Transkrip';
$lang['ktp'] = 'KTP';
$lang['kk'] = 'Kartu Keluarga';
$lang['skck'] = 'SKCK';
$lang['foto_'] = 'Foto';

$lang['nama_ao_dari'] = 'Dari AO';
$lang['nama_ao_ke'] = 'Ke AO';
$lang['nama_user'] = 'Dialihkan oleh';
$lang['nama_user_is'] = 'Yang memproses';
$lang['nasabah'] = 'Nasabah'; 

$lang['nama_restruktrisasi'] = 'Nama';
$lang['alasan'] = 'Alasan';
$lang['norek1'] = 'No.Rek 1'; 
$lang['norek2'] = 'No.Rek 2';

$lang['tenor'] = 'Tenor';
$lang['tgl_penagihan'] = 'Tgl. Penagihan';
$lang['jml_bayar'] = 'Total Tagihan';
$lang['nama_collector'] = 'Nama Ao';
$lang['angsuran_ke'] = 'Angsuran Ke';
$lang['keterlambatan'] = 'Keterlamabatan(Hari)';
$lang['tagihan_bulanan'] = 'Tagihan(Bulan)';
$lang['stat'] = 'Status';
$lang['tipe_reminders'] = 'Tipe Reminder';
$lang['tujuan'] = 'Tujuan';
$lang['stat_bayar'] = 'Status Bayar';
$lang['tgl_pembayaran'] = 'Tanggal Pembayaran';
$lang['dibayar'] = 'Di Bayar';
$lang['harus_bayar'] = 'Harus Bayar';
$lang['created_at'] = 'Tanggal';
$lang['pekerjaan'] = 'Pekerjaan';
$lang['corevalue'] = 'Core Value';
$lang['pelamar'] = 'Pelamar';
$lang['icon'] = 'Icon';
$lang['logo'] = 'Logo';
$lang['foto'] = 'Foto';
$lang['nama_kerja'] = 'Pekerjaan';
$lang['status'] = 'Status';
$lang['tipe_kerja'] = 'Jenis Pekerjaan';

$lang['norek'] = 'Nomor Rekening';
$lang['tgl_mulai'] = 'Tanggal Mulai';
$lang['tgl_jatuh_tempo'] = 'Tanggal Jatuh Tempo';
$lang['jangka_waktu'] = 'Jangka Waktu (Bulan)';
