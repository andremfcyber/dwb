<?php

/**
 * Menu English language
 */

$lang['menu_home'] = 'Home';
$lang['menu_about'] = 'About';
$lang['menu_login'] = 'Login';
$lang['menu_register'] = 'Register';
$lang['menu_logout'] = 'Logout';
