<?php

class Dt_service extends Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->load->library('adatatables');
    }

    /**
     * Dt kelurahan
     */
    function list_kelurahan() {
        // $kabkota_all_jateng = $this->db->query("SELECT id FROM _tbl_kabkot WHERE provinsi_id = '13'")->result_array();
        // $kabkota_all_jateng = map_id($kabkota_all_jateng);

        // $kecamatan_all_jateng = $this->db->query("SELECT id FROM _tbl_kecamatan WHERE kabkot_id IN ($kabkota_all_jateng)")->result_array();
        // $kecamatan_all_jateng = map_id($kecamatan_all_jateng);
        $this->adatatables->set_database('default');
        $this->adatatables->select('id, kelurahan, kecamatan, kabupaten_kota, provinsi');
        $this->adatatables->from('v_wilayah');
        return dt_response($this->adatatables->generate());
    }

    /**
     * Dt kecamatan
     */
    function list_kecamatan() {
        $this->adatatables->set_database('default');
        $this->adatatables->select('_tbl_kecamatan.id, _tbl_kecamatan.kecamatan, _tbl_kabkot.kabupaten_kota,  _tbl_provinsi.provinsi');
        $this->adatatables->join('_tbl_kabkot', '_tbl_kabkot.id = _tbl_kecamatan.kabkot_id', 'left');
        $this->adatatables->join('_tbl_provinsi', '_tbl_provinsi.id = _tbl_kabkot.provinsi_id', 'left');
        $this->adatatables->from('_tbl_kecamatan');
        $this->adatatables->group_by(['_tbl_provinsi.id','_tbl_provinsi.provinsi','_tbl_provinsi.p_bsni','_tbl_kabkot.provinsi_id','_tbl_kabkot.k_bsni','_tbl_kabkot.ibukota','_tbl_kecamatan.id', '_tbl_kecamatan.kecamatan', '_tbl_kecamatan.kabkot_id', '_tbl_kabkot.kabupaten_kota', '_tbl_kabkot.id']);
        return dt_response($this->adatatables->generate());
    }

    /**
     * Dt kabupaten
     */
    function list_kabupaten() {
        $this->adatatables->set_database('default');
        $this->adatatables->select('_tbl_kabkot.id, _tbl_kabkot.kabupaten_kota,  _tbl_provinsi.provinsi');
        $this->adatatables->join('_tbl_provinsi', '_tbl_provinsi.id = _tbl_kabkot.provinsi_id', 'left');
        $this->adatatables->from('_tbl_kabkot');
        $this->adatatables->group_by(['_tbl_provinsi.id','_tbl_provinsi.provinsi','_tbl_provinsi.p_bsni','_tbl_kabkot.provinsi_id','_tbl_kabkot.k_bsni','_tbl_kabkot.ibukota','_tbl_kabkot.kabupaten_kota', '_tbl_kabkot.id']);
        return dt_response($this->adatatables->generate());
    }

    /**
     * Dt Provinsi
     */
    function list_provinsi() {
        $this->adatatables->set_database('default');
        $this->adatatables->select('_tbl_provinsi.id, _tbl_provinsi.provinsi');
        $this->adatatables->from('_tbl_provinsi');
        return dt_response($this->adatatables->generate());
    }


}