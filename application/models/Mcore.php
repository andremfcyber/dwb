<?php

class Mcore extends CI_model{


    function __construct()
    {
        parent::__construct();
        $this->load->library('Aauth');
    }

    function getMenus(){

        $sql = "SELECT a.*, b.name as perm FROM menus a LEFT JOIN aauth_perms b ON b.id=a.permission WHERE a.status = '1' ORDER BY sequence ASC";
        // $sql = "SELECT a.*, b.name as perm FROM menus a LEFT JOIN aauth_perms b ON b.id=a.permission ORDER BY sequence ASC";
        $query = $this->db->query($sql)->result_array();
        return $query;

    }

    function getMenuChilds($id){

        $sql = "SELECT a.*, b.name as perm FROM menus a LEFT JOIN aauth_perms b ON b.id=a.permission WHERE a.status = '1' AND a.parent='".$id."' ORDER BY a.sequence ASC";
        $query = $this->db->query($sql)->result_array();
        return $query;

    }

    function setParentMenu($id, $parent_id, $seq){

        $sql = "UPDATE menus SET parent = ".$parent_id.", sequence = '".$seq."' WHERE menu_id = '".$id."'";
        $query = $this->db->query($sql);
        return $query;

    }



    function getPermission(){

        $result = $this->db->query("SELECT * FROM aauth_perms ORDER BY name ASC")->result_array();
        return $result;

    }

    function getUserGroup($user_id){

        $result = $this->db->query("SELECT b.* FROM aauth_user_to_group a
                                    LEFT JOIN aauth_groups b ON b.id = a.group_id 
                                    WHERE a.user_id='".$user_id."'")->result_array();
        return @$result[0];

    }

    function getActiveSkin(){

        $result = $this->db->query("SELECT * FROM configurations WHERE scope='LAYOUT' AND key='SKIN'")->result_array();
        return @$result[0]['value'];

    }

    function getMenuLastSquence($parent_id = null){

        if($parent_id == null){

            $result = $this->db->query("SELECT MAX(sequence) as last_seq FROM menus")->result_array();

        }else{

            $result = $this->db->query("SELECT MAX(sequence) as last_seq FROM menus WHERE parent = ".$parent_id)->result_array();


        }

        return $result[0]['last_seq'];

    }

    function checkPermission($user_group, $perm, $redirect_back = false){

        if($this->aauth->is_group_allowed($perm, $user_group)){

            return true;

        }else{

            if($redirect_back){
                echo '<script>window.history.back();</script>';
            }else{
                return false;
            }


        }

    }

    function getUserGroupName($user_id){

        $sql = "SELECT b.* FROM aauth_user_to_group a LEFT JOIN aauth_groups b ON b.id=a.group_id WHERE a.user_id='".$user_id."'";
        $query = $this->db->query($sql)->result_array();
        $user_group = @$query[0]['name'];

        return $user_group;

    }

    function getMemberByUser($user_id){

        $sql = "SELECT a.*,b.*,a.name as member_name,  b.name as bpr_name, b.address as bpr_address, b.email as bpr_email, c.name as corp, d.name as dpd_name FROM members a 
                LEFT JOIN bpr b ON b.id=a.bpr 
                LEFT JOIN corporates c ON c.id=b.corporate
                LEFT JOIN dpd d ON d.id = b.dpd
                WHERE a.aauth_user_id='".$user_id."'";
        $query = $this->db->query($sql)->result_array();
        $member = @$query[0];

        return $member;

    }

}