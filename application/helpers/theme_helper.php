<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Helper for i18n function
* @param String key of Language
*/
function i18n( $key )
{
	$text = lang($key);
	return (!$text ? $key : $text);
}

/**
* Helper for i18n Label function
*/
function i18n_label( $key, $for = '', $attributes = [], $before_text = '', $after_text = '' )
{
	if (empty($for))
		return i18n($key);

	$text = lang($key);
	$line = (!$text ? $key : $text);

	return '<label for="'.$for.'"'._stringify_attributes($attributes).'>'.$before_text.$line.$after_text.'</label>';
}

function active_menu( $uri_checked = 'home', $current_uri = 'home', $active_attr = 'active' )
{
	echo ( uri_active($current_uri, $uri_checked) ? " {$active_attr}" : "" );
}

function active_match_menu( $uri_checked = 'home', $current_uri = 'home', $active_attr = 'active' )
{
	echo ( uri_active($current_uri, $uri_checked, TRUE) ? " {$active_attr}" : "" );
}

function uri_active( $uri, $uri_checked = array(), $all = FALSE )
{
	$active = FALSE;

	if ( is_string( $uri_checked ) )
		$uri_checked = explode( ',', trim( $uri_checked ) );

	if ( ! is_array( $uri_checked ) || count( $uri_checked ) == 0 )
		return $active;

	foreach( $uri_checked as $checked )
	{
		$active = $active || ($uri == $checked) || ($all && stripos( $uri, $checked ) !== FALSE);
	}
	return $active;
}

function get_valid_posts($posts, $valid_fields)
{
	if (is_string($valid_fields))
	{
		$valids = [];
		$CI = get_instance();
		$fields = $CI->db->list_fields($valid_fields);
		foreach ($fields as $field)
      $valids[] = $field;

		$valid_fields = $valids;
	}

	if (is_array($posts) && is_array($valid_fields))
	{
		foreach ($posts as $key => $value) {
			if (!in_array($key, $valid_fields))
				unset($posts[$key]);
		}
	}
	return $posts;
}

if ( !function_exists('get_option'))
{
	function get_option($item, $config_name = NULL, $base_url = FALSE)
	{
		$CI =& get_instance();

		if (!is_null($config_name))	$CI->config->load($config_name);

		return (( $base_url ? base_url() : '' ) . $CI->config->item($item));
	}
}

function get_ip_address() {
    $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
    foreach ($ip_keys as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                // trim for safety measures
                $ip = trim($ip);
                // attempt to validate IP
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        }
    }

    return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
}

/**
* Custom Date Format
*/
function ___d( $date, $format = 'english' )
{
	$date_only = explode(" ", $date);
	$date = explode("-", $date_only[0]);
	switch($format)
	{
		case 'japanese' :
			if (isset($date[0]) && isset($date[1]) && isset($date[2]))
				return "{$date[0]}年{$date[1]}月{$date[2]}日";
			elseif (isset($date[0]) && isset($date[1]))
				return "{$date[0]}年{$date[1]}月";
			elseif (isset($date[0]))
				return "{$date[0]}年";

		case 'english' :
			return "{$date[0]}-{$date[1]}-{$date[2]}";
	}
}

/**
* Custom Year Format
*/
function i18n_date( $date, $format = 'Y-m-d', $separator = '-', $lang = 'english' )
{
	$date_only = explode(" ", $date);
	$forms = explode($separator, $format);
	$date = explode($separator, $date_only[0]);
	$sDate = "";
	switch($lang)
	{
		case 'japanese' :
			$ja_label = ['年', '月', '日'];
			foreach($forms as $i => $d)
			{
				if (isset($date[$i]))
					$sDate .= ($date[$i].$ja_label[$i]);
			}
			break;

		case 'english' :
			foreach($forms as $i => $d)
				$aDate[] = $date[$i];
				$sDate = implode($separator, $aDate);
			break;
	}
	return $sDate;
}

/*
* Make List of Year
*/
function year_list($start = FALSE, $length = 1)
{
	$start = (!$start ? date('Y') : $start);
	$years = [];
	for($i=1; $i<=$length; $i++)
	{
		$years[] = (string) $start; $start++;
	}
	return $years;
}

/*
* Make List of Year & Month
*/
function year_month_list($start = FALSE, $length = 1, $lang = 'english')
{
	$yearMonths = [];
	$years = year_list($start, $length);

	foreach($years as $y)
	{
		for($i=4; $i<=12; $i++)
		{
			$sYearMonth = $y."-".str_pad($i, 2, "0", STR_PAD_LEFT);
			$yearMonths[$sYearMonth] = i18n_date($sYearMonth, 'Y-m', '-', $lang);
		}
		for($i=1; $i<=3; $i++)
		{
			$sYearMonth = ($y+1)."-".str_pad($i, 2, "0", STR_PAD_LEFT);
			$yearMonths[$sYearMonth] = i18n_date($sYearMonth, 'Y-m', '-', $lang);
		}
	}
	return $yearMonths;
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip)
{
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
        return false;
    }
    return true;
}

/* check wether field has date name */
function is_date_field($field)
{
	return (stripos($field, 'date') !== false);
}

/* check wether date field is empty */
function isDateEmpty($value)
{
	return empty($value) || ($value == '0000-00-00');
}

/* check wether field has kana name */
function is_kana($field)
{
	return (stripos($field, 'kana') !== false);
}

/* check wether text is hiragana */
function hiragana_text($text)
{
	$pattern = "/^[ぁ-ゞー]+$/u";
	return preg_match($pattern, $text);
}

function get_column_excel($index, $loop = -1)
{
		$char = chr(65+$index);
		if (preg_match("/^[A-Z]+$/", $char))
			return ($loop >= 0 ? chr(65+$loop) : "").$char;

		$index -= 26;
		$loop++;
		return get_column_excel($index, $loop);
}

function SJIS_text($txt)
{
	return mb_convert_encoding($txt, "SJIS", 'UTF-8' . ", SJIS");
}
function _include_core_head(){

  $app['base_url'] = base_url();
  $app['assetdir'] = "assets";
  $app['skin'] = '';

  if($app['skin']==''){
      $app['skin'] = 'skin-purple';
  }

  /*
   * skin-black, skin-black-light, skin-blue,
   * skin-blue-light, skin-green, skin-green-light,
   * skin-purple, skin-purple-light, skin-red,
   * skin-red-light, skin-yellow, skin-yellow-light
  */

  $app['css'] = array(
    //   "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/css/bootstrap.min.css',
      "font_awesome" => $app['base_url'].$app['assetdir'].'/bower_components/components-font-awesome/css/font-awesome.min.css',
      "ionicons" => $app['base_url'].$app['assetdir'].'/bower_components/Ionicons/css/ionicons.min.css',
      "adminlte" => $app['base_url'].$app['assetdir'].'/dist/css/core.css',
      "icheck" => $app['base_url'].$app['assetdir'].'/plugins/iCheck/flat/blue.css',
      "morris" => $app['base_url'].$app['assetdir'].'/plugins/morris/morris.css',
      "jvectormap" => $app['base_url'].$app['assetdir'].'/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
      "timepicker" => $app['base_url'].$app['assetdir'].'/plugins/timepicker/bootstrap-timepicker.css',
      "datepicker" => $app['base_url'].$app['assetdir'].'/plugins/datepicker/datepicker3.css',
      "daterangepicker" => $app['base_url'].$app['assetdir'].'/plugins/daterangepicker/daterangepicker.css',
      "tags-input" => $app['base_url'].$app['assetdir'].'/plugins/bootstrap-tags-input/bootstrap-tagsinput.css',
      "intro-js" => $app['base_url'].$app['assetdir'].'/plugins/intro-js/introjs.min.css',
      // "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/org-chart/jquery.orgchart.css',
      "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/getorgchart/getorgchart.css',
      "jqplot" => $app['base_url'].$app['assetdir'].'/plugins/jqplot/jquery.jqplot.min.css',
      "bootstrap-wysihtml5" => $app['base_url'].$app['assetdir'].'/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
      "select2" => $app['base_url'].$app['assetdir'].'/plugins/select2/select2.min.css',
      "jexcel" => $app['base_url'].$app['assetdir'].'/plugins/jexcel/css/jquery.jexcel.css',
      "datatable" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
      "datatable-button" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/css/buttons.dataTables.min.css',
      "toastr" => $app['base_url'].$app['assetdir'].'/plugins/toastr/toastr.min.css',
      "duallistbox" => $app['base_url'].$app['assetdir'].'/plugins/duallistbox/bootstrap-duallistbox.min.css',
      "jquery_validation_engine" => $app['base_url'].$app['assetdir'].'/plugins/jquery-validation-engine/css/validationEngine.jquery.css',
      "fancybox" => $app['base_url'].$app['assetdir'].'/plugins/fancybox/source/jquery.fancybox.css?v=2.1.7',
      "smartwizard" => $app['base_url'].$app['assetdir'].'/plugins/smartWizard/css/smart_wizard.css',
      "smartwizarddot" => $app['base_url'].$app['assetdir'].'/plugins/smartWizard/css/smart_wizard_theme_dots.css',
      "custom" => $app['base_url'].$app['assetdir'].'/dist/css/custom.css',

  );
  $app['header_js'] = array(
      "jquery" => $app['base_url'].$app['assetdir'].'/plugins/jQuery/jquery-2.2.3.min.js',
      "jquery-ui" => $app['base_url'].$app['assetdir'].'/dist/js/ui/1.11.4/jquery-ui.min.js',
      "highcharts" => $app['base_url'].$app['assetdir'].'/plugins/Highcharts/code/highcharts.js',
      "datahighchart" => $app['base_url'].$app['assetdir'].'/plugins/Highcharts/code/modules/data.js',
      "drilldownhighchart" => $app['base_url'].$app['assetdir'].'/plugins/Highcharts/code/modules/drilldown.js',
      "smartwizard" => $app['base_url'].$app['assetdir'].'/plugins/smartWizard/js/jquery.smartWizard.js',
  );

  
    /* generate css */
    $css_key = array_keys($app['css']);
    for($i=0;$i < count($app['css']);$i++){
        echo '<link rel="stylesheet" href="'.$app['css'][$css_key[$i]].'">';
    }

    /* generate custom css */

    if(isset($custom_css)){
        if(count($custom_css)>0){
            $custom_css_key = array_keys($custom_css);
            for($i=0;$i < count($custom_css);$i++){
                echo '<link rel="stylesheet" href="'.$custom_css[$custom_css_key[$i]].'">';
            }
        }
    }

    /* generate js */
    $header_js_key = array_keys($app['header_js']);
    for($i=0;$i < count($app['header_js']);$i++){
        echo '<script src="'.$app['header_js'][$header_js_key[$i]].'"></script>';
    }

    echo " <script>
	        var base_url = '".base_url()."';
	        var ckfinder_config = {
	            filebrowserBrowseUrl : base_url+'assets/plugins/ckfinder/ckfinder.html',
	            filebrowserImageBrowseUrl : base_url+'assets/plugins/ckfinder/ckfinder.html?type=Images',
	            filebrowserFlashBrowseUrl : base_url+'assets/plugins/ckfinder/ckfinder.html?type=Flash',
	            filebrowserUploadUrl : base_url+'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
	            filebrowserImageUploadUrl : base_url+'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	            filebrowserFlashUploadUrl : base_url+'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	        }
	    </script>";


}

function _include_core_footer(){

	$app['base_url'] = base_url();
  	$app['assetdir'] = "assets";

	 $app['footer_js'] = array(
    "bootstrap" => $app['base_url'].$app['assetdir'].'/bootstrap/js/bootstrap.min.js',
    "raphael" => $app['base_url'].$app['assetdir'].'/dist/js/raphael-min.js',
    "morris" => $app['base_url'].$app['assetdir'].'/plugins/morris/morris.min.js',
    "sparkline" => $app['base_url'].$app['assetdir'].'/plugins/sparkline/jquery.sparkline.min.js',
    "jvectormap" => $app['base_url'].$app['assetdir'].'/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
    "jvectormap_world" => $app['base_url'].$app['assetdir'].'/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
    "knob" => $app['base_url'].$app['assetdir'].'/plugins/knob/jquery.knob.js',
    "moment" => $app['base_url'].$app['assetdir'].'/dist/js/moment.min.js',
    "daterangepicker" => $app['base_url'].$app['assetdir'].'/plugins/daterangepicker/daterangepicker.js',
    "timepicker" => $app['base_url'].$app['assetdir'].'/plugins/timepicker/bootstrap-timepicker.js',
    "datepicker" => $app['base_url'].$app['assetdir'].'/plugins/datepicker/bootstrap-datepicker.js',
    "tags-input" => $app['base_url'].$app['assetdir'].'/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js',
    "intro-js" => $app['base_url'].$app['assetdir'].'/plugins/intro-js/intro.min.js',
    // "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/org-chart/jquery.orgchart.min.js',
    "org-chat" => $app['base_url'].$app['assetdir'].'/plugins/getorgchart/getorgchart.js',
    "bootstrap-wysihtml5" => $app['base_url'].$app['assetdir'].'/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
    "slimScroll" => $app['base_url'].$app['assetdir'].'/plugins/slimScroll/jquery.slimscroll.min.js',
    "fastclick" => $app['base_url'].$app['assetdir'].'/plugins/fastclick/fastclick.js',
    "select2" => $app['base_url'].$app['assetdir'].'/plugins/select2/select2.full.min.js',
    "app" => $app['base_url'].$app['assetdir'].'/dist/js/app.js',
    "datatable" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net/js/jquery.dataTables.min.js',
    "datatable-bs" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
    "datatable-button" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/dataTables.buttons.min.js',
    "datatable-button-print" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/buttons.print.min.js',
    "datatable-button-html5" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/buttons.html5.min.js',
    "datatable-button-flash" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/buttons.flash.min.js',
    "datatable-button-jszip" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/jszip.min.js',
    "datatable-button-pdfmake" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/pdfmake.min.js',
    "datatable-button-vfs-fonts" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/vfs_fonts.js',
    "datatable-button-col-visible" => $app['base_url'].$app['assetdir'].'/bower_components/datatables.net-bs/js/buttons.colVis.min.js',
    "numeral" => $app['base_url'].$app['assetdir'].'/plugins/numeral/numeral.min.js',
    "pdfobject" => $app['base_url'].$app['assetdir'].'/plugins/PDFObject/pdfobject.min.js',
    "jcsv" => $app['base_url'].$app['assetdir'].'/plugins/jexcel/js/jquery.csv.min.js',
    "jexcel" => $app['base_url'].$app['assetdir'].'/plugins/jexcel/js/jquery.jexcel.js',
    "canvasjs" => $app['base_url'].$app['assetdir'].'/plugins/canvasjs/canvasjs.min.js',
    "jqplot" => $app['base_url'].$app['assetdir'].'/plugins/jqplot/jquery.jqplot.min.js',
    "jqplot-pieRenderer" => $app['base_url'].$app['assetdir'].'/plugins/jqplot/plugins/jqplot.pieRenderer.js',
    "toastr" => $app['base_url'].$app['assetdir'].'/plugins/toastr/toastr.min.js',
    "duallistbox" => $app['base_url'].$app['assetdir'].'/plugins/duallistbox/jquery.bootstrap-duallistbox.min.js',
    "nestedsortable" => $app['base_url'].$app['assetdir'].'/plugins/nestedsortablejs/jquery.mjs.nestedSortable.js',
    "jquery_validation_engine_en" => $app['base_url'].$app['assetdir'].'/plugins/jquery-validation-engine/js/languages/jquery.validationEngine-en.js',
    "jquery_validation_engine" => $app['base_url'].$app['assetdir'].'/plugins/jquery-validation-engine/js/jquery.validationEngine.js',
    "ckeditor" => $app['base_url'].$app['assetdir'].'/plugins/ckeditor/ckeditor.js',
    "ckfinder" => $app['base_url'].$app['assetdir'].'/plugins/ckfinder/ckfinder.js',
    "images-loaded" => $app['base_url'].$app['assetdir'].'/plugins/image-fill/js/imagesloaded.pkgd.min.js',
    "image-fill" => $app['base_url'].$app['assetdir'].'/plugins/image-fill/js/jquery-imagefill.js',
    "image-scale" => $app['base_url'].$app['assetdir'].'/plugins/image-scale/image-scale.min.js',
    "layout" => $app['base_url'].$app['assetdir'].'/dist/js/layout.js',
    "dashboard-js" => $app['base_url'].$app['assetdir'].'/dist/js/pages/dashboard.js',
    "fancybox" => $app['base_url'].$app['assetdir'].'/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.7',
    "cinta" => $app['base_url'].$app['assetdir'].'/dist/js/cinta.js',
  );

	 /* generate js */
    $footer_js_key = array_keys($app['footer_js']);
    for($i=0;$i < count($app['footer_js']);$i++){
        echo '<script src="'.$app['footer_js'][$footer_js_key[$i]].'"></script>';
    }

    /* generate custom js */
    if(isset($custom_js)){
        if(count($custom_js)>0){
            $custom_js_key = array_keys($custom_js);
            for($i=0;$i < count($custom_js);$i++){
                if($custom_js_key[$i]=="googlemaps"){
                    echo '<script src="'.$custom_js[$custom_js_key[$i]].'" async defer></script>';
                }else{
                    echo '<script src="'.$custom_js[$custom_js_key[$i]].'"></script>';
                }

            }
        }
    }

}