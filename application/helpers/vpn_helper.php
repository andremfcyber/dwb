<?php

if(!function_exists('vpn_request')){
    function vpn_request($url){
        $client = new GuzzleHttp\Client();
        $config = vpn_config();

        try {
            //code...
            $response = $client->request('GET',$url,[
                'proxy' => [
                    'https'  => $config,
                ],
            ]);
            
            $check_status = $response->getStatusCode();
            $res_body = $response->getBody()->getContents();
            $res_body = json_decode($res_body);

            return [
                'rc' => '00',
                'body' => $res_body
            ];
           
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            //throw $e;
            $res_body = $e->getResponse()->getBody()->getContents();
            $res_body = json_decode($res_body);

            return [
                'rc' => 'IE',
                'body' => $res_body,
            ];
        }
    }
}

if(!function_exists('vpn_config')){
    function vpn_config(){
        $username = @get_vpn_config('username');
        $password = @get_vpn_config('password');
        $ip = @get_vpn_config('ip');
        $port = @get_vpn_config('port');
        // return "https://tcpvpn.com-argan:argan123@103.47.208.106:443"
        return "https://".$username.":".$password."@".$ip.":".$port;
    }
}

if(!function_exists('get_vpn_config')){
    function get_vpn_config($field){
        $CI =& get_instance();
        $get = $CI->db->query("SELECT $field FROM config_vpn LIMIT 1")->result_array();
        return $get[0][$field];
    }
}