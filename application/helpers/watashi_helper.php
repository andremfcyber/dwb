<?php
/**
 * Watashi Helper
 * 
 * @author Ari 
 */

if ( ! function_exists('map_y')){
    function map_y($data){
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        exit;
    }
}

if ( ! function_exists('map_x')){
    function map_x($data){
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}

if (!function_exists('response_json')){
    function response_json($object = [], $status = '200'){
        $CI =& get_instance();
        return $CI->output
            ->set_content_type('application/json')
            ->set_output(json_encode($object))
            ->set_status_header($status);
    }
}

if (!function_exists('dt_response')){
    function dt_response($object = [], $status = '200'){
        header('Content-Type: application/json');
        echo $object;
    }
}


if (!function_exists('uuid')) {
    function uuid(){
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
}

if (!function_exists('date_to_db')) {
    function date_to_db($date){
        return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }
}

if (!function_exists('date_to_view')) {
    function date_to_view($date){
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}

if (!function_exists('timestamp')) {
    function timestamp(){
        return Carbon::now()->format('Y-m-d H:m:s');
    }
}

if (!function_exists('toNumber')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function toNumber($rupiah)
    {
        $clean = explode(',',$rupiah);
        $clean = preg_replace('/\D/','',$clean[0]);
        return $clean;
    }
}

if (!function_exists('toRupiah')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function toRupiah($number)
    {
        $number = (Float) $number;
        $hasil_rupiah = number_format($number,2,',','.');
        return $hasil_rupiah;
    }
}

if (!function_exists('request_handler')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function request_handler()
    {
        $request = $_REQUEST;
        if(!isset($request)){
            return null;
        }
        return (Object) $request;
    }
}

if (!function_exists('map_id')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function map_id($data)
    {
        $new = [];
        if(isset($data)){
            foreach($data as $val){
                $new[] = $val['id'];
            }
        }
        $new = implode(', ', $new);    
        return $new;
    }
}

/**
 * Cek Rule Jabatan Produk
 */

 if(!function_exists('is_produk_allow')){
    function is_produk_allow($id_jabatan, $id_produk){
        $CI =& get_instance();
        $status = false;
        $get_rule = $CI->db->query("SELECT * FROM conf_produk_jabatan WHERE id_jabatan = '$id_jabatan' AND id_produk='$id_produk'")->row();
        // map_x($get_rule);
        if($get_rule){
            if($get_rule->type == 'allow'){
                $status = true;
            }
        }
        return $status;
    }
 }


/**
 * Send Sms
 * @param $no_hp
 * @param $message
 */

if(!function_exists('send_sms')){
    function send_sms($no_hp, $message){
        $CI =& get_instance();
        
        $userkey = '9kn7pv';
        $passkey = 'aw00e9tjfa';
       
        $client = new GuzzleHttp\Client([
            'base_uri' => URL_SMSAPI,
        ]);
        $response = $client->request('POST', 'apps/smsapi.php',[
            'query' => [
                'userkey' => $userkey,
                'passkey' => $passkey,
                'nohp' => '+'.$no_hp,
                'pesan' => $message
            ]
        ]);
    
        $results = $response->getBody()->getContents();
        $XMLdata = new SimpleXMLElement($results);

        $data = array(
            'cr_time' => date('Y-m-d H:i:s'),
            'cr_user' => isset($_SESSION['username']) ? $_SESSION['username'] : 'User Public',
            'up_time' => date('Y-m-d H:i:s'),
            'status' => $XMLdata->message[0]->text,
            'statuscode' => $XMLdata->message[0]->status,
            'notlp' => $no_hp,
            'message' => $message,
            'definition' => $results
        );
        $CI->db->insert('sms_log', $data);
    }
}

/**
 * Send Email
 * @param $email
 * @param $message
 */

if(!function_exists('send_email')){
    function send_email($email = '', $message = '', $subject = 'BPR Nusamba Cepiring'){
        $CI =& get_instance();
        
        $client = new GuzzleHttp\Client([
            'base_uri' => URL_MAILAPI,
        ]);
        
        $data = [];
        
        try {
            //code...
            $response = $client->post('v1/send_email', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'api-key' => 'YUNNA-2019-ARGAN',
                    'mailjet_user' => 'de9a205b2f545f77b2ece89f48919d10',
                    'mailjet_pass' => 'a3059183f502963d44a6798ca515ceee'
                ],
                GuzzleHttp\RequestOptions::JSON => [
                    "from" => [
                        "email" => "noreply@bprnusambacepiring.com",
                        "name" => "BPR Nusamba Cepiring"
                    ],
                    "to" => [
                        "email" => $email,
                        "name" => ""
                    ],
                    "subject" => $subject,
                    "textpart" => "",
                    "html" => $message,
                    "customid" => "sendMail"
                ]
            ]);
    
            $status_code = $response->getStatusCode();
            $results = $response->getBody()->getContents();
            $results = json_encode($results);
    
            $status = ($status_code == '200' ? 'sukses' : 'gagal'); 
           
            if($results) {
                $data =  [
                    'status' => $status_code,
                    'log' => $results,
                ];
            }else{
                $data =  [
                    'status' => 'gagal',
                    'log' => 'Not Found',
                ];
            }
            

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            //throw $e;
            $res_body = $e->getResponse()->getBody()->getContents();
            $data =  [
                'status' => $status_code,
                'log' => $res_body,
            ];
        }
        // map_y($data);
        $CI->db->insert('mail_log', $data);
    }
}

/**
 * Resend Email
 * @param $email
 * @param $message
 */

if(!function_exists('resend_email')){
    function resend_email($email = '', $message = '', $subject = 'BPR Nusamba Cepiring'){
        $CI =& get_instance();
        
        $client = new GuzzleHttp\Client([
            'base_uri' => URL_MAILAPI,
        ]);
        
        $data = [];
        
        try {
            //code...
            $response = $client->post('v1/send_email', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'api-key' => 'YUNNA-2019-ARGAN',
                    'mailjet_user' => 'de9a205b2f545f77b2ece89f48919d10',
                    'mailjet_pass' => 'a3059183f502963d44a6798ca515ceee'
                ],
                GuzzleHttp\RequestOptions::JSON => [
                    "from" => [
                        "email" => "bprnusambacepiring.com@gmail.com",
                        "name" => "BPRNUSAMBACEPIRING"
                    ],
                    "to" => [
                        "email" => $email,
                        "name" => ""
                    ],
                    "subject" => $subject,
                    "textpart" => "",
                    "html" => $message,
                    "customid" => "sendMail"
                ]
            ]);
    
            $status_code = $response->getStatusCode();
            $results = $response->getBody()->getContents();
            $results = json_decode($results);
    
            $status = ($status_code == '200' ? 'sukses' : 'gagal'); 
           
            if($results) {
                $data =  [
                    'status' => $status_code,
                    'log' => $results,
                ];
            }else{
                $data =  [
                    'status' => 'gagal',
                    'log' => 'Not Found',
                ];
            }
            

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            //throw $e;
            $res_body = $e->getResponse()->getBody()->getContents();
            $data =  [
                'status' => $status_code,
                'log' => $res_body,
            ];
        }
            
        // map_y($results);
        $CI->db->insert('mail_log', $data);
    }
}

/**
 * Send Wa
 * @param $no_hp
 * @param $message
 */
if(!function_exists('send_wa')){
    function send_wa($no_hp, $message){
        $CI =& get_instance();
        
        $_generate = $CI->db->query("SELECT wa_token FROM setting")->row();
        $token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjAyODMyOTM2fQ.q_Wg-mKlV8rOPds9bLvkYWQ-5s5WYJGTul35yCjUCTE'; //from wablas.com

        $client = new GuzzleHttp\Client([
            'base_uri' => URL_WA,
        ]);
// 6294382234
        try {
            //code...
            $response = $client->request('POST','sendMessage',[
                'headers' => [
                    'Content-Type' => 'application/json',
                    'access-token' => $token
                ],
                'json' => [
                    'device_number' => '62894382234',
                    'message' => $message,
                    'to' => $no_hp,
                ]
            ]);
            // map_y($response);
            
            $check_status = $response->getStatusCode();
            $res_body = $response->getBody()->getContents();
            $res_body = json_decode($res_body);
           
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            //throw $e;
            $res_body = $e->getResponse()->getBody()->getContents();
            $res_body = json_decode($res_body);
        }

        // $data = array(
        //     'cr_time' => date('Y-m-d H:i:s'),
        //     'cr_user' => isset($_SESSION['username']) ? $_SESSION['username'] : 'User Public',
        //     'up_time' => date('Y-m-d H:i:s'),
        //     'status' => $res_body->message,
        //     'statuscode' => $res_body->status,
        //     'notlp' => $no_hp,
        //     'message' => $message,
        //     'definition' => json_encode($res_body),
        // );
        // $CI->db->insert('wa_log', $data);
    }
}

/**
 * Validate RegEx
 */

 if(!function_exists('validate_regex')){
    function validate_regex($pattern, $str){
        $pattern = '/'.$pattern.'/i';
        // map_y($pattern);
        $check = preg_match($pattern,$str, $matches);
        if(count($matches) > 0){
            return true;
        }else{
            return false;
        }
    }
 }

/**
 * Generate UUID
*/
if(!function_exists('uuid')){
    function uuid(){
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
}

/**
 * Generate No Pertemuan
 * 
 */
if(!function_exists('gen_no_pertemuan')){
    function gen_no_pertemuan($id_nasabah = '', $id_produk = ''){
        if(!$id_nasabah || !$id_produk){
            return 'generate invalid';
        }
        $CI =& get_instance();
        $_generate = $CI->db->query("SELECT func_gen_no_pertemuan($id_nasabah, $id_produk) as no")->row();
        $_generate = $_generate->no;
        return $_generate;
    }
}

/**
 * Save Log
 */
if(!function_exists('insert_log')){
    function insert_log($definition){
        $CI =& get_instance();

        $data = array (
            'definition' => $definition,
            'cr_time' => date('Y-m-d H:i:s'),
            'up_time' => date('Y-m-d H:i:s'),
            'cr_user' => isset($_SESSION['username']) ? $_SESSION['username'] : 'User Public',
        );
        $CI->db->insert('trans_log', $data);
    }
}

/**
 * Get User Id From Sales
 */
if(!function_exists('sales_user_id')){
    function sales_user_id($id_sales){
        $CI =& get_instance();

        $get = $CI->db->query("SELECT user_id FROM sales WHERE id = '$id_sales'")->row();
        return @$get->user_id;
    }
}

/**
 * Get User Id From Sales
 */
if(!function_exists('get_user_by_sales')){
    function get_user_by_sales($id_sales){
        $CI =& get_instance();

        $get = $CI->db->query("SELECT user_id FROM sales WHERE id = '$id_sales'")->row();
        $user_id = $get->user_id;
        $user = $CI->db->query("SELECT * FROM aauth_users WHERE id = '$user_id'")->row();
        return @$user;
    }
}

/**
 * Get User Id From Sales
 */
if(!function_exists('get_sales_id')){
    function get_sales_id(){
        $user_id = $_SESSION['id'];
        $CI =& get_instance();

        $result = $CI->db->query("SELECT * FROM sales WHERE user_id = '$user_id'")->row();
        return @$result->id;
    }
}

/**
 * Set Status User
 */
if(!function_exists('set_user_status')){
    function set_user_status($user_id, $status = 1){
        $CI =& get_instance();    
        $CI->db->query("UPDATE aauth_users SET status = $status WHERE id = '$user_id'");
    }
}

/**
 * Get Name Product
 */
if(!function_exists('get_name_product')){
    function get_name_product($id_product){
        $CI =& get_instance();   
        $get = $CI->db->query("SELECT name FROM product where id = $id_product")->row()->name;
        return @$get;
    }
}

/**
 * Remap error 
 */
if(!function_exists('map_error')){
    function map_error($str){
        $arr = explode('|', $str);
        // map_y(array_pop($arr));
        array_pop($arr);
        return $arr;
    }
}

if(!function_exists('get_total_loker')){
    function get_total_loker($id_kategori){
        $CI =& get_instance();   
        $get = $CI->db->query("SELECT count(*) FROM public.front_career where id_kategori =  '$id_kategori'")->result_array();
        return @$get;
    }
}
if(!function_exists('get_careerbyid')){
    function get_careerbyid($id){
        $CI =& get_instance();   
        $get = $CI->db->query("SELECT * FROM public.front_career where id =  '$id'")->result_array();
        return @$get;
    }
}
if(!function_exists('get_baner_by_kode')){
    function get_baner_by_kode($id){
        $CI =& get_instance();   
        $get = $CI->db->query("SELECT * FROM public.front_baner where kode = '$id'")->row_array();
        return $get;
    }
}

if(!function_exists('spoiler')){
    function spoiler($content, $length = 120){
        $new = preg_replace("/<[^>]+\>/i",' ',$content);
        return substr($new,0,$length).'...';
    }
}

if(!function_exists('star_render')){
    function star_render($star, $total = 5){
        $html = '';
        for($i = 0; $i <= $total; $i++){
            if($star >= $i){
                $html .= '<span class="fa fa-star" style="color: orange;"></span>';
            }else{
                $html .= '<span class="fa fa-star"></span>';    
            }
        }
        echo $html;
    }
}
/**
 * Get User Id From Kabid
 */
if(!function_exists('get_user_by_kabid')){
    function get_user_by_kabid($id_kabid){
        $CI =& get_instance();

        $get = $CI->db->query("SELECT user_id FROM kabid WHERE id = '$id_kabid'")->row();
        $user_id = $get->user_id;
        $user = $CI->db->query("SELECT * FROM aauth_users WHERE id = '$user_id'")->row();
        return @$user;
    }
}

/**
 * Get User Id From Kabid
 */
if(!function_exists('kabid_user_id')){
    function kabid_user_id($kabid_id){
        $CI =& get_instance();

        $get = $CI->db->query("SELECT user_id FROM kabid WHERE id = '$kabid_id'")->row();
        return @$get->user_id;
    }
}


/**
 * Get User Id From Kantor
 */
if(!function_exists('sales_by_kantor')){
    function sales_by_kantor($id_kantor){
        $CI =& get_instance();

        $get = $CI->db->query("SELECT id_kantor FROM sales WHERE id_kantor = '$id_kantor'")->row();
        return @$get->user_id;
    }
}
/**
 * Request Dukcapil
 */
if(!function_exists('request_dukcapil')){
    function request_dukcapil($nik, $username, $macAddress){
        $base_url = get_config_setup('url_dukcapil') ? get_config_setup('url_dukcapil') : URL_DUKCAPIL;
        $client = new GuzzleHttp\Client(['base_uri' => $base_url]);
        // map_y($base_url);
        try {
            //code...
            $response = $client->request('POST','/xnik/reqnik',[
                'headers' => [
                    'xapix' => 'CPD1G1T4L4M0R3KR1Y4N3S14',
                    'xincomingx' => 'CEPIRING',
                    'Content-Type' => 'application/json'
                ],
                GuzzleHttp\RequestOptions::JSON => [
                    'nik' => $nik,
                    'username' => $username,
                    'address' => $macAddress,
                ]
            ]);

            $res_body = $response->getBody()->getContents();

            $res_body = json_decode($res_body);
            return $res_body;
           
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            //throw $e;
            $res_body = $e->getResponse()->getBody()->getContents();
            $res_body = json_decode($res_body);
        }
        

        return $res_body;
    }
}


if(!function_exists('sales_by_user_id')){
    function sales_by_user_id($user_id = ''){
        $user_id = $user_id ? $user_id : $_SESSION['id'];
        $CI =& get_instance();
        $get = $CI->db->query("SELECT * FROM sales WHERE user_id='$user_id'")->result_array();
        return @$get[0];
    }
}

if(!function_exists('kabid_by_user_id')){
    function kabid_by_user_id($user_id = ''){
        $user_id = $user_id ? $user_id : $_SESSION['id'];
        $CI =& get_instance();
        $get = $CI->db->query("SELECT * FROM kabid WHERE user_id='$user_id'")->result_array();
        return @$get[0];
    }
}

if(!function_exists('get_config_setup')){
    function get_config_setup($nama_field){
        $CI =& get_instance();
        $get = $CI->db->query("SELECT * FROM setting LIMIT 1")->result_array();
        return @$get[0][$nama_field];
    }
}

if(!function_exists('get_role')){
    function get_role($user_id = ''){
        $user_id = $user_id ? $user_id : $_SESSION['id'];
        $CI =& get_instance();
        $result = $CI->db->query("SELECT group_id FROM aauth_user_to_group WHERE user_id = '$user_id'")->result_array();
        return @$result[0]['group_id'];
    }
}

if(!function_exists('generate_referal')){
    function generate_referal(){
        $unique_key  = str_random(8);
        $unique_key2 = str_random(5);
        $unique_key3 = str_random(3);
        $referal = $unique_key.'-'.$unique_key2.'-'.$unique_key3;
        return $referal;
    }
}

if(!function_exists('get_ref')){
    function get_ref($user_id = ''){
        $user_id = $user_id ? $user_id : $_SESSION['id'];
        $CI =& get_instance();
        $result = $CI->db->query("SELECT kode_referal FROM sales WHERE user_id = '$user_id'")->result_array();
        return @$result[0]['kode_referal'];
    }
}

if(!function_exists('check_pw')){
    function check_pw($pw, $user_id = ''){
        $user_id = $user_id ? $user_id : $_SESSION['id'];
        $CI =& get_instance();
        $CI->load->library("Aauth");
        $config_vars = $CI->config->item('aauth');

        $password = hash_pw($pw, $user_id);
        $user = $CI->db->query("SELECT pass FROM aauth_users WHERE id = '$user_id'")->row();
        // map_y($password);
        if($CI->aauth->verify_password($password, $user->pass)){
            return true;
        }else{
            return false;
        }
        // return $check ? 'true' : 'false';
    }
}
if(!function_exists('hash_pw')){
    function hash_pw($pw, $user_id = '0'){
        // map_y($pw.$user_id);
        $CI =& get_instance();
        $config_vars = $CI->config->item('aauth');
        
        if($config_vars['use_password_hash']){
			$_pw = password_hash($pw, $config_vars['password_hash_algo'], $config_vars['password_hash_options']);
		}else{
			$salt = MD5($user_id);
            // map_y($salt);
            $_pw = hash($config_vars['hash'], $salt.$pw);
        }
        // map_y($_pw);
        return $_pw;
    }
}

if(!function_exists('generate_name_random')){
    function generate_name_random($addon = ''){
        $unique_key  = str_random(7);
        $unique_key2 = str_random(5);
        $unique_key3 = str_random(3);
        $unique_key4 = str_random(5);
        $unique_key5 = str_random(3);
        $name = $unique_key.'-'.$unique_key2.'-'.$unique_key3.'-'.$unique_key4.'-'.$unique_key5;
        if($addon){
            $name .= '-'.$addon;
        }
        return $name;
    }
}

if(!function_exists('str_random')){
    function str_random($length = 10){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if(!function_exists('delete_file')){
    function delete_file($path){
        if($path){
            $root = $_SERVER['DOCUMENT_ROOT'].'/';
            $file = $root.$path;
            if(file_exists($file)){
                @unlink($path);
            }
        }
    }
}

if(!function_exists('get_file_name')){
    function get_file_name($table = '', $pk, $id, $nama_field){
        $CI =& get_instance();
        $result = $CI->db->query("SELECT $nama_field FROM $table WHERE $pk = '$id'")->result_array()[0];
        return @$result[$nama_field];
    }
}

if(!function_exists('print_pdf')){
    function print_pdf($name = 'PDF', $html, $path =''){
    
        $mpdf = new Mpdf\Mpdf(['tempDir' => __DIR__ . '/temp']);
        $mpdf->WriteHTML($html);
        // $mpdf->SetWatermarkText('Developed Manage by Tranyar');
        $mpdf->showWatermarkText = true;
        $mpdf->watermarkTextAlpha = 0.1;
        if($path != ''){
            $pdfFilePath = $_SERVER['DOCUMENT_ROOT']."/".$path.".pdf";
            $mpdf->Output($pdfFilePath, 'F');
        }
        $mpdf->Output($name.".pdf", 'F');
        $mpdf->Output();

    }
}

if(!function_exists('front_config')){
    function front_config($table = 'front_profil', $column = ''){
        $CI =& get_instance();
        $result = $CI->db->query("SELECT $column FROM $table")->result_array()[0];
        return @$result[$column];
    }
}

if(!function_exists('RATE')){
    function RATE($nper, $pmt, $pv, $fv = 0.0, $type = 0, $guess = 0.1) {
        $FINANCIAL_MAX_ITERATIONS = 128;
        $FINANCIAL_PRECISION = 1.0e-08;
        $rate = $guess;
        if (abs($rate) < $FINANCIAL_PRECISION) {
            $y = $pv * (1 + $nper * $rate) + $pmt * (1 + $rate * $type) * $nper + $fv;
        } else {
            $f = exp($nper * log(1 + $rate));
            $y = $pv * $f + $pmt * (1 / $rate + $type) * ($f - 1) + $fv;
        }
        $y0 = $pv + $pmt * $nper + $fv;
        $y1 = $pv * $f + $pmt * (1 / $rate + $type) * ($f - 1) + $fv;
        $i = $x0 = 0.0;
        $x1 = $rate;
        while ((abs($y0 - $y1) > $FINANCIAL_PRECISION) && ($i < $FINANCIAL_MAX_ITERATIONS)) {
            $rate = ($y1 * $x0 - $y0 * $x1) / ($y1 - $y0);
            $x0 = $x1;
            $x1 = $rate;
            if (abs($rate) < $FINANCIAL_PRECISION) {
                $y = $pv * (1 + $nper * $rate) + $pmt * (1 + $rate * $type) * $nper + $fv;
            } else {
                $f = exp($nper * log(1 + $rate));
                $y = $pv * $f + $pmt * (1 / $rate + $type) * ($f - 1) + $fv;
            }
            $y0 = $y1;
            $y1 = $y;
            ++$i;
        }
        return $rate;
    }
}


/**
 * Cek Detail Wa Profile
 */
if(!function_exists('cek_wablas')){
    function cek_wablas(){
        $CI =& get_instance();
        
        $_generate = $CI->db->query("SELECT wa_token FROM setting")->row();
        $token = $_generate->wa_token; //from wablas.com

        $client = new GuzzleHttp\Client([
            'base_uri' => URL_WA,
        ]);

        try {
            //code...
            $response = $client->request('GET','api/device/info?token='.$token);
            // map_y($response);
            
            $check_status = $response->getStatusCode();
            $res_body = $response->getBody()->getContents();
            $res_body = json_decode($res_body);
           
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            //throw $e;
            $res_body = $e->getResponse()->getBody()->getContents();
            $res_body = json_decode($res_body);
        }

        return $res_body;
    }
}
if(!function_exists('token_wa')){
    function token_wa(){
        $CI =& get_instance();
        
        $_generate = $CI->db->query("SELECT wa_token FROM setting")->row();
        $token = $_generate->wa_token; //from wablas.com

        return $token;
    }
}

if(!function_exists('substractMonth')){
    function substractMonth($date, $val, $dateFormat){
        $diff = $val.' '.'month';
        $newdate = strtotime ($diff , strtotime ($date)) ;
        $newdate = date ($dateFormat , $newdate );
        return $newdate;
    }
}

if(!function_exists('cek_hasil_penagihan')){
    function cek_hasil_penagihan($id){
        $CI =& get_instance();
        $result = $CI->db->query("SELECT * FROM c_inventory_collect where id_angsuran = $id")->result_array();
        $value = isset($result) ? true : false;
        return $value;
    }
}

if(!function_exists('tgl_indo')){
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}

if(!function_exists('tgl_indo')){
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}

if(!function_exists('hari_indo')){
    function hari_indo($hari){

        switch($hari){
            case 'Sun':
                $hari_indo = "Minggu";
            break;
     
            case 'Mon':         
                $hari_indo = "Senin";
            break;
     
            case 'Tue':
                $hari_indo = "Selasa";
            break;
     
            case 'Wed':
                $hari_indo = "Rabu";
            break;
     
            case 'Thu':
                $hari_indo = "Kamis";
            break;
     
            case 'Fri':
                $hari_indo = "Jumat";
            break;
     
            case 'Sat':
                $hari_indo = "Sabtu";
            break;
            
            default:
                $hari_indo = "Tidak di ketahui";     
            break;
        }
     
        return $hari_indo;
     
    }
}

if(!function_exists('hari_indo')){
    function hari_indo($hari){

        switch($hari){
            case 'Sun':
                $hari_indo = "Minggu";
            break;
     
            case 'Mon':         
                $hari_indo = "Senin";
            break;
     
            case 'Tue':
                $hari_indo = "Selasa";
            break;
     
            case 'Wed':
                $hari_indo = "Rabu";
            break;
     
            case 'Thu':
                $hari_indo = "Kamis";
            break;
     
            case 'Fri':
                $hari_indo = "Jumat";
            break;
     
            case 'Sat':
                $hari_indo = "Sabtu";
            break;
            
            default:
                $hari_indo = "Tidak di ketahui";     
            break;
        }
     
        return $hari_indo;
     
    }
}

if(!function_exists('format_rupiah')){
    function format_rupiah($angka){
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;
    }
}

if(!function_exists('mergePDFFiles')){
    function mergePDFFiles($filenames, $outFile) {
        $mpdf = new Mpdf\Mpdf(['tempDir' => __DIR__ . '/temp']);
        $num = 1;
        foreach ($filenames as $key ) {
            # code...
            if (file_exists($key['dir'])){
                if($key['type'] == 'pdf'){
                    $mpdf->WriteHTML('<pagebreak />');
                    $pagecount = $mpdf->SetSourceFile($key['dir']);
                    for ($i = 1; $i <= $pagecount; $i++) {
                        $tplId = $mpdf->ImportPage($i);
                        $mpdf->UseTemplate($tplId);
                        if ($num < $pagecount || $i != $pagecount) {
                            $mpdf->WriteHTML('<pagebreak />');
                        }
                    }     
                }else{
                    $mpdf->WriteHTML('<pagebreak />');
                    $html = '<p><img style="width:100%;" id="image" src="'.$key['dir'].'"></p>';
                    $mpdf->WriteHTML($html);   
                }
               
                $num++;        
            }
            
        }
        $mpdf->Output($outFile.".pdf", 'I');
        // $mpdf->Output();
    }

}
 if(!function_exists('h_get_icon')){
    function h_get_icon($key = '', $column = ''){
        $CI =& get_instance();
        $result = $CI->db->query("SELECT $column FROM front_depan where key = '$key'")->row();
        return @$result;
    }
}

 
?>