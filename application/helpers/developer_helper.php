<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('date_ind_to_eng')){
    function date_ind_to_eng($date){
        $date_id = $date;
        $temp_date = DateTime::createFromFormat('d-m-Y', $date_id);
        $date_eng = $temp_date->format('Y-m-d');
        return @$date_eng;
    }
}
if ( ! function_exists('date_eng_to_ind')){
    function date_eng_to_ind($date){
        $date_eng = $date;
        $temp_date = DateTime::createFromFormat('Y-m-d', $date_eng);
        $date_id = $temp_date->format('d-m-Y');
        return @$date_id;
    }
}
if ( ! function_exists('truncate')){

    function truncate($string, $width, $etc = ' ..')
    {
        $wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
        return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
    }


}

if ( ! function_exists('getMenuChilds')){

    function getMenuChilds($id)
    {

        $ci =& get_instance();
        $ci->load->model('Mcore');

        return $ci->mcore->getMenuChilds($id);

    }

}

if ( ! function_exists('getMenus')){

    function getMenus()
    {

        $ci =& get_instance();
        $ci->load->model('Mcore');

        return $ci->mcore->getMenus();

    }


}

if ( ! function_exists('getUserGroup')){

    function getUserGroup($user_id)
    {

        $ci =& get_instance();
        $ci->load->model('Mcore');

        return $ci->mcore->getUserGroup($user_id);

    }


}

if ( ! function_exists('getActiveSkin')){

    function getActiveSkin()
    {

        $ci =& get_instance();
        $ci->load->model('Mcore');

        return $ci->mcore->getActiveSkin();

    }


}

if ( ! function_exists('rest_api'))
{
    function rest_api($config){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Access-Code:411'));
        curl_setopt($ch, CURLOPT_URL, $config['url']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($config['request']));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        try{

            $server_output = curl_exec($ch);

            $response = json_decode($server_output);

        }catch (Exception $e){

            $response = array("error" => $e->getMessage());

        }

        return object_to_array($response);

    }
}
if ( ! function_exists('object_to_array'))
{
    function object_to_array($nested_object){
        return json_decode(json_encode($nested_object), true);
    }
}

if ( ! function_exists('generateTextInput')){

    function generateTextInput($label,$id,$data_value = null)
    {
        $field_name = strtolower(str_replace(' ', '_', $label));
        $field_name = (preg_replace('/[^A-Za-z0-9\_]/', '', $field_name));
        $class = "form-control";
        $maxlength = "";
        $minlength = "";
        $placeholder = $label;
        $event = "";
        $data_attr = "";
        $type = "text";
        $value = @$data_value;
        $disabled = "";
        $readonly = "";
        $required = "";
        $style = "";

        // if($data_value!=null){

        //     $disabled = "disabled";

        // }

        if($type=="hidden"){
            $hidden = "style='display:none;'";
        }else{
            $hidden = "";
        }

        $html = '<div class="form-group" '.$hidden.'>';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<input type="'.$type.'" name="text_'.$id.'" id="'.$field_name.'_'.$id.'" class="'.$class.'" placeholder="'.$placeholder.'" maxlength="'.$maxlength.'" minlength="'.$minlength.'" value="'.$value.'" style="'.$style.'" '.$event.' '.$data_attr.' '.$disabled.' '.$readonly.' '.$required.'>';
        $html.= '</div>';

        echo $html;

    }


}

if ( ! function_exists('generateTextInputValue')){

    function generateTextInputValue($label,$id,$data_value = null)
    {
        $field_name = strtolower(preg_replace('/\s+/', '_', $label));
        $class = "form-control";
        $maxlength = "";
        $minlength = "";
        $placeholder = $label;
        $event = "";
        $data_attr = "";
        $type = "text";
        $value = @$data_value;
        $disabled = "";
        $readonly = "";
        $required = "";
        $style = "";

        // if($data_value!=null){

        //     $disabled = "disabled";

        // }

        if($type=="hidden"){
            $hidden = "style='display:none;'";
        }else{
            $hidden = "";
        }

        $html = '<div class="form-group" '.$hidden.'>';
        $html.= '<label for="'.$id.'">'.$label.'</label>';
        $html.= '<input type="'.$type.'" name="text_'.$id.'" id="'.$field_name.'_'.$id.'" class="'.$class.'" placeholder="'.$placeholder.'" maxlength="'.$maxlength.'" minlength="'.$minlength.'" value="'.$value.'" style="'.$style.'" '.$event.' '.$data_attr.' '.$disabled.' '.$readonly.' '.$required.'>';
        $html.= '</div>';

        echo $html;

    }


}


if ( ! function_exists('generateCheckbox')){

    function generateCheckbox($label,$id,$source,$id_nasabah = null ,$id_product = null)
    {
        $ci =& get_instance();

        $disabled = "";

        $html = '<div class="form-check">';
        $html.= '<label class="form-check-label for="defaultCheck1">'.$label.'</label>';
    
        $i=1;
        foreach($source as $sc) {

            $data_formulir_nasabah = $ci->db->get_where("data_formulir_nasabah",
              array(
                  "id_nasabah" => $id_nasabah,
                  "id_product" => $id_product,
                  "value" => $sc['id']
              )
            )->result_array();


            if(@$data_formulir_nasabah[0]['value']!=null){

                $disabled = "disabled";

            }

            $field_name = strtolower(preg_replace('/\s+/', '_', $sc['label']));

            $html.= '<div class="form-check">';
            $html.= '<label class="form-check-label" for="defaultCheck2">';
            
            if(@$data_formulir_nasabah[0]['value']!=null){

                if($sc['id'] == @$data_formulir_nasabah[0]['value']){

                    $html.= '<input class="form-check-input"'.$disabled.' checked type="checkbox" name="checkbox_'.$sc['id'].'" id="'.$field_name.'_'.$i.'" value="'.$sc['value'].'"> '.$sc['label'];

                }else{

                    $html.= '<input  class="form-check-input"'.$disabled.' type="checkbox" name="checkbox_'.$sc['id'].'" id="'.$field_name.'_'.$i.'" value="'.$sc['value'].'"> '.$sc['label'];

                }

            }else{

                $html.= '<input class="form-check-input"'.$disabled.' type="checkbox" name="checkbox_'.$sc['id'].'" id="'.$field_name.'_'.$i.'" value="'.$sc['value'].'"> '.$sc['label'];

            }

            $html.= '</label>';
            $html.= '</div>';

            $i++;
        }

        $html.= '</div>';

        echo $html;

    }


}

// if ( ! function_exists('generateRadio')){

//     function generateRadio($label,$id,$source,$id_nasabah = null ,$id_product = null)
//     {
//         $ci =& get_instance();

//         $disabled = "";

//         $html = '<div class="form-group">';
//         $html.= '<label>'.$label.'</label>';
    
//         $i=1;
//         foreach($source as $sc) {

//             $data_formulir_nasabah = $ci->db->get_where("data_formulir_nasabah",
//               array(
//                   "id_nasabah" => $id_nasabah,
//                   "id_product" => $id_product,
//                   "value" => $sc['id']
//               )
//             )->result_array();


//             if(@$data_formulir_nasabah[0]['value']!=null){

//                 $disabled = "disabled";

//             }

//             $field_name = strtolower(preg_replace('/\s+/', '_', $sc['label']));

//             $html.= '<div class="radio">';
//             $html.= '<label>';
            
//             if(@$data_formulir_nasabah[0]['value']!=null){

//                 if($sc['id'] == @$data_formulir_nasabah[0]['value']){

//                     $html.= '<input '.$disabled.' checked type="radio" name="radio_'.$sc['id'].'" id="'.$field_name.'_'.$i.'" value="'.$sc['value'].'"> '.$sc['label'];

//                 }else{

//                     $html.= '<input '.$disabled.' type="radio" name="radio_'.$sc['id'].'" id="'.$field_name.'_'.$i.'" value="'.$sc['value'].'"> '.$sc['label'];

//                 }

//             }else{

//                 if($i==0){
//                     $checked = "checked";
//                 }else{
//                     $checked = "";
//                 }

//                 $html.= '<input '.$checked.' type="radio" name="radio_'.$sc['id'].'" id="'.$field_name.'_'.$i.'" value="'.$sc['value'].'"> '.$sc['label'];

//             }

//             $html.= '</label>';
//             $html.= '</div>';

//             $i++;

//             }

//         }

//         $html.= '</div>';

//         echo $html;

//     }
