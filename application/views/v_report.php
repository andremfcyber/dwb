<style>
.card {
    border: 2px #ddd solid;
    border-radius: 10px;
    padding: 10px;
    margin-bottom: 10px;
}
.card .card-body span {
    font-size: 13px;
}
.card-img {
    text-align: center;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
            
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-img">
                        <img src="<?= base_url('assets/dist/img/pdf.png') ?>">
                    </div>
                    <div class="card-body">
                        <span>Pertemuan</span>
                        <a target="_blank" href="<?= $url_report.'?type=pertemuan' ?>">Cetak Laporan</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-img">
                        <img src="<?= base_url('assets/dist/img/pdf.png') ?>">
                    </div>
                    <div class="card-body">
                        <span>Detail Nasabah</span>
                        <a target="_blank" href="<?= $url_report.'?type=detail_nasabah' ?>">Cetak Laporan</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

