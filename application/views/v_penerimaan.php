<style>
    .text{
        border: 1px #ddd solid;
        padding: 14px;
        border-radius: 3px;
    }
    .argan{
        resize: vertical; 
        border: 1px #ddd solid;
        width: 100%;
        margin-top: 3px; 
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- <?= $url_reject ?> -->
        <div class="alert alert-info">
            <strong><i class="fa fa-info"></i> </strong> Masukan keterangan , contohnya: Plafon/jumlah nominal, beserta keterangan jangka waktu 
        </div>
        <form id="form-penerimaan" action="<?= $url_reject ?>">
            <div class="form-group">
                <label> Pesan yang akan dikirim ke nasabah </label>
                <div class="text">
                    Selamat Pengajuan <?= $produk ?> anda di BPR Nusamba Cepiring dapat setuju dengan
                    <textarea name="keterangan" class="argan" placeholder="Isi Sendiri"></textarea>
                    Silahkan datang di kantor BPR Nusamba Cepiring kantor kas (sesuai dengan kantor AO yang memproses).
                    BAWA KTP dan Tunjukan Link Tracking Anda
                </div>
                <!-- <label>Keterangan Diterima :</label> -->
                    
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ok</button>
                <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-"></i> Batal</button>
            </div>
        </form>
    </div>
</div>

<script>
    $('#form-penerimaan').submit((e) => {
        // console.log(e.target.action)
        blockUI("Selalu berdo'a semuanya lancar");
        e.preventDefault()
        let keterangan = $('[name="keterangan"]').val();
        if(!keterangan){
            alert('Keterangan Wajib Diisi');
            $.unblockUI();
            return false;
        }
        $.post(e.target.action,{
            'keterangan' : keterangan,
        }).done((res) => {
            if(res.status == 1){
                toastr.success('Berhasil', res.msg);
                setTimeout(() => {
                    window.location.reload()
                }, 2000);
            }else{
                toastr.error('Gagal', res.msg);
                $.unblockUI();
            }
        }).fail((xhr) => {

        })
    })
</script>