<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Detail Nasabah</title>
</head>
<style media="print">
    .table { 
        width: 100%;
        padding: 10px;
        border-collapse: collapse;
    }
    .table tr td {
        padding: 10px 15px;
        border: 1px solid #000; 
    }
    .header2{
       font-weight: bold;
    }
    .table_head{
        margin-bottom: 10px;
    }
    .table_foot{
        margin-top: 10px;
        border-collapse: collapse;
    }
    .table_foot tr td{
        padding: 5px 15px;
        border: 1px solid #000; 
    }
</style>
<body>
    <table align="right" class="table_head">
        <tr>
            <td>Tanggal Pengajuan : </td>
            <td><?= Carbon\Carbon::parse($nasabah['tgl_pengajuan'])->format('d/m/Y H:i:s') ?></td>
        </tr>
    </table>
    <table class="table">
        <tr>
            <td colspan="2" style="font-size: 20px; text-align: center; text-transform: uppercase;">Formulir Nasabah</td>
        </tr>
        <?php if(count($detail_dinamis_nasabah) > 0) { ?>
            <?php foreach($detail_dinamis_nasabah as $key => $value) { ?>
                <tr>
                    <td colspan="2" class="header2" style="padding-left: 10px;"><?= $key ?></td>
                </tr>
                <?php foreach($value as $data) { ?>
                    <tr>
                        <td><?= $data['label'] ?></td>
                        <td><?= $data['value'] ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </table>
</body>
</html>