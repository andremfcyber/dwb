<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Surat Tugas</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    </head>
    <style type="text/css">}
        .wd-80 {
            width: 80% !important;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        
        @media only screen and (min-width: 600px) {
            .row{
                padding: 50px;
            }
        }

        .row{
            padding-top: 30px;
        }
    </style>
    <body>
    <div class="container">
        <div class="row" >
            <div class="col-md-12">
                <div class="jumbotron" style="border: 2px solid #d1d1d1">
                    <img src="<?= base_url()?>/themes/admin/adminlte/assets/img/Logo-Bpr-Nusamba-cepiring.png" width="184" height="77" /><hr /><br />
                    <div style="margin-bottom: 10px;">
                    <p style="text-align: center; font-weight: bold; font-size: 16px;">SURAT PENAGIHAN</p>
                    </div>
                    <br />    
                    <div style="margin-bottom: 10px;">
                        <p>Yth Pak/Bu</p>
                        <p>Terimakasih atas kerjasama yang baik dan sudah mempercayakan pembiayaan pada BPR NUSAMBA CEPIRING.</p>
                        <p>Kami mengingatkan tagihan Pak/bu <?= $nasabah['nama_nasabah'] ? $nasabah['nama_nasabah'] : '' ?> </p>
                        
                        <div>
                            <table >
                                <tr>
                                    <td>Jumlah Tagihan</td>
                                    <td style="width: 5%">:</td>
                                    <td><?= $bulanan ? $bulanan : '' ?></td>
                                </tr>
                                <tr>
                                    <td>Tagihan ke</td>
                                    <td style="width: 5%">:</td>
                                    <td>Bulan <?= $nasabah['angsuran_ke'] ? $nasabah['angsuran_ke'] : '' ?></td>
                                </tr>
                                <tr>
                                    <td>Nomor penagihan</td>
                                    <td style="width: 5%">:</td>
                                    <td><?= $nasabah['id'] ? $nasabah['id'] : '' ?></td>
                                </tr>
                                <tr>
                                    <td>Sisa Tagihan</td>
                                    <td style="width: 5%">:</td>
                                    <td><?= $nasabah['sisa_tagihan'] ? $nasabah['sisa_tagihan'] : '' ?></td>
                                </tr>
                                <tr>
                                    <td>Total Sisa Tagihan</td>
                                    <td style="width: 5%">:</td>
                                    <td><?= $nasabah['total_sisa_tagihan'] ? $total_sisa_tagihan : '' ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <p><b>TERLEWAT <?= $nasabah['telat'] ? $nasabah['telat'] : '' ?>  hari dari JATUH TEMPO PADA <?= $nasabah['tgl_penagihan'] ? $tgl_penagihan : '' ?> / Hari <?= $nasabah['tgl_penagihan'] ? $hari_penagihan : '' ?></b></p>
                    <p>Mohon segera dibayarkan agar skor kredit bapak/Ibu <?= $nasabah['nama_nasabah'] ? $nasabah['nama_nasabah'] : '' ?> bagus di BI atau OJK, menghindari blacklist.</p>
                    <p>Untuk bantuan silakan hub customer service kami klik wa.me/62294382234</p>
                    <p>Kami tunggu ya pembayarannya pak / <?= $nasabah['nama_nasabah'] ? $nasabah['nama_nasabah'] : '' ?> paling telat sesuai waktu jatuh tempo di atas</p>
                    <p>Semoga rejeki bpk/ibu <?= $nasabah['nama_nasabah'] ? $nasabah['nama_nasabah'] : '' ?> lancar dan berkah selalu, aamiin</p>
                    <p>PT BPR NUSAMBA CEPIRING mengutus collector atas nama <?= $nasabah['nama_collector'] ? $nasabah['nama_collector'] : '' ?></p>
                    <img src="<?= base_url('/upload/photo/').$nasabah['photo']?>" width="120">
                   <br>
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 49.0662%;">
                                <p>Terimakasih Banyak</p>
                                <br>
                                <br>
                                <br>
                                <br>
                                <p>Salam</p>
                                <p>Tim Penagihan</p>
                                <p>PT BPR NUSAMBA CEPRIRING</p>
                            </td>
                        </tr>
                    </table>
                   <!--  <a class="btn btn-success btn-sm" target="_blank" href="<?= base_url('public/home/download_surattugas/').$nasabah['id'] ?>"> Download</a> -->
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  </body>
</html>