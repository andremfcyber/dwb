<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->

<style type="text/css">

.card-base > .card-icon {
    text-align: center;
    position: relative;
}

.imagecard { 
    z-index: 2;
    display: block;
    position: relative;
    width: 88px;
    height: 88px;
    border-radius: 50%;
    border: 5px solid white;
    box-shadow: 1px 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    margin: 0 auto;
    color: white;
}
.card-base > .card-icon > .card-data {
    min-height: 250px !important;
    margin-top: -24px;
    background: ghostwhite;
    border: 1px solid #e0e0e0;
    padding: 15px 0 10px 0;
     box-shadow: 1px 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    min-height: 215px;
    }
#widgetCardIcon {
    background: #ff3636  !important;
    font-size: 28px;
    line-height: 78px;
}
.error-wrapper{
	position: relative;
    top: 0;
    background: white;
    padding: 20px;
    width: 100%;
}
</style>

<div class="container error-wrapper">
<div class = "row">
    <div class = "col-md-12">
        <div class="card-base">
<div class="card-icon"><a href="#" title="Widgets" id="widgetCardIcon" class="imagecard"><span class="glyphicon glyphicon-fire" style="top: 25px;"></span></a>
<div class="card-data widgetCardData">
<h2 class="box-title" style="color: #ff3636;">PHP Error</h2>
<p class="card-block text-center">
	

	<p>Severity: <?php echo $severity; ?></p>
	<p><label class="alert alert-danger"><?php echo $message; ?></label></p>
	<p>Filename: <b><?php echo $filepath; ?></b></p>
	<p>Line Number:  <b><?php echo $line; ?></b></p>

	<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

		<p>Backtrace:</p>
		<?php foreach (debug_backtrace() as $error): ?>

			<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

				<p style="margin-left:10px">
				File: <?php echo $error['file'] ?><br />
				Line: <b><?php echo $error['line'] ?></b><br />
				Function: <?php echo $error['function'] ?>
				</p>

			<?php endif ?>

		<?php endforeach ?>

	<?php endif ?>

</p>
<a href="http://google.com/?q=<?php echo $message." stackoverflow"; ?>" target="_blank" title="Ngopi dulu bro, biar nggak pusing." class="anchor btn btn-default" style="background: #ff3636; border: #ff3636; color:white;"> <i class="fa fa-paper-plane" aria-hidden="true"></i>  Stackoverflow </a></div>
</div>
<div class="space"></div>
</div>
    </div>
</div>
	
</div>
<script type="text/javascript">
	$(document).ready(function(){

	});
</script>