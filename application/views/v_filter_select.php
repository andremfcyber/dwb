
<div style="width: 40%; display: none;position: relative" id="filter_select_wrap">
    <select class="form-control input-sm select2" style="width: 100% !important;" id="filter_select">
        <option value="">All</option>
        <?php 
        if(count($select) > 0){
            foreach($select as $item){
                echo '<option value='.$item['value_status'].'>'.$item['value_status'].'</option>';
            }
        }
        ?>
    </select>
    <div style="position: absolute;top: 4px;right: -29px;">
        <i class="fa fa-filter fa-2x"></i>
    </div>
</div>



<script>
    $(document).ready(function(){
        setTimeout(function(){
            $('#filter_select_wrap').css('display', 'flex')
            $('#addon_dt').appendTo($('#dttable_wrapper > div:nth-child(1) > div:nth-child(1)'));
        }, 3000);
    });
    /**
     * Filter Status
     */
    $('#filter_select').on('change', function(e){
        $('#dttable').DataTable().search(e.target.value).draw();   
    });
</script>