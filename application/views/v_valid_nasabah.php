<!-- style -->
<style type="text/css">
	.notif-valid {
		display: block;
		margin: 5px;
		color: #fff;
		padding: 5px;
		border-radius: 4px; 
		font-weight: 500;
		text-align: center;
		max-height: 0;
		overflow:hidden;
		transition: max-height 0.5s, overflow 0s;
	}
	.notif-valid-success{
		background-color: #85D6F5 !important;
	}
	.notif-valid-error{
		background-color: #F9707C !important;
	}
	.notif-valid-show{
		max-height: 5em;
		overflow:auto;
		transition: max-height 0.5s, overflow 0.5s 0.5s;
	}
	.d_wrap{
		margin: 1.8px 3px;
	}
	.d_con{
		text-transform: capitalize;
	}
</style>
<?php if (!$isName) {?>
	<div class="row d_wrap" style="text-align: center">
		<i class="fa fa-exclamation-triangle fa-5x text-danger"></i>
		<div class="col-md-12" style="margin-top: 10px;">
			<div class="alert alert-danger">
				<i class="fa fa-exclamation-triangle"></i>
				<span>Nama calon nasabah tidak sesuai dengan data di DUKCAPIL.</span>
				<p> Nama yang terdaftar <strong><?= $nasabah['nama'] ?></strong>, nama di Dukcapil <strong><?= isset($dukcapil->NAMA_LGKP)? $dukcapil->NAMA_LGKP : '-'; ?></strong></p>
			</div>
		</div>
	</div>
<?php } else {?>
	<div class="row d_wrap" style="text-align: center">
		<i class="fa fa-check-circle fa-5x text-success"></i>
		<div class="col-md-12" style="margin-top: 10px;">
			<div class="alert alert-success">
				<i class="fa fa-check-circle"></i>
				<strong>Data calon nasabah sesuai, silahkan lakukan validasi !</strong>
			</div>
		</div>
	</div>
<?php } ?>

<div class="row d_wrap">
	<div class="col-md-12" style="margin-bottom: 5px;">
		<h4>Hasil pengecekan data di Dukcapil</h4>
	</div>
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		Nama 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->NAMA_LGKP)? strtolower($dukcapil->NAMA_LGKP) : null ?>
	</div>
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		NIK 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->NIK)? $dukcapil->NIK : null ?>
	</div>
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		No. KK
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->NO_KK)? $dukcapil->NO_KK : null ?>
	</div>
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		Tempat Lahir	 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->TMPT_LHR)? strtolower($dukcapil->TMPT_LHR) : null ?>
	</div>
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		Tanggal Lahir 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->TGL_LHR)? $dukcapil->TGL_LHR : null ?>
	</div>
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		Jenis Kelamin 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->JENIS_KLMIN)? strtolower($dukcapil->JENIS_KLMIN) : null ?>
	</div>
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		Pekerjaan 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->JENIS_PKRJN)? strtolower($dukcapil->JENIS_PKRJN) : null ?>
	</div>
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		Status Perkawinan 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->STATUS_KAWIN)? strtolower($dukcapil->STATUS_KAWIN) : null ?>
	</div>
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		Agama 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->AGAMA)? strtolower($dukcapil->AGAMA) : null ?>
	</div>	
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		Nama Ibu 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->NAMA_LGKP_IBU)? strtolower($dukcapil->NAMA_LGKP_IBU) : null ?>
	</div>	
</div>
<div class="row d_wrap">
	<div class="col-sm-4 col-md-4">
		Alamat 
	</div>
	<div class="col-md-8 col-sm-8 d_con">
		: <?= isset($dukcapil->ALAMAT)? strtolower($dukcapil->ALAMAT) : null ?>
		<div class="row">
			<div class="col-md-5 d_con">&nbsp;&nbsp;RT/RW</div>
			<div class="col-md-7 d_con"> : <?= isset($dukcapil->NO_RT)? $dukcapil->NO_RT : null; ?>/<?= isset($dukcapil->NO_RW)? $dukcapil->NO_RW : null; ?></div>
			<div class="col-md-5 d_con">&nbsp;&nbsp;Desa/Kelurahan</div>
			<div class="col-md-7 d_con"> : <?= isset($dukcapil->KEL_NAME)? strtolower($dukcapil->KEL_NAME) : null; ?></div>
			<div class="col-md-5 d_con">&nbsp;&nbsp;Kecamatan</div>
			<div class="col-md-7 d_con"> : <?= isset($dukcapil->KEC_NAME)? strtolower($dukcapil->KEC_NAME) : null; ?></div>
			<div class="col-md-5 d_con">&nbsp;&nbsp;Kabupaten / Kota</div>
			<div class="col-md-7 d_con"> : <?= isset($dukcapil->KAB_NAME)? strtolower($dukcapil->KAB_NAME) : null; ?></div>
			<div class="col-md-5 d_con">&nbsp;&nbsp;Provinsi</div>
			<div class="col-md-7 d_con"> : <?= isset($dukcapil->PROP_NAME)? strtolower($dukcapil->PROP_NAME) : null; ?></div>
			<div class="col-md-5 d_con">&nbsp;&nbsp;Kode Pos</div>
			<div class="col-md-7 d_con"> : <?= isset($dukcapil->KODE_POS)? $dukcapil->KODE_POS : null; ?></div>
		</div>
	</div>
</div>


<div class="row" style="margin-top: 25px;">
	<?php if ($nasabah['photo_selfie']) {?>
	<div class="col-md-6">
		<a class="fancybox" href="<?= base_url('upload/photo/'.$nasabah['photo_selfie'])?>">
			<img src="<?= base_url('upload/photo/'.$nasabah['photo_selfie'])?>" class="img-thumbnail"/>
		</a>
	</div>
	<?php }?>
	<?php if ($nasabah['photo_ktp']) {?>
	<div class="col-md-6">
		<a class="fancybox" href="<?= base_url('upload/photo/'.$nasabah['photo_ktp'])?>">
			<img src="<?= base_url('upload/photo/'.$nasabah['photo_ktp'])?>" class="img-thumbnail"/>
		</a>
	</div>
	<?php }?>
</div>


<br/>
<div class="row">
	<div class="col-md-12">
		<p class="notif-valid">test</p>
		<a class="btn btn-success" href="javascript:onValidate(<?= $id_nasabah?>)"><i class="fa fa-check-square-o"></i> Validasi</a>
		<a class="btn btn-default" data-dismiss="modal">Batal</a>
	</div>
</div>


<script type="text/javascript">
	function cancelValidate(id){
		return false;
	}

	function onValidate(id){
		blockUI();
		setTimeout(function() {
			var nasabah = <?= json_encode($nasabah);?>;
			var dukcapil = <?= json_encode($dukcapil)?>;
			var data = {
				nasabah : JSON.stringify(nasabah),
				dukcapil : JSON.stringify(dukcapil)
			}
			$.post(module_url + '/do_validate/'+id, data, function(response){
				if (response.status === "1"){
					$.unblockUI();
					$(".notif-valid").html(response.msg);
					$(".notif-valid").addClass('notif-valid-success notif-valid-show');
					setTimeout(function(){
						// $('#dttable').dataTable().fnDestroy();
						// InitDatatable();
						/**
						 * DataTable Draw
						 */
					    $('#dttable').DataTable().draw();
						$('#dynamicModal').modal('hide');
						$(".notif-valid").removeClass('notif-valid-success notif-valid-error notif-valid-show');
						toastr.success('Validasi berhasil dilakukan', 'Berhasil')
					}, 3000);
				}else{
					$.unblockUI();
					toastr.error('Terjadi Kesalahan', 'Duarrr ***** !!')
				}
			}, 'json');	
		}, 1000);
		
	}

</script>



