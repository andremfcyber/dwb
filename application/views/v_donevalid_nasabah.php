<div class="row">
	<div style="text-align: center">
		<i class="fa fa-exclamation-circle  fa-5x" style="color: #49c3ff;"></i>
		<h4>Status calon nasabah saat ini <strong><?= $nasabah['value_status'];?></strong></h4>
		<p>NIK : <?= $nasabah['nik']?></p>
		<p>Nama : <?= $nasabah['nama']?></p>
		<?php if($nasabah['status'] == '8' || $nasabah['status'] == '6' || $nasabah['status'] == '2'){ ?>
			<p>Keterangan : <?= $nasabah['keterangan'] ?></p>
		<?php } else { ?>
			<p>Mohon untuk lanjutkan ke tahap selanjutnya</p>
		<?php }?>
	</div>
	<div class="col-md-12">
		<?php if($nasabah['status'] == '7') { ?>
			<table class="table table-bordered">
				<tr>
					<td>Tanggal Pertemuan</td>
					<td><?= $appointment['tanggal_appoiment'] ?></td>
				</tr>
				<tr>
					<td>Waktu Pertemuan</td>
					<td><?= $appointment['waktu_appoiment'] ?></td>
				</tr>
				<tr>
					<td>Lokasi</td>
					<td><a target="_blank" href="http://www.google.com/maps/place/<?= isset($appointment['lat']) ? $appointment['lat'] : '' ?>,<?=  isset($appointment['lng']) ? $appointment['lng'] : '' ?>"><?= isset($appointment['lokasi']) ? $appointment['lokasi'] : '' ?></a></td>
				</tr>
				<tr>
					<td>Pihak lain yg dapat dihubungi</td>
					<td>
						<?= $appointment['pihak_lain'] ?>
					</td>
				</tr>
				<tr>
					<td>Hubungan</td>
					<td>
						<?= $appointment['hubungan'] ?>
					</td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>
						<?= $appointment['alamat_pihak_lain'] ?>
					</td>
				</tr>
				<tr>
					<td>Kontak Pihak Lain</td>
					<td>
						<a href="https://wa.me/<?= isset($appointment['no_telp_pihak_lain']) ? $appointment['no_telp_pihak_lain'] : '-' ?>" target="_blank"><img style="height: 45px;"><?= $appointment['no_telp_pihak_lain'] ?></a>
					</td>
				</tr>
			</table>
		<?php } ?>
	</div>
</div>