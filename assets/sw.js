// install service worker
const staticCacheName = 'site-eka-v1';
const dynamicCache = 'eka-v1';
const assets =[
    
    "themes/public/materialkit/css_frontend/bootstrap.min.css", 
    "themes/public/materialkit/css_frontend/owl.carousel.css", 
    "themes/public/materialkit/css_frontend/animate.css", 
    "themes/public/materialkit/css_frontend/style.css", 
    "assets/plugins/toastr/toastr.min.css", 
    "themes/public/materialkit/css_frontend/select2.min.css", 
    "themes/public/materialkit/css_frontend/select2-bootstrap.css"  

] 
self.addEventListener('install', evt => {
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
            console.log('caching assets');
            cache.addAll(assets);
        })
    );
});

// activate event
self.addEventListener('activate', evt => {
    console.log('Berhasil activate',evt);
});

// fetch event
self.addEventListener('fetch', evt => {
    // console.log('Berhasil fetch',evt);
    evt.respondWith(
        caches.match(evt.request).then(cacheRes => {
            return cacheRes || fetch(evt.request).then(fetchRes => {
                return caches.open(dynamicCache).then(cache => {
                    cache.put(evt.request.url, fetchRes.clone());
                    return fetchRes;
                })
            });
        })
    );
}); 