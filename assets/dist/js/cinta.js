 // Restricts input for the set of matched elements to the given inputFilter function.
(function($) {
$.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
    if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
    } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
    } else {
        this.value = "";
    }
    });
};
}(jQuery));
$(".number_only").inputFilter(function(value) {
    return /^\d*$/.test(value);    // Allow digits only, using a RegExp
});

 $(document).ready(function(){

    // load a locale
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal : function (number) {
            return number === 1 ? 'er' : 'ème';
        },
        currency: {
            symbol: 'Rp. '
        }
    });

    // switch between locales
    numeral.locale('id');
        
    $("#pp").change(function(){
        readURL(this, "#pp");
    });
    $("#frm_change_pass").validationEngine();
    $("#frm_edit_profile").validationEngine();

    $("#form").on("keypress", ":input:not(textarea)", function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });

    if (typeof target != 'undefined'){

        $("#form").submit(function(){

            showLoading();

            if($(this).validationEngine("validate")){

                toastr.info("Saving data...", "Loading");
                $("#btn-submit").button("loading");
                saveFormData();

                return false;

            }

        });

        $("#form").validationEngine({promptPosition : "topLeft"});

    }

    
});

if (typeof target != 'undefined'){

    function saveFormData(){

        var formData = new FormData($("#form")[0]);

        $.ajax({
            url: target,
            type: "POST",
            data: formData,
            dataType: "json",
            async: true,
            xhr: function()
              {
                var xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    //Do something with upload progress

                    $("#upload-progress").show();
                    $("#progressbar").attr("aria-valuenow", Math.round(percentComplete));
                    $("#progressbar").attr("style", "width:"+Math.round(percentComplete)+"%");

                  }
                }, false);
                //Download progress
                xhr.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with download progress
                    // console.log(percentComplete);
                  }
                }, false);
                return xhr;
            },
            success: function (res) {

                // console.log(res);
                hideLoading();
                resetForm();

                resetFileUpload("#attachement");
                resetFileUpload("#cover");
                $("#upload-progress").hide();

                if(res.status=="1"){
                    toastr.success(res.msg, "Response Server");
                    window.history.back();
                }else{
                    toastr.error(res.msg, "Response Server");
                }

            },
            error: function(xhr, textStatus, error){
                 
                  alert(textStatus + " : " + xhr.statusText);
                 
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

}

function hideLoading(){

    $("body,html").animate({ scrollTop: 0 }, 600);
    $("#form_wrapper").removeClass("js");
    $("#preloader").hide();
    $("#btn-submit").button("reset");

}
function showLoading(){

    $("#form_wrapper").addClass("js");
    $("#preloader").show();

}
function cancelForm(){

    window.history.back();

}
function resetForm(){

    $('#form')[0].reset();
    $("#cover_preview").html('<h1>Cover Module</h1>');
    for (var i in CKEDITOR.instances) {

        CKEDITOR.instances[i].setData('');

    }

}

function convertToSlug(Text)
{
    return Text
        .toString()
        .trim()
        .toLowerCase()
        .replace(/\s+/g, "-")
        .replace(/[^\w\-]+/g, "")
        .replace(/\-\-+/g, "-")
        .replace(/^-+/, "")
        .replace(/-+$/, "");
}

function filterSlug(Text){

    return Text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w\-]+/g, "")
    .replace(/\-\-+/g, "-")

}

function setPermalink(source, destination){

    var txt = $("#" + source).val();
    var permalink = convertToSlug(txt);

    $("#"+destination).val(permalink);


}

function filterPermalink(selector){

    var txt = $("#" + selector).val();
    var permalink = filterSlug(txt);

    $("#"+selector).val(permalink);


}

function readURL(input, selector) {
    for(var i =0; i< input.files.length; i++){
        if (input.files[i]) {            

            var reader = new FileReader();
            var sFileName = input.files[i].name;
            var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
            

            if(sFileExtension === 'jpg' || sFileExtension === 'jpeg' || sFileExtension === 'png'){

                reader.onload = function (e) {
                    
                    
                    var image = new Image();

                    image.src = e.target.result;

                    //Validate the File Height and Width.
                    image.onload = function () {
                        
                        var height = this.height;
                        var width = this.width;

                        if (height < 768 && width < 1366) {
                            alert("Ukuran gambar harus 1366px x 768px");
                            return false;
                        }
                        
                        var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive img-pop-prev"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                        img.attr('src', e.target.result);
                        $(selector + '_preview').html(img);
                        $(selector + '_preview').show();

                        return true;
                    };

                }

            }else if(sFileExtension === 'pdf'){
                
                reader.onload = function (e) {

                    var fpdf = $('<div id="pdfviewer" style="height: 700px;margin-top:20px;"><object data="'+e.target.result+'" type="application/pdf" width="100%" height="100%">This browser does not support PDFs. Please change or update your browser.</object></div>');
                    $(selector + '_preview').html(fpdf);
                    $(selector + '_preview').show();

                    
                }

            }else if(sFileExtension === 'csv' || sFileExtension === 'xls' || sFileExtension === 'xlsx' || sFileExtension === 'doc' || sFileExtension === 'docx' || sFileExtension === 'ppt' || sFileExtension === 'pptx'){
                
                reader.onload = function (e) {

                    $(selector + '_preview').show();
                    $(selector + '_preview').html("<h1>Loading preview ...</h1>");

                    var formData = new FormData($("#form")[0]);
                    var temp_target = base_url + 'dashboard/upload_file_tmp';
                    $.ajax({
                        url: temp_target,
                        type: "POST",
                        data: formData,
                        dataType: "json",
                        async: true,
                        xhr: function()
                          {
                            var xhr = new window.XMLHttpRequest();
                            //Upload progress
                            xhr.upload.addEventListener("progress", function(evt){
                              if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                //Do something with upload progress

                                $("#upload-progress").show();
                                $("#progressbar").attr("aria-valuenow", Math.round(percentComplete));
                                $("#progressbar").attr("style", "width:"+Math.round(percentComplete)+"%");

                              }
                            }, false);
                            //Download progress
                            xhr.addEventListener("progress", function(evt){
                              if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress
                                // console.log(percentComplete);
                              }
                            }, false);
                            return xhr;
                        },
                        success: function (res) {

                            $("#upload-progress").hide();
                                
                            var iframe = '<iframe class="form-control" style="height: 700px;margin-top:20px;border: none;" src="https://view.officeapps.live.com/op/embed.aspx?src='+res.url+'"></div>'; 

                            $(selector + '_preview').show();
                            $(selector + '_preview').html(iframe);
                            $(selector + '_preview').removeClass("img-prev");

                        },
                        error: function(xhr, textStatus, error){
                             
                              alert(textStatus + " : " + xhr.statusText);
                             
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }


            }
            
            reader.readAsDataURL(input.files[i]);
        }
    }
}

function resetFileUpload(selector){

    $(selector + '_preview').html("<h1>Preview " + upperCaseFirst(selector.replace('#','')) + "</h1>");
    $(selector).val('');

}

function upperCaseFirst(value) {
    var regex = /(\b[a-z](?!\s))/g;
    return value ? value.replace(regex, function (v) {
      return v.toUpperCase();
    }) : '';
}

function remove(id){

    $("#modal_confirm").modal('show');
    $("#btn_action").attr("onclick","proceedRemove('"+id+"')");

}

function reaset_password(id, base_uri){
    console.log(base_uri)
    let url_accept = base_uri+'/form_reaset_sales/'+id;
    blockUI();
    let kimochi_img = `${base_url}assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Reaset Password</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_accept,function(){
        $.unblockUI();
        $('#dynamicModal').modal('show');
    });
}

function send_penagihan2(id, base_uri,header){
    console.log(base_uri)
    let url_accept = base_uri+'/'+id;
    blockUI();
    let kimochi_img = `${base_url}assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>${header}</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_accept,function(){
        $.unblockUI();
        $('#dynamicModal').modal('show');
    });
}
function send_penagihan(id, base_uri,header){
     console.log(base_uri)
    let url_accept = base_uri+'/'+id;
    blockUI();
    let kimochi_img = `${base_url}assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>${header}</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_accept,function(){
        $.unblockUI();
        $('#dynamicModal').modal('show');
    });
}

/**
 * Function 2x For Nasabah
 * Action
 */
if (typeof validate_url != 'undefined'){
    function validasiSub(id){
        blockUI();
        let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
        let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Validasi</h3></span>`;
        var kimochi = validate_url + "/" + id;
        $('#dynamicModal .modal-header').html(title);
        $('#dynamicModal .modal-body').load(kimochi,function(){
            $.unblockUI();
            $('#dynamicModal').modal({show:true});
        });
    }
}

/**
 * Check Status
 */
if (typeof check_status_url != 'undefined'){
    function checkSub(id){
        blockUI();
        let kimochi_img = `${base_url}/assets/dist/img/verify.png`;
        let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 60px;"> <h3>Status</h3></span>`;
        var kimochi = check_status_url + "/" + id;
        $('#dynamicModal .modal-header').html(title);
        $('#dynamicModal .modal-body').load(kimochi,function(){
            $.unblockUI();
            $('#dynamicModal').modal({show:true});
        });
    }
}

if (typeof verify_url != 'undefined'){
    function verifySub(id){
        blockUI();
        let kimochi_img = `${base_url}/assets/dist/img/verify.png`;
        let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 60px;"> <h3>Status</h3></span>`;
        var kimochi = verify_url + "/" + id;
        $('#dynamicModal .modal-header').html(title);
        $('#dynamicModal .modal-body').load(kimochi,function(){
            $.unblockUI();
            $('#dynamicModal').modal({show:true});
        });
    }
}
if (typeof print_url != 'undefined'){
    function printSub(id){
        setTimeout(function(){
            var mari = print_url + "/" + id;
            // $.post(mari, function(res){},'json');  
            window.location = mari;  
        }, 500);
    }
}

if (typeof detail_url != 'undefined'){
    function detailSub(id){
        var mari = detail_url + "/" + id;
        window.location = mari;
    }
}
    




if (typeof edit_url != 'undefined'){

    function edit(id){

        window.location.href = edit_url+"/"+id;

    }

}  

function send(id){
        alert("Pesan Sudah Terkirim")
    } 

// function button(input_type){
//      $('#tampil').submit(function(){

//        //Ajax Data
//        $.ajax({  
//           type:'POST',  
//           url:'form_fields.php',  
//           data:'YOUR DATA',
//           success: function() {

//               //Hide Button
//               $('#YOUR SUBMIT ID').attr("disabled", true);
//           },
//           error: function() { 

//               //YOUR Code  
//           }
//        });

//     });
//     }

if (typeof target != 'undefined'){

    function proceedRemove(id){

        var data = { id : id }

        $("#btn_action").button("loading");

        $.post(target, data, function(res){

            $("#btn_action").button("reset");
            $("#modal_confirm").modal('hide');
            if(res.status=="1"){
                toastr.success(res.msg, 'Response Server');

                // $('#dttable').dataTable().fnDestroy();
                // InitDatatable(); 
                /**
                 * Change By Argan
                 */
                $('#dttable').DataTable().draw();


            }else{
                toastr.error(res.msg, 'Response Server');
            }

        },'json');

    }

}

$('.close-dynamic').click(function() {
    $('#dttable').dataTable().fnDestroy();

    InitDatatable();
});


$.widget.bridge('uibutton', $.ui.button);

$(function() {
    $("img.scale").imageScale();
});

function change_password(){
    $('#change_password').modal({
        backdrop: false,
        show: true
    });
}

function edit_profile(){
    $('#edit_profile').modal({
        backdrop: false,
        show: true
    });
}
function readURL(input, selector) {
    for(var i =0; i< input.files.length; i++){
        if (input.files[i]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var img = $('<img id="'+selector+'_img_preview'+'" class="img-responsive"><a class="btn btn-sm btn-danger btn-reset-preview" href="javascript:resetFileUpload(\''+selector+'\');"><i class="fa fa-remove"></i></a>');
                img.attr('src', e.target.result);
                $(selector + '_preview').html(img);
            }
            reader.readAsDataURL(input.files[i]);
        }
    }
}

function resetFileUpload(selector){

    $(selector + '_preview').html("<h1>Photo Preview</h1>");
    $(selector).val('');

}

function update_password(){

    var target = base_url + 'dashboard/dashboard/update_password';
    var data = $("#frm_change_pass").serialize();

    if($("#frm_change_pass").validationEngine('validate')) {

        showLoadingUpProf("btn_update_pass");
        
        blockUI();
        $.post(target, data, function (res) {

            if (res.status == "1") {

                toastr.success(res.msg, 'Response Server');

                setTimeout('window.location.reload();', 1000);

            } else {
                toastr.error(res.msg, 'Response Server');
                $.unblock();
            }

            hideLoadingUpProf("btn_update_pass");

        }, 'json');

    }

}

function update_profile(){

    var target = base_url + 'dashboard/dashboard/update_profile';
    var formData = new FormData($("#frm_edit_profile")[0]);

    if($("#frm_edit_profile").validationEngine('validate')) {

        showLoadingUpProf("btn_update_profile");
         
        blockUI();
        
        $.ajax({
            url: target,
            type: 'POST',
            data: formData,
            dataType: "json",
            async: false,
            success: function (data) {

                if (data.status == "1") {

                    toastr.success(data.msg, 'Response Server');

                    setTimeout('window.location.reload();', 3000);

                } else {
                    toastr.error(data.msg, 'Response Server');
                    $.unblock();
                }

                hideLoadingUpProf("btn_update_profile");

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
}
function showLoadingUpProf(btn_id){
    $("#"+btn_id).button("loading");
}
function hideLoadingUpProf(btn_id){
    $("#"+btn_id).button("reset");
}

function formatWilayah (data) {
    if (data.loading) {
        return data.text;
    }

    var $container = $(
        "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__avatar'><img src='"+base_url+"assets/dist/img/map.png'/></div>" +
        "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'></div>" +
            "<div class='select2-result-repository__description'></div>" +
            "<div class='select2-result-repository__statistics'>" +
            "<div class='select2-result-repository__forks'><i class='fa fa-circle-o'></i> </div>" +
            "<div class='select2-result-repository__stargazers'><i class='fa fa-circle-o'></i> </div>" +
            "<div class='select2-result-repository__watchers'><i class='fa fa-circle-o'></i> </div>" +
            "</div>" +
        "</div>" +
        "</div>"
    );
    
    $container.find(".select2-result-repository__title").text(data.kelurahan);
    $container.find(".select2-result-repository__description").text('Kode Pos : '+ data.kodepos);
    $container.find(".select2-result-repository__forks").append(data.kecamatan);
    $container.find(".select2-result-repository__stargazers").append(data.kabupaten_kota);
    $container.find(".select2-result-repository__watchers").append(data.provinsi);

    return $container;
}

function formatWilayahSelection (data) {
    return data.kelurahan || data.text;
}
function select2_wilayah_kelurahan(){
    $('#wilayah_ids').empty();
    $('#wilayah_ids').select2({
        width: '100%',
        placeholder: "Pilih Wilayah",
        delay: 3000,
        multiple: true,
        ajax: {
            url: `${base_url}/public_service/ajax_select2_jateng_only`,
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term || '',
                    page: params.page || 1,
                }
            },
            processResults: function(data, params) {
                params.page = params.page || 1;

                return {
                    'results': data.results,
                    'pagination': {
                        'more': data.pagination.more
                    },
                }
            },
            cache: true,
        },
        // minimumInputLength: 10,
        templateResult: formatWilayah,
        templateSelection: formatWilayahSelection
    });
    // $('#wilayah_ids').select2({
    //     width: '100%',
    //     placeholder: "Pilih Wilayah",
    //     delay: 3000,
    //     multiple: true,
    //     maximumSelectionLength: 3,
    //     ajax: {
    //         url: `${base_url}/public_service/ajax_select2_jateng_only`,
    //         data: function(params) {
    //             return {
    //                 term: params.term || '',
    //                 page: params.page || 1,
    //             }
    //         },
    //         processResults: function(data, params) {
    //             params.page = params.page || 1;

    //             return {
    //                 'results': data.results,
    //                 'pagination': {
    //                     'more': data.pagination.more
    //                 },
    //             }
    //         },
    //         cache: true
    //     },
    // });
}
// function select2_wilayah_kelurahan(){
//     $('#wilayah_ids').empty();
//     $('#wilayah_ids').select2({
//         width: '100%',
//         placeholder: "Pilih Wilayah",
//         delay: 3000,
//         multiple: true,
//         maximumSelectionLength: 3,
//         ajax: {
//             url: `${base_url}/public_service/ajax_select2_jateng_only`,
//             data: function(params) {
//                 return {
//                     term: params.term || '',
//                     page: params.page || 1,
//                 }
//             },
//             processResults: function(data, params) {
//                 params.page = params.page || 1;

//                 return {
//                     'results': data.results,
//                     'pagination': {
//                         'more': data.pagination.more
//                     },
//                 }
//             },
//             cache: true
//         },
//     });
// }

function select2_wilayah_kelurahan_single(){
    $('#id_kelurahan').empty();
    $('#id_kelurahan').select2({
        width: '100%',
        placeholder: "Pilih Kelurahan",
        delay: 3000,
        ajax: {
            url: `${base_url}/public_service/ajax_select2_jateng_only`,
            data: function(params) {
                return {
                    term: params.term || '',
                    page: params.page || 1,
                }
            },
            processResults: function(data, params) {
                params.page = params.page || 1;

                return {
                    'results': data.results,
                    'pagination': {
                        'more': data.pagination.more
                    },
                }
            },
            cache: true
        },
    });
}

/*--------------------------------------
        BlockUI 
---------------------------------------*/
function blockUI(message){
    $.blockUI({
        message: '<span class="text-semibold"><img src="'+base_url+'/assets/dist/img/loading_argan.gif'+'" style="height: 30px;">'+ (message ? message : ' Sedang Memproses') +'</span>',
        baseZ: 10000,
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.3,
            cursor: 'wait'
        },
        css: {
            'z-index': 10020,
            padding: '10px 5px',
            margin: '0px',
            width: '20%',
            top: '40%',
            left: '40%',
            'text-align': 'center',
            color: 'rgb(92, 132, 50)',
            border: '0px',
            'background-color': 'rgb(255, 255, 255)',
            cursor: 'wait',
            'border-radius': '12px',
            border: '2px rgb(92, 132, 50) solid',
            'font-size': '16px',
            'min-width': "95px",
        } 
    })
}

function blockElement(element, message, border){
    border = border ? border : '0px';
    $(element).block({
        message: '<span class="text-semibold"><img src="'+base_url+'/assets/dist/img/loading_argan.gif'+'" style="height: 30px;">'+(message ? message : ' Sedang Memproses') +'</span>',
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.3,
            cursor: 'wait',
            borderRadius: border,
        },
        css: {
            'z-index': 10020,
            padding: '10px 5px',
            margin: '0px',
            width: '20%',
            top: '40%',
            left: '40%',
            'text-align': 'center',
            color: 'rgb(92, 132, 50)',
            border: '0px',
            'background-color': 'rgb(255, 255, 255)',
            cursor: 'wait',
            'border-radius': '12px',
            border: '2px rgb(92, 132, 50) solid',
            'font-size': '16px',
            'min-width': "95px",
        } 
    });

}

function on_assign(id, base_uri){
    console.log(base_uri)
    let url_accept = base_uri+'/form_assign/'+id;
    blockUI();
    let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Alihkan Nasabah</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_accept,function(){
        $.unblockUI();
        $('#dynamicModal').modal({show:true});
    });
}

function on_report(id, base_uri){
    console.log(base_uri)
    let url_accept = base_uri+'/form_report/'+id;
    blockUI();
    let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Cetak</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_accept,function(){
        $.unblockUI();
        $('#dynamicModal').modal({show:true});
    });
}

function on_accept(id, base_uri){
    console.log(base_uri)
    let url_accept = base_uri+'/form_accept/'+id;
    blockUI();
    let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Persetujuan Penerimaan</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_accept,function(){
        $.unblockUI();
        $('#dynamicModal').modal({show:true});
    });
}

function on_reject(id, base_uri){
    console.log(base_uri)
    let url_reject = base_uri+'/form_reject/'+id;
    blockUI();
    let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Alasan Penolakan</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_reject,function(){
        $.unblockUI();
        $('#dynamicModal').modal({show:true});
    });
}

function on_pertemuan(id, base_uri){
    console.log(base_uri)
    let url_reject = base_uri+'/form_pertemuan/'+id;
    blockUI();
    let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Tentukan Lokasi</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_reject,function(){
        $.unblockUI();
        $('#dynamicModal').modal({show:true});
    });
}

function on_upload_surat(id, base_uri){
    console.log(base_uri)
    let url_reject = base_uri+'/form_upload_surat/'+id;
    blockUI();
    let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Upload Surat Tugas</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_reject,function(){
        $.unblockUI();
        $('#dynamicModal').modal({show:true});
    });
}

function copyToHeart(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    toastr.info('Tersalin')
}

function clipboard_modal(ref){
    let url_modal = base_url+'/sales/sales/clipboard/'+ref;
    blockUI();
    let kimochi_img = `${base_url}/assets/dist/img/validate.png`;
    let title = `<span style="display: flex;"><img src="${kimochi_img}" style="height: 50px;"> <h3>Link Referal</h3></span>`;

    $('#dynamicModal .modal-header').html(title);
    $('#dynamicModal .modal-body').load(url_modal,function(){
        $.unblockUI();
        $('#dynamicModal').modal({show:true});
    });
}

  
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $(".fancybox").fancybox();
});

$(document).ready(function(){

    //Init Select
    setTimeout(() => {
        select2_wilayah_kelurahan();
    },1000);


    $('#dynamicModal').on('hidden.bs.modal', () => {
        $('#dynamicModal .modal-body').html('')
    });
});


//Setup
$(() => {
    $('.input-bulan').mask('000');
    $('.no_hp').mask('0000000000000');
})

$(function() { 
    $('#id_kategori_galeri').change(function(){

        if($('#id_kategori_galeri').val() != "2") {
            $('.hide_field_galeri').show(); 
            $('.link_yt').hide(); 
        } else {
            $('.link_yt').show(); 
            $('.hide_field_galeri').hide();
        } 
    });
});


function set_provinsi(name){
    $('[name="provinsi_'+name+'"]').empty();
    $('[name="provinsi_'+name+'"]').append('<option selected="true" value="" disabled>Pilih Provinsi</option>');
    $('[name="kota_'+name+'"]').empty();
    $('[name="kota_'+name+'"]').append('<option selected="true" value="" disabled>Pilih Kabupaten</option>');
    $('[name="kecamatan_'+name+'"]').empty();
    $('[name="kecamatan_'+name+'"]').append('<option selected="true" value="" disabled>Pilih Kecamatan</option>');
    $('[name="kelurahan_'+name+'"]').empty();
    $('[name="kelurahan_'+name+'"]').append('<option selected="true" value="" disabled>Pilih Kelurahan</option>');
	$.get(uri_special.get_all_provinces,function(data){
        $.each(data,function(index, val) {
        $('[name="provinsi_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
        });
    });
}

function change_provinsi(name){
    // alert('test');
    let id = $('[name="provinsi_'+name+'"]').val();

    $('[name="kota_'+name+'"]').empty();
    $('[name="kota_'+name+'"]').append('<option disabled selected="true" value="" disabled>Pilih Kabupaten</option>');

    $('[name="kecamatan_'+name+'"]').empty();
    $('[name="kecamatan_'+name+'"]').append('<option disabled selected="true" value="" disabled>Pilih Kecamatan</option>');

    $('[name="kelurahan_'+name+'"]').empty();
    $('[name="kelurahan_'+name+'"]').append('<option disabled selected="true" value="" disabled>Pilih Kelurahan</option>');

	$('[name="kodepos_'+name+'"]').val(null);

    if(_TRIGGER_CHANGE.PROVINSI){
      $.get(uri_special.get_cities_by_province_id.replace(':id',id),function(data){
        $.each(data,function(index, val) {
          // console.log('kabupaten',val)
          $('[name="kota_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
        });
      });     
    }
}

function change_kabupaten(name){

    let id = $('[name="kota_'+name+'"]').val();

    $('[name="kecamatan_'+name+'"]').empty();
    $('[name="kecamatan_'+name+'"]').append('<option value="-1" disabled selected="true" value="" disabled>Pilih Kecamatan</option>');

    $('[name="kelurahan_'+name+'"]').empty();
    $('[name="kelurahan_'+name+'"]').append('<option value="-1" disabled selected="true" value="" disabled>Pilih Kelurahan</option>');

    if(_TRIGGER_CHANGE.KABUPATEN){
      $.get(uri_special.get_districts_by_city_id.replace(':id',id),function(data){
        $.each(data,function(index, val) {
          // console.log('kecamatan',val)
          $('[name="kecamatan_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
        });
      });
    }
}

function change_kecamatan(name){

    let id = $('[name="kecamatan_'+name+'"]').val();

    $('[name="kelurahan_'+name+'"]').empty();
    $('[name="kelurahan_'+name+'"]').append('<option value="-1" disabled selected="true" value="" disabled>Pilih Kelurahan</option>');

    if(_TRIGGER_CHANGE.KECAMATAN){
      $.get(uri_special.get_villages_by_district_id.replace(':id',id),function(data){
        $.each(data,function(index, val) {
          // console.log('kelurahan',val)
          $('[name="kelurahan_'+name+'"]').append('<option value="'+val.id+'"  data-kodepos="'+val.kodepos+'">'+val.name+'</select>');
        });
      });
    }
}

function change_kelurahan(name){
	let kodepos = $('[name="kelurahan_'+name+'"] option:selected').data('kodepos');
	// alert(kodepos)
	$('[name="kodepos_'+name+'"]').val(kodepos);
}

function set_wilayah(provinsi_id,kabupaten_id,kecamatan_id,kelurahan_id,name){
    _TRIGGER_CHANGE = {
      PROVINSI  : false,
      KABUPATEN : false,
      KECAMATAN : false,
      KELURAHAN : false,
    };

    $('[name="provinsi_'+name+'"]').val(provinsi_id).trigger('change');
    $.get(uri_special.get_cities_by_province_id.replace(':id',provinsi_id),function(data){
      $.each(data,function(index, val) {
        // console.log('kabupaten',val)
        $('[name="kota_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
      });
      $('[name="kota_'+name+'"]').val(kabupaten_id).trigger('change');
      $.get(uri_special.get_districts_by_city_id.replace(':id',kabupaten_id),function(data){
        $.each(data,function(index, val) {
          // console.log('kecamatan',val)
          $('[name="kecamatan_'+name+'"]').append('<option value="'+val.id+'">'+val.name+'</select>');
        });
        $('[name="kecamatan_'+name+'"]').val(kecamatan_id).trigger('change');
        $.get(uri_special.get_villages_by_district_id.replace(':id',kecamatan_id),function(data){
          $.each(data,function(index, val) {
            // console.log('kelurahan',val)
            $('[name="kelurahan_'+name+'"]').append('<option value="'+val.id+'" data-kodepos="'+val.kodepos+'">'+val.name+'</select>');
          });
          $('[name="kelurahan_'+name+'"]').val(kelurahan_id).trigger('change');
          let kodepos = $('[name="kelurahan_'+name+'"] option:selected').data('kodepos');
          $('[name="kodepos_'+name+'"]').val(kodepos);
          _TRIGGER_CHANGE = {
            PROVINSI  : true,
            KABUPATEN : true,
            KECAMATAN : true,
            KELURAHAN : true,
          };
        });
      });
    });
}

if (typeof import_url != 'undefined'){
    $("#excel").on("change",function(){

        var formData = new FormData($("#form")[0]);
    
        $.ajax({
    
            type: "POST",
            url: import_url,
            data: formData,
            dataType: "json",
            contentType: false,
            processData: false,
            beforeSend: function() {
    
                toastr.info("Importing file...", "Loading");
                
            },
    
            success: function(res) {
    
                if(res.status!="1"){
    
                    toastr.error(res.msg, "Response Server");
    
                }else{
    
                    toastr.success(res.msg, "Response Server");
    
                }
    
                $("#dttable").dataTable().fnDestroy();
    
                InitDatatable();
              
               
            },
            error: function() {
    
                toastr.error("Something went wrong!", "Response Server");
            }
    
        });
    
    });
}

function cek_file(url, id){
    let html = '<a href="'+base_url+url+'" target="_blank" download>Download</a>';
    $.ajax({    
        type: 'HEAD',
        url: base_url+url,
        success: function() {
            $('#'+id).html(html);
        },  
        error: function() {
            $('#'+id).html('<span style="color:red">tidak ada</span>');
        }
    });
}

function changeStatus(url, id, event){
    console.log(url)
    console.log(id)
    console.log(event)
    let el = $(event);
    let status = el[0].checked ? 1 : 0;
    console.log(status)

    toastr.info("Updating data...", "Loading");
    let target = `${url}/${id}/${status}`
    $.get(target).done((res) => {
        toastr.success("Data Updated", "Response Server");
    }).fail((xhr) => {
        toastr.error("Server Busy", "Response Server");
    })
}

function changeStatusCareer(id, event){
    let b_uri = base_url+'frontend/career/change_status';
    changeStatus(b_uri, id, event)
}

