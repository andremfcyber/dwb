function is_same_ktp(e){

    let isTrue = $(e).val();
    // console.log(isTrue)
    if(isTrue == 't'){
        $('[name="provinsi_ktp"]').parent().hide()
        $('[name="kota_ktp"]').parent().hide()
        $('[name="kecamatan_ktp"]').parent().hide()
        $('[name="kelurahan_ktp"]').parent().hide()
        $('[name="alamat_lengkap_ktp"]').parent().hide()
        $('[name="kodepos_ktp"]').parent().hide()
    }else{
        $('[name="provinsi_ktp"]').parent().show()
        $('[name="kota_ktp"]').parent().show()
        $('[name="kecamatan_ktp"]').parent().show()
        $('[name="kelurahan_ktp"]').parent().show()
        $('[name="alamat_lengkap_ktp"]').parent().show()
        $('[name="kodepos_ktp"]').parent().show()
    }
}

$(() => {
    set_provinsi('sekarang')
    set_provinsi('ktp')
    set_map(0,0)
})

function set_map(lat,lng){
    lng = parseFloat(lng);
    lat = parseFloat(lat);
    var map = new google.maps.Map(document.getElementById('map-canvas'),{
        center:{
            lat: lat ? lat : 0,
            lng: lng ? lng : 0
        },
        zoom: 16
    });

    var marker = new google.maps.Marker({
        position: {
            lat: lat ? lat : 0,
            lng: lng ? lng : 0
        },
        map: map,
        draggable: true
    });
    var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
    
    google.maps.event.addListener(searchBox, 'places_changed', function(){

        var places = searchBox.getPlaces();
        var bounds = new google.maps.LatLngBounds();
        var i, place;

        for(i=0; place=places[i]; i++){
            bounds.extend(place.geometry.location);
            marker.setPosition(place.geometry.location);
        }

        map.fitBounds(bounds);
        map.setZoom(16);

    });

    google.maps.event.addListener(marker,'position_changed', function(){

        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();

        $('#latitude').val(lat);
        $('#longitude').val(lng);

    });
}
